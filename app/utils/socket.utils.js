const Ws = use('Ws')

function broadcast(percent) {
    const channel = Ws.getChannel('teste');
   
    if (!channel) return

    const topic = channel.topic('teste')
    if (!topic) {
        return
    }

    // emit, broadcast, broadcastToAll
    topic.broadcastToAll(`message`, {
        percent
    });
}

module.exports = {
    broadcast
}