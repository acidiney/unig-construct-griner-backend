'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ClienteFornecedor extends Model {

  cliente() {
    return this.belongsTo('App/Models/Cliente')
  }

  fornecedor() {
    return this.belongsTo('App/Models/Fornecedor')
  }
}

module.exports = ClienteFornecedor
