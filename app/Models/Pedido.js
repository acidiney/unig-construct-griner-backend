"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
class Pedido extends Model {


static async storePedido(request, user) {
    var pedido = {
            cliente_id: request.cliente_id,
            tipoPedido: request.tipoPedido,
            observacao: request.observacao,
            dataPedido: request.dataPedido,
            telefone: request.telefone,
            tarifario: request.tarifario,
            capacidade: request.capacidade,
            origem: request.origem,
            destino: request.destino,
        }

    let filial = null;
    let str = "Luanda";
    let provincia = str.toUpperCase();
    //let provincia = "Luanda";

    filial = await Database.select("filials.id as provincia").table("users")
    .leftJoin("lojas", "lojas.id", "users.loja_id").leftJoin("filials", "filials.id", "lojas.filial_id")
    .where("users.id", user).first(); 

    if (filial.provincia == null) { 
      filial = await Database.select("filials.id as provincia").from("filials").whereRaw("UPPER(nome) = ?", [provincia]).first();
    } 

    const p = await Pedido.create({
        cliente_id: pedido.cliente_id,
        tipoPedido: pedido.tipoPedido,
        observacao: pedido.observacao,
        dataPedido: pedido.dataPedido,
        telefone: pedido.telefone,
        tarifario_id: pedido.tarifario,
        capacidade: pedido.capacidade,
        origem: pedido.origem,
        destino: pedido.destino,
        provincia_id: filial.provincia,
        user_id:user,
    });

    return  p;
  }
}

module.exports = Pedido;
