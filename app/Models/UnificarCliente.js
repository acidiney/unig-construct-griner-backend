'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const LogoUnificacao = use('App/Models/LogUnificacao');
const Database = use('Database');

class UnificarCliente extends Model {



    static async unificar(id_cliente_manter, id_cliente_unificado,cliente_nome_manter, cliente_nome_unificar){
 
        const descricao= cliente_nome_unificar +"->"+cliente_nome_manter
         
            LogoUnificacao.registrarLogo("adiantamentos",id_cliente_unificado, id_cliente_manter ,descricao)
            
            LogoUnificacao.registrarLogo("pedidos",id_cliente_unificado, id_cliente_manter ,descricao);
            LogoUnificacao.registrarLogo("facturas",id_cliente_unificado, id_cliente_manter ,descricao);
            LogoUnificacao.registrarLogo("parceria_clientes",id_cliente_unificado, id_cliente_manter ,descricao);
            LogoUnificacao.registrarLogo("reclamacaos",id_cliente_unificado, id_cliente_manter ,descricao);
            LogoUnificacao.registrarLogo("contas",id_cliente_unificado, id_cliente_manter ,descricao);

            
            
            

         const unificados= await Database.table("unificar_clientes").where("id_unificar_cliente", id_cliente_unificado).where("id_manter_cliente", id_cliente_manter).update("unificado", 1);
         const adiantamento = await Database.table("adiantamentos").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter)

        const pedidos = await Database.table("pedidos").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const factura = await Database.table("facturas").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const parceiros= await Database.table("parceria_clientes").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const reclamacoes= await Database.table("reclamacaos").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const contas= await Database.table("contas").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const cliente= await Database.table("clientes").where("id",id_cliente_unificado).update("unificado", "sim");
  


    }

}

module.exports = UnificarCliente
