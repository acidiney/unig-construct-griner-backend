'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database");
class Numeracao extends Model {
  static get table() {
    return "numeracoes";
  }
  tecnologia() {
    return this.belongsTo("App/Models/Tecnologia");
  }

  filial() {
    return this.belongsTo("App/Models/Filial");
  }

  static async findAllNumeration(search, filters) {
    return Numeracao.query()
      .select(
        "*",
        Database.raw(
          "IF(IFNULL(TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1)=-1,IF(status,'OCUPADO','DISPONIVEL'),'INDISPONIVEL') as estado"
        ),
        Database.raw(
          "IFNULL(TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1) as meses"
        ),
        Database.raw("IF(IFNULL(TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1)=-1,status,2) as status")
      )
      .where(function () {
        if (search) {
          if (filters.keys instanceof Array) {
            filters.keys.forEach((key) => {
              this.orWhere(`numeracoes.${key}`, "like", "%" + search + "%");
            });
          } else {
            this.where(
              `numeracoes.${filters.keys}`,
              "like",
              "%" + search + "%"
            );
          }
        }
        if (filters.filial_id) {
          this.where("filial_id", filters.filial_id);
        }
        if (filters.tecnologia_id) {
          this.where("tecnologia_id", filters.tecnologia_id);
        }
        if (filters.status) {
          this.whereRaw(
            "IF(IFNULL(TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1)=-1,status,2) =" +
              filters.status
          );
        }
      })
      .with("tecnologia")
      .with("filial")
      .orderBy(filters.orderBy, filters.typeOrderBy)
      .paginate(filters.page, filters.perPage);
  }

   /**
    * 
    * @param {*} numero 
    * @param {*} filial_id 
    */
  static async findNumber(numero, filial_id=undefined, tecnologia_id=undefined) {
    return await Numeracao.query().select("*", Database.raw("IF(IFNULL(TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1)=-1,IF(status,'OCUPADO','DISPONIVEL'),'INDISPONIVEL') as estado"), Database.raw("IFNULL(TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1) as meses"), Database.raw("IF(IFNULL(TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1)=-1,status,2) as status"))
      .where('numero', numero)
      .where(function () {
        if (filial_id) { this.where("filial_id", filial_id) }
        if (tecnologia_id) { this.where("tecnologia_id", tecnologia_id); }
      })
      .with("tecnologia").with("filial").first();
  }

  /**
   * 
   * @param {*} startNumber 
   * @param {*} endNumber 
   */
  static async validatRange(startNumber, endNumber) {
    var count = await Numeracao.numbersRange(startNumber, endNumber);
    count = count.toJSON();
    return count.length > 0 ? true : false;
  }

  /**
   * 
   * @param {*} startNumber 
   * @param {*} endNumber 
   */
  static async numbersRange(startNumber, endNumber) {
    return await Numeracao.query()
      .whereBetween("numero", [startNumber, endNumber])
      .where("status", 0)
      .orWhereRaw(
        "(IFNULL( TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1) BETWEEN 0 and 6)"
      ) .whereBetween("numero", [startNumber, endNumber]).fetch();
  }

  static async verifyStatusNumberNumberByFilial(numero, filial_id, tecnologia_id) {
    const data = await Numeracao.findNumber(numero, filial_id, tecnologia_id);
 
    
    if (!data) {   
      return { code: 404, message: "Esse número não existe", data: data };// response.status(404).send(Mensagem.response(response.response.statusCode, , null))
    }
    const code = data.estado == 'DISPONIVEL' ? 200 : 302
    return { code: code, message: data.estado, data:data };
  }



}

module.exports = Numeracao
