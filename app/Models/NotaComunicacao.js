'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database");

class NotaComunicacao extends Model {
  cliente() {
    return this.belongsTo("App/Models/Cliente");
  }

  user() {
    return this.belongsTo("App/Models/User");
  }

  tipificacao() {
    return this.belongsTo("App/Models/TipoComunicacao");
  }
 

  /**
   * @override
   * Returns getAllCommunication Notes paginated by `filters.perPage` or 5
   *
   * @param { Number } page
   * @param { Object } filters
   * @param { String } filters.search
   * @param { Number } filters.perPage 
   */
  static async getAllCommunicationNotes(page = 1, filters = {}) {
    const r = await NotaComunicacao.query()

      .where(function () {
        if (filters.type_id) {
          this.where("tipo_comunicacao_id", filters.type_id);
        } else { 
          this.whereIn("tipo_comunicacao_id", Database.raw("SELECT id FROM tipo_comunicacaos"))
        }
      })
      .whereHas("cliente", (builder) => {
        if (filters.search) { 
          builder.where("nome", "LIKE", `%${filters.search}%`);
        }
      })   

      .with("user")
      .with("cliente")
      .with("tipificacao")
      .paginate(page, filters.perPage || 5);
    return r;
  }

  /**
   * @overwrite
   *
   * @param { Number } user_id
   * @param { Object } note
   * @param { Date } note.data
   * @param { Time } note.hora
   * @param { String } note.feedback
   * @param { Integer } note.cliente_id
   * @param { String } note.feedback
   *
   * @Author Caniggia Moreira <caniggia.moreira@ideiasdinamicas.com>
   */
  static async createNotaComunicacao(user_id, note) { 
      const created = await NotaComunicacao.create({ user_id: user_id, ...note }); 
      return created; 
  }

  /**
   * @overwrite
   *
   * @param { Number } user_id
   * @param { Object } note
   * @param { Date } note.data
   * @param { Time } note.hora
   * @param { String } note.feedback
   * @param { Integer } note.cliente_id
   * @param { String } note.feedback
   *
   * @Author Caniggia Moreira <caniggia.moreira@ideiasdinamicas.com>
   */
  static async updateNotaComunicacao(params, note) { 
      const update = await NotaComunicacao.query()
        .where("id", params)
        .update({ ...note }); 
      return update; 
  }
  /**
   * Return an element
   */
  static async getNotaComunicacaoById(id) {
    const result = await NotaComunicacao.query()
      .where("id", id)
      .with("user")
      .with("cliente")
      .with("tipificacao")
      .first();
    if (!result) {
      throw new NotFoundException();
    }
    return r;
  }

  /**
   * Return an element
   */
  static async findById(modelId) {
    const result = await NotaComunicacao.find(modelId); 
    return result;
  }

  static async deleteNotaComunicacao(param) { 
      const r = await this.findById(param);
      const result = await r.delete();
      return "Dados eliminado com sucesso"; 
  }

  static async statusNotaComunicacao(param, data) { 
        const result = await this.findById(param);
        await result.merge({ data: data == true ? false : true });
        result.save();
      return result; 
  }
}

module.exports = NotaComunicacao
