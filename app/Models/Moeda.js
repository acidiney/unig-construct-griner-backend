'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Moeda extends Model {
  cambios() {
    return this.hasMany("App/Models/Cambio");
  }
  user() {
    return this.belongsTo("App/Models/User");
  }

  contas() {
    return this.hasMany("App/Models/Conta");
  }
}

module.exports = Moeda
