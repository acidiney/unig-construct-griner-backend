"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Cliente extends Model {
  tipoCliente() {
    return this.belongsTo("App/Models/TipoCliente");
  }

  tipoIdentidade() {
    return this.belongsTo("App/Models/TipoIdentidade");
  }

  user() {
    return this.belongsTo("App/Models/User");
  }

  clienteFornecedor() {
    return this.hasMany("App/Models/clienteFornecedor");
  }
  static get rules() {
    return {
      nome: "required",
      //contribuente: "",
      email: "required",
      tipo_identidade_id: "required",
      tipo_cliente_id: "required",
      direccao_id: "required",
      telefone: "required",
      morada: "required",
    };
  }
  facturas() {
    return this.hasMany("App/Models/Factura");
  }

  static scopeHasFactura(query) {
    return query.has("factura");
  }

  factura() {
    return this.hasOne("App/Models/Factura");
  }

  user() {
    return this.belongsTo("App/Models/User");
  }
  adiantamento() {
    return this.belongsTo("App/Models/Adiantamento");
  }
}

module.exports = Cliente;
