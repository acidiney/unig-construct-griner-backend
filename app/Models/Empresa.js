'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Empresa extends Model {
	
	static get rules () {
    return {
      companyName: 'required',
      telefone: 'required',
      taxRegistrationNumber: 'required',

    }
  }
}

module.exports = Empresa
