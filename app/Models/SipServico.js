'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class SipServico extends Model {
    servico() {
        return this.belongsTo("App/Models/Servico");
      }
    user() {
    return this.belongsTo("App/Models/User");
  }
  
  numeros() {
    return this.belongsToMany('App/Models/Numeracao').pivotModel('App/Models/SipServicoNumero')
  }

  sipNumeros() {
    return this.hasMany("App/Models/SipServicoNumero");
  }

  static async sipSercoByID(id) {
    return await SipServico.query().with("numeros").where("id", id).first();
  }

  static async deleteSipNumber(id) {
    const sip = await SipServico.find(id); 
    const d = await sip.sipNumeros().delete();
    return d;
  }

  /**
   * 
   * @param {*} id 
   * @param {*} numero 
   * @param {*} status 
   */
  static async updateStatusSipNumber(id, numero = null, status=0) {
    const sip = await SipServico.find(id);
    const updated = await sip.numeros().where(function () { if (numero != null) { this.where("numeracoes.numero",numero)}  }).update({ status: status}); 
    return updated;
  }

  static async deletesipservico(id){
    const  deleted = await SipServico.find(id)
    await  deleted.delete();
    return deleted;
  }

  static async verifica(numero_inicio){
    
    const getinicio = await Database.select('*').from('numeracoes').where("numero",numero_inicio).where("status",1).getCount();
    const getfim = await Database.select('*').from('numeracoes').where("numero",data.numeracao_fim).where("status",1).getCount();
    return getinicio;
  }
  
}

module.exports = SipServico
