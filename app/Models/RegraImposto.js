'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class RegraImposto extends Model {

  produtoImposto() {
    return this.hasMany('App/Models/ProdutoImposto')
  }

  user() {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = RegraImposto
