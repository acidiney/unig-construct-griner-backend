'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const bd = require('mssql');


class Prevision extends Model {

 static async add_balance_prevision(args){

    var data=args.split(";"); 
    var sql="";
    //------------------ 

    var AccaoID; // NOT NULL	Identificador da accão e é preenchido automaticamente quando é inserido um registo na tabela.
    var RequestID="1"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var ActivityID="99"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    
    if(data[1]=="IN"){
        var TipoAccao="ADD_BALANCE"; // NULL	Preencher com o campo "Comando" das folhas IN e AAA
    }else if(data[1]=="AAA"){
        var TipoAccao="ADD_BALANCE"; // NULL	Preencher com o campo "Comando" das folhas IN e AAA
    }else{
        var TipoAccao="ADD_BALANCE";
    }
    
    var balance=(data[3]/10)*100;
    if(data[1]=="IN"){
        var balance=(data[3]/9.5)*100;
    }else if(data[1]=="AAA"){
        var balance=(data[3]/10)*100;
    }else{
        var balance=(data[3]/10)*100;
    }
    var Mainkey=data[2]; //NULL	Preencher com o numero do telefone
    //var balance=(data[3]/10)*100;
    
    balance=Math.round(balance);

    var dval = new Date();
    dval.setDate(dval.getDate()+Math.round(balance/4));
    var valdate=dval.toISOString().split('T')[0].replace("-","").replace("-","");

    if(data[1]=="IN"){
        var ParametrosServico="<values><mainacc>"+ Mainkey +"</mainacc><value>"+ balance +"</value><valdate>"+valdate+"</valdate><comment>UNIG</comment></values>";
        // NULL	Preencher com o campo Dados daa folhas IN e AAA onde todos os campos com prefixo & deverá ser preenchido
    }else if(data[1]=="AAA"){
        var ParametrosServico="<values><mainkey>"+ Mainkey +"</mainkey><username>"+data[4]+"</username><domain>adsl.supernet.ao</domain><substype>ADSLNAPre</substype><value>" + balance +"</value></values>";
        // NULL	Preencher com o campo Dados daa folhas IN e AAA onde todos os campos com prefixo & deverá ser preenchido
    }else{
        var ParametrosServico="ERRO";
        // NULL	Preencher com o campo Dados daa folhas IN e AAA onde todos os campos com prefixo & deverá ser preenchido
    }
    
    var LastUserID="0"; // NOT NULL	Podem preencher com 0 para já. Mas provavelmente iremos criar um user do lado do TIMM chamado SIGO e depois irei enviar o ID deste user.
     
    if(data[1]=="IN"){
        var CentralType="HUAWEI-IN"; //NOT NULL	Preencher com o campo CentralProvisioning das folhas IN e AAA
    }else if(data[1]=="AAA"){
        var CentralType="HUAWEI-AAA"; //NOT NULL	Preencher com o campo CentralProvisioning das folhas IN e AAA
    }else{
        var CentralType=""; //NOT NULL	Preencher com o campo CentralProvisioning das folhas IN e AAA
    }
    
    var d = new Date();
    d.setDate(d.getDate()+15);

    /*new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') */
    
    var LastUpdate=new Date().toISOString(); // NOT NULL	Preencher com a data de inserção do registo da tabela
    var DoTime=d.toISOString(); // NULL	Preencher com a data de inserção do registo da tabela mais 15 dias.

    var InsertMassivo="0"; // NOT NULL	Preencher com 0
    var Retries ="0"; // NULL	Preencher com 0

    if(data[1]=="IN"){
        var SubsType="PPT"; // NOT NULL	Para a IN preencher com "PPT". Para os AAA preencher com WIMAX, CDMA e ADSLNAPos ou ADSLNAPre dependendo do numero de telefone
    }else if(data[1]=="AAA"){
        var SubsType="ADSLNAPre"; // NOT NULL	Para a IN preencher com "PPT". Para os AAA preencher com WIMAX, CDMA e ADSLNAPos ou ADSLNAPre dependendo do numero de telefone
    }else{
        var SubsType="PPT"; // NOT NULL	Para a IN preencher com "PPT". Para os AAA preencher com WIMAX, CDMA e ADSLNAPos ou ADSLNAPre dependendo do numero de telefone
    }
    
    var DoUndo=null;// NULL	Preencher com NULL
    
    var ScheduleTime=null; // NULL	Preencher com NULL
    var AccaoStatus="exec"; // NULL	Preencher com "exec"
    var RetryTime="0"; // NULL	Preencher com 0
    var PrevErrorCode=null; // NULL	Preencher com NULL
    var control_status="pending" // NOT NULL	Preencher com "pending"
    var ParserError=null; // NULL	Preencher com NULL
    var Comment="UNIG_BILL;"+data[0]; //NULL	Preencher com "Northbound Insert"

    sql="INSERT INTO TAE_AccoesQueue (RequestID,ActivityID,TipoAccao,Mainkey,ParametrosServico,LastUserID,CentralType,LastUpdate,DoTime,InsertMassivo,Retries,SubsType,DoUndo,ScheduleTime,AccaoStatus,RetryTime,PrevErrorCode,control_status,ParserError,Comment) ";
    sql+="VALUES('"+RequestID+"',"+ActivityID+",'"+TipoAccao+"','"+Mainkey+"','";
    sql+=ParametrosServico+"',"+LastUserID+",'"+CentralType+"','"+LastUpdate+"','"+DoTime+"',",
    sql+=InsertMassivo+","+Retries+",'"+SubsType+"',"+DoUndo+","+ScheduleTime+",'"+AccaoStatus+"',";
    sql+=RetryTime+","+PrevErrorCode+",'"+control_status+"',"+ParserError+",'"+Comment+"');"; 
     
     this.insert_Table(sql); 
    return true;
}

static async change_account(args){
    var data=args.split(";"); 
    var sql="";
    //------------------ 
    var AccaoID; // NOT NULL	Identificador da accão e é preenchido automaticamente quando é inserido um registo na tabela.
    var RequestID="1"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var ActivityID="99"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var TipoAccao="CHANGE_RADIUS_ACCOUNT"; // NULL	Preencher com o campo "Comando" das folhas IN e AAA    
    var Mainkey=data[1]; //NULL	Preencher com o numero do telefone

    var ParametrosServico="<values><mainkey>"+Mainkey+"</mainkey><username>"+data[2]+"</username><domain>"+data[3]+"</domain><profileplanid>"+data[4]+"</profileplanid><profileplanbegin>"+data[5]+"</profileplanbegin><paytype>"+data[6]+"</paytype><substype>"+data[7]+"</substype><imsi>null</imsi></values>";
    
    var LastUserID="0"; // NOT NULL	Podem preencher com 0 para já. Mas provavelmente iremos criar um user do lado do TIMM chamado SIGO e depois irei enviar o ID deste user.   
    var CentralType="HUAWEI-AAA"; //NOT NULL	Preencher com o campo CentralProvisioning das folhas IN e AAA
    var d = new Date();
    d.setDate(d.getDate()+15);
    var LastUpdate=new Date().toISOString(); // NOT NULL	Preencher com a data de inserção do registo da tabela
    var DoTime=d.toISOString(); // NULL	Preencher com a data de inserção do registo da tabela mais 15 dias.
    var InsertMassivo="0"; // NOT NULL	Preencher com 0
    var Retries ="0"; // NULL	Preencher com 0
    var SubsType=data[7]; // NOT NULL	Para a IN preencher com "PPT". Para os AAA preencher com WIMAX, CDMA e ADSLNAPos ou ADSLNAPre dependendo do numero de telefone
    var DoUndo=null;// NULL	Preencher com NULL
    var ScheduleTime=null; // NULL	Preencher com NULL
    var AccaoStatus="exec"; // NULL	Preencher com "exec"
    var RetryTime="0"; // NULL	Preencher com 0
    var PrevErrorCode=null; // NULL	Preencher com NULL
    var control_status="pending" // NOT NULL	Preencher com "pending"
    var ParserError=null; // NULL	Preencher com NULL
    var Comment="UNIG_BILL;"+data[0]; //NULL	Preencher com "Northbound Insert"

    sql="INSERT INTO TAE_AccoesQueue (RequestID,ActivityID,TipoAccao,Mainkey,ParametrosServico,LastUserID,CentralType,LastUpdate,DoTime,InsertMassivo,Retries,SubsType,DoUndo,ScheduleTime,AccaoStatus,RetryTime,PrevErrorCode,control_status,ParserError,Comment) ";
    sql+="VALUES('"+RequestID+"',"+ActivityID+",'"+TipoAccao+"','"+Mainkey+"','";
    sql+=ParametrosServico+"',"+LastUserID+",'"+CentralType+"','"+LastUpdate+"','"+DoTime+"',",
    sql+=InsertMassivo+","+Retries+",'"+SubsType+"',"+DoUndo+","+ScheduleTime+",'"+AccaoStatus+"',";
    sql+=RetryTime+","+PrevErrorCode+",'"+control_status+"',"+ParserError+",'"+Comment+"');"; 
     
     this.insert_Table(sql); 
    return true;
}

static async create_account_wimax(args){
    var data=args.split(";"); 
    var sql="";
    //------------------ 
    var AccaoID; // NOT NULL	Identificador da accão e é preenchido automaticamente quando é inserido um registo na tabela.
    var RequestID="1"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var ActivityID="99"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var TipoAccao="CREATE_RADIUS_ACCOUNT"; // NULL	Preencher com o campo "Comando" das folhas IN e AAA    
    var Mainkey=data[1]; //NULL	Preencher com o numero do telefone

    var ParametrosServico="<values><mainkey>"+Mainkey+"</mainkey><username>"+data[2]+"</username><pwd>"+data[3]+"</pwd><domain>wimax</domain><profileplanid>"+data[4]+"</profileplanid><paytype>2</paytype><substype>WIMAX</substype><radiusexpdt>20381230</radiusexpdt><imsi></imsi><value>30</value></values>";
    
    var LastUserID="0"; // NOT NULL	Podem preencher com 0 para já. Mas provavelmente iremos criar um user do lado do TIMM chamado SIGO e depois irei enviar o ID deste user.   
    var CentralType="HUAWEI-AAA"; //NOT NULL	Preencher com o campo CentralProvisioning das folhas IN e AAA
    var d = new Date();
    d.setDate(d.getDate()+15);
    var LastUpdate=new Date().toISOString(); // NOT NULL	Preencher com a data de inserção do registo da tabela
    var DoTime=d.toISOString(); // NULL	Preencher com a data de inserção do registo da tabela mais 15 dias.
    var InsertMassivo="0"; // NOT NULL	Preencher com 0
    var Retries ="0"; // NULL	Preencher com 0
    var SubsType="WIMAX"; // NOT NULL	Para a IN preencher com "PPT". Para os AAA preencher com WIMAX, CDMA e ADSLNAPos ou ADSLNAPre dependendo do numero de telefone
    var DoUndo=null;// NULL	Preencher com NULL
    var ScheduleTime=null; // NULL	Preencher com NULL
    var AccaoStatus="exec"; // NULL	Preencher com "exec"
    var RetryTime="0"; // NULL	Preencher com 0
    var PrevErrorCode=null; // NULL	Preencher com NULL
    var control_status="pending" // NOT NULL	Preencher com "pending"
    var ParserError=null; // NULL	Preencher com NULL
    var Comment="UNIG_CRM;"+data[0]; //NULL	Preencher com "Northbound Insert"

    sql="INSERT INTO TAE_AccoesQueue (RequestID,ActivityID,TipoAccao,Mainkey,ParametrosServico,LastUserID,CentralType,LastUpdate,DoTime,InsertMassivo,Retries,SubsType,DoUndo,ScheduleTime,AccaoStatus,RetryTime,PrevErrorCode,control_status,ParserError,Comment) ";
    sql+="VALUES('"+RequestID+"',"+ActivityID+",'"+TipoAccao+"','"+Mainkey+"','";
    sql+=ParametrosServico+"',"+LastUserID+",'"+CentralType+"','"+LastUpdate+"','"+DoTime+"',",
    sql+=InsertMassivo+","+Retries+",'"+SubsType+"',"+DoUndo+","+ScheduleTime+",'"+AccaoStatus+"',";
    sql+=RetryTime+","+PrevErrorCode+",'"+control_status+"',"+ParserError+",'"+Comment+"');"; 
     
     this.insert_Table(sql);
     this.create_account_ngn(args);
     this.create_account_in(data[0]+";"+data[1]+";"+data[5]+";"+data[6]);
    return true;
}

static async create_account_adsl(args){
    var data=args.split(";"); 
    var sql="";
    //------------------ 
    var AccaoID; // NOT NULL	Identificador da accão e é preenchido automaticamente quando é inserido um registo na tabela.
    var RequestID="1"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var ActivityID="99"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var TipoAccao="CREATE_RADIUS_ACCOUNT"; // NULL	Preencher com o campo "Comando" das folhas IN e AAA    
    var Mainkey=data[1]; //NULL	Preencher com o numero do telefone

    var ParametrosServico="<values><mainkey>"+Mainkey+"</mainkey><username>"+data[2]+"</username><pwd>"+data[3]+"</pwd><domain>adsl.supernet.ao</domain><profileplanid>"+data[4]+"</profileplanid><paytype>2</paytype><substype>ADSL</substype><radiusexpdt>20381230</radiusexpdt><imsi></imsi><value>30</value></values>";
    
    var LastUserID="0"; // NOT NULL	Podem preencher com 0 para já. Mas provavelmente iremos criar um user do lado do TIMM chamado SIGO e depois irei enviar o ID deste user.   
    var CentralType="HUAWEI-AAA"; //NOT NULL	Preencher com o campo CentralProvisioning das folhas IN e AAA
    var d = new Date();
    d.setDate(d.getDate()+15);
    var LastUpdate=new Date().toISOString(); // NOT NULL	Preencher com a data de inserção do registo da tabela
    var DoTime=d.toISOString(); // NULL	Preencher com a data de inserção do registo da tabela mais 15 dias.
    var InsertMassivo="0"; // NOT NULL	Preencher com 0
    var Retries ="0"; // NULL	Preencher com 0
    var SubsType="ADSLNAPre"; // NOT NULL	Para a IN preencher com "PPT". Para os AAA preencher com WIMAX, CDMA e ADSLNAPos ou ADSLNAPre dependendo do numero de telefone
    var DoUndo=null;// NULL	Preencher com NULL
    var ScheduleTime=null; // NULL	Preencher com NULL
    var AccaoStatus="exec"; // NULL	Preencher com "exec"
    var RetryTime="0"; // NULL	Preencher com 0
    var PrevErrorCode=null; // NULL	Preencher com NULL
    var control_status="pending" // NOT NULL	Preencher com "pending"
    var ParserError=null; // NULL	Preencher com NULL
    var Comment="UNIG_CRM;"+data[0]; //NULL	Preencher com "Northbound Insert"

    sql="INSERT INTO TAE_AccoesQueue (RequestID,ActivityID,TipoAccao,Mainkey,ParametrosServico,LastUserID,CentralType,LastUpdate,DoTime,InsertMassivo,Retries,SubsType,DoUndo,ScheduleTime,AccaoStatus,RetryTime,PrevErrorCode,control_status,ParserError,Comment) ";
    sql+="VALUES('"+RequestID+"',"+ActivityID+",'"+TipoAccao+"','"+Mainkey+"','";
    sql+=ParametrosServico+"',"+LastUserID+",'"+CentralType+"','"+LastUpdate+"','"+DoTime+"',",
    sql+=InsertMassivo+","+Retries+",'"+SubsType+"',"+DoUndo+","+ScheduleTime+",'"+AccaoStatus+"',";
    sql+=RetryTime+","+PrevErrorCode+",'"+control_status+"',"+ParserError+",'"+Comment+"');"; 
     
     this.insert_Table(sql);
     this.create_account_ngn(args);
     this.create_account_in(data[0]+";"+data[1]+";"+data[5]+";"+data[6]);
    return true;
}

static async create_account_in(args){
    var tipofact=1; //Para Prepago usem o 1 para o PosPago usem o 2
    var data=args.split(";");
    var sql="";
    //------------------ 
    var AccaoID; // NOT NULL	Identificador da accão e é preenchido automaticamente quando é inserido um registo na tabela.
    var RequestID="1"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var ActivityID="99"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var TipoAccao="CREATE_SUBSCRIBER"; // NULL	Preencher com o campo "Comando" das folhas IN e AAA    
    var Mainkey=data[1]; //NULL	Preencher com o numero do telefone
    var balance=(data[3]/10)*100;
    var ParametrosServico="<values><mainacc>"+Mainkey+"</mainacc><usertype>"+tipofact+"</usertype><subscri>1</subscri><accleft>12500</accleft><areaid>1</areaid><valdate>21001231</valdate><userclassid>"+data[2]+"</userclassid><localcallflg>1</localcallflg><nationcallflg>1</nationcallflg><internacallflg>1</internacallflg><mobilecallflg>1</mobilecallflg><addsercallflg>1</addsercallflg></values>";
    
    var LastUserID="0"; // NOT NULL	Podem preencher com 0 para já. Mas provavelmente iremos criar um user do lado do TIMM chamado SIGO e depois irei enviar o ID deste user.   
    var CentralType="HUAWEI-IN"; //NOT NULL	Preencher com o campo CentralProvisioning das folhas IN e AAA
    var d = new Date();
    d.setDate(d.getDate()+15);
    var LastUpdate=new Date().toISOString(); // NOT NULL	Preencher com a data de inserção do registo da tabela
    var DoTime=d.toISOString(); // NULL	Preencher com a data de inserção do registo da tabela mais 15 dias.
    var InsertMassivo="0"; // NOT NULL	Preencher com 0
    var Retries ="0"; // NULL	Preencher com 0
    var SubsType="PPT"; // NOT NULL	Para a IN preencher com "PPT". Para os AAA preencher com WIMAX, CDMA e ADSLNAPos ou ADSLNAPre dependendo do numero de telefone
    var DoUndo=null;// NULL	Preencher com NULL
    var ScheduleTime=null; // NULL	Preencher com NULL
    var AccaoStatus="exec"; // NULL	Preencher com "exec"
    var RetryTime="0"; // NULL	Preencher com 0
    var PrevErrorCode=null; // NULL	Preencher com NULL
    var control_status="pending" // NOT NULL	Preencher com "pending"
    var ParserError=null; // NULL	Preencher com NULL
    var Comment="UNIG_CRM;"+data[0]; //NULL	Preencher com "Northbound Insert"

    sql="INSERT INTO TAE_AccoesQueue (RequestID,ActivityID,TipoAccao,Mainkey,ParametrosServico,LastUserID,CentralType,LastUpdate,DoTime,InsertMassivo,Retries,SubsType,DoUndo,ScheduleTime,AccaoStatus,RetryTime,PrevErrorCode,control_status,ParserError,Comment) ";
    sql+="VALUES('"+RequestID+"',"+ActivityID+",'"+TipoAccao+"','"+Mainkey+"','";
    sql+=ParametrosServico+"',"+LastUserID+",'"+CentralType+"','"+LastUpdate+"','"+DoTime+"',",
    sql+=InsertMassivo+","+Retries+",'"+SubsType+"',"+DoUndo+","+ScheduleTime+",'"+AccaoStatus+"',";
    sql+=RetryTime+","+PrevErrorCode+",'"+control_status+"',"+ParserError+",'"+Comment+"');"; 
     
     this.insert_Table(sql); 
    return true;
}

static async create_account_ngn(args){
    //MAINKEY;
    var data=args.split(";"); 
    var sql="";
    //------------------ 
    var AccaoID; // NOT NULL	Identificador da accão e é preenchido automaticamente quando é inserido um registo na tabela.
    var RequestID="1"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var ActivityID="99"; // NULL	Podem preencher com 0 ou caso necessitem podem preencher com algum ID do SV que depois poderá ser usado para identificar a accao
    var TipoAccao="CREATE_SUBSCRIBER"; // NULL	Preencher com o campo "Comando" das folhas IN e AAA    
    var Mainkey=data[1]; //NULL	Preencher com o numero do telefone

    //var ParametrosServico="<values><mainkey>"+Mainkey+"</mainkey><username>"+data[2]+"</username><pwd>"+data[3]+"</pwd><domain>adsl</domain><profileplanid>"+data[4]+"</profileplanid><paytype>2</paytype><substype>ADSL</substype><radiusexpdt>20381230</radiusexpdt><imsi></imsi><value>30</value></values>";
    var ParametrosServico="<values><mainkey>"+Mainkey+"</mainkey><siplogin>"+Mainkey+"</siplogin><mn>"+data[2]+"</mn><wimaxpass>"+data[3]+"</wimaxpass><aclp>"+data[6]+"</aclp><rchs></rchs><csc></csc><imsi></imsi><akey></akey><esn></esn></values>";
    
    var LastUserID="0"; // NOT NULL	Podem preencher com 0 para já. Mas provavelmente iremos criar um user do lado do TIMM chamado SIGO e depois irei enviar o ID deste user.   
    var CentralType="HUAWEI-NGN"; //NOT NULL	Preencher com o campo CentralProvisioning das folhas IN e AAA
    var d = new Date();
    d.setDate(d.getDate()+15);
    var LastUpdate=new Date().toISOString(); // NOT NULL	Preencher com a data de inserção do registo da tabela
    var DoTime=d.toISOString(); // NULL	Preencher com a data de inserção do registo da tabela mais 15 dias.
    var InsertMassivo="0"; // NOT NULL	Preencher com 0
    var Retries ="0"; // NULL	Preencher com 0
    var SubsType="WIMAX"; // NOT NULL	Para a IN preencher com "PPT". Para os AAA preencher com WIMAX, CDMA e ADSLNAPos ou ADSLNAPre dependendo do numero de telefone
    var DoUndo=null;// NULL	Preencher com NULL
    var ScheduleTime=null; // NULL	Preencher com NULL
    var AccaoStatus="exec"; // NULL	Preencher com "exec"
    var RetryTime="0"; // NULL	Preencher com 0
    var PrevErrorCode=null; // NULL	Preencher com NULL
    var control_status="pending" // NOT NULL	Preencher com "pending"
    var ParserError=null; // NULL	Preencher com NULL
    var Comment="UNIG_CRM;"+data[0]; //NULL	Preencher com "Northbound Insert"

    sql="INSERT INTO TAE_AccoesQueue (RequestID,ActivityID,TipoAccao,Mainkey,ParametrosServico,LastUserID,CentralType,LastUpdate,DoTime,InsertMassivo,Retries,SubsType,DoUndo,ScheduleTime,AccaoStatus,RetryTime,PrevErrorCode,control_status,ParserError,Comment) ";
    sql+="VALUES('"+RequestID+"',"+ActivityID+",'"+TipoAccao+"','"+Mainkey+"','";
    sql+=ParametrosServico+"',"+LastUserID+",'"+CentralType+"','"+LastUpdate+"','"+DoTime+"',",
    sql+=InsertMassivo+","+Retries+",'"+SubsType+"',"+DoUndo+","+ScheduleTime+",'"+AccaoStatus+"',";
    sql+=RetryTime+","+PrevErrorCode+",'"+control_status+"',"+ParserError+",'"+Comment+"');"; 
     
     this.insert_Table(sql);
     this.create_account_in(data[0]+";"+data[1]+";"+data[5]+";"+data[6]);
    return true;
}

 static async insert_Table(sql){
     
        const config = {
        user: "itgest",
        password: "itgest",
        server: "172.31.134.22",
        database: "TIMM_PRV_QUEUES",
            options: {
                encrypt: true
            }
        };
     
    var dbConn = new bd.ConnectionPool(config);
    
    dbConn.connect().then(function () {
        var transaction = new bd.Transaction(dbConn);
        transaction.begin().then(function () {
            var request = new bd.Request(transaction);
            request.query(sql).then(function () {
                transaction.commit().then(function (recordSet) { 
                    dbConn.close();
                }).catch(function (err) { 
                    dbConn.close();
                });
            }).catch(function (err) { 
                dbConn.close();
            });
             
        }).catch(function (err) { 
            dbConn.close();
        });
    }).catch(function (err) { 
    });
 

    return true
}

 
}

module.exports = Prevision
