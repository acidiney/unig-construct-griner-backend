'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Produto extends Model {
  
  static get rules () {
    return {
      nome: 'required|unique:produtos',
      valor: 'required',
      tipo: 'required',
      imposto_id: 'required',
      moeda_id: 'required',
      is_active: 'required', 
    }
  }

  static get rules_update () {
    return {
      nome: 'required',
      valor: 'required',
      tipo: 'required', 
    }
  }

  linhasFacturas() {
    return this.hasMany('App/Models/LinhaFactura')
  }

  produtoImposto() {
    return this.hasMany('App/Models/ProdutoImposto')
  }

  user () {
    return this.belongsTo('App/Models/User')
  }
  
  fornecedor() {
    return this.belongsTo('App/Models/Fornecedor')
  }
}

module.exports = Produto
