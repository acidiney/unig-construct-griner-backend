'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Conta extends Model {
  moeda() {
    return this.belongsTo("App/Models/Moeda");
  }
}

module.exports = Conta
