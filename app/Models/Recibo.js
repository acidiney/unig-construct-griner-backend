'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Recibo extends Model {
 

  user () {
    return this.belongsTo('App/Models/User')
  }
   


  cliente() {
    return this.belongsTo("App/Models/Cliente");
  } 

  linhasRecibos() {
    return this.hasMany("App/Models/LinhaRecibo");
  }
  
  facturas() {
    //return this.belongsToMany('App/Models/Factura').pivotModel('App/Models/LinhaRecibo')
  }

  pagamento() {
    return this.belongsTo('App/Models/Pagamento')
  }
    
  serie() {
    return this.belongsTo("App/Models/Serie");
  }
  

  adiantamento() {
    return this.belongsTo("App/Models/MovimentoAdiantamento",'id', 'saldado_recibo');
  }
   
  facturas() {
    //return this.hasMany("App/Models/LinhaRecibo")
    return this.belongsToMany('App/Models/Factura').pivotModel('App/Models/LinhaRecibo')
  }


   static async findById(id) {  
      var recibo = await Recibo.query().with("serie").with("serie.documento").with("cliente")
        .with("cliente.tipoCliente").with("cliente.tipoIdentidade")
        .with('pagamento.lines.forma_pagamento')
        .with('adiantamento').with("facturas").withCount('facturas') 
        .where("id", id).first();  
      if (recibo == null) {
          return  null;
      }   
    return recibo.toJSON(); 
  } 
}

module.exports = Recibo
