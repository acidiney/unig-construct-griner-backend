'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Model = use('Model')
var numeral = require("numeral");

class Factura extends Model {
  static castDates(field, value) {
    if (field == "status_date") {
      return value.format("YYYY-MM-DD h:mm:ss");
    }
    return super.formatDates(field, value);
  }

  cliente() {
    return this.belongsTo("App/Models/Cliente");
  }

  linhasFacturas() {
    return this.hasMany("App/Models/LinhaFactura");
  }

  linhasRecibos() {
    return this.hasMany("App/Models/LinaRecibo");
    
  }
 
  
  recibos() {
    return this.belongsToMany('App/Models/Recibo').pivotModel('App/Models/LinhaRecibo')
  }
  pagamento() {
    return this.belongsTo('App/Models/Pagamento')
  }
  user() {
    return this.belongsTo("App/Models/User");
  }

  serie() {
    return this.belongsTo("App/Models/Serie");
  }

  conta() {
    return this.belongsTo("App/Models/Conta",'conta_id');
  }

  adiantamento() {
    return this.belongsTo("App/Models/MovimentoAdiantamento",'id', 'saldado_factura');
  }
  
  fornecedor() {
    return this.belongsTo("App/Models/Fornecedor");
  } 
  lines() {
    return this.hasMany("App/Models/LinhaFactura").select("*","artigo_id as produto_id","valor_imposto as valorImposto")
  }

  static get rules_anular() {
    return {
      status_reason: "required",
    };
  }

  static findAllInvoices(search, filters) { 
    return Factura.query().select('facturas.*',Database.raw('DATE_FORMAT(facturas.created_at, "%m") as mes'), { nome: 'clientes.nome' })
        .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
        .innerJoin("series", "series.id", "facturas.serie_id")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .whereIn('documentos.sigla', filters.documents) 
        .where(function () {          
          if (filters.typeFilter) {
                
                if (filters.typeFilter=='chave_serviço') { 
                  this.where(`facturas.servico_id`, Database.select('id').from('servicos').where("chaveServico", `${search}`))
                }else if (filters.typeFilter=='número_de_conta') { 
                  this.where(`facturas.conta_id`, `${search}`);
                }else if (filters.typeFilter=='nome_cliente') {
                  this.whereIn(`facturas.cliente_id`, Database.select('id').from('clientes').where("nome", "LIKE", `%${search}%`))
                } else if (filters.typeFilter == 'número_de_contribuinte') {
                 // const c = await Database.select('id').from('clientes').where("contribuente", search).first();
                  console.log("typeFilter");
                  this.whereIn(`facturas.cliente_id`, Database.select('id').from('clientes').where("contribuente", `${search}`))
                }else if (filters.typeFilter == "número_da_factura") {
                  this.where(`facturas.factura_sigla`, `${search}`);
                }
              } else {
                if (search) {
                  if (filters.keys.invoice instanceof Array) { filters.keys.invoice.forEach(key => { this.orWhere(`facturas.${key}`, 'like', "%" + search + "%") }) }
                  else { this.where(`facturas.${filters.keys.invoice}`, 'like', "%" + search + "%") }
                } else {
                  this.whereBetween(Database.raw('IF(IFNULL(facturas.created_at,0)=0, DATE_FORMAT(facturas.status_date, "%Y-%m-%d"), DATE_FORMAT(facturas.created_at, "%Y-%m-%d"))'), [filters.startDate, filters.endDate])
                } 
              }
        }) 
      .with("serie.documento").with("cliente") 
      .orderBy(filters.orderBy, filters.typeOrderBy)
      .paginate(filters.page, filters.perPage)
  } 
  
  /**
   *  factura details.
   * PUT or PATCH clientes/:id
   * @author caniggiamoreira@hotmail.com ou caniggiamoreira@itgest.pt
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  static async gerarFactura(params) {
    let cliente = null;
    let user = null;
    let linhasFacturas = null;
    let factura = null;
    let projecto = null;
    let contas_bancarias = null;

    let servico_conta = null;
    let linha_pagamentos = null;

    let tecnologia = null;
    let tecnologia_servico = null;
    let nota_credito_iplc = null;

    let facturas_clientes = null;
    let por_pagar = null;
    let detalhesInterconexao = null;
    let detalhesInterconexaoNac = null;
    let detalhesInterconexaoInt = null;
    let nota_credito_ict = null;

    factura = await Database.select(
      "facturas.id as factura_id",
      "facturas.factura_sigla",
      "facturas.observacao",
      "facturas.hash",
      "facturas.hash_control",
      "facturas.status",
      "facturas.status_date",
      "facturas.status_reason",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.user_id",
      "facturas.id",
      "facturas.numero",
      "facturas.cliente_id",
      "facturas.created_at",
      "facturas.pago",
      "facturas.serie_id",
      "facturas.user_id",
      "facturas.numero_origem_factura",
      "facturas.data_origem_factura",
      "facturas.is_nota_credito",
      "facturas.is_iplc",
      "facturas.moeda_iso as moeda_iso",
      "facturas.total_contra_valor",
      "facturas.valor_cambio",
      "facturas.totalKwanza",
      "facturas.user_id",
      "facturas.user_id",
      "facturas.conta_id",
      "facturas.servico_id",
      "facturas.facturacao",
      "facturas.data_vencimento",
      "facturas.minutos_interconexao",
      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla",
      "facturas.pagamento_id",
      "pagamentos.valor_recebido",
      "pagamentos.troco",
      "bancos.nome as banco",
      "forma_pagamentos.designacao",
      "pagamentos.forma_pagamento_id",
      "contas.numeroExterno as numeroExterno",
      "contas.contaDescricao as contaDescricao",
      "lojas.nome as lojaNome",
      "filials.nome as filialNome",
      "movimento_adiantamentos.saldado_factura"
    )
      .from("facturas")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .leftJoin("pagamentos", "pagamentos.id", "facturas.pagamento_id")
      .leftJoin(
        "forma_pagamentos",
        "forma_pagamentos.id",
        "pagamentos.forma_pagamento_id"
      )
      .leftJoin("bancos", "bancos.id", "pagamentos.banco_id")
      .leftJoin("contas", "contas.id", "facturas.conta_id")
      .leftJoin("lojas", "contas.agencia_id", "lojas.id")
      .leftJoin("filials", "filials.id", "lojas.filial_id")
      .leftJoin(
        "movimento_adiantamentos",
        "movimento_adiantamentos.saldado_factura",
        "facturas.id"
      )
      .where("facturas.id", params)
      .first();

    nota_credito_iplc = await Database.select(
      "facturas.id as factura_id",
      "facturas.factura_sigla as factura_sigla",
      "facturas.moeda_iso as moeda_iso"
    )
      .from("facturas")
      .where("facturas.factura_sigla", factura.numero_origem_factura)
      .first();

    nota_credito_ict = await Database.select(
      "facturas.id as factura_id",
      "facturas.factura_sigla as factura_sigla",
      "facturas.moeda_iso as moeda_iso",
      "tecnologias.nome as tecnologiaNome"
    )
      .from("facturas")
      .innerJoin("bill_runs", "bill_runs.factura_utilitie_id", "facturas.id")
      .innerJoin(
        "bill_run_headers",
        "bill_run_headers.id",
        "bill_runs.bill_run_header_id"
      )
      .innerJoin(
        "tecnologias",
        "tecnologias.id",
        "bill_run_headers.tecnologia_id"
      )
      .where("tecnologias.nome", "INTERCONEXAO")
      .where("facturas.factura_sigla", factura.numero_origem_factura)
      .first();

    //console.log(nota_credito_ict)

    if (factura == null) {
      let data = {
        clientes: null,
        impostos: null,
        produtos: null,
        facturas: null,
      };
      return data;
    }

    if (factura.sigla == "NC") { 
        var f = await Database.select("facturas.factura_sigla","facturas.facturacao","contas.id as conta_id","contas.contaDescricao as contaDescricao").from("facturas").leftJoin("contas", "contas.id", "facturas.conta_id").where("facturas.factura_sigla", factura.numero_origem_factura).first();        
        factura.contaDescricao = f.contaDescricao;
        factura.conta_id = f.conta_id;
        factura.facturacao = f.facturacao;  
      }

    if (factura.facturacao == "POS-PAGO") {
      tecnologia = await Database.select(
        "facturas.id",
        "tecnologias.nome as tecnologiaNome",
        "bill_run_headers.mes as mes",
        "bill_run_headers.ano as ano",
        Database.raw(
          'CONCAT(bill_run_headers.ano, "-", bill_run_headers.mes, "-", "01") as concatTecnologia'
        ),
        Database.raw(
          'CONCAT(bill_run_headers.ano, "",IF(bill_run_headers.mes < 10,CONCAT(0, "",bill_run_headers.mes) , bill_run_headers.mes) ) as mesAnoConcat'
        )
      )
        .from("facturas")
        .innerJoin("bill_runs", "bill_runs.factura_utilitie_id", "facturas.id")
        .innerJoin(
          "bill_run_headers",
          "bill_run_headers.id",
          "bill_runs.bill_run_header_id"
        )
        .innerJoin(
          "tecnologias",
          "tecnologias.id",
          "bill_run_headers.tecnologia_id"
        )
        .where((factura.sigla == 'NC'? 'facturas.factura_sigla':"facturas.id"), factura.sigla == 'NC'? factura.numero_origem_factura: params)
        .first();
       
 

      if (tecnologia != undefined) {
        detalhesInterconexao = await Database.select(
          "p.codigo_interconexao",
          "p.nacao_sigla",
          "p.cliente_id",
          "d.periodo",
          "d.origin",
          "d.destination",
          "d.service",
          "d.call",
          "d.minutes",
          "d.rate",
          "d.amount"
        )
          .from("parceria_clientes as p")
          .innerJoin(
            "detalhe_interconexaos as d",
            "d.billing_operator",
            "p.codigo_interconexao"
          )
          .where("d.periodo", tecnologia.mesAnoConcat)
          .where("p.cliente_id", factura.cliente_id);

        detalhesInterconexaoInt = await Database.select(
          "p.codigo_interconexao",
          "p.nacao_sigla",
          "p.cliente_id",
          "d.nacao_sigla as DnacaoSigla",
          "d.periodo",
          "d.origin",
          "d.destination",
          "d.service",
          "d.call",
          "d.minutes",
          "d.rate",
          "d.amount"
        )
          .from("parceria_clientes as p")
          .innerJoin(
            "detalhe_interconexaos as d",
            "d.billing_operator",
            "p.codigo_interconexao"
          )
          .where("d.nacao_sigla", "INT")
          .where("d.periodo", tecnologia.mesAnoConcat)
          .where("p.cliente_id", factura.cliente_id);

        detalhesInterconexaoNac = await Database.select(
          "p.codigo_interconexao",
          "p.nacao_sigla",
          "p.cliente_id",
          "d.nacao_sigla as DnacaoSigla",
          "d.periodo",
          "d.origin",
          "d.destination",
          "d.service",
          "d.call",
          "d.minutes",
          "d.rate",
          "d.amount"
        )
          .from("parceria_clientes as p")
          .innerJoin(
            "detalhe_interconexaos as d",
            "d.billing_operator",
            "p.codigo_interconexao"
          )
          .where("d.nacao_sigla", "NAC")
          .where("d.periodo", tecnologia.mesAnoConcat)
          .where("p.cliente_id", factura.cliente_id);
      }

      //console.log(detalhesInterconexaoInt)

      facturas_clientes = await Database.select(
        "facturas.id",
        "facturas.factura_sigla",
        "facturas.total",
        "facturas.pago",
        "facturas.cliente_id",
        "facturas.created_at",
        "bill_run_headers.mes",
        "bill_run_headers.ano",
        Database.raw(
          'CONCAT(bill_run_headers.ano, "-", bill_run_headers.mes, "-", "01") as concatFactCliente'
        )
      )
        .from("facturas")
        .innerJoin("bill_runs", "bill_runs.factura_utilitie_id", "facturas.id")
        .innerJoin(
          "bill_run_headers",
          "bill_run_headers.id",
          "bill_runs.bill_run_header_id"
        )
        .where("facturas.pago", false)
        //.where("bill_run_headers.mes", "<", tecnologia.mes)
        .where(
          Database.raw(
            'CONCAT(bill_run_headers.ano, "-", bill_run_headers.mes, "-", "01")'
          ),
          "<",
          tecnologia != undefined ? tecnologia.concatTecnologia : null
        )
        .where("facturas.cliente_id", factura.cliente_id)
        .whereNot("facturas.id", params)
        .orderBy("facturas.created_at", "DESC")
        .limit(4);

      //console.log(facturas_clientes)

      por_pagar = await Database.select(
        Database.raw("count(facturas.id) as total_por_pagar")
      )
        .from("facturas")
        .innerJoin("bill_runs", "bill_runs.factura_utilitie_id", "facturas.id")
        .innerJoin(
          "bill_run_headers",
          "bill_run_headers.id",
          "bill_runs.bill_run_header_id"
        )
        .where("facturas.pago", false)
        //.where("bill_run_headers.mes", "<", tecnologia.mes)
        .where(
          Database.raw(
            'CONCAT(bill_run_headers.ano, "-", bill_run_headers.mes, "-", "01")'
          ),
          "<",
          tecnologia != undefined ? tecnologia.concatTecnologia : null
        )
        .where("facturas.cliente_id", factura.cliente_id)
        .whereNot("facturas.id", params)
        .first();

        tecnologia_servico = await Database.select(
        "facturas.facturacao",
        "facturas.factura_sigla",
        "servicos.tecnologia_id",
        "tecnologias.nome as tecnologiaNome"
        ).from("linha_facturas")
        .innerJoin("facturas","facturas.id","linha_facturas.factura_id")
        .innerJoin("servicos","servicos.id","linha_facturas.servico_id")
        .innerJoin("tecnologias","tecnologias.id","servicos.tecnologia_id")
        .where((factura.sigla == 'NC'? 'facturas.factura_sigla':"facturas.id"), factura.sigla == 'NC'? factura.numero_origem_factura: params)
        .first();
        
      // console.log(por_pagar)
    }

    if (factura.facturacao == "PRE-PAGO" && nota_credito_ict) {
      tecnologia = await Database.select(
        "facturas.id",
        "tecnologias.nome as tecnologiaNome",
        "bill_run_headers.mes as mes",
        "bill_run_headers.ano as ano",
        Database.raw(
          'CONCAT(bill_run_headers.ano, "-", bill_run_headers.mes, "-", "01") as concatTecnologia'
        ),
        Database.raw(
          'CONCAT(bill_run_headers.ano, "", bill_run_headers.mes) as mesAnoConcat'
        )
      )
        .from("facturas")
        .innerJoin("bill_runs", "bill_runs.factura_utilitie_id", "facturas.id")
        .innerJoin(
          "bill_run_headers",
          "bill_run_headers.id",
          "bill_runs.bill_run_header_id"
        )
        .innerJoin(
          "tecnologias",
          "tecnologias.id",
          "bill_run_headers.tecnologia_id"
        )
        .where("facturas.id", nota_credito_ict.factura_id)
        .first();
      
    }

    linha_pagamentos = await Database.select("*")
      .from("linha_pagamentos")
      .innerJoin(
        "forma_pagamentos",
        "forma_pagamentos.id",
        "linha_pagamentos.forma_pagamento_id"
      )
      .where("linha_pagamentos.pagamento_id", factura.pagamento_id);

    if (factura.forma_pagamento_id != null) {
      linha_pagamentos = [
        {
          designacao: factura.designacao,
          valor_recebido: factura.valor_recebido,
        },
      ];
    }

    contas_bancarias = await Database.from("bancos")
      .innerJoin("factura_bancos", "bancos.id", "factura_bancos.banco_id")
      .where("factura_bancos.factura_id", factura.id);

    if (factura.conta_id != null) {
      servico_conta = await Database.select(
        "servicos.id",
        "servicos.chaveServico",
        "servicos.conta_id",
        "servicos.tarifario_id",
        "tarifarios.descricao",
        "tarifarios.tecnologia",
        "plano_precos.precoDescricao"
      )
        .from("servicos")
        .innerJoin("tarifarios", "tarifarios.id", "servicos.tarifario_id")
        .innerJoin(
          "plano_precos",
          "plano_precos.id",
          "tarifarios.plano_preco_id"
        )
        .where("servicos.id", factura.servico_id)
        .where("servicos.conta_id", factura.conta_id)
        .first();
    }

    cliente = await Database.select(
      "clientes.id",
      "clientes.nome",
      "clientes.telefone",
      "clientes.morada",
      "clientes.contribuente",
      "clientes.genero",
      "clientes.contactPerson",
      "clientes.buildingNumber",
      "clientes.streetName",
      "clientes.addressDetail",
      "clientes.pais",
      "clientes.city",
      "clientes.province",
      "clientes.email",
      "clientes.observacao",
      "clientes.user_id",
      "clientes.created_at",
      "clientes.updated_at",
      "contribuinte_comercial",
      "clientes.clienteID",
      "clientes.tipo_identidade_id",
      "clientes.tipo_cliente_id",
      "clientes.direccao",
      "clientes.numeroExterno",
      "clientes.is_entidade_cativadora",
      "clientes.unificado",
      "clientes.entidade_cativadora_id",
      "tipo_identidades.tipoIdentificacao",
      "tipo_clientes.tipoClienteDesc"
    )
      .from("clientes")
      .leftJoin(
        "tipo_identidades",
        "tipo_identidades.id",
        "clientes.tipo_identidade_id"
      )
      .leftJoin("tipo_clientes", "tipo_clientes.id", "clientes.tipo_cliente_id")
      .where("clientes.id", factura.cliente_id)
      .first();

    user = await Database.select(
      "users.id",
      "users.nome",
      "users.telefone",
      "users.email",
      "users.username",
      "users.morada",
      "users.status",
      "users.created_at",
      "users.empresa_id",
      "users.role_id",
      "users.updated_at",
      "empresas.companyName",
      "empresas.addressDetail",
      "empresas.email",
      "empresas.telefone",
      "empresas.city",
      "empresas.province",
      "empresas.taxRegistrationNumber",
      "empresas.logotipo",
      "empresas.projecto_isActive",
      "lojas.nome as loja"
    )
      .from("users")
      .innerJoin("empresas", "empresas.id", "users.empresa_id")
      .leftJoin("lojas", "lojas.id", "users.loja_id")
      .where("users.id", factura.user_id)
      .first();

    linhasFacturas = await Database.select(
      "produtos.id as produto_id",
      "produtos.nome",
      "produtos.barcode",
      "produtos.tipo",
      "linha_facturas.id as line",
      "linha_facturas.valor",
      "linha_facturas.quantidade",
      "linha_facturas.created_at",
      "linha_facturas.total",
      "linha_facturas.valor_desconto",
      "linha_facturas.valor_imposto as valorImposto",
      "linha_facturas.imposto_id",
      "linha_facturas.valor_desconto as desconto",
      "linha_facturas.linhaTotalSemImposto",
      "linha_facturas.observacao",
      "impostos.descricao as descricaoImposto",
      "impostos.valor as vImposto",
      "impostos.codigo as codigoImposto",
      "linha_facturas.moeda_iso",
      "servicos.id as idServico",
      "servicos.descricao_operacao as descricao_operacao",
      "servicos.chaveServico",
      "iplc_servicos.capacidade",
      "iplc_servicos.origem",
      "iplc_servicos.destino"
    )
      .from("linha_facturas")
      .leftJoin("produtos", "produtos.id", "linha_facturas.artigo_id")
      .innerJoin("impostos", "impostos.id", "linha_facturas.imposto_id")
      .leftJoin("servicos", "servicos.id", "linha_facturas.servico_id")
      .leftJoin(
        "iplc_servicos",
        "iplc_servicos.servico_id",
        "linha_facturas.servico_id"
      )
      .where("linha_facturas.factura_id", factura.id);

    let data = {
      factura: factura,
      linha_pagamentos: linha_pagamentos,
      cliente,
      produtos: linhasFacturas,
      user: user,
      servico_conta: servico_conta,
      projecto: projecto ? projecto : null,
      // moedas: moedas,
      contas_bancarias: contas_bancarias,
      tecnologia: tecnologia ? tecnologia : tecnologia_servico,
      facturas_clientes: facturas_clientes ? facturas_clientes : null,
      por_pagar: por_pagar,
      detalhesInterconexao: detalhesInterconexao,
      detalhesInterconexaoNac: detalhesInterconexaoNac,
      detalhesInterconexaoInt: detalhesInterconexaoInt,
      nota_credito_ict: nota_credito_ict ? nota_credito_ict : null,
      nota_credito_iplc: nota_credito_iplc,
      //formasPagamento: formasPagamento
    };

    return data;
  }

  static async findFactFromNc(params) {
    
    var factura = await Factura.findById(params);

    if (factura == null) { return DataResponse.response("error", 500, "Nenhum resultado encontrado", null); }
    else if (factura.status == "A") { return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " está anulada", null); }
    else if (factura.serie.documento.sigla == "NC") { return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + ", não é possivel criar nota de crédito numa nota de crédito",null); }
       
    if (factura.__meta__.total_recibos > 0 && factura.__meta__.totalReciboAnulados > 0) {
      return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " não pode ser criada nota de crédito porque já possui recebimentos", factura);
    }

    var totalNC = 0;

    factura.lines.forEach(element => { Object.assign(element, { qtd_fixo: element.quantidade }); });

    if (factura.notas_credito.length > 0) {
      for (let index = 0; index < factura.notas_credito.length; index++) {
        const element = factura.notas_credito[index];
        totalNC+= (element.status == "N" ? element.total: 0)
      }
      totalNC = numeral(totalNC).format("0.00");

      if (numeral(factura.total).format("0.00") == totalNC) {
        return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " Ja atingiu o limite de nota de crédito",null);
      }

      var ncLines = await Database.raw("SELECT l.factura_id, l.servico_id, l.artigo_id, sum(l.quantidade) as quantidade, SUM(l.total) as total, SUM(l.linhaTotalSemImposto) as linhaTotalSemImposto, SUM(l.valor_imposto) as valorImposto FROM linha_facturas l, facturas f WHERE l.factura_id = f.id AND f.status = 'N' AND f.numero_origem_factura = '" + factura.factura_sigla + "' GROUP BY l.factura_id, l.artigo_id,l.servico_id")
      for (let i = 0; i < ncLines[0].length; i++) {
        const ncLine = ncLines[0][i];
        for (let j = 0; j < factura.lines.length; j++) {
          const ftline = factura.lines[j];
          if (ncLine.artigo_id == ftline.artigo_id && ncLine.servico_id==ftline.servico_id) {
                factura.lines[j].quantidade -= ncLine.quantidade;
                factura.lines[j].qtd_fixo -= ncLine.quantidade;
                factura.lines[j].linhaTotalSemImposto -= ncLine.linhaTotalSemImposto;
                factura.lines[j].valor_imposto -= ncLine.valor_imposto;
                factura.lines[j].valorImposto -= ncLine.valorImposto;
                factura.lines[j].total -= ncLine.total;
            if (factura.lines[j].quantidade <= 0) {
              factura.lines.splice(j, 1);
            }
          }
        }
      }
    }   
    let data = {
      factura: factura,
      cliente: factura.cliente,
      produtos: factura.lines,
    };  
    
    return DataResponse.response("success", 200, "", data);
  }
 
  static async findById(id) {  
      var factura = await Factura.query()
      .with("serie").with("serie.documento").with("cliente")
        .with("cliente.tipoCliente").with("cliente.tipoIdentidade")
      .with('pagamento.lines.forma_pagamento')
      .with('adiantamento')
        
      .with("lines").with('lines.imposto')
      .with("lines.produto").with("lines.servico").with('recibos')
      .withCount('recibos as total_recibos')
      .withCount('recibos as totalReciboAnulados', (builder) => {builder.where("status", 'N')})
      .where("id", id).first(); 
      if (factura == null) {
          return  null;
      }
    factura  = factura.toJSON();
    factura = { ...factura, notas_credito: await Factura.findNCFromFT(factura.factura_sigla) };

    
    return factura; 
  } 
  
  static async findNCFromFT(factura) {  
      var factura = await Factura.query()
      .with("serie").with("serie.documento").with("cliente")
      .with("cliente.tipoCliente").with("cliente.tipoIdentidade")
      .with("lines").with('lines.imposto')
      .with("lines.produto").where("numero_origem_factura", factura).fetch(); 
      if (factura == null) {
          return  [];
      }
    
    return factura.toJSON()  
  }
  
}

module.exports = Factura
