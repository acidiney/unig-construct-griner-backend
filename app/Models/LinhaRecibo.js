'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class LinhaRecibo extends Model {

  recibo () {
    return this.belongsTo('App/Models/Recibo')
  }

  factura () {
    return this.belongsTo('App/Models/Factura')
  }
  
  user() {
    return this.belongsTo('App/Models/User')
  }

  fornecedor() {
    return this.belongsTo('App/Models/Fornecedor')
  }
}

module.exports = LinhaRecibo
