'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class LinhaPagamento extends Model {
  forma_pagamento() {
    return this.belongsTo("App/Models/FormaPagamento");
  }
}

module.exports = LinhaPagamento
