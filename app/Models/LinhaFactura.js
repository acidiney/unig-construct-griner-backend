'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class LinhaFactura extends Model {
  fatura() {
    return this.belongsTo("App/Models/Fatura");
  }

  user() {
    return this.belongsTo("App/Models/User");
  }

  fornecedor() {
    return this.belongsTo("App/Models/Fornecedor");
  }

  produto() {
    return this.belongsTo("App/Models/Produto", "artigo_id");
  }
  servico() {
    return this.belongsTo("App/Models/Servico");
  }
  imposto() {
    return this.belongsTo("App/Models/Imposto");
  }
}

module.exports = LinhaFactura
