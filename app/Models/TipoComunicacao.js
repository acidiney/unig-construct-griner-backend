'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database");
class TipoComunicacao extends Model {
  
  user() {
    return this.belongsTo("App/Models/User").select(["id", "nome"]);
  } 

  /**
   * @override
   * Returns getAllCommunication Notes paginated by `filters.perPage` or 5 
   *
   * @param { Number } page
   * @param { Object } filters
   * @param { String } filters.search
   * @param { Number } filters.perPage
   */
  static async getAllTiposComunicacao(page = 1, filters = {}) {
    const r = await TipoComunicacao.query()

      .where(function () {
        if (filters.search) { 
         this.where("nome", "LIKE", `%${filters.search}%`);
        } else { 
          this.whereIn("nome", Database.raw("SELECT nome FROM tipo_comunicacaos"+(filters.search == null ? " " : " WHERE nome  LIKE '%" + filters.search + "%'" )))
        } 
      })
      .orWhereHas("user", (builder) => {
        if (filters.search) {
          builder.where("nome", "LIKE", `%${filters.search}%`);
        }
      })
      .with("user")
      .paginate(page, filters.perPage || 5);
    return r;
  }

  /**
   * @overwrite
   *
   * @param { Number } user_id
   * @param { Object } data 
   *
   * @Author Caniggia Moreira <caniggia.moreira@ideiasdinamicas.com>
   */
  static async createTipoComunicacao(user_id, data) { 
        const created = await TipoComunicacao.create({ user_id: user_id, ...data });   
        return created; 
  }

  /**
   * @overwrite
   *
   * @param { Number } user_id
   * @param { Object } data 
   *
   * @Author Caniggia Moreira <caniggia.moreira@ideiasdinamicas.com>
   */
  static async updateTipoComunicacao(params, data) { 
        const update = await TipoComunicacao.query().where("id", params).update({ ...data }); 
        return update; 
  }
  /**
   * Return an element
   */
  static async getTipoComunicacaoById(id) {
    const result = await TipoComunicacao.query().where("id", id).with("user") .first(); 
    return r;
  }

  /**
   * Return an element
   */
  static async findById(modelId) {
    const result = await TipoComunicacao.find(modelId); 
    return result;
  }

  static async deleteTipoComunicacao(param) { 
      const r = await this.findById(param);
      const result = await r.delete();
      return "Dados eliminado com sucesso"; 
  }

/**
 * 
 * @param {*} param 
 * @param {*} data 
 */
  static async statusTipoComunicacao(param, data) { 
      const result = await this.findById(param);
      await result.merge({ data: data == true ? false : true });
      result.save();
      return result; 
  }
  
}

module.exports = TipoComunicacao
