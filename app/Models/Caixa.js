'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database");
class Caixa extends Model {


 

   static async reportResumoRecebimentoPorCaixa(loja, data){ 
        var moment = require("moment");
        var dataActual = moment(new Date(data)).format("DD-MM-YYYY");

        let resumo = []
        let recibosRecibos = [];
 
        const l = await Database.select('lojas.id', 'filials.nome as filialNome', 'lojas.nome as lojaNome').from('lojas').innerJoin('filials', 'filials.id', 'lojas.filial_id').where('lojas.id', loja).first()
         
        const users =  await Database.select('users.id','users.nome', "caixas.id as caixa_id", 'caixas.data_abertura', 'caixas.data_fecho')
        .from('users').innerJoin('caixas', 'caixas.user_id', 'users.id')
        .where('caixas.data_abertura', data)
        .where("caixas.is_active", false) 
        .where('caixas.loja_id', l.id).orderBy('users.nome');
  
          
        
        for (let index = 0; index < users.length; index++) {
            const user = users[index]; 
            let res = null 
            res = await Database.raw("SELECT fp.designacao, SUM(lp.valor_recebido) as total, lp.forma_pagamento_id FROM linha_pagamentos lp, forma_pagamentos fp  WHERE lp.forma_pagamento_id = fp.id AND lp.pagamento_id "+ 
            "IN (SELECT f.pagamento_id from facturas f, caixas c WHERE f.caixa_id = c.id AND f.status = 'N' AND c.id = "+user.caixa_id+" AND c.loja_id = "+l.id+") AND lp.user_id = "+user.id+" GROUP BY lp.forma_pagamento_id "
            +"UNION "
            +" select 'Troco', -sum(pag.troco), 0 from pagamentos pag where pag.id in (select lp.pagamento_id FROM linha_pagamentos lp, forma_pagamentos fp "+
            "WHERE lp.forma_pagamento_id = fp.id AND lp.pagamento_id IN (SELECT f.pagamento_id from facturas f, caixas c WHERE f.caixa_id = c.id AND f.status = 'N' AND c.id ="+user.caixa_id+" AND c.loja_id = "+l.id+") "
            +"AND lp.user_id = "+user.id+" GROUP BY lp.forma_pagamento_id)");

            let r =''; 
            r = await Database.raw("SELECT * FROM forma_pagamentos fp, (SELECT lp.forma_pagamento_id, r.user_id, SUM(lp.valor_recebido) as total FROM recibos r, pagamentos p, linha_pagamentos lp, users u WHERE r.pagamento_id = p.id AND lp.pagamento_id = p.id AND r.user_id = u.id AND r.status = 'N' AND r.user_id = "+user.id+" AND u.loja_id = "+l.id+" AND DATE_FORMAT(r.created_at, '%Y-%m-%d') IN(SELECT c.data_abertura as created_at FROM caixas c, facturas f WHERE c.id = f.caixa_id AND c.id = "+user.caixa_id+" GROUP BY c.data_abertura) GROUP BY lp.forma_pagamento_id) recebimentos WHERE fp.id = recebimentos.forma_pagamento_id  ORDER BY fp.id")

            resumo.push({
              user: user,
              vendas:[res[0],r[0]]
            });
        
        }
        

        

        let rr = {
            users:users,
          loja: l,
          recebimentos: resumo,
          data: dataActual
        };
        
        return rr;
    } 

    static async getDiarioVendas(user, data1, data2, caixa_id) {

        const utilizador = await Database
            .select('us.id as id', 'us.nome as OperadorNome','fi.nome as filialNome', 'lo.nome as lojaNome')
            .table('users as us')
            .innerJoin('lojas as lo', 'us.loja_id', 'lo.id')
            .innerJoin('filials as fi', 'fi.id', 'lo.filial_id')
            .where('us.id', user).first() 

          

        const produtos = await Database.distinct('p.nome as produto', Database.raw('SUM(lf.quantidade) as quantidade'), Database.raw('SUM(lf.total) as valor'), 'lf.user_id')
            .table('linha_facturas as lf')
            .innerJoin('produtos as p', 'lf.artigo_id', 'p.id')
            .innerJoin('facturas as f', 'f.id', 'lf.factura_id')
            .whereBetween(Database.raw('DATE_FORMAT(lf.created_at, "%Y-%m-%d")'), [data1, data2])
            .where('lf.user_id', utilizador.id)
            .where('f.status','N')
            .where('f.pago',1)
            .groupBy('lf.artigo_id')

        let data = {
            data_venda: data1,
            data1: data1,
            data2: data2,
            utilizador: utilizador,
            produtos: produtos,
            caixa_id: caixa_id

        };
        return data
    }

     static async fechoCaixaResumoRecebimentoVendas(loja, data,user){ 
        
        var moment = require("moment"); 
        var dataActual = moment(new Date(data)).format("DD-MM-YYYY");
        var data = moment(new Date(data)).format("YYYY-MM-DD");
         
        let resumo = [] 
         
        const users =  await Database.select('caixas.id','caixas.data_abertura','users.id','users.nome','users.loja_id').from('users')
        .innerJoin('caixas', 'caixas.user_id', 'users.id')
        .where('caixas.data_abertura', data).distinct('users.id')
        .where('caixas.loja_id', loja).where('users.id',user);

          
        
        for (let index = 0; index < users.length; index++) {
            const user = users[index]; 
            let res = null 
            var data_abertura = moment(new Date(user.data_abertura)).format("YYYY-MM-DD");
            res = await Database.raw("SELECT * FROM forma_pagamentos fp, (SELECT lp.forma_pagamento_id, f.user_id, SUM(lp.valor_recebido) as total FROM facturas f, pagamentos p, linha_pagamentos lp, users u WHERE f.pagamento_id = p.id AND lp.pagamento_id = p.id AND f.user_id = u.id AND f.status = 'N' AND f.user_id = "+user.id+" AND u.loja_id = "+user.loja_id+" AND DATE_FORMAT(f.created_at, '%Y-%m-%d') = '"+data_abertura+"'  GROUP BY lp.forma_pagamento_id) recebimentos WHERE fp.id = recebimentos.forma_pagamento_id  ORDER BY fp.id ");
 
            
            var r = await Database.raw("SELECT * FROM forma_pagamentos fp, (SELECT lp.forma_pagamento_id, r.user_id, SUM(lp.valor_recebido) as total FROM recibos r, pagamentos p, linha_pagamentos lp, users u WHERE r.pagamento_id = p.id AND lp.pagamento_id = p.id AND r.user_id = u.id AND r.status = 'N' AND r.user_id = "+user.id+" AND u.loja_id = "+user.loja_id+" AND DATE_FORMAT(r.created_at, '%Y-%m-%d') = '"+data_abertura+"'  GROUP BY lp.forma_pagamento_id) recebimentos WHERE fp.id = recebimentos.forma_pagamento_id  ORDER BY fp.id")

            resumo.push({  user: user, vendas: [res[0],r[0]] }); 

        }
 

        let rr = { 
          recebimentos: resumo,
          data: dataActual
        }; 
        
        return rr;
    }  
}


module.exports = Caixa
