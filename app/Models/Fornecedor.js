'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Fornecedor extends Model {

  users() {
    return this.hasMany('App/Models/User')
  }

  facturas() {
    return this.hasMany('App/Models/Factura')
  }

  artigos() {
    return this.hasMany('App/Models/Produto')
  }

  clientes() {
    return this.hasMany('App/Models/Cliente')
  }

  recibos() {
    return this.hasMany('App/Models/Recibo')
  }

  facturas() {
    return this.hasMany('App/Models/Factura')
  }

  facturas() {
    return this.hasMany('App/Models/Factura')
  }

  linhasFacturas() {
    return this.hasMany('App/Models/LinhaFactura')
  }

  linhasRecibos() {
    return this.hasMany('App/Models/LinaRecibo')
  }

  clienteFornecedor() {
    return this.hasMany('App/Models/clienteFornecedor')
  }
}

module.exports = Fornecedor
