'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database");

class RelatorioMediation extends Model {
  /**
   * @description FIXO-EXTERIOR
   * Tráfego da rede de telefonia fixa para o exterior do país (minutos)
   * @param {*} filter
   */
  static async relatorioMediationFixoExterior(filter) {
    const connection = "mysql_mediation";
    const datas = await Database.connection(connection).raw(
      "select date_format(answerTime,'%Y%m') as periodo,countryCode as prefixo,sum(callduration) tempo from cdr_inputs where (length(callerNumber)=9 and substr(callerNumber,1,1)=2) and substr(calledNumber,1,4)<>2449 and countryCode<>0 and countryCode<>244 and DATE_FORMAT(answerTime, '%Y%m')= '" +
        filter.periodo +
        "' group by date_format(answerTime,'%Y%m'),countryCode;"
    );
    return datas[0];
  }

  /**
   * @description FIXO-FIXO
   * Tráfego doméstico de uma rede de telefonia fixa para outra rede de telefonia fixa (minutos)
   * @param {*} filter
   */
  static async relatorioMediationFixoFixo(filter) {
    const connection = "mysql_mediation";
    const datas = await Database.connection(connection).raw(
      "select date_format(answerTime,'%Y%m') as periodo,substr(calledNumber,1,3) prefixo,sum(callduration) tempo from cdr_inputs where (length(callerNumber)=9 and substr(callerNumber,1,1)=2) and substr(calledNumber,1,4)<>2449 and (length(calledNumber)=9 and substr(calledNumber,1,1)=2) and DATE_FORMAT(answerTime, '%Y%m')='" +
        filter.periodo +
        "' group by date_format(answerTime,'%Y%m'),substr(calledNumber,1,3)"
    );
    return datas[0];
  }

  /**
   * @description FIXO-MOVEL
   * Tráfego doméstico de uma rede de telefonia fixa para uma rede móvel (minutos)
   * @param {*} filter
   */
  static async relatorioMediationFixoMovel(filter) {
    const connection = "mysql_mediation";
    const datas = await Database.connection(connection).raw(
      "select date_format(answerTime,'%Y%m') as periodo,substr(calledNumber,1,5) prefixo,sum(callduration) as tempo from cdr_inputs where (length(callerNumber)=9 and substr(callerNumber,1,1)=2) and (substr(calledNumber,1,4)=2449  or (substr(calledNumber,1,1)=9 and length(calledNumber)=9))  and DATE_FORMAT(answerTime, '%Y%m')='" +
        filter.periodo +
        "' group by date_format(answerTime,'%Y%m'),substr(calledNumber,1,5)"
    );
    return datas[0];
  }
  /**
   * @description EXTERIOR-FIXO
   * Tráfego da rede de telefonia fixa para o exterior do país (minutos)
   * @param {*} filter
   */
  static async relatorioMediationExteriorFixo(filter) {
    const connection = "mysql_mediation";
    const datas = await Database.connection(connection).raw(
      "select date_format(answerTime,'%Y%m') as periodo,substr(callerNumber,1,5) as prefixo, sum(callduration) as tempo from cdr_inputs where (length(calledNumber)=9 and substr(calledNumber,1,1)=2) and substr(calledNumber,1,4)<>2449 and substr(callerNumber,1,1)=0 and DATE_FORMAT(answerTime, '%Y%m')='" +
        filter.periodo +
        "' group by date_format(answerTime,'%Y%m'),substr(callerNumber,1,5)"
    );
    return datas[0];
  }
 
}

module.exports = RelatorioMediation
 