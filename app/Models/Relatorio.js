"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");
const Database = use("Database");
const Recibo = use("App/Models/Recibo");
class Relatorio extends Model {


  static async relatorioFacturacaoRealizadaCobrancaGlobal(filter) {
    //console.log(filter);
    var direccao = "";
    var produto = "";
    var loja = "";
    if (filter.direccao != "T") {
      direccao += " AND c.direccao = '" + filter.direccao + "'";
    }
    if (filter.produto != "T") {
      produto +=
        " AND id IN (SELECT factura_id FROM linha_facturas WHERE artigo_id = " +
        filter.produto +
        ")";
    }
    if (filter.loja != "T") {
      loja +=
        " AND caixa_id IN (SELECT id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    let raw
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      raw = await Database.raw(
        "SELECT facturacao, direccao, SUM(total) total, mes FROM (SELECT facturacao, DATE_FORMAT(created_at, '%m') as mes, SUM(total) as total, cliente_id FROM facturas WHERE DATE_FORMAT(created_at, '%Y') = " +
        filter.ano +
        " AND  status = 'N' " +
        loja +
        " " +
        produto +
        "  GROUP BY DATE_FORMAT(created_at, '%m'), facturacao, cliente_id ORDER BY DATE_FORMAT(created_at, '%m')) ft, clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.province == 'T' ? " " : " AND c.province = " + filter.province) + " " +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );
    } else {
      raw = await Database.raw(
        "SELECT facturacao, direccao, SUM(total) total, mes FROM (SELECT f.facturacao, brh.mes as mes, SUM(f.total) as total, f.cliente_id FROM facturas f, bill_runs br, bill_run_headers brh WHERE f.id = br.factura_utilitie_id AND br.bill_run_header_id = brh.id AND brh.ano = " +
        filter.ano + " AND  status = 'N' " + loja + " " + produto +
        "  GROUP BY   brh.mes, f.facturacao, f.cliente_id ORDER BY brh.mes) ft, clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.province == 'T' ? " " : " AND c.province = " + filter.province) + " " +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );



    }

    var facturacaos = [];
    var meses = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 }
    ];

    var meses2 = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 },
      { nome_mes: "d", numero_mes: "13", valor: 0 }
    ];

    let d = raw[0];

    for (let i = 0; i < d.length; i++) {
      const element = d[i];
      if (facturacaos.length == 0) {
        facturacaos.push({ tipoFacturacao: element.facturacao, direccaos: [] });
      } else {
        var v = 0;
        for (let j = 0; j < facturacaos.length; j++) {
          const e = facturacaos[j];
          if (e.tipoFacturacao === element.facturacao) {
            v = 1;
          }
        }
        if (v == 0) {
          facturacaos.push({
            tipoFacturacao: element.facturacao,
            direccaos: []
          });
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < d.length; i++) {
        const element = d[i];
        if (elef.tipoFacturacao === element.facturacao) {
          if (facturacaos[f].direccaos.length == 0) {
            facturacaos[f].direccaos.push({
              direccao: element.direccao,
              saldos: [],
              total: 0
            });
          } else {
            var v = 0;
            for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
              const e = facturacaos[f].direccaos[j];
              if (e.direccao == element.direccao) {
                v = 1;
              }
            }
            if (v == 0) {
              facturacaos[f].direccaos.push({
                direccao: element.direccao,
                saldos: [],
                total: 0
              });
            }
          }
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < elef.direccaos.length; i++) {
        const element = elef.direccaos[i];
        for (let j = 0; j < d.length; j++) {
          const el = d[j];
          if (
            element.direccao === el.direccao &&
            elef.tipoFacturacao === el.facturacao
          ) {
            var mes = "";
            for (let index = 0; index < meses.length; index++) {
              const ele = meses[index];
              if (ele.numero_mes === el.mes) {
                mes = ele.nome_mes;
              }
            }
            facturacaos[f].direccaos[i].saldos.push({
              nome_mes: mes,
              numero_mes: "" + (el.mes < 10 && filter.tipoFacturacao == 'POS-PAGO' ? "0" + el.mes : el.mes),
              valor: el.total
            });
          }
        }
      }
    }

    for (let i = 0; i < meses.length; i++) {
      const mes = meses[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          var v = 0;
          const element = facturacaos[f].direccaos[j].saldos;
          for (let k = 0; k < element.length; k++) {
            const el = element[k];

            if (el.numero_mes === mes.numero_mes) {
              v = 1;
              facturacaos[f].direccaos[j].total += el.valor;
            }
          }
          if (v == 0) {
            facturacaos[f].direccaos[j].saldos.push(meses[i]);
          }
        }
      }
    }
    //return facturacaos;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let index = 0; index < facturacaos[f].direccaos.length; index++) {
        facturacaos[f].direccaos[index].saldos.sort(function (a, b) {
          return a.numero_mes - b.numero_mes;
        });
      }
    }

    for (let i = 0; i < meses2.length; i++) {
      var total = 0;
      const element = meses2[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          for (let s = 0; s < facturacaos[f].direccaos[j].saldos.length; s++) {
            if (element.numero_mes == facturacaos[f].direccaos[j].saldos[s].numero_mes) {
              total += facturacaos[f].direccaos[j].saldos[s].valor;
            }
          }
        }
      }
      meses2[i].valor = total;
    }

    total = 0;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
        total += facturacaos[f].direccaos[j].total;
      }
    }

    meses2[12].valor = total;

    var mesesCalculoHorizontal = meses2;

    return { facturacaos, mesesCalculoHorizontal };
  }

  static async relatorioFacturacaoRealizadaCobrancaGlobalPago(filter) {
    var direccao = "";
    var produto = "";
    var loja = "";
    if (filter.direccao != "T") {
      direccao += " AND c.direccao = '" + filter.direccao + "'";
    }
    if (filter.produto != "T") {
      produto +=
        " AND id IN (SELECT factura_id FROM linha_facturas WHERE artigo_id = " +
        filter.produto +
        ")";
    }
    if (filter.loja != "T") {
      loja +=
        " AND caixa_id IN (SELECT id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    let raw
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      raw = await Database.raw(
        "SELECT facturacao, direccao, SUM(total) total, mes FROM (SELECT facturacao, DATE_FORMAT(created_at, '%m') as mes, SUM(total) as total, cliente_id FROM facturas WHERE  DATE_FORMAT(created_at, '%Y') = " +
        filter.ano +
        " AND  status = 'N' " +
        loja +

        " " +
        produto +

        " GROUP BY DATE_FORMAT(created_at, '%m'), facturacao, cliente_id ORDER BY DATE_FORMAT(created_at, '%m')) ft, clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );
    } else {
      raw = await Database.raw(
        "SELECT facturacao, direccao, SUM(total) total, mes FROM (SELECT f.facturacao, brh.mes as mes, SUM(f.total) as total, f.cliente_id FROM facturas f, bill_runs br, bill_run_headers brh WHERE f.id = br.factura_utilitie_id AND br.bill_run_header_id = brh.id AND brh.ano = " +
        filter.ano + " AND  status = 'N' " + loja + " " + produto +
        "  GROUP BY   brh.mes, f.facturacao, f.cliente_id ORDER BY brh.mes) ft, clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );



    }

    var facturacaos = [];
    var meses = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 }
    ];

    var meses2 = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 },
      { nome_mes: "d", numero_mes: "13", valor: 0 }
    ];

    let d = raw[0];

    for (let i = 0; i < d.length; i++) {
      const element = d[i];
      if (facturacaos.length == 0) {
        facturacaos.push({ tipoFacturacao: element.facturacao, direccaos: [] });
      } else {
        var v = 0;
        for (let j = 0; j < facturacaos.length; j++) {
          const e = facturacaos[j];
          if (e.tipoFacturacao === element.facturacao) {
            v = 1;
          }
        }
        if (v == 0) {
          facturacaos.push({
            tipoFacturacao: element.facturacao,
            direccaos: []
          });
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < d.length; i++) {
        const element = d[i];
        if (elef.tipoFacturacao === element.facturacao) {
          if (facturacaos[f].direccaos.length == 0) {
            facturacaos[f].direccaos.push({
              direccao: element.direccao,
              saldos: [],
              total: 0
            });
          } else {
            var v = 0;
            for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
              const e = facturacaos[f].direccaos[j];
              if (e.direccao == element.direccao) {
                v = 1;
              }
            }
            if (v == 0) {
              facturacaos[f].direccaos.push({
                direccao: element.direccao,
                saldos: [],
                total: 0
              });
            }
          }
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < elef.direccaos.length; i++) {
        const element = elef.direccaos[i];
        for (let j = 0; j < d.length; j++) {
          const el = d[j];
          if (
            element.direccao === el.direccao &&
            elef.tipoFacturacao === el.facturacao
          ) {
            var mes = "";
            for (let index = 0; index < meses.length; index++) {
              const ele = meses[index];
              if (ele.numero_mes === el.mes) {
                mes = ele.nome_mes;
              }
            }
            facturacaos[f].direccaos[i].saldos.push({
              nome_mes: mes,
              numero_mes: "" + (el.mes < 10 && filter.tipoFacturacao == 'PRE-PAGO' ? "0" + el.mes : el.mes),
              valor: el.total
            });
          }
        }
      }
    }

    for (let i = 0; i < meses.length; i++) {
      const mes = meses[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          var v = 0;
          const element = facturacaos[f].direccaos[j].saldos;
          for (let k = 0; k < element.length; k++) {
            const el = element[k];

            if (el.numero_mes === mes.numero_mes) {
              v = 1;
              facturacaos[f].direccaos[j].total += el.valor;
            }
          }
          if (v == 0) {
            facturacaos[f].direccaos[j].saldos.push(meses[i]);
          }
        }
      }
    }
    //return facturacaos;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let index = 0; index < facturacaos[f].direccaos.length; index++) {
        facturacaos[f].direccaos[index].saldos.sort(function (a, b) {
          return a.numero_mes - b.numero_mes;
        });
      }
    }

    for (let i = 0; i < meses2.length; i++) {
      var total = 0;
      const element = meses2[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          for (let s = 0; s < facturacaos[f].direccaos[j].saldos.length; s++) {
            if (element.numero_mes == facturacaos[f].direccaos[j].saldos[s].numero_mes) {
              total += facturacaos[f].direccaos[j].saldos[s].valor;
            }
          }
        }
      }
      meses2[i].valor = total;
    }

    total = 0;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
        total += facturacaos[f].direccaos[j].total;
      }
    }

    meses2[12].valor = total;

    var mesesCalculoHorizontal = meses2;

    return { facturacaos, mesesCalculoHorizontal };
  }

  static async relatorioLoja(filter) {
    //var data = filter.ano + "-" + filter.mes+  "-" + (filter.dia < 10 ? "0" + filter.dia : filter.dia);
    let datas = null;

    datas = await Database.select(
      "facturas.id as factura_id",
      "facturas.numero",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.status",
      "facturas.pago",
      "facturas.valor_aberto",
      "facturas.is_iplc",
      "facturas.factura_sigla",
      "facturas.numero_origem_factura",
      "facturas.data_origem_factura",
      "facturas.data_vencimento",
      "facturas.status_date",
      "facturas.created_at",
      Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d") as data'),

      "clientes.id as cliente_id",
      "clientes.nome",
      "clientes.telefone",
      "clientes.contribuente",

      "lojas.id as loja_id",
      "lojas.nome as loja_nome",

      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla"
    )
      .from("facturas")
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
      .innerJoin("users", "users.id", "facturas.user_id")
      .innerJoin("lojas", "lojas.id", "users.loja_id")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .whereNot("documentos.sigla", "OR")
      .whereIn("facturas.pago", filter.estado == "T" ? [1, 0] : [filter.estado])
      .whereIn("facturas.user_id", filter.loja == "T" ? Database.select("id").from("users") : Database.select("id").from("users").where("loja_id", filter.loja))
      //.where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), data)
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .orderBy("lojas.nome");
    return datas;
  }

  static async facturacaoDiariaGlobal(filter) {


    let datas = null;

    var direccao = "";
    var loja = "";
    if (filter.direccao != "T") {
      direccao += " AND clientes.direccao = '" + filter.direccao + "'";
    }

    if (filter.loja != "T") {
      loja +=
        " AND facturas.caixa_id IN (SELECT id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    if (filter.tipoFacturacao == 'PRE-PAGO') {
      datas = await Database.raw(
        "SELECT facturas.id as factura_id, facturas.numero, facturas.total, facturas.totalComImposto, facturas.totalSemImposto, facturas.status, facturas.pago, facturas.valor_aberto, lojas.nome as loja_nome, filials.nome as filial_nome, facturas.is_iplc, facturas.factura_sigla, facturas.numero_origem_factura, facturas.data_origem_factura, facturas.data_vencimento, facturas.status_date, facturas.created_at, DATE_FORMAT(facturas.created_at, '%Y-%m-%d') as data, series.nome as serie, clientes.direccao, facturas.facturacao FROM facturas LEFT JOIN clientes ON clientes.id = facturas.cliente_id LEFT JOIN series ON series.id = facturas.serie_id LEFT JOIN lojas ON lojas.serie_id = series.id LEFT JOIN filials ON filials.id = lojas.filial_id WHERE facturas.status= 'N' AND facturas.facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.filial == 'T' ? " " : " AND clientes.province = " + filter.filial) + " " + loja + " AND date_format(facturas.created_at, '%Y-%m-%d') BETWEEN '" + filter.data1 + "' AND '" + filter.data2 + "' ORDER BY lojas.nome "
      );
    } else {
      datas = await Database.raw(
        "SELECT facturas.id as factura_id, facturas.numero, facturas.total, facturas.totalComImposto, facturas.totalSemImposto, facturas.status, facturas.pago, facturas.valor_aberto, lojas.nome as loja_nome, filials.nome as filial_nome, facturas.is_iplc, facturas.factura_sigla, facturas.numero_origem_factura, facturas.data_origem_factura, facturas.data_vencimento, facturas.status_date, facturas.created_at, DATE_FORMAT(facturas.created_at, '%Y-%m-%d') as data, series.nome as serie, clientes.direccao, facturas.facturacao FROM facturas LEFT JOIN clientes ON clientes.id = facturas.cliente_id LEFT JOIN series ON series.id = facturas.serie_id LEFT JOIN lojas ON lojas.serie_id = series.id LEFT JOIN filials ON filials.id = lojas.filial_id LEFT JOIN bill_runs ON bill_runs.factura_utilitie_id = facturas.id LEFT JOIN bill_run_headers ON bill_run_headers.id = bill_runs.bill_run_header_id WHERE facturas.status= 'N' AND facturas.facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.filial == 'T' ? " " : " AND clientes.province = " + filter.filial) + " " + loja + " AND date_format(facturas.created_at, '%Y-%m-%d') BETWEEN '" + filter.data1 + "' AND '" + filter.data2 + "' ORDER BY lojas.nome "
      );
    }

    // console.log(datas[0].length)

    return datas[0];

  }

  static async relatorioIVA(filter) {

    // Nova versão do relatório IVA 1.0.0
    //Implementação da separação entre Créditos e Débitos
    let Debito = null;
    let Credito = null;

    Debito = await Database.select(
      "facturas.numero",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.factura_sigla",
      "facturas.created_at",
      "facturas.conta_id",
      "documentos.sigla",
      "clientes.direccao",
      "clientes.nome",
      "facturas.valor_cativo",
      "facturas.imposto_cativo",
      Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as datas'),
    )
      .from("facturas")
      .where("facturas.is_nota_credito", '!=', 1)
      .where("facturas.status", 'N')
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("facturas.moeda_id", 1)
      .whereIn("clientes.direccao", filter.direccao == "T" || filter.direccao == null ? [1, 0] : [filter.direccao])
      // .whereIn("documentos.id", filter.documento == "T" ? Database.raw('SELECT id FROM documentos') : [filter.documento])
      //.where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), data)
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])

    Credito = await Database.select(Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as datas'),
      "facturas.numero",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.factura_sigla",
      "facturas.created_at",
      "facturas.conta_id",
      "documentos.sigla",
      "clientes.direccao",
      "clientes.nome",
      "facturas.valor_cativo",
      "facturas.imposto_cativo",
    )
      .from("facturas")
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("facturas.is_nota_credito", 1)
      .where("facturas.status", 'N')
      .where("facturas.moeda_id", 1)
      //.whereIn("facturas.pago", filter.estado == "T" ? [1, 0] : [filter.estado])
      .whereIn("clientes.direccao", filter.direccao == "T" || filter.direccao == null ? [1, 0] : [filter.direccao])
      // .whereIn("documentos.id", filter.documento == "T" ? Database.raw('SELECT id FROM documentos') : [filter.documento])
      //.where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), data)
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])


    let Dados = null;
    Dados = {
      Debito: Debito,
      Credito: Credito
    }

    //console.log(Dados.Debito)
    return Dados;
  }

  static async relatorioServicoContratados(filter) {


    let datas = await Database.raw("SELECT f.conta_id as conta,p.nome as produto, Any_value(s.chaveServico) as nome_servico, any_value(s.id) as numero_servico, any_value(s.dataEstado) as data_servico, any_value(e.nome) as nome_estado_servicos, SUM(lf.total) as mensalidade " +
      "FROM servicos s, produtos p, facturas f, linha_facturas lf, estado_servicos e WHERE f.conta_id = s.conta_id AND s.estado = e.id AND  lf.factura_id =  f.id AND lf.artigo_id = p.id AND f.cliente_id = " + filter.cliente_id + " " +

      " " + (filter.estado == "T" ? "" : " AND e.id = " + filter.estado) + " " +
      " " + (filter.conta == "T" ? '' : " AND f.conta_id =" + filter.conta) + "" +
      " " + (filter.servico == "T" ? '' : " AND s.id =" + filter.servico) + "" +
      " " + (filter.produto == "T" ? '' : " AND lf.artigo_id =" + filter.produto) + "" +
      " GROUP BY lf.artigo_id, s.id, f.conta_id");

    return datas[0];
  }

  static async relatorioMovimentosCaixa(filter) {
    var loja = ""
    var operador = ""
    if (filter.loja != "T") {
      loja += " AND l.id = '" + filter.loja + "'";
    }
    if (filter.operador != "T") {
      operador += " AND u.id = '" + filter.operador + "'";
    }

    let datas = await Database.raw("SELECT u.nome as operador,l.nome as loja ,c.valor_abertura as valor_abertura,c.is_active,DATE_FORMAT(c.data_abertura,'%d-%m-%Y') as data_abertura,c.hora_abertura,c.hora_fecho,c.valor_fecho,DATE_FORMAT(c.data_fecho,'%d-%m-%Y') as data_fecho,c.valor_vendas,c.valor_deposito,c.data_deposito,b.numero_conta,c.referencia_banco " +
      " from  caixas c LEFT join bancos b on c.banco_id=b.id LEFT join lojas l on c.loja_id=l.id  LEFT JOIN users u on c.user_id=u.id" +
      " where date_format(c.created_at, '%Y-%m-%d') BETWEEN '" + filter.data1 + "' AND '" + filter.data2 + "'" + loja + " " + operador + "ORDER BY l.nome"

    );

    return datas[0];
  }

  static async relatorioClients(request) {
    const { search, orderBy, filter } = request.all();
    let res = null;
    const unificado = "nao";



    res = await Database.select("clientes.id",
      "clientes.nome",
      "clientes.contribuente",
      "clientes.numeroExterno",
      "clientes.morada",
      "clientes.telefone",
      "clientes.unificado",
      "clientes.direccao",
      "clientes.created_at",
      "clientes.genero",
      "clientes.province",
      "clientes.email",
      "clientes.gestor_conta",
      "clientes.observacao",
      "clientes.tipo_identidade_id",
      "clientes.tipo_cliente_id",
      "clientes.direccao_id",
      "clientes.is_entidade_cativadora",
      "direccaos.designacao",
      "tipo_identidades.tipoIdentificacao",
      "tipo_clientes.tipoClienteDesc")
      .from("clientes")
      .leftJoin("direccaos", "direccaos.designacao", "clientes.direccao")
      .leftJoin("tipo_clientes", "tipo_clientes.id", "clientes.tipo_cliente_id")
      .leftJoin("	tipo_identidades", "	tipo_identidades.id", "clientes.	tipo_identidade_id")
      .whereIn("clientes.id ", Database.raw("SELECT id FROM clientes "
        + " " + (filter.email != "T" || filter.genero != "T" || filter.tipo_cliente != "T" || filter.tipo_identidade != "T" || filter.gestor != "T" || filter.contribuente != "T" || filter.direccao != "T" ? " where id IS NOT NULL" : "")
        + " " + (filter.email == "S" ? " AND email IS NULL " : (filter.email == "C" ? " AND email IS NOT NULL" : ''))
        + " " + (filter.genero == "S" ? " AND  genero IS NULL " : (filter.genero == "T" || filter.genero == null ? '' : " AND genero = '" + filter.genero + "'"))
        + " " + (filter.tipo_cliente == "S" ? " AND  clientes.tipo_cliente_id IS NULL " : (filter.tipo_cliente == "T" || filter.tipo_cliente == null ? '' : " AND clientes.tipo_cliente_id = '" + filter.tipo_cliente + "'"))
        + " " + (filter.tipo_identidade == "S" ? " AND  clientes.tipo_identidade_id IS NULL " : (filter.tipo_identidade == "T" || filter.tipo_identidade == null ? '' : " AND clientes.tipo_identidade_id = '" + filter.tipo_identidade + "'"))
        + " " + (filter.gestor == "S" ? " AND  clientes.gestor_conta IS NULL " : (filter.gestor == "T" || filter.gestor == null ? '' : " AND clientes.gestor_conta = '" + filter.gestor + "'"))
        + "  " + (filter.contribuente == "S" ? " AND contribuente IS NULL " : (filter.contribuente == "C" ? " AND  contribuente IS NOT NULL" : ''))
        //+"  "+(filter.telefone == "S" ? " AND telefone IS NULL " : " AND telefone IS NOT NULL")
        + " " + (filter.direccao == "S" ? " AND  direccao IS NULL" : (filter.direccao == "T" || filter.direccao == null ? '' : " AND direccao = '" + filter.direccao + "'"))
      ))
      .where("clientes.unificado", unificado)
      .orderBy(orderBy == null ? "clientes.created_at" : orderBy, orderBy == "created_at" ? "DESC" : "ASC")


    return res;
  }

  static async relatorioFacturacaopagamentoGlobal(filter) {

    var direccao = "";
    var produto = "";
    var loja = "";
    if (filter.direccao != "T") {
      direccao += " AND c.direccao = '" + filter.direccao + "'";
    }
    if (filter.produto != "T") {
      produto +=
        " AND id IN (SELECT factura_id FROM linha_facturas WHERE artigo_id = " +
        filter.produto +
        ")";
    }
    if (filter.loja != "T") {
      loja +=
        " AND caixa_id IN (SELECT id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    let raw
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      raw = await Database.raw(
        "SELECT facturacao, c.direccao as direccao, SUM(total) total, mes FROM (SELECT cliente.direccao AS direccao,facturas.facturacao,ANY_VALUE(facturas.cliente_id) as cliente_id,date_format(facturas.created_at, '%m') AS mes, sum(facturas.total) AS total FROM facturas LEFT JOIN series serie_id ON facturas.serie_id = serie_id.id LEFT JOIN clientes cliente ON facturas.cliente_id = cliente.id WHERE (facturas.status = 'N' AND facturas.pago = TRUE AND facturas.facturacao = 'PRE-PAGO' AND date_format(facturas.created_at, '%Y')='" + filter.ano + "')  AND facturas.facturacao ='" + filter.tipoFacturacao + "' " + loja + " " + produto +
        "  GROUP BY DATE_FORMAT(facturas.created_at, '%m'),direccao) ft , clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.province == 'T' ? " " : " AND c.province = " + filter.province) + " " +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );
    } else {
      raw = await Database.raw(
        "SELECT facturacao, c.direccao as direccao, SUM(total) total, mes FROM (SELECT cliente.direccao AS direccao,facturas.facturacao, bill_run_header_id.mes AS mes,cliente.id cliente_id, sum(facturas.total) AS total FROM facturas LEFT JOIN bill_runs bill_runs ON facturas.id = bill_runs.factura_utilitie_id LEFT JOIN bill_run_headers bill_run_header_id ON bill_runs.bill_run_header_id = bill_run_header_id.id LEFT JOIN clientes cliente ON facturas.cliente_id = cliente.id LEFT JOIN series serie_id ON facturas.serie_id = serie_id.id WHERE (facturas.facturacao = 'POS-PAGO' AND facturas.pago = TRUE AND facturas.status = 'N' AND bill_run_header_id.ano ='" + filter.ano + "' AND facturas.serie_id <> 58)  AND facturas.facturacao ='" + filter.tipoFacturacao + "' " + loja + " " + produto + "  GROUP BY cliente.direccao, bill_run_header_id.mes, cliente_id) ft , clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.province == 'T' ? " " : " AND c.province = " + filter.province) + " " +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );

    }

    var facturacaos = [];
    var meses = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 }
    ];

    var meses2 = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 },
      { nome_mes: "d", numero_mes: "13", valor: 0 }
    ];

    let d = raw[0];

    for (let i = 0; i < d.length; i++) {
      const element = d[i];
      if (facturacaos.length == 0) {
        facturacaos.push({ tipoFacturacao: element.facturacao, direccaos: [] });
      } else {
        var v = 0;
        for (let j = 0; j < facturacaos.length; j++) {
          const e = facturacaos[j];
          if (e.tipoFacturacao === element.facturacao) {
            v = 1;
          }
        }
        if (v == 0) {
          facturacaos.push({
            tipoFacturacao: element.facturacao,
            direccaos: []
          });
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < d.length; i++) {
        const element = d[i];
        if (elef.tipoFacturacao === element.facturacao) {
          if (facturacaos[f].direccaos.length == 0) {
            facturacaos[f].direccaos.push({
              direccao: element.direccao,
              saldos: [],
              total: 0
            });
          } else {
            var v = 0;
            for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
              const e = facturacaos[f].direccaos[j];
              if (e.direccao == element.direccao) {
                v = 1;
              }
            }
            if (v == 0) {
              facturacaos[f].direccaos.push({
                direccao: element.direccao,
                saldos: [],
                total: 0
              });
            }
          }
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < elef.direccaos.length; i++) {
        const element = elef.direccaos[i];
        for (let j = 0; j < d.length; j++) {
          const el = d[j];
          if (
            element.direccao === el.direccao &&
            elef.tipoFacturacao === el.facturacao
          ) {
            var mes = "";
            for (let index = 0; index < meses.length; index++) {
              const ele = meses[index];
              if (ele.numero_mes === el.mes) {
                mes = ele.nome_mes;
              }
            }
            facturacaos[f].direccaos[i].saldos.push({
              nome_mes: mes,
              numero_mes: "" + (el.mes < 10 && filter.tipoFacturacao == 'POS-PAGO' ? "0" + el.mes : el.mes),
              valor: el.total
            });
          }
        }
      }
    }

    for (let i = 0; i < meses.length; i++) {
      const mes = meses[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          var v = 0;
          const element = facturacaos[f].direccaos[j].saldos;
          for (let k = 0; k < element.length; k++) {
            const el = element[k];

            if (el.numero_mes === mes.numero_mes) {
              v = 1;
              facturacaos[f].direccaos[j].total += el.valor;
            }
          }
          if (v == 0) {
            facturacaos[f].direccaos[j].saldos.push(meses[i]);
          }
        }
      }
    }
    //return facturacaos;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let index = 0; index < facturacaos[f].direccaos.length; index++) {
        facturacaos[f].direccaos[index].saldos.sort(function (a, b) {
          return a.numero_mes - b.numero_mes;
        });
      }
    }

    for (let i = 0; i < meses2.length; i++) {
      var total = 0;
      const element = meses2[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          for (let s = 0; s < facturacaos[f].direccaos[j].saldos.length; s++) {
            if (element.numero_mes == facturacaos[f].direccaos[j].saldos[s].numero_mes) {
              total += facturacaos[f].direccaos[j].saldos[s].valor;
            }
          }
        }
      }
      meses2[i].valor = total;
    }

    total = 0;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
        total += facturacaos[f].direccaos[j].total;
      }
    }

    meses2[12].valor = total;

    var mesesCalculoHorizontal = meses2;

    return { facturacaos, mesesCalculoHorizontal };
  }

  static async relatorioFacturacaoporGestorGlobal(filter) {

    var direccao = "";
    var produto = "";
    var loja = "";
    var gestor = "";
    if (filter.gestor != "T") {
      gestor += " AND c.gestor_conta = '" + filter.gestor + "'";
    }
    if (filter.direccao != "T") {
      direccao += " AND c.direccao = '" + filter.direccao + "'";
    }
    if (filter.produto != "T") {
      produto +=
        " AND id IN (SELECT factura_id FROM linha_facturas WHERE artigo_id = " +
        filter.produto +
        ")";
    }
    if (filter.loja != "T") {
      loja +=
        " AND caixa_id IN (SELECT id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    let raw
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      raw = await Database.raw(
        "SELECT facturacao, c.gestor_conta as direccao, SUM(total) total, mes FROM (SELECT facturacao, DATE_FORMAT(created_at, '%m') as mes, SUM(total) as total, cliente_id FROM facturas WHERE DATE_FORMAT(created_at, '%Y') = " +
        filter.ano +
        " AND  status = 'N' " +
        loja +
        " " +
        produto +
        "  GROUP BY DATE_FORMAT(created_at, '%m'), facturacao, cliente_id ORDER BY DATE_FORMAT(created_at, '%m')) ft, clientes c WHERE ft.cliente_id = c.id AND c.gestor_conta IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + gestor + " " + (filter.province == 'T' ? " " : " AND c.province = " + filter.province) + " " +
        " GROUP BY c.gestor_conta, mes, facturacao ORDER BY c.gestor_conta, mes"
      );
    } else {
      raw = await Database.raw(
        "SELECT facturacao,c.gestor_conta as direccao, SUM(total) total, mes FROM (SELECT f.facturacao, brh.mes as mes, SUM(f.total) as total, f.cliente_id FROM facturas f, bill_runs br, bill_run_headers brh WHERE f.id = br.factura_utilitie_id AND br.bill_run_header_id = brh.id AND brh.ano = " +
        filter.ano + " AND  status = 'N' " + loja +
        "  GROUP BY   brh.mes, f.facturacao, f.cliente_id ORDER BY brh.mes) ft, clientes c WHERE  ft.cliente_id = c.id AND c.gestor_conta IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + gestor + " " + (filter.province == 'T' ? " " : " AND c.province = " + filter.province) + " " +
        " GROUP BY c.gestor_conta, mes, facturacao ORDER BY c.gestor_conta, mes"
      );



    }

    var facturacaos = [];
    var meses = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 }
    ];

    var meses2 = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 },
      { nome_mes: "d", numero_mes: "13", valor: 0 }
    ];

    let d = raw[0];

    for (let i = 0; i < d.length; i++) {
      const element = d[i];
      if (facturacaos.length == 0) {
        facturacaos.push({ tipoFacturacao: element.facturacao, direccaos: [] });
      } else {
        var v = 0;
        for (let j = 0; j < facturacaos.length; j++) {
          const e = facturacaos[j];
          if (e.tipoFacturacao === element.facturacao) {
            v = 1;
          }
        }
        if (v == 0) {
          facturacaos.push({
            tipoFacturacao: element.facturacao,
            direccaos: []
          });
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < d.length; i++) {
        const element = d[i];
        if (elef.tipoFacturacao === element.facturacao) {
          if (facturacaos[f].direccaos.length == 0) {
            facturacaos[f].direccaos.push({
              direccao: element.direccao,
              saldos: [],
              total: 0
            });
          } else {
            var v = 0;
            for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
              const e = facturacaos[f].direccaos[j];
              if (e.direccao == element.direccao) {
                v = 1;
              }
            }
            if (v == 0) {
              facturacaos[f].direccaos.push({
                direccao: element.direccao,
                saldos: [],
                total: 0
              });
            }
          }
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < elef.direccaos.length; i++) {
        const element = elef.direccaos[i];
        for (let j = 0; j < d.length; j++) {
          const el = d[j];
          if (
            element.direccao === el.direccao &&
            elef.tipoFacturacao === el.facturacao
          ) {
            var mes = "";
            for (let index = 0; index < meses.length; index++) {
              const ele = meses[index];
              if (ele.numero_mes === el.mes) {
                mes = ele.nome_mes;
              }
            }
            facturacaos[f].direccaos[i].saldos.push({
              nome_mes: mes,
              numero_mes: "" + (el.mes < 10 && filter.tipoFacturacao == 'POS-PAGO' ? "0" + el.mes : el.mes),
              valor: el.total
            });
          }
        }
      }
    }

    for (let i = 0; i < meses.length; i++) {
      const mes = meses[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          var v = 0;
          const element = facturacaos[f].direccaos[j].saldos;
          for (let k = 0; k < element.length; k++) {
            const el = element[k];

            if (el.numero_mes === mes.numero_mes) {
              v = 1;
              facturacaos[f].direccaos[j].total += el.valor;
            }
          }
          if (v == 0) {
            facturacaos[f].direccaos[j].saldos.push(meses[i]);
          }
        }
      }
    }
    //return facturacaos;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let index = 0; index < facturacaos[f].direccaos.length; index++) {
        facturacaos[f].direccaos[index].saldos.sort(function (a, b) {
          return a.numero_mes - b.numero_mes;
        });
      }
    }

    for (let i = 0; i < meses2.length; i++) {
      var total = 0;
      const element = meses2[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          for (let s = 0; s < facturacaos[f].direccaos[j].saldos.length; s++) {
            if (element.numero_mes == facturacaos[f].direccaos[j].saldos[s].numero_mes) {
              total += facturacaos[f].direccaos[j].saldos[s].valor;
            }
          }
        }
      }
      meses2[i].valor = total;
    }

    total = 0;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
        total += facturacaos[f].direccaos[j].total;
      }
    }

    meses2[12].valor = total;

    var mesesCalculoHorizontal = meses2;

    return { facturacaos, mesesCalculoHorizontal };
  }

  static async relatorioVendaporLojaporProdutoGlobal(filter) {
    let datas = null;

    datas = await Database.select(
      "linha_facturas.quantidade as quantidade",
      "facturas.facturacao as facturacao",
      "facturas.id",
      Database.raw('DATE_FORMAT(facturas.created_at, "%m/%d") as dia'),
      "clientes.direccao as direccao",
      "lojas.nome as loja",
      "provincias.nome as provincia",
      "produtos.nome as produto"
    )
      .sum("linha_facturas.total as total")
      .from("facturas")
      .where("facturas.status", "N")
      .innerJoin("users", "users.id", "facturas.user_id")
      .innerJoin("lojas", "users.loja_id", "lojas.id")
      .innerJoin("linha_facturas", "facturas.id", "linha_facturas.factura_id")
      .innerJoin("produtos", "linha_facturas.artigo_id", "produtos.id")
      .innerJoin("clientes", "facturas.cliente_id", "clientes.id")
      .innerJoin("provincias", "lojas.filial_id", "provincias.id")
      .whereIn("provincias.id", filter.province == 'T' ? Database.select("id").from("filials") : [filter.province])
      .whereIn("produtos.id", filter.produto == 'T' ? Database.select("id").from("produtos") : [filter.produto])
      .whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao])
      .whereIn("lojas.id", filter.loja == 'T' ? Database.select("id").from("lojas") : [filter.loja])
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .groupByRaw("id,produto, loja, dia, provincia, direccao, facturacao,quantidade")
      .orderBy("dia");

    return datas;
  }


  static async relatorioFacturacaoDiariaPorServico(filter) {
    let datas = null;


    datas = await Database.select(
      "facturas.facturacao as facturacao",
      "linha_facturas.quantidade as quantidade",
      "produtos.nome as produto",
      Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as data'),
      "lojas.nome as loja",
      "provincias.nome as provincia"
    )
      .sum("linha_facturas.total as total")
      .from("facturas")
      .where("facturas.status", "N")
      .where("produtos.tipo", "S")
      .innerJoin("users", "users.id", "facturas.user_id")
      .innerJoin("lojas", "users.loja_id", "lojas.id")
      .innerJoin("linha_facturas", "facturas.id", "linha_facturas.factura_id")
      .innerJoin("produtos", "linha_facturas.artigo_id", "produtos.id")
      .innerJoin("clientes", "facturas.cliente_id", "clientes.id")
      .innerJoin("provincias", "lojas.filial_id", "provincias.id")
      .whereIn("produtos.id", filter.servico == 'T' ? Database.select("id").from("produtos") : [filter.servico])
      .whereIn("provincias.id", filter.province == 'T' ? Database.select("id").from("filials") : [filter.province])
      .whereIn("lojas.id", filter.loja == 'T' ? Database.select("id").from("lojas") : [filter.loja])
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .groupByRaw("produto,quantidade, facturacao,loja,data,provincia")
      .orderBy("quantidade");

    return datas;
  }


  static async relatorioFacturacaoDetalhadaPosPago(filter) {
    
    let datas = null;
    datas = await Database.select(
      "facturas.facturacao as facturacao",
      //"facturas.valor_aberto",
      "clientes.id as numero_cliente",
      "clientes.nome as nome_cliente",
      "clientes.gestor_conta as gestor_conta",
      "contas.id as conta",
      "clientes.direccao as direccao",
      "servicos.chaveServico as id_servico",
      "produtos.nome as nome_servico",
      "linha_facturas.linhaTotalSemImposto as valor_SemImposto",
      "linha_facturas.total as total",
      "facturas.factura_sigla as factura",
      "moedas.codigo_iso as moeda",
      Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as data'),
      "facturas.pago as pago",
      Database.raw("IF(facturas.pago,'Pago','Não Pago')as pagos")
    )
      .from("facturas")
      .where("facturas.status", "N")
      .where("facturas.facturacao", "POS-PAGO")
      //.whereIn("facturas.serie_id",filter.mes=='T'? Database.select("id").from("series").where("nome",'like','%'+filter.ano+'%') : Database.select("id").from("series").where("nome",'like','%'+filter.ano+filter.mes+'%'))
      .where("bill_run_headers.ano", filter.ano)
      .whereIn("bill_run_headers.mes", filter.mes == 'T' ? Database.select("mes").from("bill_run_headers") : [filter.mes])
      .innerJoin("contas", "facturas.conta_id", "contas.id")
      .innerJoin("linha_facturas", "facturas.id", "linha_facturas.factura_id")
      .innerJoin("produtos", "linha_facturas.artigo_id", "produtos.id")
      .innerJoin("clientes", "facturas.cliente_id", "clientes.id")
      .innerJoin("servicos", "linha_facturas.servico_id", "servicos.id")
      .innerJoin("moedas", "facturas.moeda_id", "moedas.id")
      .innerJoin("bill_runs", "facturas.id", "bill_runs.factura_utilitie_id")
      .innerJoin("bill_run_headers", "bill_runs.bill_run_header_id", "bill_run_headers.id")
      .whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao])
      .whereIn("clientes.nome", filter.cliente == null ? Database.select("nome").from("clientes") : [filter.cliente])
      .whereIn("moedas.id", filter.moeda_id == null || filter.moeda_id=="T" ? Database.select("id").from("moedas") : [filter.moeda_id])
      .where(function () {
        if (filter.gestor == null || filter.gestor == 'T') {
          this
            .whereNotNull("clientes.gestor_conta")
            .orWhere("clientes.gestor_conta", null)
        } else {
          this
            .where("clientes.gestor_conta", [filter.gestor])
        }
      })
      //.whereIn("clientes.gestor_conta", filter.gestor=='T' ? Database.select("gestor_conta").from("clientes"): [filter.gestor])
      .whereIn("produtos.id", filter.servico_id == 'T' ? Database.select("id").from("produtos") : [filter.servico_id])
      .orderBy("clientes.nome","asc")

    return datas;
  }


  static async relatorioDiarioVendasProdutoServicos(filter) {
    let datas = null;


    datas = await Database.select(
      "facturas.facturacao as facturacao",
      "linha_facturas.quantidade as quantidade",
      "produtos.nome as produto"
    )
      .sum("linha_facturas.total as total")
      .from("facturas")
      .where("facturas.status", "N")
      .where("clientes.direccao", "DVR")
      .innerJoin("users", "users.id", "facturas.user_id")
      .innerJoin("lojas", "users.loja_id", "lojas.id")
      .innerJoin("linha_facturas", "facturas.id", "linha_facturas.factura_id")
      .innerJoin("produtos", "linha_facturas.artigo_id", "produtos.id")
      .innerJoin("clientes", "facturas.cliente_id", "clientes.id")
      .innerJoin("provincias", "lojas.filial_id", "provincias.id")
      .whereIn("provincias.id", filter.province == 'T' ? Database.select("id").from("filials") : [filter.province])
      .whereIn("lojas.id", filter.loja == 'T' ? Database.select("id").from("lojas") : [filter.loja])
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .groupByRaw("produto,quantidade, facturacao")
      .orderBy("quantidade");

    return datas;
  }


  static async relatorioFacturaEnvidasPorEmail(filter) {
    let datas = null;
   // console.log(filter)

    datas = await Database.select(Database.raw("(CASE WHEN bill_run_emails.server_response IS NULL THEN 'Não Enviado'  WHEN bill_run_emails.server_response = 'Destinatáro inválido' THEN 'Não Enviado' ELSE 'Enviado'END )as estados"),
      "facturas.facturacao as facturacao",
      "facturas.factura_sigla as numero_factura",
      "bill_run_emails.server_response as estado",
      "bill_run_emails.cliente_email as email",
      "clientes.nome as nome",
      "clientes.direccao",
      "bill_run_headers.ano",
      "bill_run_headers.mes",
      Database.raw('DATE_FORMAT(bill_run_emails.created_at, "%d-%m-%Y %H:%i") as data'),
      Database.raw('CONCAT(bill_run_headers.ano,bill_run_headers.mes) as periodo')
    )
      .from("facturas")
      .innerJoin("bill_run_emails", "facturas.id", "bill_run_emails.factura_id")
      .leftJoin("bill_run_email_headers", "bill_run_email_headers.id", "bill_run_emails.bill_run_email_header_id")
      .leftJoin("bill_run_headers", "bill_run_headers.id", "bill_run_email_headers.bill_run_header_id")
      .leftJoin("clientes", "clientes.id", "facturas.cliente_id")
      .where(function () {
        if (filter.estado == 'T') {
          this
            .whereNotNull("bill_run_emails.server_response")
            .orWhereNull("bill_run_emails.server_response")
        } else if (filter.estado == '0') {
          this
            .whereNull("bill_run_emails.server_response")
            .orWhere("bill_run_emails.server_response", 'Destinatáro inválido')
        } else if (filter.estado == '1') {
          this
            .whereNotNull("bill_run_emails.server_response")
            .andWhere("bill_run_emails.server_response", '!=', 'Destinatáro inválido')
        }
      })
      .whereIn("bill_run_headers.ano", filter.ano == null ? Database.select("ano").from("bill_run_headers") : [filter.ano])
      .whereIn("bill_run_headers.mes", filter.mes == null ? Database.select("mes").from("bill_run_headers") : [filter.mes])
      .whereIn("clientes.id", filter.cliente_id == null ? Database.select("id").from("clientes") : [filter.cliente_id])
      .whereIn("clientes.direccao", filter.direccao == "T" || filter.direccao == null ? [1, 0] : [filter.direccao])
      .where(function () {
        if (filter.data1 != null) {
          this.whereBetween(Database.raw('DATE_FORMAT(bill_run_emails.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
        } else {
          this.whereIn("bill_run_emails.created_at", Database.select("created_at").from("bill_run_emails"))
        }
      })

    //.whereIn( Database.raw('DATE_FORMAT(bill_run_emails.created_at, "%Y-%m-%d")'), filter.data==null ? Database.select(Database.raw('DATE_FORMAT(bill_run_emails.created_at, "%Y-%m-%d")')).from("bill_run_emails"): [filter.data])
    return datas;
  }

  static async relatorioRecibos(filter) {
    let datas = null;

    console.log('AQUI...')

   /* datas = await Database.select(
      "facturas.facturacao as tipo",
      "clientes.direccao as direccao",
      "forma_pagamentos.designacao as forma_pagamento",
      "recibos.recibo_sigla as recibo",
      "provincias.nome as provincia",
      "recibos.total as total",
      "facturas.factura_sigla as factura_sigla",
      Database.raw('DATE_FORMAT(recibos.created_at, "%d-%m-%Y") as data_recibo')
    )
      .from("recibos")
      .where("recibos.status", "N")
      .leftJoin("linha_recibos", "linha_recibos.recibo_id", "recibos.id")
      .leftJoin("facturas", "linha_recibos.factura_id", "facturas.id")
      .leftJoin("clientes", "recibos.cliente_id", "clientes.id")
      .leftJoin("pagamentos", "recibos.pagamento_id", "pagamentos.id",)
      .leftJoin("linha_pagamentos", "pagamentos.id", "linha_pagamentos.pagamento_id")
      .leftJoin("forma_pagamentos", "linha_pagamentos.forma_pagamento_id", "forma_pagamentos.id")
      .leftJoin("users", "recibos.user_id", "users.id")
      .leftJoin("lojas", "users.loja_id", "lojas.id")
      .leftJoin("provincias", "provincias.id", "lojas.filial_id")
      .whereIn("provincias.id", filter.province == 'T' ? Database.select("id").from("filials") : [filter.province])
      .whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao])
      .whereIn("forma_pagamentos.id", filter.forma_pagamento == 'T' ? Database.select("id").from("forma_pagamentos") : [filter.forma_pagamento])
      .whereBetween(Database.raw('DATE_FORMAT(recibos.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .groupBy("recibo")*/

      var recibos = await Recibo.query().select("recibos.*",{nome_cliente:'clientes.nome'},{direccao:'clientes.direccao'},{provincia:'provincias.nome'})
      .innerJoin("clientes", "recibos.cliente_id", "clientes.id")
      .innerJoin("users", "recibos.user_id", "users.id")
      .innerJoin("lojas", "users.loja_id", "lojas.id")
      .innerJoin("provincias", "provincias.id", "lojas.filial_id")
      .innerJoin("pagamentos", "recibos.pagamento_id", "pagamentos.id",)
      .innerJoin("linha_pagamentos", "pagamentos.id", "linha_pagamentos.pagamento_id")
      .innerJoin("forma_pagamentos", "linha_pagamentos.forma_pagamento_id", "forma_pagamentos.id")
      .whereIn("provincias.id", filter.province == 'T' ? Database.select("id").from("filials") : [filter.province])
      .whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao])
      .whereIn("forma_pagamentos.id", filter.forma_pagamento == 'T' ? Database.select("id").from("forma_pagamentos") : [filter.forma_pagamento])
      .whereBetween(Database.raw('DATE_FORMAT(recibos.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .where("recibos.status","like","N")
      /*.where(function(){
        if(filter.direccao){
          this.whereIn("cliente_id",Database.select("id").from("clientes").whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao]))
        }
      })*/
      .with("cliente")
      .with("cliente.tipoCliente").with("cliente.tipoIdentidade")
      .with('pagamento.lines.forma_pagamento')
      .with('adiantamento').with("linhasRecibos").with("linhasRecibos.factura").withCount('facturas').orderBy('id','desc').fetch() 
      


    return recibos;
  }

  static async relatorioFacturacaoDiariaPorGestorGlobal(filter) {
    let datas = null;

    datas = await Database.select(
      "facturas.facturacao as facturacao",
      "facturas.factura_sigla as documento",
      "produtos.nome as produto",
      "clientes.gestor_conta as gestor",
      "linha_facturas.quantidade",
      "provincias.nome as provincia",
      Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as data')
    )
      .sum("linha_facturas.total as total")
      .from("facturas")
      .innerJoin("users", "facturas.user_id", "users.id")
      .leftJoin("lojas", "users.loja_id", "lojas.id")
      .leftJoin("provincias", "lojas.filial_id", "provincias.id")
      .innerJoin("linha_facturas", "facturas.id", "linha_facturas.factura_id")
      .innerJoin("produtos", "linha_facturas.artigo_id", "produtos.id")
      .innerJoin("clientes", "facturas.cliente_id", "clientes.id")
      .where("facturas.facturacao", 'PRÉ-PAGO')
      .whereIn("provincias.id", filter.province == 'T' ? Database.select("id").from("filials") : [filter.province])
      .whereIn("produtos.id", filter.produto == 'T' ? Database.select("id").from("produtos") : [filter.produto])
      .whereIn("clientes.gestor_conta", filter.gestor == 'T' ? Database.select("gestor_conta").from("clientes") : [filter.gestor])
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .groupByRaw("facturacao, documento, produto, provincia, quantidade, gestor, data")
      .orderBy("gestor");

    return datas;
  }


  static async relatorioFacturacaoClienteGlobal(filter) {
    let datas = null;
    // console.log(filter)
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      datas = await Database.select(
        "clientes.direccao as direccao",
        //"facturas.factura_sigla as sigla",
        "clientes.nome as nome",
        "clientes.id as id",
        "clientes.gestor_conta as gestor",
        Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as data'),
        Database.raw('count(*) as numero_facturas')
      )
        .sum("facturas.total as total")
        .from("facturas")
        .innerJoin("clientes", "facturas.cliente_id", "clientes.id")
        .innerJoin("lojas", "facturas.serie_id", "lojas.serie_id")
        .where("facturas.status", 'N')
        .whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao])
        .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
        .groupByRaw("gestor,data,nome,direccao,id")
        .orderBy("nome");

      return datas;
    }
    else {
      datas = await Database.select(
        "clientes.direccao as direccao",
        //"facturas.factura_sigla as sigla",
        "clientes.nome as nome",
        "clientes.id as id",
        "clientes.gestor_conta as gestor",
        Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as data'),
        Database.raw('count(*) as numero_facturas')
      )
        .sum("facturas.total as total")
        .from("facturas")
        .innerJoin("clientes", "facturas.cliente_id", "clientes.id")
        .where("facturas.status", 'N')
        .whereIn("facturas.serie_id", Database.select("id").from("series").where("nome", 'like', '%' + 'MRC' + '%'))
        .whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao])
        .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
        .groupByRaw("gestor,data,nome,direccao,id,serie_id")
        .orderBy("data");

      return datas;
    }
  }


  static async relatorioPagamentosClienteGlobal(filter) {
    let datas = null;

    datas = await Database.select(
      "clientes.direccao as direccao",
      "clientes.nome as nome",
      "clientes.id as id",
      "clientes.gestor_conta as gestor",
      Database.raw('DATE_FORMAT(recibos.created_at, "%d-%m-%Y") as data'),
      Database.raw('count(*) as numero_facturas')
    )
      .sum("recibos.total as total")
      .from("recibos")
      .innerJoin("clientes", "recibos.cliente_id", "clientes.id")
      .where("recibos.status", 'N')
      .whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao])
      .whereBetween(Database.raw('DATE_FORMAT(recibos.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .groupByRaw("gestor,data,nome,direccao,id,serie_id")
      .orderBy("data");

    return datas;
  }

  static async relatorioResumoContaCorrente(filter) {
    let datas = null;
   // console.log(filter)
    datas = await Database.select(
      "clientes.id",
      "clientes.nome",
      "clientes.telefone",
      "clientes.email",
      "clientes.gestor_conta",
      "clientes.direccao",
      "provincias.nome as provincia",
      "adiantamentos.valor as valor_adiantamento"
    )
      .sum("facturas.valor_aberto as valor_divida")
      .sum("facturas.total as total")
      .from("facturas")
      .leftJoin("clientes", "facturas.cliente_id", "clientes.id")
      .leftJoin("adiantamentos", "adiantamentos.cliente_id", "clientes.id")
      .leftJoin("provincias", "clientes.province", "provincias.id")
      .leftJoin("bill_runs", "bill_runs.factura_utilitie_id", "facturas.id")
      .leftJoin("bill_run_headers", "bill_run_headers.id", "bill_runs.bill_run_header_id")
      .where("facturas.pago", false)
      .where(function () {
        if (filter.gestor == null || filter.gestor == 'T') {
          this
            .whereNotNull("clientes.gestor_conta")
            .orWhere("clientes.gestor_conta", null)
        } else {
          this
            .where("clientes.gestor_conta", [filter.gestor])
        }
      })
      .whereIn("provincias.id", filter.province == 'T' ? Database.select("id").from("filials") : [filter.province])
      .whereIn("clientes.id", filter.cliente_id == null ? Database.select("id").from("clientes") : [filter.cliente_id])
      .whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao])
      .where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data])
      .groupByRaw("clientes.id, nome,telefone, email, gestor_conta,direccao,valor_adiantamento,provincia")

    return datas;
  }

  static async relatorioFacturacaoporServicoGlobal(filter) {
    //console.log(filter);
    var direccao = "";
    var produto = "";
    var loja = "";
    if (filter.direccao != "T") {
      direccao += " AND c.direccao = '" + filter.direccao + "'";
    }
    if (filter.produto != "T") {
      produto +=
        " AND lf.artigo_id = " +
        filter.produto +
        "";
    }
    if (filter.loja != "T") {
      loja +=
        " AND caixa_id IN (SELECT id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    let raw
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      raw = await Database.raw(
        "SELECT facturacao, ft.direccao, SUM(total) total, mes FROM (SELECT f.facturacao facturacao ,p.nome as direccao,sum(f.total) as total, date_format(f.created_at,'%m') as mes ,any_value(f.cliente_id) as cliente_id from facturas f join linha_facturas lf on lf.factura_id=f.id join produtos p on lf.artigo_id=p.id WHERE p.tipo = 'S' and f.facturacao='PRE-PAGO' and date_format(f.created_at,'%Y') =  " +
        filter.ano +
        " AND  f.status = 'N' " +
        loja +
        " " +
        produto +
        "  GROUP by p.nome,mes, cliente_id ORDER BY DATE_FORMAT(f.created_at, '%m')) ft, clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.province == 'T' ? " " : " AND c.province = " + filter.province) + " " +
        " GROUP BY ft.direccao, mes, facturacao ORDER BY mes, direccao"
      );
    } else {
      raw = await Database.raw(
        "SELECT facturacao, ft.direccao, SUM(total) total, mes FROM (SELECT f.facturacao,f.cliente_id, brh.mes as mes,p.nome as direccao, sum(f.total) as total from facturas f join bill_runs br on f.id=br.factura_utilitie_id JOIN bill_run_headers brh on brh.id=br.bill_run_header_id  JOIN linha_facturas lf on f.id=lf.factura_id join produtos p on p.id=lf.artigo_id WHERE  brh.ano= " +
        filter.ano + " AND  f.status = 'N' " + loja + " " + produto +
        "  GROUP BY   brh.mes, f.facturacao ,p.nome, f.cliente_id,p.nome ORDER BY brh.mes) ft, clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.province == 'T' ? " " : " AND c.province = " + filter.province) + " " +
        " GROUP BY ft.direccao, mes, facturacao ORDER BY  mes"
      );



    }

    var facturacaos = [];
    var meses = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 }
    ];

    var meses2 = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 },
      { nome_mes: "d", numero_mes: "13", valor: 0 }
    ];

    let d = raw[0];

    for (let i = 0; i < d.length; i++) {
      const element = d[i];
      if (facturacaos.length == 0) {
        facturacaos.push({ tipoFacturacao: element.facturacao, direccaos: [] });
      } else {
        var v = 0;
        for (let j = 0; j < facturacaos.length; j++) {
          const e = facturacaos[j];
          if (e.tipoFacturacao === element.facturacao) {
            v = 1;
          }
        }
        if (v == 0) {
          facturacaos.push({
            tipoFacturacao: element.facturacao,
            direccaos: []
          });
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < d.length; i++) {
        const element = d[i];
        if (elef.tipoFacturacao === element.facturacao) {
          if (facturacaos[f].direccaos.length == 0) {
            facturacaos[f].direccaos.push({
              direccao: element.direccao,
              saldos: [],
              total: 0
            });
          } else {
            var v = 0;
            for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
              const e = facturacaos[f].direccaos[j];
              if (e.direccao == element.direccao) {
                v = 1;
              }
            }
            if (v == 0) {
              facturacaos[f].direccaos.push({
                direccao: element.direccao,
                saldos: [],
                total: 0
              });
            }
          }
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < elef.direccaos.length; i++) {
        const element = elef.direccaos[i];
        for (let j = 0; j < d.length; j++) {
          const el = d[j];
          if (
            element.direccao === el.direccao &&
            elef.tipoFacturacao === el.facturacao
          ) {
            var mes = "";
            for (let index = 0; index < meses.length; index++) {
              const ele = meses[index];
              if (ele.numero_mes === el.mes) {
                mes = ele.nome_mes;
              }
            }
            facturacaos[f].direccaos[i].saldos.push({
              nome_mes: mes,
              numero_mes: "" + (el.mes < 10 && filter.tipoFacturacao == 'POS-PAGO' ? "0" + el.mes : el.mes),
              valor: el.total
            });
          }
        }
      }
    }

    for (let i = 0; i < meses.length; i++) {
      const mes = meses[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          var v = 0;
          const element = facturacaos[f].direccaos[j].saldos;
          for (let k = 0; k < element.length; k++) {
            const el = element[k];

            if (el.numero_mes === mes.numero_mes) {
              v = 1;
              facturacaos[f].direccaos[j].total += el.valor;
            }
          }
          if (v == 0) {
            facturacaos[f].direccaos[j].saldos.push(meses[i]);
          }
        }
      }
    }
    //return facturacaos;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let index = 0; index < facturacaos[f].direccaos.length; index++) {
        facturacaos[f].direccaos[index].saldos.sort(function (a, b) {
          return a.numero_mes - b.numero_mes;
        });
      }
    }

    for (let i = 0; i < meses2.length; i++) {
      var total = 0;
      const element = meses2[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          for (let s = 0; s < facturacaos[f].direccaos[j].saldos.length; s++) {
            if (element.numero_mes == facturacaos[f].direccaos[j].saldos[s].numero_mes) {
              total += facturacaos[f].direccaos[j].saldos[s].valor;
            }
          }
        }
      }
      meses2[i].valor = total;
    }

    total = 0;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
        total += facturacaos[f].direccaos[j].total;
      }
    }

    meses2[12].valor = total;

    var mesesCalculoHorizontal = meses2;

    return { facturacaos, mesesCalculoHorizontal };
  }

  static async reportingBI() {
    let responses = null;

    responses = await Database.raw(
      "SELECT str_to_date(concat(date_format(`facturas`.`created_at`, '%Y-%m'), '-01'), '%Y-%m-%d') AS `created_at`, sum(`facturas`.`total`) AS `sum` " +
      "FROM `facturas` " +
      "LEFT JOIN `clientes` `cliente_id` ON `facturas`.`cliente_id` = `cliente_id`.`id` " +
      "LEFT JOIN `series` `serie_id` ON `facturas`.`serie_id` = `serie_id`.`id` " +
      "LEFT JOIN `lojas` `lojas` ON `serie_id`.`id` = `lojas`.`serie_id` " +
      "WHERE `facturas`.`serie_id` <> 58 " +
      "AND `facturas`.`facturacao` = 'PRE-PAGO' " +
      "AND `facturas`.`status` = 'N' " +
      "AND DATE_FORMAT(`facturas`.`created_at`, '%Y') = YEAR(CURDATE()) " +
      "GROUP BY str_to_date(concat(date_format(`facturas`.`created_at`, '%Y-%m'), '-01'), '%Y-%m-%d') " +
      "ORDER BY str_to_date(concat(date_format(`facturas`.`created_at`, '%Y-%m'), '-01'), '%Y-%m-%d') ASC"

      /*
     "SELECT DATE_FORMAT(fact.created_at, '%m-%Y') as 'created_at', SUM(fact.total) AS 'sum' "+
     "FROM facturas AS fact  "+
     "WHERE (fact.serie_id <> 58) AND DATE_FORMAT(fact.created_at, '%Y') = YEAR(CURDATE()) "+
     "GROUP BY DATE_FORMAT(fact.created_at, '%m-%Y') "+
     "ORDER BY DATE_FORMAT(fact.created_at, '%m-%Y') ASC"
     */
    )
    return responses;
  }

  static async reportingBILastMonth() {
    let responses = null;

    responses = await Database.raw(
      "SELECT date(`facturas`.`created_at`) AS `created_at`, sum(`facturas`.`total`) AS `sum` FROM `facturas` " +
      "LEFT JOIN `clientes` `cliente_id` ON `facturas`.`cliente_id` = `cliente_id`.`id` " +
      "LEFT JOIN `series` `serie_id` ON `facturas`.`serie_id` = `serie_id`.`id` " +
      "LEFT JOIN `lojas` `lojas` ON `serie_id`.`id` = `lojas`.`serie_id` " +
      "WHERE (`facturas`.`serie_id` <> 58 " +
      "AND `facturas`.`facturacao` = 'PRE-PAGO' " +
      "AND `facturas`.`status` = 'N' " +
      "AND str_to_date(concat(date_format(`facturas`.`created_at`, '%Y-%m'), '-01'), '%Y-%m-%d') = str_to_date(concat(date_format(now(), '%Y-%m'), '-01'), '%Y-%m-%d')) " +
      "GROUP BY date(`facturas`.`created_at`) " +
      "ORDER BY date(`facturas`.`created_at`) ASC"
      /*
     "SELECT DATE_FORMAT(fact.created_at, '%m-%Y') as 'created_at', SUM(fact.total) AS 'sum' "+
     "FROM facturas AS fact  "+
     "WHERE (fact.serie_id <> 58) AND DATE_FORMAT(fact.created_at, '%m-%Y') = DATE_FORMAT(CURDATE(), '%m-%Y') "+
     "GROUP BY DATE_FORMAT(fact.created_at, '%m-%Y') "+
     "ORDER BY DATE_FORMAT(fact.created_at, '%m-%Y') ASC"
     */
    )
    return responses;
  }



}
module.exports = Relatorio;

