'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Pagamento extends Model {

  users() {
    return this.hasMany('App/Models/User')
  }
   lines() {
     return this.hasMany('App/Models/LinhaPagamento')  
  } 
}

module.exports = Pagamento
