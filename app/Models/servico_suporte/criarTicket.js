
'use strict'
const Database = use('Database');
const DataResponse = use("App/Models/DataResponse");
const UnigSuportJsPdf = use("App/Models/servico_suporte/ticketJsPdf");
const Env = use('Env')
const nodemailer = require("nodemailer");
var moment = require('moment');
const fs = require("fs");
const Model = require("@adonisjs/lucid/src/Lucid/Model");

class gestaoTicket  {

    //  desativar ou activar Categoria, Prioridade, Estado
    async activar_desativarfuncao(valor, id, table, descrciao) {
        try {

            await Database.table(table).where("id", id).update({ "estado": valor })
            return DataResponse.response('success', 200, `${descrciao} Activo com sucesso`, null);
        } catch (error) {
            return DataResponse.response('success', 500, ` Houve um erro ao activar${descrciao} Contacte o Administrador`, null);
        }


    }
    async criar_categoria_prioridade_estado(designacao, table, table_descricao) {
        const created_at = moment().format("YYYY-MM-DD");
        try {
            const ticket_gestao = await Database.select('*')
                .table(table)
                .where(table_descricao, designacao)
            if (ticket_gestao.length > 0) {
                return DataResponse.response("success", 500, "Já existe  uma designação com este nome", null);
            } else {
                if (table == "categoria_suportes") {
                    await Database.table(table).insert({
                        created_at: created_at,
                        tipoCategoria: designacao
                    })

                    return DataResponse.response("success", 200, "Adicionado com sucesso", null)

                }
                await Database.table(table).insert({
                    created_at: created_at,
                    designacao: designacao
                })
                return DataResponse.response("success", 200, "Adicionado com sucesso", null)

            }

        } catch (error) {
            return DataResponse.response("success", 500, "Houve um erro contact o Administrador", null);

        }

    }
    async Actualizar_categoria_prioridade_estado(id, descricao, table) {
        const updated_at = moment().format("YYYY-MM-DD");
        try {
            if (table == "categoria_suportes") {
                await Database.table(table).where("id", id).update({ "tipoCategoria": descricao, "updated_at": updated_at })
                return DataResponse.response("success", 200, "Atualizado com sucesso", null);

            }
            await Database.table(table).where("id", id).update({ "designacao": descricao, "updated_at": updated_at })
            return DataResponse.response("success", 200, "Atualizado com sucesso", null);


        } catch (error) {
            console.log(error)
            return DataResponse.response("success", 500, "Houve um erro contacte o Administrador", null);

        }
    }


    // criar Suporte ou ticket
    async CriarSuporte(categoria_suporte_id, descricao, img_base64, user_id, estado_suporte_id, suporte_prioridade_id, ip, url, table) {
        console.log(categoria_suporte_id)
        const created_at = moment().format("YYYY-MM-DD");
        try {
            await Database.table(table).insert({
                categoria_suporte_id,
                descricao,
                img_base64,
                user_id,
                estado_suporte_id,
                suporte_prioridade_id,
                ip,
                url,
                created_at
            })
            return DataResponse.response("successo", 200, "Supporte registrado com sucesso", null);

        } catch (error) {
          console.log(error)
            return DataResponse.response("successo", 500, "Houve um erro contacte o administrador", null);

        }




    }

    // actualizar Suporte ou ticket 
    async ActualizarSuporte(categoria, prioridade, estado, descricao, id, table) {
        const updated_at = moment().format("YYYY-MM-DD");
        try {
            await Database.table(table)
                .where('id', id).update({
                    'categoria_suporte_id': categoria,
                    'descricao': descricao,
                    'estado_suporte_id': estado,
                    'suporte_prioridade_id': prioridade,
                    "updated_at": updated_at
                })
            return DataResponse.response('sucess', 200, "Suporte Actualizado com sucesso", null);

        } catch (error) {
            console.log(error)
            return DataResponse.response('sucess', 500, "Houve um erro contacte o Adminstrador", null);

        }

    }
    // Mostrar suporte Ticket Img 
    async MostrarSuporteImg(id) {

        const imgTicket = await Database.select("supportes.img_base64")
            .from("supportes").where("id", id);
        return DataResponse.response("sucesso", 200, null, imgTicket)

    }
    // listar  Suporte ou ticket 
     async ListarSuporteTicket(pagination, search, orderBy) {
        let suporte = null;
        try {
            if (search == null) {
                suporte = await Database.select(
                    'supportes.id as suporteId',
                    'supportes.descricao',
                    "supportes.created_at",
                    "supportes.url",
                    "supportes.ip",
                    "supportes.enviado_por_email",
                    "users.nome",
                    "categoria_suportes.tipoCategoria",
                    "estado_supportes.designacao as estado",
                    "suporte_prioridades.designacao"
                ).from("supportes")
                    .innerJoin('categoria_suportes', "categoria_suportes.id", "supportes.categoria_suporte_id")
                    .innerJoin('estado_supportes', "estado_supportes.id", "supportes.estado_suporte_id")
                    .leftJoin("suporte_prioridades", "suporte_prioridades.id", "supportes.suporte_prioridade_id")
                    .leftJoin("users", "users.id", "supportes.user_id")
                    .orderBy(orderBy == null ? "supportes.created_at" : orderBy, "ASC")
                    .paginate(pagination.page, pagination.perPage);
                    return DataResponse.response("success", 200, suporte);

            } else {
                suporte = await Database.select(
                    'supportes.id as suporteId',
                    'supportes.descricao',
                    "supportes.created_at",
                    "supportes.url",
                    "supportes.ip",
                    "supportes.enviado_por_email",
                    "users.nome",
                    "categoria_suportes.tipoCategoria",
                    "estado_supportes.designacao as estado",
                    "suporte_prioridades.designacao"
                ).from("supportes")
                    .innerJoin('categoria_suportes', "categoria_suportes.id", "supportes.categoria_suporte_id")
                    .innerJoin('estado_supportes', "estado_supportes.id", "supportes.estado_suporte_id")
                    .leftJoin("suporte_prioridades", "suporte_prioridades.id", "supportes.suporte_prioridade_id")
                    .leftJoin("users", "users.id", "supportes.user_id")
                    .where("categoria_suportes.tipoCategoria", "like", "%" + search + "%")
                    .orWhere("estado_supportes.designacao", "like", "%" + search + "%")
                    .orWhere("supportes.descricao", "like", "%" + search + "%")
                    .orWhere(
                        Database.raw('DATE_FORMAT(supportes.created_at, "%Y-%m-%d")'),
                        "like",
                        "%" + search + "%"
                    )
                    .orderBy(orderBy == null ? "supportes.created_at" : orderBy, "ASC")
                    .paginate(pagination.page, pagination.perPage);
                    return DataResponse.response("success", 200,  suporte);

               

            }
            
           

        } catch (error) {

        }


    }
    // listar Prioridade Categoria  Estados ,
    async listarPrioridadeEstadoCategoria(table, ativo) {

        let gestao_ticket = null;
        if (ativo == undefined || ativo == null) {
            gestao_ticket = await Database.select("*").table(table);
            return DataResponse.response("success", 200, "", gestao_ticket);

        } else {
            gestao_ticket = await Database.select('*').table(table).where("estado", true)
            return DataResponse.response("success", 200, "", gestao_ticket);
        }

    }
    // envio de ticket por email 
    async envioTicketPorEmail(ticket_id) {
        const ticket = await Database.select(
            'supportes.id',
            'supportes.descricao',
            "supportes.created_at",
            "supportes.url",
            "supportes.img_base64",
            "categoria_suportes.tipoCategoria",
            "estado_supportes.designacao as estado",
            "suporte_prioridades.designacao"
        ).from("supportes")
            .innerJoin('categoria_suportes', "categoria_suportes.id", "supportes.categoria_suporte_id")
            .innerJoin('estado_supportes', "estado_supportes.id", "supportes.estado_suporte_id")
            .leftJoin("suporte_prioridades", "suporte_prioridades.id", "supportes.suporte_prioridade_id")
            .where("supportes.id", ticket_id)
        // console.log(ticket);

        const file_reference = await UnigSuportJsPdf.PdfSuporte(ticket);
       
        let transporter = nodemailer.createTransport({
            host: Env.get('NODEMAILER_HOST'),
            port: Env.get('NODEMAILER_PORT'),
            secure: false, // true for 465, false for other ports
            auth: {
                user: Env.get('NODEMAILER_USER'), // generated ethereal user
                pass: Env.get('NODEMAILER_PASS') // generated ethereal password
            }
        });


        // send mail with defined transport object
        let info =  await transporter.sendMail({
            from: '"Angola Telecom Supporte" <' + Env.get("NODEMAILER_USER") + '>',
            to:"testportalangola@gmail.com",
            subject: 'Angola telecom supporte',
            html: 'Estimado/a  enviamos  o ticket  em anexo. Para visualizar o ficheiro precisa do Acrobat Reader versão 6 ou superior , aguardando um feedback o mais rápido possivel',
            attachments: [{
                filename: file_reference + '.pdf',
                path: `public/uploads/suporte/${file_reference}.pdf`,
                cid: file_reference
            }]
        });
        const path = `public/uploads/suporte/${file_reference}.pdf`;
        fs.unlink(path, (err) => { })

        await Database.table("supportes").where("id", ticket_id).update({ "enviado_por_email": 1 });

        return DataResponse.response("success", 200, " Ticket enviado com sucesso", "");



    }

    async envioMassaEmial() {
        const ticket = await Database.select(
            'supportes.id',
            'supportes.descricao',
            "supportes.created_at",
            "supportes.img_base64",
            "supportes.url",
            "categoria_suportes.tipoCategoria",
            "estado_supportes.designacao as estado",
            "suporte_prioridades.designacao"
        ).from("supportes")
            .innerJoin('categoria_suportes', "categoria_suportes.id", "supportes.categoria_suporte_id")
            .innerJoin('estado_supportes', "estado_supportes.id", "supportes.estado_suporte_id")
            .leftJoin("suporte_prioridades", "suporte_prioridades.id", "supportes.suporte_prioridade_id")

        const file_reference = await UnigSuportJsPdf.PdfSuporteFiles(ticket);
        let transporter = nodemailer.createTransport({
            host: Env.get('NODEMAILER_HOST'),
            port: Env.get('NODEMAILER_PORT'),
            secure: false, // true for 465, false for other ports
            auth: {
                user: Env.get('NODEMAILER_USER'), // generated ethereal user
                pass: Env.get('NODEMAILER_PASS') // generated ethereal password
            }
        });


        // send mail with defined transport object
        let info = await transporter.sendMail({
            from: '"Angola Telecom Supporte" <' + Env.get("NODEMAILER_USER") + '>',
            to: "testportalangola@gmail.com",
            subject: 'Angola telecom supporte',
            html: 'Estimado/a  enviamos  o ticket  em anexo. Para visualizar o ficheiro precisa do Acrobat Reader versão 6 ou superior , aguardando um feedback o mais rápido possivel',
            attachments: [{
                filename: file_reference +".zip",
                path:  file_reference +".zip",
                cid: file_reference
            }]
        });
     
        

        await Database.table("supportes").update({ "enviado_por_email": 1 });
   
        return DataResponse.response("success", 200, " Ticket enviado com sucesso", "");
    }


   


}

module.exports = gestaoTicket