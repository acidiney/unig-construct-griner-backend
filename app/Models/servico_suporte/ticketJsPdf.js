
const fs = require("fs");
const moment = require('moment');
var zipper = require('zip-local');
const atob = require('atob');
const pngToJpeg = require("png-to-jpeg");
const Model = require("@adonisjs/lucid/src/Lucid/Model");
global.window = { document: { createElementNS: () => { return {} } } };
global.navigator = {};
global.html2pdf = {};
global.btoa = () => { };
global.atob = atob;
const jsPDF = require('jspdf');
class UnigSuportJsPdf extends Model {

    static async PdfSuporte(dados) {
        let bufferImg = null;
        let imgConverted = null;
        let filemane=null;

        const img = fs.readFileSync('public/images/logo_at.jpg');
        var imgLogo = 'data:image/jpeg;base64,' + img.toString('base64');

        var lMargin=95;
        var rMargin=15;
        var pdfInMM = 210;

        const doc = new jsPDF();
        for (let i = 0; i < dados.length; i++) {
          console.log("test");
        
           
            doc.addImage(imgLogo, "jpeg", 10, 4, 25, 18);
    
            doc.setFontSize(40);
            doc.setFontType("bold");
            doc.setTextColor(10, 73, 134);
            doc.setFontSize(10);
            filemane=`Suporte ${'_'} ${moment(dados[i].created_at).format("YYYY-MM-DD")} ${'_'} ${dados[i].id}`
            bufferImg = new Buffer(dados[i].img_base64.split(/,\s*/)[1], 'base64')
            await pngToJpeg({ quality: 90 })(bufferImg).then(imgjpeg =>{ 
            imgConverted='data:image/jpeg;base64,' + imgjpeg.toString('base64');

             })
       
            
              var descricao = doc.splitTextToSize(dados[i].descricao, pdfInMM - lMargin - rMargin);
             doc.setFontSize(40);
                       doc.setFontType("bold");
                       doc.setTextColor(10, 73, 134);
                       doc.text("Angola Telecom Suporte", 8, 35);
                       doc.setFontSize(10);
                       doc.setTextColor(0, 0, 0);
                       doc.text(8, 45, `Ticket número: ${dados[i].id}` );
                       doc.text(90, 45,  `Data da criação: ${moment(dados[i].created_at).format("YYYY-MM-DD")}`);
                       doc.setFontType("normal");
                       doc.setFillColor(0, 18, 135);
                       doc.rect(0, 183, 240, 10, 'F')
                       doc.setTextColor(255, 255, 255);
                       doc.text("local", 5, 189);
                       doc.text("Prioridade", 68, 189);
                       doc.text("Categoria", 100, 189);
                       doc.text("descrição", 140, 189)
                       doc.setFillColor(208, 211, 212);
                       doc.setFontSize(9);
                       doc.setTextColor(10, 73, 134);
                       doc.text(dados[i].url, 0, 199);
                       doc.text(dados[i].designacao, 69, 199);
                       doc.text(dados[i].tipoCategoria, 99, 199);
                       doc.text(descricao, 140, 199);
                       doc.addImage(imgConverted,  "jpeg", 0, 50, 280, 130);

                       var buffer = new Buffer(doc.output('arraybuffer'))
                       let dir = 'public/uploads/suporte'
                       if (!(fs.existsSync(dir))) {
                           fs.mkdirSync(dir);
                           fs.appendFile(dir + '/' + filemane + '.pdf', buffer, function (err, fd) { });
               
                       } else {
                           fs.appendFile('public/uploads/suporte/' + filemane + '.pdf', buffer, function (err, fd) { })
                       }
               
                       return filemane
        }
     


    }

    
    static async PdfSuporteFiles(dados){
        let bufferImg = null;
        let imgConverted = null;
        let filemane=null;
        const created_at = moment().format("YYYY-MM-DD");
       
        for (let i = 0; i < dados.length; i++) {
         let doc = new jsPDF();
        const img = fs.readFileSync('public/images/logo_at.jpg');

        var imgLogo = 'data:image/jpeg;base64,' + img.toString('base64');
        doc.addImage(imgLogo, "jpeg", 10, 4, 25, 18);

        doc.setFontSize(40);
        doc.setFontType("bold");
        doc.setTextColor(10, 73, 134);
        doc.setFontSize(10);


       
            filemane=`Suporte ${'_'} ${moment(dados[i].created_at).format("YYYY-MM-DD")} ${'_'} ${dados[i].id}`
            bufferImg = new Buffer(dados[i].img_base64.split(/,\s*/)[1], 'base64')
            await pngToJpeg({ quality: 90 })(bufferImg).then(imgjpeg =>{ 
            imgConverted='data:image/jpeg;base64,' + imgjpeg.toString('base64');

             })
             var lMargin=95;
             var rMargin=15;
             var pdfInMM = 210;
          
            
              var descricao = doc.splitTextToSize(dados[i].descricao, pdfInMM - lMargin - rMargin);
             doc.setFontSize(40);
                       doc.setFontType("bold");
                       doc.setTextColor(10, 73, 134);
                       doc.text("Angola Telecom Suporte", 8, 35);
                       doc.setFontSize(10);
                       doc.setTextColor(0, 0, 0);
                       doc.text(8, 45, `Ticket número: ${dados[i].id}` );
                       doc.text(90, 45,  `Data da criação: ${moment(dados[i].created_at).format("YYYY-MM-DD")}`);
                       doc.setFontType("normal");
                       doc.setFillColor(0, 18, 135);
                       doc.rect(0, 183, 240, 10, 'F')
                       doc.setTextColor(255, 255, 255);
                       doc.text("local", 5, 189);
                       doc.text("Prioridade", 68, 189);
                       doc.text("Categoria", 100, 189);
                       doc.text("descrição", 140, 189)
                       doc.setFillColor(208, 211, 212);
                       doc.setFontSize(9);
                       doc.setTextColor(10, 73, 134);
                       doc.text(0, 199,'' +(dados[i].url));
                       doc.text(69, 199,'' +(dados[i].designacao));
                       doc.text(99, 199,'' +(dados[i].tipoCategoria));
                       doc.text('' +(descricao), 140, 199);
                       doc.addImage(imgConverted,  "jpeg", 0, 50, 280, 130);
       
        var buffer = new Buffer(doc.output('arraybuffer'))
        let dir = 'public/uploads/suporte'
        if (!(fs.existsSync(dir))) {
            fs.mkdirSync(dir);
            fs.appendFile(dir + '/' + filemane + '.pdf', buffer, function (err, fd) { });

        } else {
            fs.appendFile('public/uploads/suporte/' + filemane + '.pdf', buffer, function (err, fd) { })
        }


        const zipfilesPath="public/uploads/zip_suporte"
        if(!(fs.existsSync(zipfilesPath))){
            fs.mkdirSync(zipfilesPath);
            zipper.sync.zip(dir).compress().save(`${zipfilesPath} ${'_'} ${created_at}${'.zip'}`);
        }else{
            zipper.sync.zip(dir).compress().save(`${zipfilesPath} ${'_'} ${created_at}${'.zip'}`);
        }
    
  
        
    }
        

    const zipfilesPath="public/uploads/zip_suporte"
    const suporte_files= `${zipfilesPath} ${'_'} ${created_at}`
    return suporte_files;

       // return filemane


          
    }


}

module.exports = UnigSuportJsPdf