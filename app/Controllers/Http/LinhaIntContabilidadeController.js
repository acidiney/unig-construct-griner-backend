'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const LinhaIntContabilidade = use('App/Models/LinhaIntContabilidade');
const Database = use('Database');
const DataResponse = use('App/Models/DataResponse');

/**
 * Resourceful controller for interacting with linhaintcontabilidades
 */
class LinhaIntContabilidadeController {
  /**
   * Show a list of all linhaintcontabilidades.
   * GET linhaintcontabilidades
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination, filter } = request.all();
    let res = null; 
    
    res =  await Database.select("linha_int_contabilidades.id",
    "linha_int_contabilidades.contaRazaoDebito",
    "linha_int_contabilidades.contaRazaoMercadoria",
    "linha_int_contabilidades.contaRazaoImposto",
    "linha_int_contabilidades.int_contabilidade_id",
    "linha_int_contabilidades.moeda_id",
    "linha_int_contabilidades.direccao_id",
    "linha_int_contabilidades.created_at",
    "int_contabilidades.ano","int_contabilidades.diario",
    "int_contabilidades.tipoLancamento",
    "direccaos.designacao as direccao",
    "moedas.nome as designacao"
    ).from("linha_int_contabilidades")
        .leftJoin("direccaos","direccaos.id","linha_int_contabilidades.direccao_id")
        .leftJoin("moedas","moedas.id","linha_int_contabilidades.moeda_id")
        .leftJoin("	int_contabilidades","	int_contabilidades.id","linha_int_contabilidades.int_contabilidade_id")
        .whereIn("linha_int_contabilidades.id ", Database.raw("SELECT id FROM linha_int_contabilidades  "          
          +"  "+(search == null ? " " : " WHERE linha_int_contabilidades.nome  LIKE '%" + search + "%'" 
          +" OR DATE_FORMAT(linha_int_contabilidades.created_at, '%Y-%m-%d')  LIKE '%" + search + "%'"
          ))) 
        .orderBy("linha_int_contabilidades.created_at", "DESC")
        .paginate( pagination.page == null ? 1 : pagination.page, pagination.perPage);
 
    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Render a form to be used for creating a new linhaintcontabilidade.
   * GET linhaintcontabilidades/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new linhaintcontabilidade.
   * POST linhaintcontabilidades
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request,  auth  }) {
    const { contaRazaoDebito, contaRazaoMercadoria,  contaRazaoImposto, moeda_id, direccao_id, int_contabilidade_id } = request.all();

    const linhaIntContabilidade = await LinhaIntContabilidade.create({
      contaRazaoDebito: contaRazaoDebito,
      contaRazaoMercadoria: contaRazaoMercadoria,
      contaRazaoImposto: contaRazaoImposto,
      moeda_id: moeda_id,
      direccao_id:direccao_id,
      int_contabilidade_id:int_contabilidade_id,
      user_id: auth.user.id
    });    

    return DataResponse.response("success", 200, "Registo efectuado com sucesso.", linhaIntContabilidade);
  }
 
  /**
   * Display a single linhaintcontabilidade.
   * GET linhaintcontabilidades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing linhaintcontabilidade.
   * GET linhaintcontabilidades/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update linhaintcontabilidade details.
   * PUT or PATCH linhaintcontabilidades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ request,params,response}) {
   
    const dados = request.only([ "contaRazaoDebito", "contaRazaoMercadoria",  "contaRazaoImposto", "moeda_id", "direccao_id", "int_contabilidade_id"]);
    const linhaIntContabilidade = await LinhaIntContabilidade.find(params.id);
    linhaIntContabilidade.merge(dados);
    await linhaIntContabilidade.save();
    return DataResponse.response("success", 200, "Dados actualizados com sucesso",  dados);
   }

  /**
   * Delete a linhaintcontabilidade with id.
   * DELETE linhaintcontabilidades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = LinhaIntContabilidadeController
