'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const EstadoReclamacao = use("App/Models/EstadoReclamacao");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with estadoreclamacaos
 */
class EstadoReclamacaoController {
  /**
   * Show a list of all estadoreclamacaos.
   * GET estadoreclamacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "id",
        "designacao",
        "sigla",
        "created_at"
      )
        .from("estado_reclamacaos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "id",
        "designacao",
        "sigla",
        "created_at"
      )
        .from("estado_reclamacaos")
        .where("designacao", "like", "%" + search + "%")
        .orWhere("sigla", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new estadoreclamacao.
   * GET estadoreclamacaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new estadoreclamacao.
   * POST estadoreclamacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { designacao, sigla } = request.all();
    const estado = await EstadoReclamacao.query()
      .where("designacao", designacao)
      .getCount();
    if (estado > 0) {
      return DataResponse.response(
        "success",
        500,
        "Já existe um estado com a mesma designação",
        estado
      );
    } else {
      const estado = await EstadoReclamacao.create({
        designacao: designacao,
        sigla: sigla,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Estado registado com sucesso",
        null
      );
    }
  }

  /**
   * Display a single estadoreclamacao.
   * GET estadoreclamacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing estadoreclamacao.
   * GET estadoreclamacaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update estadoreclamacao details.
   * PUT or PATCH estadoreclamacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["designacao","sigla"]);

    //console.log(data)

    const estado = await EstadoReclamacao.query()
    .where("designacao", data.designacao)
    .whereNot({ id: params.id })
    .getCount();

    //console.log(params.id)
  if (estado > 0) {
    return DataResponse.response(
      "success",
      500,
      "Já existe um estado com a mesma designação",
      estado
    );
  }else{

    // update with new data entered
    const estado = await EstadoReclamacao.find(params.id);
    estado.merge(data);
    await estado.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      estado
    );

  }
  }

  /**
   * Delete a estadoreclamacao with id.
   * DELETE estadoreclamacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = EstadoReclamacaoController
