'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const TipoComunicacao = use("App/Models/TipoComunicacao");
const Database = use("Database");

/**
 * Resourceful controller for interacting with tipocomunicacaos
 */
class TipoComunicacaoController {
   
  /**
   * Show a list of all tipocomunicacao.
   * GET tipocomunicacao
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const page = request.input("page");
    const filters = {
      search: request.input("search"),
      perPage: request.input("perPage"),
    };
    const r = await TipoComunicacao.getAllTiposComunicacao(page, filters);
    return DataResponse.response("success", 200, "", r);
  }

  /**
   * Create/save a new tipocomunicacao.
   * POST tipocomunicacao
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const data = request.all();
    // create with new data entered
    const r = await TipoComunicacao.createTipoComunicacao(auth.user.id, data);
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      r
    );
  }

  /**
   * Display a single tipocomunicacao.
   * GET tipocomunicacao/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params }) {
    const r = await TipoComunicacao.findById(params.id);
    return DataResponse.response("success", 2000, null, r);
  }

  /**
   * Update tipocomunicacao details.
   * PUT or PATCH tipocomunicacao/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.all();
    const r = await TipoComunicacao.updateTipoComunicacao(params.id, data);
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      r
    );
  }
  /**
   * Delete a tipocomunicacao with id.
   * DELETE tipocomunicacao/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params }) {
    const r = await TipoComunicacao.deleteTipoComunicacao(params.id);
    return DataResponse.response("success", 200, r, null);
  }

  async status({ params, request }) {
    const data = request.only(["status"]);
    const r = await TipoComunicacao.statusTipoComunicacao(params.id, data);
    return DataResponse.response("success", 200, r, null);
  }
  /**
   * Delete a tipocomunicacao with id.
   * DELETE tipocomunicacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getAll() {
    const r = await TipoComunicacao.query().fetch();
    return DataResponse.response("success", 200, "", r);
  }
}

module.exports = TipoComunicacaoController
