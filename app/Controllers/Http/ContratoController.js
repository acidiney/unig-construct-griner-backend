'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Database = use("Database"); 
const Contrato = use("App/Models/Contrato");
const DataResponse = use("App/Models/DataResponse");
/**
 * Resourceful controller for interacting with contratoes
 */
class ContratoController {
  /**
   * Show a list of all contratoes.
   * GET contratoes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ params,request }) {

       const { parceria_id } = request.all();
       const parceria =  await Database.select('contratos.id',Database.raw('DATE_FORMAT(contratos.data_inicio, "%Y-%m-%d") as data_inicio'),Database.raw('DATE_FORMAT(contratos.data_fim, "%Y-%m-%d") as data_fim'),'contratos.status','contratos.observacao','contratos.parceria_id')
      .from('contratoes as contratos')
      .leftJoin("parceria_clientes", "parceria_clientes.id", "contratos.parceria_id")
      .where("contratos.parceria_id", parceria_id)

      return DataResponse.response("success", 200, null, parceria);
  
  }

  /**
   * Render a form to be used for creating a new contrato.
   * GET contratoes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new contrato.
   * POST contratoes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response,auth }) {

    const {
      observacao,
      parceria_id,
      data_inicio,
      data_fim,
      status
    } = request.all();

    if(status==1){
      await  Contrato.query().where("parceria_id",parceria_id).andWhere('status',1).update({status:0});
    }

    const contrato = await Contrato.create({
      observacao: observacao,
      parceria_id: parceria_id,
      data_inicio: data_inicio,
      data_fim: data_fim,
      status: status,
      user_id: auth.user.id
    });
    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      contrato
    );
  }

  /**
   * Display a single contrato.
   * GET contratoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing contrato.
   * GET contratoes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update contrato details.
   * PUT or PATCH contratoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response,auth }) {

    const data = request.only([
      "id",
      "parceria_id",
      "data_inicio",
      "data_fim",
      "status",
      "observacao"
    ]);

    console.log(data)
    if(data.status==1){
      await  Contrato.query().where("parceria_id",data.parceria_id).andWhere('status',1).update({status:0});
    }
 
  await Contrato.query().where("id",params.id).update({ 
  data_inicio: data.data_inicio,
  data_fim: data.data_fim,
  observacao: data.observacao,
  status: data.status,
  user_id: auth.user.id
});
   
    return DataResponse.response("success", 200, "Registo Actualizado com sucesso.",null);


  }

  /**
   * Delete a contrato with id.
   * DELETE contratoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ContratoController
