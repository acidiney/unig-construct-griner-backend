'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const IntContabilidade = use('App/Models/IntContabilidade');
const Database = use('Database');
const DataResponse = use('App/Models/DataResponse');
/**
 * Resourceful controller for interacting with intcontabilidades
 */
class IntContabilidadeController {
  /**
   * Show a list of all intcontabilidades.
   * GET intcontabilidades
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null; 
 
      res = await Database.select("*").from("int_contabilidades")
        .whereIn('id', Database.raw("SELECT id FROM int_contabilidades "+(search == null ? " " : " WHERE tipoLancamento  LIKE '%" + search + "%'"))) 
        .orderBy("created_at", "DESC").paginate(pagination.page == null ? 1 : pagination.page,pagination.perPage);    

    return DataResponse.response("success", 200, null, res);
  } 

  /**
   * Render a form to be used for creating a new intcontabilidade.
   * GET intcontabilidades/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new intcontabilidade.
   * POST intcontabilidades
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request,  auth  }) {
    const { tipoLancamento, ano,  diario } = request.all();

    const intContabilidade = await IntContabilidade.create({
      tipoLancamento: tipoLancamento,
      ano: ano,
      diario: diario,
      user_id: auth.user.id
    }); 
    
    return DataResponse.response("success", 200, "Registo efectuado com sucesso.", intContabilidade);
  }

  /**
   * Display a single intcontabilidade.
   * GET intcontabilidades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing intcontabilidade.
   * GET intcontabilidades/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update intcontabilidade details.
   * PUT or PATCH intcontabilidades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a intcontabilidade with id.
   * DELETE intcontabilidades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async selectBox () {
    const res = await Database.select('*').from('int_contabilidades').orderBy('created_at','DESC')
    return DataResponse.response("success", 200, "", res);
  }
}

module.exports = IntContabilidadeController
