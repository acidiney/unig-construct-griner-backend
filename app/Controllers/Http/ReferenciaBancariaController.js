'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
 
const Referencia = use("App/Models/LinhaPagamento");
const Database = use("Database");
const DataResponse = use("App/Models/DataResponse");

/**
 * Resourceful controller for interacting with referenciabancarias
 */
class ReferenciaBancariaController {
  /**
   * Show a list of all referenciabancarias.
   * GET referenciabancarias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {}

  /**
   * Render a form to be used for creating a new referenciabancaria.
   * GET referenciabancarias/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new referenciabancaria.
   * POST referenciabancarias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {}

  /**
   * Display a single referenciabancaria.
   * GET referenciabancarias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing referenciabancaria.
   * GET referenciabancarias/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update referenciabancaria details.
   * PUT or PATCH referenciabancarias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a referenciabancaria with id.
   * DELETE referenciabancarias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}


  /**
   * @author caniggiamoreira@gmail.com
   * @description 'validação das referencias bancarias'
   * 
   * validation a referenciabancaria.
   * POST referenciabancarias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async validarReferenciaBancaria2({ request }) {
    const {referencia, banco_id} = request.all()       
    const r = await Database.from('referencia_bancarias').where('referencia', referencia).where('banco_id', banco_id).where('estado', true) 
    if(r.length > 0){
      return DataResponse.response("success", 500,"Referencia Bancaria já usada", 0);
    }
    return DataResponse.response("success", 200,null,1);
}

async validarReferenciaBancaria({ request }) {
  const {referencia, banco_id} = request.all()   
  const r = await Database.from('linha_pagamentos').whereNotNull('referencia')
  .where('referencia', referencia).whereIn('pagamento_id', Database.raw("SELECT pagamento_id FROM linha_pagamentos WHERE referencia IS NOT NULL AND pagamento_id IN "+
  "(SELECT pagamento_id FROM recibos WHERE status = 'N' UNION SELECT pagamento_id FROM facturas WHERE status = 'N' AND pagamento_id IS NOT NULL)")) 
  
  if(r.length > 0){
    return DataResponse.response("success", 500,"Referencia Bancaria já usada", 0);
  }
  return DataResponse.response("success", 200,null,1);
}

 
  
  
}

module.exports = ReferenciaBancariaController
