'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Conta = use("App/Models/Conta");
const LogEstadoServico = use("App/Models/LogEstadoServico");
const DataResponse = use("App/Models/DataResponse");

const FacturaUtilitie = use("App/Models/FacturaUtilitie");
const FacturaMesAutomatica = use("App/Models/FacturaMesAutomatica");
const Produto = use("App/Models/Produto");
const BillRunHeader = use("App/Models/BillRunHeader");
const Charge = use("App/Models/Charge");
const DetalheInterconexao = use("App/Models/DetalheInterconexao");
const flatRateServico = use("App/Models/DetalheInterconexao");




const Database = use("Database"); 
var numeral = require("numeral");
var moment = require("moment"); 
const { broadcast } = require('../../utils/socket.utils');
/**
 * Resourceful controller for interacting with facturautilities
 */
class FacturaUtilitieController {
  /**
   * Show a list of all facturautilities.
   * GET facturautilities
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {}

  /**
   * Create/save a new facturautilitie.
   * POST facturautilities
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const {  tecnologia_id  } = request.all();

    let tecn = null;
    var retorno;
    tecn = await Database.select("*").from("tecnologias").where("id", tecnologia_id).first();
    

    if(tecn.nome == "VOZ" || tecn.nome == 'Voz' || tecn.nome == "Circuito Empresarial"){
      retorno = await this.storeProgressFacturaVoice({request, auth});  
    }else if (tecn.nome == "IPLC") {
      retorno = await this.storeProgressFacturaIPLC({ request, auth }); 
    }else if (tecn.nome == "INTERCONEXAO" || tecn.nome == "INTERCONEXÃO" || tecn.nome == "Inter-Conexão" || tecn.nome == "INTER CONEXÃO") {
      retorno = await this.storeFacturacaoInteConexao({ request, auth }); 
    }else{
      return DataResponse.response( "success", 500, "Nenhuma Factura gerada, tecnologia invalida.",  null);
    }  
    return DataResponse.response( "success", 200, retorno,  null);
  }

  async storeProgressFacturaIPLC({request, auth}) {
    const { contas, mes, ano, tipoFacturacao, serie_id, tecnologia_id } = request.all();
     

    let brh = null;

    brh = await Database.select("*").from("bill_run_headers").where("mes", mes).where("ano", ano).where("serie_id", serie_id).where('tecnologia_id', tecnologia_id).first();

    if (brh == null) {
      brh = await BillRunHeader.create({
        mes: mes,
        ano: ano, 
        tecnologia_id:tecnologia_id,
        serie_id: serie_id,
        user_id: auth.user.id
      });
    }else{
      if (brh.estado == 0) {
        return "Facturação em processo..."
      }
      //return "Facturação já efectuada"
    }

    let fma = null;
    fma = await Database.select("*")
      .from("factura_mes_automaticas")
      .where("mes", mes)
      .where("ano", ano)
      .where("tecnologia_id", tecnologia_id)
      .first();

    if (fma == null) {
      fma = await FacturaMesAutomatica.create({
        mes: mes,
        ano: ano,
        tecnologia_id: tecnologia_id,
        serie_id: serie_id,
        user_id: auth.user.id
      });
    }

    for (let i = 0; i < contas.length; i++) {
      const conta = contas[i];
      var factura = {
        conta: conta,
        servicos: null,
        serie: serie_id,
        totalComImposto: 0.0,
        totalSemImposto: 0.0,
        total: 0.0,
        moeda: conta.moeda_id
      };

      let servicos = [];
      let servicosConta = null;

      servicosConta = await Database.select(
        "servicos.id", 
        "servicos.conta_id",
        "flat_rate_servicos.valor as preco",
        "flat_rate_servicos.moeda_id",
        "impostos.valor as valor_imposto",
        "impostos.id as imposto_id",
        "flat_rate_servicos.artigo_id"
      )
        .from("servicos")
        .innerJoin("flat_rate_servicos", "flat_rate_servicos.servico_id", "servicos.id")
        .leftJoin("impostos", "impostos.id", "flat_rate_servicos.imposto_id")
        .leftJoin("moedas", "moedas.id", "flat_rate_servicos.moeda_id")
        .where("servicos.tecnologia_id", tecnologia_id)
        .where("servicos.estado", 1)
        .where("servicos.conta_id", conta.id); 

      if (servicosConta.length > 0) {
        for (let j = 0; j < servicosConta.length; j++) {
          const element = servicosConta[j];
          var valor = element.preco;
          var cambio = null;
          cambio = await Database.select(
            "moedas.codigo_iso",
            "cambios.valor_cambio",
            "cambios.id"
          )
            .from("cambios")
            .innerJoin("moedas", "moedas.id", "cambios.moeda_id")
            .where("moedas.id", element.moeda_id)
            .where("cambios.is_active", true)
            .first();

          if (cambio != null) {
            if (cambio.codigo_iso != "AOA") {
              valor = element.preco * cambio.valor_cambio; 
            }
          }

          let retorno = {
            charge_id: null,
            servico_id: element.id,
            artigo_id: element.artigo_id,
            nome: element.descricao,
            valor: 0.0,
            quantidade: 1,
            desconto: 0.0,
            total: 0.0,
            valorImposto: 0.0,
            imposto_id: element.imposto_id,
            moeda_id: element.moeda_id,
            linhaTotalSemImposto: 0.0,
            cliente_id: element.cliente_id,
            conta_id: element.conta_id,
            observacao: null
          };

          retorno.valorImposto = parseFloat(
            parseFloat(
              retorno.quantidade * (valor * (element.valor_imposto / 100))
            ).toFixed(2)
          );
          retorno.linhaTotalSemImposto = parseFloat(
            parseFloat(valor * retorno.quantidade).toFixed(2)
          );
          retorno.total = retorno.linhaTotalSemImposto + retorno.valorImposto;

          retorno.valor = valor;
          retorno.imposto_id = element.imposto_id;

          factura.total += retorno.total;
          factura.totalSemImposto += retorno.linhaTotalSemImposto;
          factura.totalComImposto += retorno.valorImposto;
          servicos.push(retorno);
        }

        factura.servicos = servicos;

        if (servicos.length !== 0) {
          await FacturaUtilitie.facturar(factura, fma.id,brh.id, auth, 'IPLC');
        }
      }
      
    }
    await BillRunHeader.query().where("id",brh.id).update({ estado: 1});
    return "Facturas geras com sucesso";
  }

  async storeProgressFacturaVoice({ request, auth }) {
    const { mes, ano, serie_id,tipoFacturacao, tecnologia_id } = request.all();
    var periodo = "" + ano + "" + (mes < 10 ? "0" + mes : mes);

    let contas = null;
    var contador_contas = 0;
    var contador_servicos = 0; 
       

    let brh = null;
    brh = await Database.select("*").from("bill_run_headers").where("mes", mes).where("ano", ano).where('tecnologia_id', tecnologia_id).where('serie_id', serie_id ).first();

    contas =  await Database.select("contas.cliente_id","contas.id","contas.estado",'charges.periodo').from("charges")
      .innerJoin("contas", "contas.id", "charges.conta_id") 
      .where("contas.tipoFacturacao", tipoFacturacao)
      .where('charges.is_facturado', 0).where('contas.estado', 1)
      .where('charges.periodo', periodo)
      .groupBy('charges.conta_id')
 
    if (brh == null && contas!=null) {
      brh = await BillRunHeader.create({
        mes: mes,
        ano: ano, 
        tecnologia_id:tecnologia_id,
        serie_id: serie_id,
        user_id: auth.user.id
      });
    }else{
      
      if (brh.estado == 0 && contas != null) {
        return "Facturação em processo...";
      }
      await BillRunHeader.query().where("id",brh.id).update({ estado: 0});
      //return "Facturação já efectuada"
    }

    let fma = null;
    fma = await Database.select("*").from("factura_mes_automaticas").where("mes", mes).where("ano", ano).where("tecnologia_id", tecnologia_id).first();

    if (fma == null) {
      fma = await FacturaMesAutomatica.create({
        mes: mes,
        ano: ano,
        tecnologia_id: tecnologia_id,
        serie_id: serie_id,
        user_id: auth.user.id
      });
    }
      

    for (let i = 0; i < contas.length; i++) {
      const conta = contas[i];
      var factura = {
        conta: conta,
        servicos: null,
        serie: serie_id,
        totalComImposto: 0.0,
        totalSemImposto: 0.0,
        total: 0.0,
        moeda: 1
      };

      let servicos = [];
      let servicosConta = null; 

       servicosConta =  await Database.select("charges.id as charge_id", "charges.valor",  "charges.invoiceText", "servicos.conta_id", "servicos.id").from("charges")
      .innerJoin("contas", "contas.id", "charges.conta_id")
      .innerJoin("servicos", "servicos.id", "charges.servico_id") 
         .where("contas.tipoFacturacao", tipoFacturacao).where('charges.is_facturado', 0)
         .where('contas.estado', 1).where('servicos.estado', 1)
      .where('charges.periodo',periodo)
      .where('servicos.conta_id',conta.id)
      .where("charges.conta_id", conta.id)

      if (servicosConta.length > 0) {
        for (let j = 0; j < servicosConta.length; j++) {
          const element = servicosConta[j];
          var valor = element.valor;
         // console.log("id charge" + element.charge_id, element.valor);

          let produto = null;
          produto = await Database.select("*").from("produtos").where("nome", element.invoiceText).first();

          if (produto == null) {
            produto = await Produto.create({
              nome: element.invoiceText,
              valor: 0,
              tipo: "S",
              user_id: auth.user.id,
              imposto_id: 1,
              moeda_id: 1,
              is_trigger: 0
            });
          }

          var valor_imposto = 14;

          var cambio = null;
          cambio = await Database.select(
            "moedas.codigo_iso",
            "cambios.valor_cambio",
            "cambios.id"
          )
            .from("cambios")
            .innerJoin("moedas", "moedas.id", "cambios.moeda_id")
            .where("moedas.id", produto.moeda_id)
            .where("cambios.is_active", true)
            .first();

          if (cambio != null) {
            if (cambio.codigo_iso != "AOA") {
              valor = element.valor * cambio.valor_cambio;
            }
          }

          let retorno = {
            charge_id: element.charge_id,
            servico_id: element.id, 
            artigo_id: produto.id,
            valor: 0.0,
            quantidade: 1,
            desconto: 0.0,
            total: 0,
            valorImposto: 0.0,
            imposto_id: produto.imposto_id,
            moeda_id: produto.moeda_id,
            linhaTotalSemImposto: 0.0,
            cliente_id: conta.cliente_id,
            conta_id: element.conta_id
          };
          factura.moeda = produto.moeda_id;

          retorno.valorImposto = parseFloat(
            parseFloat(
              retorno.quantidade * (valor * (valor_imposto / 100))
            ).toFixed(2)
          );
          retorno.linhaTotalSemImposto = parseFloat(
            parseFloat(valor * retorno.quantidade).toFixed(2)
          );
          retorno.total = retorno.linhaTotalSemImposto + retorno.valorImposto;

          retorno.valor = valor;

          factura.total += retorno.total;
          factura.totalSemImposto += retorno.linhaTotalSemImposto;
          factura.totalComImposto += retorno.valorImposto;

          servicos.push(retorno);
          contador_servicos++;
        }

        factura.servicos = servicos;
  
        if (servicos.length !== 0 && factura.total != 0) {
          contador_contas++;
          await FacturaUtilitie.facturar(factura, fma.id, brh.id, auth, "");
        } else {
          for (let index = 0; index < servicos.length; index++) {
            const element = servicos[index]; 
              await Charge.query().where("id",element.charge_id).update({ is_facturado: 2, observacao: 'Anulação Automatica: A charge anulada porque o valor de facturação é igual a zero'});
          }
        }
        
      }
      
    }

    await BillRunHeader.query().where("id",brh.id).update({ estado: 1});
    return "Facturas geras com sucesso, foram facturadas: "+contador_contas+" contas e "+contador_servicos+" serviços";
  }



  async storeFacturacaoInteConexao({ request, auth }) {
    const { mes, ano, serie_id,tipoFacturacao, tecnologia_id } = request.all();
    var periodo = "" + ano + "" + (mes < 10 ? "0" + mes : mes);
    var user = auth.user.id
    var contador_facturas = 0;

    let brh = null;
    brh = await Database.select("*").from("bill_run_headers").where("mes", mes).where("ano", ano).where("serie_id", serie_id).where('tecnologia_id', tecnologia_id).first();

    if (brh == null) {
      brh = await BillRunHeader.create({
        mes: mes,
        ano: ano, 
        tecnologia_id:tecnologia_id,
        serie_id: serie_id,
        user_id: user
      });
    }else{
      if (brh.estado == 0) {
        //return "Facturação em processo..."
      } 
    }
 
 
    const parceiros = await  Database.select("*").from("parceria_clientes") 
    .innerJoin("servicos", "servicos.id", "parceria_clientes.servico_id") 
    .where('servicos.tecnologia_id', tecnologia_id)
    .whereIn("parceria_clientes.codigo_interconexao", Database.distinct("billing_operator").from("charge_interconexaos").where('is_facturado',0).where('periodo',periodo).where('event_direction','I'))
    .where('parceria_clientes.estado', true); 
    
    let facturas = [] 
    for (let p = 0; p < parceiros.length; p++) {
      
      const parceiro = parceiros[p]; 
      var factura = {
        conta: {cliente_id: parceiro.cliente_id, id: parceiro.conta_id},        
        serie: serie_id,
        totalComImposto: 0.0,
        totalSemImposto: 0.0,
        total: 0.0,
        moeda: parceiro.moeda_id,
        minutos_interconexao: 0,
        servicos: [],
      }; 
       
      const cict  = await Database.select("*").from("charge_interconexaos").where('is_facturado',0).where('periodo',periodo).where('billing_operator',parceiro.codigo_interconexao);
       
      for (let index = 0; index < cict.length; index++) {
          const element = cict[index];
          
          let produto = null;
          produto = await Database.select("*").from("produtos").where("nome", element.invoiceText).first();
          
          if (produto == null) {
            produto = await Produto.create({
                nome: element.invoiceText,
                valor: 0,
                tipo: "S",
                user_id: user,
                imposto_id: 1,
                moeda_id: 1,
                is_trigger: 0
            }); 
          }

         const imposto = await Database.select("*").from("impostos").where("id", parceiro.imposto_id).first();

          produto = await Database.select(
            "produtos.id", "produtos.tipo", "produtos.nome",
            "produtos.valor", "produtos.imposto_id",
            "produtos.moeda_id", "produtos.is_trigger", "impostos.valor as valor_imposto").from("produtos")
            .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
            .where('produtos.id',produto.id).first();  

          
          var valor = element.amount; 

          let retorno = {
            codigo_interconexao: parceiro.codigo_interconexao,
            charge_id: element.id,
            servico_id: parceiro.servico_id,
            artigo_id: produto.id,
            valor: 0.0,
            quantidade: 1,
            desconto: 0.0,
            total: 0,
            valorImposto: 0.0,
            imposto_id: imposto.id,
            moeda_id: parceiro.moeda_id,
            linhaTotalSemImposto: 0.0,
            cliente_id: parceiro.cliente_id,
            conta_id: parceiro.conta_id
          }; 

          


          retorno.valorImposto = parseFloat(parseFloat(retorno.quantidade * (valor * (imposto.valor / 100))).toFixed(2));
          retorno.linhaTotalSemImposto = parseFloat(parseFloat(valor * retorno.quantidade).toFixed(2));
          retorno.total = retorno.linhaTotalSemImposto + retorno.valorImposto;

          retorno.valor = valor;

          factura.total += retorno.total;
          factura.totalSemImposto += retorno.linhaTotalSemImposto;
          factura.totalComImposto += retorno.valorImposto;
          factura.minutos_interconexao += element.mins;

          factura.servicos.push(retorno);
      }   
      
      if (factura.servicos.length !== 0 && factura.total != 0) {        
        const fact = await FacturaUtilitie.facturarICT(factura,null, brh.id, user);
        await DetalheInterconexao.query().where("periodo",periodo).where('billing_operator',parceiro.codigo_interconexao).update({ factura_id: fact});
        contador_facturas++;
     } else {
          for (let index = 0; index < servicos.length; index++) {
            const element = servicos[index]; 
              await Charge.query().where("id",element.charge_id).update({ is_facturado: 2, observacao: 'Anulação Automatica: A charge anulada porque o valor de facturação é igual a zero'});
          }
      }
  }

    await BillRunHeader.query().where("id",brh.id).update({ estado: 1});
    return "Facturas geras com sucesso, foram facturadas: "+contador_facturas;
 
  }
 
  async processarContas({ request, auth }) {
    const { mes, ano, tipoFacturacao, tecnologia_id } = request.all();
    let contas = null;
    contas = await Database.select(
      "clientes.nome as nome_cliente",
      "contas.id",
      "contas.contaDescricao",
      "contas.tipoFacturacao",
      "contas.estado",
      "lojas.nome",
      "moedas.nome as moeda",
      "contas.moeda_id",
      "moedas.codigo_iso",
      "contas.cliente_id"
    )
      .from("contas")
      .leftJoin("lojas", "lojas.id", "contas.agencia_id")
      .leftJoin("clientes", "clientes.id", "contas.cliente_id")
      .leftJoin("moedas", "moedas.id", "contas.moeda_id")
      .whereIn("contas.id",  Database.raw( "SELECT id FROM contas WHERE id in ( SELECT conta_id FROM servicos s, contas c WHERE s.estado = 1 AND s.tecnologia_id=" +  tecnologia_id +  " AND s.conta_id = c.id AND c.tipoFacturacao = 'POS-PAGO'  GROUP BY conta_id HAVING COUNT(conta_id) > 0)"))
      .whereNotIn("contas.id", Database.select("conta_id").from("bill_runs").innerJoin("factura_mes_automaticas", "factura_mes_automaticas.id", "bill_runs.facturaMesAuto_id" ).where("factura_mes_automaticas.mes", mes).where("factura_mes_automaticas.ano", ano).where("factura_mes_automaticas.tecnologia_id", tecnologia_id))
      .where("contas.tipoFacturacao", tipoFacturacao)
      .where("contas.estado", true).limit(5);

    if (contas == null || contas.length == 0) {
      return DataResponse.response(
        "success",
        500,
        "Nenhum resultado encontrado!",
        contas
      );
    }
    return DataResponse.response("success", 200, " sucesso", contas);
  }


  async preFacturacao({ request, auth }){
    const { mes, ano, tecnologia_id } = request.all();
    var periodo = "" + ano + "" + (mes < 10 ? "0" + mes : mes);
 
    var moment = require("moment");

    let servicos = await Database.select(
    "servicos.id", "servicos.conta_id",
    "flat_rate_servicos.servico_id",
    "flat_rate_servicos.valor",  
    Database.raw('DATE_FORMAT(servicos.created_at, "%Y-%m-%d") as data'),
    Database.raw('DATE_FORMAT(servicos.created_at, "%Y-%m") as mesAno'),
    Database.raw('DATE_FORMAT(servicos.created_at, "%d") as dia'),
    "produtos.nome as invoiceText",
    "servicos.created_at",
    "servicos.estado",
    "servicos.chaveServico"
  )
    .from("flat_rate_servicos")
    .innerJoin("servicos","servicos.id", "flat_rate_servicos.servico_id")
    .innerJoin("contas", "contas.id", "servicos.conta_id") 
    .innerJoin("produtos","produtos.id", "flat_rate_servicos.artigo_id")
    .where("contas.tipoFacturacao",'POS-PAGO')   
    .where("contas.estado", 1)
    .whereNotIn("servicos.id",  Database.select('servico_id').from("charges").where('periodo', periodo));
  
    var Current = new Date(ano+"-"+mes + "-01");     
    var ultDia = new Date(Current.getFullYear(), Current.getMonth() + 1, 0); 
    var ultimoDia = moment(ultDia).format("DD");  
    var count_servico = 0;
  
    for (let index = 0; index < servicos.length; index++) {
      var i = index+1;
      const percent = ((i*100)/servicos.length);

      const element = servicos[index];   
      const perido_prefact = "" + ano + "-" +(mes < 10 ? "0" + mes : mes);


      let log_estado = await LogEstadoServico.query().select(Database.raw('DATE_FORMAT(log_estado_servicos.created_at, "%Y-%m") as mesAno'),
      "log_estado_servicos.created_at","log_estado_servicos.id_estado_novo",
      Database.raw('DATE_FORMAT(log_estado_servicos.created_at, "%d") as dia') ).where("servico_id",element.servico_id).orderBy("id","desc").first();
      
     
      var dailyestadoServico = 0;
      if(log_estado != null){
      if(perido_prefact == log_estado.mesAno){
        dailyestadoServico = log_estado.id_estado_novo ==1 ? (ultimoDia - log_estado.dia+1):log_estado.dia ; 
      }

      element.estado = log_estado.mesAno > perido_prefact && element.estado != 1?1: element.estado 
      }
      
     // 1250 ____ 30
     // x ________26
     // X=(1250x26)/30

      var daily = (perido_prefact == element.mesAno ? (ultimoDia - element.dia+1) : ultimoDia)

      daily = (dailyestadoServico > 0? dailyestadoServico:daily)
     
      var valor = (element.valor*daily)/ultimoDia; 
     
      var dataservico = element.created_at == null? element.created_at:moment(element.created_at).format("YYYY-MM");
/*
      console.log("chaveservico "+element.chaveServico)
      console.log("estado "+element.estado)
      console.log("dailyestadoServico "+dailyestadoServico)
      console.log("("+perido_prefact+ " >= "+ dataservico+" || " +dataservico +"== null ) && " + element.estado +" == 1")
      console.log("data log_estado"+log_estado.mesAno)*/
     // console.log("chaveservico "+element.chaveServico)
     // console.log("data log_estado"+log_estado.mesAno == null?'nulo':log_estado.mesAno)
     // console.log("("+perido_prefact+ " >= "+ dataservico+" || " +dataservico +"== null ) && " + element.estado +" == 1")

     
      if(dailyestadoServico > 0){
        count_servico++;
        
       await Charge.create({  
        invoiceText: element.invoiceText,
        valor: valor,
        servico_id: element.servico_id,
        conta_id: element.conta_id,
        periodo: periodo,
        user_id: auth.user.id,
        daily: daily,
        valorOriginal:  element.valor
      })}
      else if ((perido_prefact >= dataservico || dataservico == null) && element.estado==1){
       count_servico++;
      // console.log(daily);
      // console.log(ultimoDia);
      
       await Charge.create({  
        invoiceText: element.invoiceText,
        valor: valor,
        servico_id: element.servico_id,
        conta_id: element.conta_id,
        periodo: periodo,
        user_id: auth.user.id,
        daily: daily,
        valorOriginal:  element.valor
      })  
     }

     
     broadcast(percent)
    }
 
    return DataResponse.response("success", (count_servico> 0 ? 200 : 201 ), (count_servico > 0 ? "Pré-facturação: ("+count_servico+") serviço facturados com sucesso...": "Nenhum registo encontrado, verifica se a pré factura do perido selecione já foi processado..."), null);

  }
}

module.exports = FacturaUtilitieController
