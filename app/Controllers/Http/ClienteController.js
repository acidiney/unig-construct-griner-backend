'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const {
  validate
} = use('Validator')

const Cliente = use("App/Models/Cliente");
const TipoIdentidade = use("App/Models/TipoIdentidade");
const Direccao = use("App/Models/Direccao");
const Gestor = use("App/Models/User");
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');
const Mensagem = use('App/Models/ResponseMessage')

const dataClientAgt = null
/**
 * Resourceful controller for interacting with clientes
 */
class ClienteController {
  /**
   * Show a list of all clientes.
   * GET clientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination, filter, filtro } = request.all();
    let res = null;
    const unificado = "nao";  
 
    res =  await Database.select("clientes.id",
        "clientes.nome","clientes.contribuente","clientes.numeroExterno","clientes.morada","clientes.telefone","clientes.unificado",
        "clientes.direccao", "clientes.created_at", "clientes.genero", "clientes.province", "clientes.email", "clientes.gestor_conta",
        "clientes.observacao", "clientes.tipo_identidade_id", "clientes.tipo_cliente_id","clientes.direccao_id", "clientes.is_entidade_cativadora",
        "direccaos.designacao", "tipo_identidades.tipoIdentificacao", "tipo_clientes.tipoClienteDesc","users.nome as gestor","clientes.gestor_id")
        .from("clientes").leftJoin("direccaos","direccaos.designacao","clientes.direccao")
        .leftJoin("tipo_clientes","tipo_clientes.id","clientes.tipo_cliente_id")
        .leftJoin("tipo_identidades","	tipo_identidades.id","clientes.	tipo_identidade_id")
        .leftJoin("users","users.id","clientes.gestor_id")
        .whereIn("clientes.id ", Database.raw("SELECT id FROM clientes  "         
          //+" "+(filter.email == "S" ? " email IS NULL " : " email IS NOT NULL")
         // +" "+(filter.genero == "S" ? " AND  genero IS NULL " : (filter.genero == "T" || filter.genero == null? '' : " AND genero = '"+filter.genero+"'"))
         // +"  "+(filter.contribuente == "S" ? " AND contribuente IS NULL " : " AND contribuente IS NOT NULL ")
          //+"  "+(filter.telefone == "S" ? " AND telefone IS NULL " : " AND telefone IS NOT NULL")
          //+" "+(filter.direccao == "S" ? " direccao IS NULL" :  (filter.direccao == "T" || filter.direccao == null? '' : " direccao = '"+filter.direccao+"'"))
           ))
        .where("clientes.unificado", unificado) 
        .orderBy(orderBy == null ? "clientes.created_at" : orderBy, orderBy == "created_at" ? "DESC" : "DESC")
        .paginate(  1, 5);
 
    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Show a list of all clientes.
   * GET clientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async searchCliente({ request }) {
    const { start, end, search } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "nome",
        "telefone",
        "morada",
        "contribuente",
        "email"
      )
        .from("clientes")
        .paginate(start, end);
    } else {
      res = await Database.select(
        "id",
        "nome",
        "telefone",
        "morada",
        "contribuente",
        "email"
      )
        .from("clientes")
        .where("nome", "like", "%" + search + "%")
        .orWhere("id", "like", "%" + search + "%")
        .orWhere("contribuente", "like", "%" + search + "%")
        .orWhere("telefone", "like", "%" + search + "%")
        .orWhere("email", "like", "%" + search + "%")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, null, res);
  }

  async searchClienteInFacturas({ request }) {
    const { start, end, search } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select("id", "nome", "telefone", "morada", "contribuente")
        .from("clientes")
        .whereIn('id', Database.select("cliente_id").from('facturas').where('pago', 0).where('status', 'N'))
        .paginate(start, end);
    } else {
      res = await Database.select("id", "nome", "telefone", "morada", "contribuente")
        .from("clientes")
        .whereIn('id', Database.select("cliente_id").from('facturas').where('pago', 0).where('status', 'N'))
        .where("nome", "like", "%" + search + "%")
        .orWhere("id", "like", "%" + search + "%")
        .orWhere("contribuente", "like", "%" + search + "%")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, null, res);
  }

  async searchClienteTelefone({ request }) {
    const { chaveServico } = request.all();

    let res = null;

    res = await Database.select("clientes.nome", "clientes.id", "clientes.contribuente")
      .from("clientes")
      .innerJoin("contas", "contas.cliente_id", "clientes.id")
      .innerJoin("servicos", "servicos.conta_id", "contas.id")
      .where("servicos.chaveServico", chaveServico)
      .first();
    if (res == null) {
      return DataResponse.response(
        "success",
        500,
        "",
        res
      );
    }

    return DataResponse.response("success", 200, null, res);
  }

  async searchClienteFacturaEmail({ request }) {
    const { start, end, search, filtros } = request.all();
    let res = null;

    if (filtros!= null) {

      res = await Database.select(
        "clientes.id",
        "clientes.nome",
        "clientes.telefone",
        "clientes.morada",
        "clientes.contribuente",
        "clientes.email"
      )
        .from("clientes")
        .innerJoin("facturas", "facturas.cliente_id", "clientes.id")
        .innerJoin("bill_runs", "facturas.id", "bill_runs.factura_utilitie_id")
        .innerJoin("bill_run_headers", "bill_runs.bill_run_header_id", "bill_run_headers.id")
        .innerJoin("linha_facturas", "facturas.id", "linha_facturas.factura_id")
        .innerJoin("produtos", "linha_facturas.artigo_id", "produtos.id")
        .where("clientes.nome", "like", "%" + search + "%")
        .where("bill_run_headers.ano", filtros.ano)
        .whereIn("bill_run_headers.mes", filtros.mes == 'T' ? Database.select("mes").from("bill_run_headers") : [filtros.mes])
        .whereIn("clientes.direccao", filtros.direccao == 'T' || filtros.direccao == null ? Database.select("designacao").from("direccaos") : [filtros.direccao])
        .whereIn("clientes.gestor_conta", filtros.gestor == 'T' || filtros.gestor == null ? Database.select("gestor_conta").from("clientes") : [filtros.gestor])
        .whereIn("produtos.id", filtros.servico_id == 'T' || filtros.servico_id== null ? Database.select("id").from("produtos") : [filtros.servico_id])
        .groupBy('clientes.id','clientes.nome','clientes.telefone','clientes.morada','clientes.contribuente','clientes.email')
        .orderBy('clientes.nome', 'asc')
        .paginate(start, end);
    } else {

      res = await Database.select(
        "clientes.id",
        "nome",
        "telefone",
        "morada",
        "contribuente",
        "email"
      )
        .from("clientes")
        .innerJoin("facturas", "facturas.cliente_id", "clientes.id")
        .rightJoin("bill_run_emails", "bill_run_emails.factura_id", "facturas.id")
        .where("nome", "like", "%" + search + "%")
        .orWhere("clientes.id", "like", "%" + search + "%")
        .orWhere("contribuente", "like", "%" + search + "%")
        .orWhere("telefone", "like", "%" + search + "%")
        .orWhere("email", "like", "%" + search + "%")
        .groupBy('clientes.id', 'nome', 'telefone', 'morada', 'contribuente', 'email')
        .paginate(start, end);

    }

    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Create/save a new cliente.
   * POST clientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const data = request.only([
      "contribuente",
      "morada",
      "nome",
      "telefone",
      "observacao",
      "tipo_cliente_id",
      "tipo_identidade_id",
      "email",
      "province",
      "genero",
      "direccao_id",
      "direccao",
      "gestor_conta",
      "gestor_id"
    ]);
    var c = null;
    var fone = null;
    var email = null;

    c = await Cliente.query()
      .where("contribuente", data.contribuente)
      .select("*")
      .first();

    /*fone = await Cliente.query()
      .where("telefone", data.telefone)
      .select("telefone")
      .first();

    email = await Cliente.query()
      .where("email", data.email)
      .whereNot("email", null)
      .select("email")
      .first();*/


    if (c != null && data.contribuente != "999999999") {
      const tid = await TipoIdentidade.query()
        .where("id", data.tipo_identidade_id)
        .select("*")
        .first();
      return DataResponse.response(
        "success",
        201,
        "Aviso: Já existe um cliente com este número de " +
        tid.tipoIdentificacao + " " + data.contribuente,
        null
      );
    } /*else if (fone != null) {

      return DataResponse.response(
        "success",
        201,
        "Aviso: Já existe um cliente com este número de telefone",
        null
      );
    } else if (email != null) {

      return DataResponse.response(
        "success",
        201,
        "Aviso: Já existe um cliente com este email",
        null
      );
    }*/
    const direccao = await Direccao.query().where("id", data.direccao_id).select("*").first();
    data.direccao = direccao.slug;
    const gestor_conta = await Gestor.query().where("id", data.gestor_id).select("*").first();
    data.gestor_conta = gestor_conta.nome;
    const cliente = await Cliente.create({ user_id: auth.user.id, ...data });

    return DataResponse.response(
      "success",
      200,
      "Cliente registado com sucesso.",
      cliente
    );
  }

  /**
   * Display a single cliente.
   * GET clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ request }) {
    const { contribuente } = request.all();
    const cliente = await Database.select(
      "id",
      "nome",
      "telefone",
      "morada",
      "contribuente"
    )
      .from("clientes")
      .where("contribuente", contribuente);
    return cliente;
  }

  /**
   * Render a form to update an existing cliente.
   * GET clientes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) { }

  /**
   * Update cliente details.
   * PUT or PATCH clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { contribuente } = request.all();
    const data = request.only([
      //"contribuente",
      "morada",
      "nome",
      "telefone",
      "observacao",
      "tipo_cliente_id",
      "tipo_identidade_id",
      "email",
      "province",
      "genero",
      "direccao_id",
      "direccao",
      "gestor_conta",
      "gestor_id"
    ]);

    /*const count = await Database.from("facturas").where("cliente_id", params.id).getCount() // returns array
    //const total = count[0]['count("cliente_id")'];   
    if (count != 0 || count == undefined) {
      //return DataResponse.response("error", 500, "Os dados não podem ser editados porque o cliente já possui facturas associada", data);
    }*/


    const validation = await validate(data, Cliente.rules);
    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages()[0].message + " " + validation.messages()[0].field, data);
    }

    var c = null;
    var fone = null;
    var email = null;

    c = await Cliente.query()
      .where("contribuente", contribuente)
      .whereNot("id", params.id)
      .whereNot("contribuente", "999999999")
      .select("*")
      .first();

    /*fone = await Cliente.query()
      .where("telefone", data.telefone)
      .whereNot("id", params.id)
      .select("telefone")
      .first();

      email = await Cliente.query()
      .where("email", data.email)
      .whereNot("email", null)
      .whereNot("id", params.id)
      .select("email")
      .first();*/


    if (c != null && contribuente != "999999999") {
      const tid = await TipoIdentidade.query().where("id", data.tipo_identidade_id).select("*").first();
      return DataResponse.response("success", 201, "Aviso: Já existe um cliente com este número de " + tid.tipoIdentificacao + " " + contribuente, null);
    }/* else if (fone != null) {

      return DataResponse.response("success", 201,  "Aviso: Já existe um cliente com este número de telefone",null);
    }else if (email != null) {

      return DataResponse.response( "success", 201, "Aviso: Já existe um cliente com este email", null);
    }*/
    // update with new data entered
    const direccao = await Direccao.query().where("id", data.direccao_id).select("*").first();
    data.direccao = direccao.slug;
    const gestor_conta = await Gestor.query().where("id", data.gestor_id).select("*").first();
    data.gestor_conta = gestor_conta.nome;
    const cliente = await Cliente.find(params.id);
    cliente.merge(data);
    await cliente.save();

    return DataResponse.response("success", 200, "Dados actualizados com sucesso", cliente);

  }

  /**
   * Delete a cliente with id.
   * DELETE clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) { }

  //Busca Cliente para Mudança de Titularidade ou Conta
  async getCliente({ params, response }) {
    let cliente = null;
    let contas = null;
    cliente = await Cliente.query()
      .where("telefone", params.id)
      .orWhere("contribuente", params.id)
      .orWhere("email", params.id)
      .select("*")
      .first();

    if (cliente) {
      contas = await Database.select(
        "co.id as contaID",
        "co.contaDescricao as contaDescricao"
      )
        .from("contas as co")
        .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
        .where("co.cliente_id", cliente.id);

      let data = {
        cliente: cliente,
        contas: contas
      };

      return response
        .status(200)
        .send(Mensagem.response(response.response.statusCode, "success", data));
    }

    return response
      .status(404)
      .send(
        Mensagem.response(response.response.statusCode, "Não encontrado!", null)
      );
  }

  /**
   * serviceAGT cliente details.
   * PUT or PATCH clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async serviceAGT({ params }) {
    const axios = require("axios");
    const rota =
      "http://www.agt.minfin.gov.ao/servico-comum/api/publico/comum/PortalAGT/consultar-nif/";

    let clientAgt = await axios
      .post(rota, { ivNuContribuinte: params.id })
      .then(data => {
        return data;
      });

    if (clientAgt.data.quantidadeTotalItens >= 1) {
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        clientAgt.data.data[0]
      );
    } else {
      return DataResponse.response(
        "success",
        404,
        "Nenhum dado encontrado",
        null
      );
    }
  }

  async clienteAll({ request, view, response, auth }) {
    let res = null;
    res = await Database.select("*").from("clientes");
    return DataResponse.response("success", 200, "", res);
  }

  async createParceriaCliente({ request, auth }) { 
    const {
      codigo,
      cliente_id,
      moeda_id,
      estado,
      tarifario_id,
      chaveServico,
      trunk_in,
      trunk_out,
      origem,
      nacao_sigla,
      destino,
      imposto_id
    } = request.all();

    const Conta = use("App/Models/Conta");
    const Servico = use("App/Models/Servico");
    const ParceriaCliente = use("App/Models/ParceriaCliente");
    const FlatRateServico = use("App/Models/FlatRateServico");
    var moment = require("moment");

    var pc = null;

    pc = await ParceriaCliente.query()
      .where("codigo_interconexao", codigo)
      .select("*")
      .first();

    if (pc != null) {
      return DataResponse.response(
        "success",
        500,
        "Código de interconexão já usado.",
        null
      );
    }

    var c = null;

    c = await ParceriaCliente.query()
      .where("cliente_id", cliente_id)
      .select("*")
      .first();

    if (c != null) {
      return DataResponse.response(
        "success",
        500,
        "Já existe uma parceria para este cliente",
        null
      );
    }

    const cliente = await Cliente.query()
      .where("id", cliente_id)
      .select("*")
      .first();

    const tecnologia = await Database.select("*")
      .from("tecnologias")
      .where("nome", "INTERCONEXAO")
      .first();
    var dataActual = moment(new Date()).format("YYYY-MM-DD");

    const conta = await Conta.create({
      contaDescricao: cliente.nome + " - Parceria Interconexão",
      cliente_id: cliente.id,
      moeda_id: moeda_id,
      agencia_id: auth.user.loja_id == null ? null : auth.user.loja_id,
      estado: 1,
      dataEstado: dataActual,
      tipoFacturacao: "POS-PAGO",
      user_id: auth.user.id
    });

    const servico = await Servico.create({
      chaveServico: chaveServico,
      conta_id: conta.id,
      estado: 1,
      dataEstado: dataActual,
      tarifario_id: tarifario_id,
      tecnologia_id: tecnologia.id,
      user_id: auth.user.id
    });

    const flatRateServico = await FlatRateServico.create({
      servico_id: servico.id,
      valor: 0,
      moeda_id: moeda_id,
      imposto_id: imposto_id,
      origem: origem,
      destino: destino,
      user_id: auth.user.id
    });

    const parceriaCliente = await ParceriaCliente.create({
      cliente_id: cliente.id,
      trunk_in: trunk_in,
      trunk_out: trunk_out,
      moeda_id: moeda_id,
      codigo_interconexao: codigo,
      estado: estado,
      nacao_sigla: nacao_sigla,
      imposto_id: imposto_id,
      servico_id: servico.id,
      user_id: auth.user.id
    });

    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso.",
      parceriaCliente
    );
  }

  async direccaosSelectBox({ request, view, response, auth }) {
    let direccaos = await Cliente.query().whereNotNull("direccao").distinct('direccao').fetch()
    return DataResponse.response("success", 200, null, direccaos);
  }

  async direccaosSelectBoxreport({ request, view, response, auth }) {
    const { filtros} = request.all();
    let direccaos = await Database.select("direccaos.designacao")
    .from("direccaos")
    .innerJoin("clientes", "clientes.direccao_id", "direccaos.id")
    .innerJoin("facturas", "facturas.cliente_id", "clientes.id")
    .innerJoin("moedas", "facturas.moeda_id", "moedas.id")
    .innerJoin("bill_runs", "facturas.id", "bill_runs.factura_utilitie_id")
    .innerJoin("bill_run_headers", "bill_runs.bill_run_header_id", "bill_run_headers.id")
    .where("bill_run_headers.ano", filtros.ano)
    .whereIn("bill_run_headers.mes", filtros.mes == 'T' || filtros.mes == null ? Database.select("mes").from("bill_run_headers") : [filtros.mes])
    .whereIn("facturas.moeda_id", filtros.moeda_id == null || filtros.moeda_id=="T" ? Database.select("id").from("moedas") : [filtros.moeda_id])
    .groupBy("direccaos.designacao")
    return DataResponse.response("success", 200, null, direccaos);
  }

  async gestoresSelectBox({ request, view, response, auth }) {
    const { filtros} = request.all();
   let gestor = null;

  // gestores = await Database.select("nome").from("users").where("role_id",6).distinct('nome').orderBy('nome', 'asc')
   if (filtros== null){
    gestor = await Cliente.query().whereNotNull("gestor_conta").distinct('gestor_conta').orderBy('gestor_conta', 'asc').fetch()
   }else
   {
    gestor = await Database.select("clientes.gestor_conta")
      .from("clientes")
      .innerJoin("facturas", "facturas.cliente_id", "clientes.id")
      .innerJoin("linha_facturas", "linha_facturas.factura_id", "facturas.id")
      .innerJoin("moedas", "facturas.moeda_id", "moedas.id")
      .innerJoin("bill_runs", "linha_facturas.factura_id", "bill_runs.factura_utilitie_id")
      .innerJoin("bill_run_headers", "bill_runs.bill_run_header_id", "bill_run_headers.id")
      .where("bill_run_headers.ano", filtros.ano)
      .whereIn("bill_run_headers.mes", filtros.mes == 'T' || filtros.mes == null ? Database.select("mes").from("bill_run_headers") : [filtros.mes])
      .whereIn("facturas.moeda_id", filtros.moeda_id == null || filtros.moeda_id=="T" ? Database.select("id").from("moedas") : [filtros.moeda_id])
      .whereIn("clientes.direccao", filtros.direccao == 'T' || filtros.direccao == null ? Database.select("direccao").from("clientes") : [filtros.direccao])
      .whereIn("clientes.gestor_conta", filtros.gestor == 'T' || filtros.gestor == null ? Database.select("gestor_conta").from("clientes") : [filtros.gestor])
      //.where("produtos.tipo","S")
      .groupBy('clientes.gestor_conta')
      .whereNotNull("gestor_conta").distinct('gestor_conta')
      .orderBy('clientes.gestor_conta','asc')
   }
  



    return DataResponse.response("success", 200, null, gestor);
  }

  async TipoclienteSelectBox({ request, view, response, auth }) {
    let tipocliente = await Database.select("*")
      .from("tipo_clientes")
    return DataResponse.response("success", 200, null, tipocliente);
  }

  async TipoIdentidadeSelectBox({ request, view, response, auth }) {
    let tipocliente = await Database.select("*")
      .from("tipo_identidades")
    return DataResponse.response("success", 200, null, tipocliente);
  }

  async searchGestor({ request }) {
    const { start, end, search } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "users.id",
        "users.nome",

      )
        .from("users")
        .leftJoin("role_user", "role_user.user_id", "users.id")
        .leftJoin("roles", "roles.id", "role_user.role_id")
        .where("roles.slug", "caixa")
        .orWhere("roles.slug", "chefe_loja")                   //MULTIPLAS SLUGS
        .orWhere("roles.slug", "gestor")                   //MULTIPLAS SLUGS
        .orWhere("roles.slug", "Facturação")                   //MULTIPLAS SLUGS
        .orWhere("roles.slug", "DSCE")                   //MULTIPLAS SLUGS
        .orWhere("roles.slug", "Gestor_Q_Anula")                   //MULTIPLAS SLUGS
        .orWhere("roles.slug", "super_admin")                   //MULTIPLAS SLUGS
        .paginate(start, end);
    } else {
      res = await Database.select(
        "users.id",
        "users.nome",
      )
        .from("users")
        .leftJoin("role_user", "role_user.user_id", "users.id")
        .leftJoin("roles", "roles.id", "role_user.role_id")
        .where("roles.slug", "caixa")
        .where("users.nome", "like", "%" + search + "%")
        .orWhere("roles.slug", "chefe_loja")
        .where("users.nome", "like", "%" + search + "%")                  //MULTIPLAS SLUGS
        .orWhere("roles.slug", "gestor")
        .where("users.nome", "like", "%" + search + "%")                //MULTIPLAS SLUGS
        .orWhere("roles.slug", "Facturação")
        .where("users.nome", "like", "%" + search + "%")                   //MULTIPLAS SLUGS
        .orWhere("roles.slug", "DSCE")
        .where("users.nome", "like", "%" + search + "%")                  //MULTIPLAS SLUGS
        .orWhere("roles.slug", "Gestor_Q_Anula")
        .where("users.nome", "like", "%" + search + "%")              //MULTIPLAS SLUGS
        .orWhere("roles.slug", "super_admin")
        .where("users.nome", "like", "%" + search + "%")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, null, res);
  }

  async incumprimentCliente({ request, auth }) {
    const { search, pagination, filter } = request.all();
    const PaginationHelper = use("App/Helpers/PaginationHelper");

    const query = await Database.raw("SELECT f.cliente_id as id, c.nome, c.telefone,c.email,c.gestor_conta, d.designacao, u.nome as gestor, f.cliente_id, COUNT(f.cliente_id) as qtdFacturas ,sum(f.total) as total, SUM(IF(IFNULL(valor_aberto,0) = 0, total,valor_aberto)) as valor_aberto, MIN(f.status_date) as dateLastFact, TIMESTAMPDIFF(MONTH, MIN(f.status_date), NOW()) as meses "
      + " FROM facturas f, clientes c, direccaos d, users u  WHERE f.cliente_id = c.id AND  c.direccao = d.designacao AND c.gestor_id = u.id "
      + " AND f.pago = 0 AND f.status = 'N' AND f.status_date IS NOT NULL AND TIMESTAMPDIFF(MONTH, f.status_date, NOW()) > 0 "
      + (search == null || search == "" ? " " : " AND c.nome  LIKE '%" + search + "%'")
      + " GROUP BY f.cliente_id ORDER BY MIN(f.status_date) ASC limit " + (pagination.perPage) + " offset " + (((pagination.page || 1) - 1) * pagination.perPage))

    // 2. Make sure that you get the total. If your controller has search functionality,
    // ensure that you recount the total of the filtered result and store in the `total` variable.
    let total = await Database.select("cliente_id").from('facturas').where('pago', 0).where('status', 'N').whereNotNull('status_date').having(Database.raw('sum(TIMESTAMPDIFF(MONTH, status_date, NOW()))'), '>', 0).groupBy('cliente_id').getCount()

    const records = new PaginationHelper(query[0],
      request,
      { custom_build: true },
      pagination.perPage,
      pagination.page,
      total
    ).paginate;

    return DataResponse.response("success", 200, null, records);
  }


}

module.exports = ClienteController
