'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
'use strict'

const User = use('App/Models/User');
const Database = use('Database'); 
const DataResponse = use('App/Models/DataResponse');  
 
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const commons = use('App/Models/Commons');   


/**
 * Resourceful controller for interacting with tfas
 */
class TfaController {
  /**
   * Create/save a new tfa.
   * POST tfas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async setupPost({ request, auth }) {
    console.log(`DEBUG: Received TFA setup request`);

    const secret = speakeasy.generateSecret({
      length: 10,
      name: commons.userObject.uname,
      issuer: "NarenAuth v0.0"
    });
    var url = speakeasy.otpauthURL({
      secret: secret.base32,
      label: commons.userObject.uname,
      issuer: "UnigAuth",
      encoding: "base32"
    });
    const dataURL = QRCode.toDataURL(url, (err, dataURL) => {
      commons.userObject.tfa = {
        secret: "",
        tempSecret: secret.base32,
        dataURL,
        tfaURL: url
      };
      return DataResponse.response("success", 200, "", {
        message: "TFA Auth needs to be verified",
        tempSecret: secret.base32,
        dataURL,
        tfaURL: secret.otpauth_url
      });
    });
  }

  async setupGet({ request, auth }) {
    console.log("DEBUG: Received FETCH TFA request");
    return DataResponse.response(
      "success",200,"",commons.userObject.tfa ? commons.userObject.tfa : null);
  }

  async setupVerify({ request, auth }) {
    console.log(`DEBUG: Received TFA Verify request`);
    console.log(request.body);
    let isVerified = speakeasy.totp.verify({
      secret: commons.userObject.tfa.tempSecret,
      encoding: "base32",
      token: request.body.token
    });

    if (isVerified) {
      console.log(`DEBUG: TFA is verified to be enabled`);
      commons.userObject.tfa.secret = commons.userObject.tfa.tempSecret;
      return DataResponse.response(
        "success",
        200,
        "Two-factor Auth is enabled successfully",
        null
      );
    }

    console.log(`ERROR: TFA is verified to be wrong`);

    return DataResponse.response(
      "success",
      403,
      "Código de Autenticação Inválido, falha na verificação. Por favor verifique o sistema Data e Hora",
      null
    );
  }

  async setupDelete({ request, auth }) {
      console.log(`DEBUG: Received DELETE TFA request`); 
      delete commons.userObject.tfa; 
      return DataResponse.response( "success", 200,"" ,null  );
  }

  async loginTFA({ request, response }) {
    const { uname, upass, headers} = request.all();
	return  headers
    let user = null;
    user = await Database.select("*").from("users").where("email", uname);
    console.log(`DEBUG: Received login request`);

    console.log(uname);
    console.log(commons.userObject);

    if (commons.userObject.uname) {
      if (!commons.userObject.tfa || !commons.userObject.tfa.secret) {
        if (request.body.uname == commons.userObject.uname) {
          console.log("DEBUG: Login without TFA is successful");
          return DataResponse.response("success", 200, "", null);
        }
        console.log("ERROR: Login without TFA is not successful");

        return DataResponse.response(
          "success",403,"Invalid username or password",null
        );
      } else {
        if ( request.body.uname != commons.userObject.uname ||  request.body.upass != commons.userObject.upass ) {
          console.log("ERROR: Login with TFA is not successful");

          return DataResponse.response(
            "success",
            403,
            "Invalid username or password",
            null
          );
        }
        if (headers ==null || headers == "") {
          console.log("WARNING: Login was partial without TFA header"); 
          return DataResponse.response( "success",206,"Please enter the Auth Code",null);
        }
		

        let isVerified = speakeasy.totp.verify({
          secret: commons.userObject.secret,
          encoding: "base32",
          token: request.headers["x-tfa"]
        });
		
		

        if (isVerified) {
          console.log("DEBUG: Login with TFA is verified to be successful");

          return DataResponse.response("success", 200, "success", null);
        } else {
          console.log("ERROR: Invalid AUTH code");

          return DataResponse.response("error", 206, "Invalid Auth Code", null);
        }
      }
    }

    /*return DataResponse.response(
      "error",
      404,
      "Por favor, registre-se para entrar",
      null
    );*/
	commons.userObject.uname = uname // result.uname;
		commons.userObject.upass = "123"; //result.upass;	
		 console.log(commons.userObject);
	return DataResponse.response("success", 200, "success", null);
  }
}

module.exports = TfaController
