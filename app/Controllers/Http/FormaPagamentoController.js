'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const FormaPagamento = use("App/Models/FormaPagamento"); 
const LojaFormaPagamento = use("App/Models/LojaFormaPagamento"); 
const User = use('App/Models/User');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');

/**
 * Resourceful controller for interacting with formapagamentos
 */
class FormaPagamentoController { 
  /**
   * Show a list of all facturas.
   * GET facturas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({request,response,view}) {
    const {start, end, search, order, searchData} = request.all();
    let res = null;
	 
    if (search == null) {
      res = await Database.select('*').from('forma_pagamentos').orderBy('created_at', 'ASC').paginate(start, end) 
    } else {
      res = await Database.select('*').from('forma_pagamentos') 
		.where('designacao', 'like', '%' + search + "%")  
		.orWhere(Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'), 'like', '%' + search + "%") 
		.orderBy(order, 'asc').paginate(start, end)
    }

    return DataResponse.response("success", 200, "", res);
  }
  
  /**
   * Show a list of all facturas.
   * GET facturas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async formas({auth}) { 
    let res = null; 


    if(auth.user.loja_id != null){ 
      let subquery = null;
      subquery = await Database.select("forma_pagamento_id").from("loja_forma_pagamentos").where('loja_id',auth.user.loja_id); 

      if(subquery.length !=0){        
        subquery =  Database.select("forma_pagamento_id").from("loja_forma_pagamentos").where('loja_id',auth.user.loja_id).where('is_active', 1);
        res = await Database.select('*').from('forma_pagamentos').whereIn('id', subquery)
        return DataResponse.response("success", 200, "", res);
      }  
    }
    res = await Database.select('*').from('forma_pagamentos')  
    return DataResponse.response("success", 200, "", res);
  }


  /**
   * Render a form to be used for creating a new formapagamento.
   * GET formapagamentos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new formapagamento.
   * POST formapagamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({request,auth}) {
    const { designacao, descricao } = request.all();
    const pagamento = await FormaPagamento.findOrCreate({ designacao: designacao },{ designacao: designacao, descricao: descricao, user_id: auth.user.id });
	   return DataResponse.response("success", 200, "Registado com sucesso", pagamento);
  }

  /**
   * Display a single formapagamento.
   * GET formapagamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing formapagamento.
   * GET formapagamentos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update formapagamento details.
   * PUT or PATCH formapagamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a formapagamento with id.
   * DELETE formapagamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async selectBoxformas({ request, view, response, auth }) {
    let res = null; 
    res = await Database.select('*').from('forma_pagamentos').distinct('designacao').orderBy('designacao', 'asc')  
    return DataResponse.response("success", 200, "", res);
  }
}

module.exports = FormaPagamentoController
