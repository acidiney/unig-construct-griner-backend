'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");  
const Adiantamento = use("App/Models/Adiantamento");
const Database = use("Database");
const MovimentoAdiantamento = use("App/Models/MovimentoAdiantamento");
/**
 * Resourceful controller for interacting with adiantamentos
 */
class AdiantamentoController {
  /**
   * Show a list of all adiantamentos.
   * GET adiantamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { search, orderBy, pagination, cliente } = request.all(); 
    console.log(cliente);
    let movimentacao = [];
    const adiantamento = await Adiantamento.query().where("cliente_id", cliente.id).select("*").first();
    if(adiantamento == null){return DataResponse.response("success", 500, "Nenhum registo encontrado", null)}
    const movimentos =  await Database.select("id","valor","descritivo","factura_id","recibo_id","saldado_factura","saldado_recibo",Database.raw('DATE_FORMAT(created_at, "%d-%m-%Y") as data'), Database.raw('DATE_FORMAT(created_at, "%H:%m:%s") as hora'))
    .from("movimento_adiantamentos").where('adiantamento_id', adiantamento.id)
       // .whereIn("id ", Database.raw("SELECT id FROM movimento_adiantamentos "+(search == null ? " " : " WHERE clientes.nome  LIKE '%" + search + "%'" )))  
        .orderBy("created_at", "DESC")//.paginate( pagination.page == null ? 1 : pagination.page, pagination.perPage);
 
    movimentacao.push({data: movimentos[0].data, movimentos: [movimentos[0]]});    
    for (let index = 1; index < movimentos.length; index++) {
      var validar = true;
      const element = movimentos[index];  
        for (let i = 0; i < movimentacao.length; i++) {
          const subElement = movimentacao[i];   
          if(element.data == subElement.data){ 
            movimentacao[i].movimentos.push(element);
            validar = false;
          }
        }  
        if(validar){
          movimentacao.push({data: element.data, movimentos: [element]});
        } 
     } 
    return DataResponse.response("success", 200, "", {adiantamento, movimentacao, cliente});
  }

  /**
   * Render a form to be used for creating a new adiantamento.
   * GET adiantamentos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new adiantamento.
   * POST adiantamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store({request, auth }) {
    const { valor, cliente_id, referencia, banco_id, data_pagamento} = request.all();  
      let a = null
      a = await Database.table("adiantamentos").where("cliente_id", cliente_id).first();
      if (a == null) {
        a = await Adiantamento.create({
          valor: valor,          
          cliente_id: cliente_id,
          descricao: "criação por interface",
          user_id: auth.user.id
        });
      }

    const mov = await MovimentoAdiantamento.create({
      data_pagamento: data_pagamento,
      referencia: referencia,
      banco_id: banco_id,
      valor: valor,
      descritivo: "adiantamento gerado manualmente",
      saldado: false,
      adiantamento_id: a.id,
      user_id: auth.user.id
    }); 
    const list = await Database.raw("UPDATE adiantamentos SET valor = (SELECT SUM(valor) FROM movimento_adiantamentos WHERE saldado = 0 AND adiantamento_id = " + a.id + ") WHERE cliente_id = " + a.cliente_id);       
    
    return DataResponse.response("success", 200, "Registo efectuado com sucesso.", list);
  }


  /**
   * Display a single adiantamento.
   * GET adiantamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing adiantamento.
   * GET adiantamentos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update adiantamento details.
   * PUT or PATCH adiantamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a adiantamento with id.
   * DELETE adiantamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async adiantamentoFactura({ request }){
     const {cliente_id}=request.all();
     const list = await Adiantamento.AdiantamentoFactura(cliente_id);

     return DataResponse.response("success",200,null, list );
  }
}

module.exports = AdiantamentoController
