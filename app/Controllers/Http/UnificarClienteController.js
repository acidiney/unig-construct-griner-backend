'use strict'


const UnificarClienteDados = use('App/Models/UnificarCliente');
const LogoUnificacao = use('App/Models/LogUnificacao');
const Database = use('Database');
const DataResponse = use('App/Models/DataResponse');

class UnificarClienteController {
    async index({ request }) {


    }

    async create({ request, response }) {

    }

    async client_a_Unificar({ request }) {
        const { search, orderBy, pagination } = request.all();
        let res = null;

        const contaId =  Database.from('contas').distinct('cliente_id');

        res = await Database.select('*').from('clientes').where('unificado', "nao")
            .where('nome', 'like', '%' + (search == null ? 'a' : search) + "%")
            .paginate((pagination.page == null ? 1 : pagination.page), pagination.perPage);

        return DataResponse.response("success", 200, null, res);

    }

    async unificarCliente({ request, auth }) {
        const { cliente_manter, cliente_unificar, cliente_nome_manter, cliente_nome_unificar } = request.all();
        await Database.table("clientes").where("id",cliente_unificar).update("unificado", "sim");
        const clientesUnificar = await UnificarClienteDados.create(
                {
                    id_manter_cliente: cliente_manter,
                    nome_cliente_manter: cliente_nome_manter,
                    id_unificar_cliente: cliente_unificar,
                    nome_cliente_unificar: cliente_nome_unificar,
                    user_id: auth.user.id

                }
            );
        

            return DataResponse.response("sucesso", 200, "Primeira fase da unificação", clientesUnificar);

        

    }

    async getClente_a_Unificar({ request }) {
        const { search, orderBy, pagination } = request.all();
        let res = null;

        res = await Database.select('*').from('unificar_clientes').innerJoin("users", "unificar_clientes.user_id", "users.id")
            .where('nome_cliente_manter', 'like', '%' + (search == null ? 'a' : search) + "%")
            .paginate((pagination.page == null ? 1 : pagination.page), pagination.perPage);

        return DataResponse.response("success", 200, null, res);
    }

    async conta_cliente_manter({ request }) {
        const { id_cliente_manter } = request.all();
        let res = null;
        res = await Database.select('*').from('contas').where('cliente_id', id_cliente_manter);

        return DataResponse.response("success", 200, null, res);

    }

    async conta_cliente_unificar({ request }) {
        const { id_cliente_manter } = request.all();
        let res = null;

        res = await Database.select('*').from('contas').where('cliente_id', id_cliente_manter);
        return res;


    }

    async delete_unificar_cliente({ request }) {
        const { id_cliente_manter, id_cliente_unificado } = request.all();
        let response = null;
        response = await Database.select('*').from('unificar_clientes').where('id_manter_cliente', id_cliente_manter).where('id_unificar_cliente', id_cliente_unificado).delete()
        await Database.table("clientes").where("id",id_cliente_unificado).update("unificado", "nao");
        return DataResponse.response("sucess", 200, "Dado Eliminado com sucesso", response);
    }

    async upDateClienteUnificar({ request }) {
        const { id_cliente_manter, id_cliente_unificado,  cliente_nome_manter, cliente_nome_unificar, conta_id } = request.all();
                 
      const unificados= await Database.table("unificar_clientes").where("id_unificar_cliente", id_cliente_unificado).where("id_manter_cliente", id_cliente_manter).update("unificado", 1);
        const descricao= cliente_nome_unificar +"->"+cliente_nome_manter
       
            LogoUnificacao.registrarLogo("adiantamentos",id_cliente_unificado, id_cliente_manter ,descricao)

            LogoUnificacao.registrarLogo("pedidos",id_cliente_unificado, id_cliente_manter ,descricao);
            LogoUnificacao.registrarLogo("facturas",id_cliente_unificado, id_cliente_manter ,descricao);
            LogoUnificacao.registrarLogo("parceria_clientes",id_cliente_unificado, id_cliente_manter ,descricao);
            LogoUnificacao.registrarLogo("reclamacaos",id_cliente_unificado, id_cliente_manter ,descricao);
            LogoUnificacao.registrarLogo("contas",id_cliente_unificado, id_cliente_manter ,descricao);
            
            

     
        const adiantamento = await Database.table("adiantamentos").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter)

       const pedidos = await Database.table("pedidos").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const factura = await Database.table("facturas").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const parceiros= await Database.table("parceria_clientes").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const reclamacoes= await Database.table("reclamacaos").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const contas= await Database.table("contas").where("cliente_id", id_cliente_unificado).update("cliente_id", id_cliente_manter);
        const cliente= await Database.table("clientes").where("id",id_cliente_unificado).update("unificado", "sim");
  
     
        return DataResponse.response("sucesso", 200, "unificacção feito com sucesso", null);
      
    }

    async unificar_em_massa({request}){
     const dadosUnificados=   await Database.select("*").from('unificar_clientes');
       console.log(dadosUnificados)
          for(let i=0; i< dadosUnificados.length; ++i){
            UnificarClienteDados.unificar(dadosUnificados[i].id_manter_cliente,dadosUnificados[i].id_unificar_cliente,dadosUnificados[i].nome_cliente_manter,dadosUnificados[i].nome_cliente_unificar);

           
          }

           return DataResponse.response("sucesss", 200 , "Unificação feito Com sucesso");

    }
    async desfazerUnificacao({ request }) {
        const { id_cliente_manter, id_cliente_unificado } = request.all();
        let upDateEstado = null;
        let response = null;
        upDateEstado = await Database.select('*').from('unificar_clientes').where('id_unificar_cliente', id_cliente_unificado).update('unificado', "Pendente");

        return DataResponse.response("sucess", 200, "A unificação foi desfeita com sucesso", null);

    }

}

module.exports = UnificarClienteController
