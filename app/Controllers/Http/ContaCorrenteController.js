'use strict'
var numeral = require('numeral');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Factura = use('App/Models/Factura');
const Adiantamento = use('App/Models/Adiantamento');
const Recibo = use('App/Models/Recibo');
const LinhaFactura = use('App/Models/LinhaFactura');
const LinhaRecibo = use('App/Models/LinhaRecibo');
const Produto = use('App/Models/Produto');
const Imposto = use('App/Models/Imposto');
const User = use('App/Models/User');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');

/**
 * Resourceful controller for interacting with contacorrentes
 */
class ContaCorrenteController {
	/**
	 * Show a list of all contacorrentes.
	 * GET contacorrentes
	 *
	 * @param {object} ctx
	 * @param {Request} ctx.request
	 * @param {Response} ctx.response
	 * @param {View} ctx.view
	 */
	async contas({ params, request }) {

		let recibos = null;
		let facturas = null;
		let notaCreditos = null;
		let cliente = null;
		let facturas_recibo = null
		let facturas_em_Aberto = null;
		
		const estado_factura = request.all();

		const cliente_id = request.all(); 

		//console.log(estado_factura.estado_factura)
		cliente = await Database.select('nome','contribuente','id').from('clientes').where('id', cliente_id.cliente_id).first()

		recibos = await Database.select(
			"recibos.recibo_sigla", 
			"recibos.total", 
			"recibos.pago",  
			"recibos.numero", 
			Database.raw('DATE_FORMAT(recibos.created_at, "%d-%m-%Y") as data_format'),
			"recibos.status_date",
			"documentos.nome as tipo",
			"documentos.sigla",
			)
			  .from("recibos") 
			  .innerJoin("series", "series.id", "recibos.serie_id")
			  .innerJoin("documentos", "documentos.id", "series.documento_id") 
			  .where('recibos.cliente_id', cliente.id) 
			  .where('recibos.status', "N")
		
		//Database.select('*').from('recibos').where('cliente_id', cliente.id).where('status', "N");

		facturas_em_Aberto = await Database.select('*').from('facturas')
		.where('cliente_id', cliente.id)
		.where('pago',0)
		.where('status','N')
		.where('is_nota_credito','!=',1);

		
		
			facturas_recibo = await Database.select(
				'facturas.cliente_id',
				'facturas.factura_sigla',
				'facturas.observacao',
				Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as data_format'),
				'facturas.created_at',
				'facturas.hash',
				'facturas.pago',
				'facturas.hash_control',
				'facturas.status',
				'facturas.status_date',
				'facturas.status_reason',
				'facturas.total',
				'facturas.valor_aberto',
				'facturas.totalComImposto',
				'facturas.totalSemImposto',
				'facturas.user_id',
				'facturas.id',
				Database.raw('DATE_FORMAT(facturas.created_at, "%m") as mes'),
				'facturas.numero',
				'facturas.numero_origem_factura',
				'facturas.data_origem_factura',
				'users.nome as user',
				'clientes.nome as cliente',
				'series.nome as serie',
				'documentos.sigla',
				'documentos.nome as documentoDesc',
				'contas.contaDescricao',
				'contas.id as contaNumero'
			).from('facturas')
				.innerJoin('series', 'series.id', 'facturas.serie_id')
				.innerJoin('users', 'users.id', 'facturas.user_id')
				.innerJoin('clientes', 'clientes.id', 'facturas.cliente_id')
				.innerJoin('documentos', 'documentos.id', 'series.documento_id')
				.leftJoin('contas', 'contas.id', 'facturas.conta_id')
				.where('documentos.sigla', 'FR')
				
				.whereIn('facturas.pago', estado_factura.estado_factura == null ? [1, 0] : [estado_factura.estado_factura])
				.where('facturas.status', 'N')
				.where('facturas.cliente_id', cliente.id)
				.orderBy('contas.id')

		facturas = await Database.select(
			'facturas.cliente_id',
			'facturas.factura_sigla',
			'facturas.observacao',
			Database.raw("IF(facturas.pago,'Saldado','Não Saldado')as estado"),
			Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as data_format'),
			Database.raw('DATE_FORMAT(facturas.data_origem_factura, "%d-%m-%Y") as data_origem'),
			'facturas.created_at',
			'facturas.hash',
			'facturas.pago',
			'facturas.hash_control',
			'facturas.status',
			'facturas.status_date',
			'facturas.status_reason',
			'facturas.total',
			'facturas.valor_aberto',
			'facturas.totalComImposto',
			'facturas.totalSemImposto',
			'facturas.user_id',
			'facturas.id',
			Database.raw('DATE_FORMAT(facturas.created_at, "%m") as mes'),
			'facturas.numero',
			'facturas.numero_origem_factura',
			'facturas.data_origem_factura',
			'users.nome as user',
			'clientes.nome as cliente',

			'series.nome as serie',
			'documentos.sigla',
			'documentos.nome as documentoDesc',
			'contas.contaDescricao',
			'contas.id as contaNumero'
		).from('facturas')
			.innerJoin('series', 'series.id', 'facturas.serie_id')
			.innerJoin('users', 'users.id', 'facturas.user_id')
			.innerJoin('clientes', 'clientes.id', 'facturas.cliente_id')
			.innerJoin('documentos', 'documentos.id', 'series.documento_id')
			.leftJoin('contas', 'contas.id', 'facturas.conta_id')
			.where('documentos.sigla', 'FT')
			.whereIn('facturas.pago', estado_factura.estado_factura == null ? [1, 0] : [estado_factura.estado_factura])
			.where('facturas.status', 'N')
			.where('facturas.cliente_id', cliente.id)
			.orderBy('contas.id')

		notaCreditos = await Database.select(
			'facturas.cliente_id',
			'facturas.factura_sigla',
			'facturas.observacao',
			'facturas.created_at',
			 Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y") as data_format'),
			'facturas.hash',
			'facturas.pago',
			'facturas.hash_control',
			'facturas.status',
			'facturas.status_date',
			'facturas.status_reason',
			'facturas.total',
			'facturas.valor_aberto',
			'facturas.totalComImposto',
			'facturas.totalSemImposto',
			'facturas.user_id',
			'facturas.id',
			'facturas.numero',
			'facturas.numero_origem_factura',
			'facturas.data_origem_factura',
			'users.nome as user',
			'clientes.nome as cliente',


			'series.nome as serie',
			'documentos.sigla',
			'documentos.nome as documentoDesc',
			'contas.contaDescricao',
			'contas.id as contaNumero'
		).from('facturas')
			.innerJoin('series', 'series.id', 'facturas.serie_id')
			.innerJoin('users', 'users.id', 'facturas.user_id')
			.innerJoin('clientes', 'clientes.id', 'facturas.cliente_id')
			.innerJoin('documentos', 'documentos.id', 'series.documento_id')
			.leftJoin('contas', 'contas.id', 'facturas.conta_id')
			.where('documentos.sigla', 'NC')
			.whereIn('facturas.pago', estado_factura.estado_factura == null ? [1, 0] : [estado_factura.estado_factura])
			.where('facturas.status', 'N')
			.where('facturas.cliente_id', cliente.id)
			.orderBy('contas.id')

		let data = {
			recibos: recibos,
			facturas: facturas,
			cliente: cliente,
			notaCreditos: notaCreditos,
			facturas_recibo:facturas_recibo,
			facturas_em_Aberto:facturas_em_Aberto
		}

		//console.log(data)
		return DataResponse.response("success", 200, null, data);
	}

	async contaConrrenteReport({ params,request }) {

		let contas = null;
		const facturas = [];
		let cliente = null;
		const estado_factura = request.all();
		const cliente_id = request.all();

		cliente = await Database.select('id','nome','contribuente').from('clientes').where('id', cliente_id.cliente_id).first();		
		contas = await Database.select('*').from('contas').where('cliente_id', cliente.id);
	  
	  
		  for (let clientefilhos of contas) { 
			const contacorrente = await Database.select(
				"facturas.id as factura_id", 
				"facturas.total as valor", 
				"facturas.pago as estado", 
				"facturas.factura_sigla as numero", 
				Database.raw('DATE_FORMAT(facturas.status_date, "%d-%m-%Y") as data'),
				"facturas.status_date",
				"documentos.nome as tipo",
				"documentos.sigla",   
			)
			  .from("facturas") 
        	  .innerJoin("series", "series.id", "facturas.serie_id")
			  .innerJoin("documentos", "documentos.id", "series.documento_id")
			  .whereIn('facturas.pago', estado_factura.estado_factura == null ? [1, 0] : [estado_factura.estado_factura])
			  .where('facturas.status', 'N')
			  .where("facturas.conta_id", clientefilhos.id)
			  .where('facturas.cliente_id', cliente.id)
				
			  if(contacorrente.length > 0){
				facturas.push({ clientefilhos:clientefilhos.id+" - "+clientefilhos.contaDescricao, contacorrente });
			  }
			  
		  } 
		 var contacorrente = await Database.select(
			"facturas.id as factura_id", 
			"facturas.total as valor", 
			"facturas.pago as estado", 
			"facturas.factura_sigla as numero", 
			Database.raw('DATE_FORMAT(facturas.status_date, "%d-%m-%Y") as data'),
			"facturas.status_date",
			"documentos.nome as tipo",
			"documentos.sigla",
			)
			  .from("facturas") 
        	  .innerJoin("series", "series.id", "facturas.serie_id")
			  .innerJoin("documentos", "documentos.id", "series.documento_id")
			  .where('facturas.status', 'N')
			  //.where('facturas.pago', false)
			  .whereIn('facturas.pago', estado_factura.estado_factura == null ? [1, 0] : [estado_factura.estado_factura])
			  .whereIn("facturas.id", Database.select('id').from('facturas').whereNull('conta_id').where('cliente_id',cliente.id))
			  .where('facturas.cliente_id', cliente.id) 
			  facturas.push({ clientefilhos: "",contacorrente }); 

			
			  // recibos 
			  contacorrente=null;
			  contacorrente = await Database.select(
				"recibos.id as factura_id", 
				"recibos.total as valor", 
				"recibos.pago as estado",  
				"recibos.recibo_sigla as numero", 
				Database.raw('DATE_FORMAT(recibos.status_date, "%d-%m-%Y") as data'),
				"recibos.status_date",
				"documentos.nome as tipo",
				"documentos.sigla",
				)
				  .from("recibos") 
				  .innerJoin("series", "series.id", "recibos.serie_id")
				  .innerJoin("documentos", "documentos.id", "series.documento_id") 
				  .where('recibos.cliente_id', cliente.id) 
				  .where('recibos.status', "N") 
				  if(contacorrente!==null){
				  facturas.push({ clientefilhos: "",contacorrente }); 
				  }
			 

		  return DataResponse.response("success", 200,"", {cliente,facturas});

	}

	async adiantamentos({ request }) {
		const cliente_id = request.all();
		//console.log(cliente_id)
		const cliente_adiantamento = await Database.select("adiantamentos.valor").from('adiantamentos')
		.innerJoin("movimento_adiantamentos","movimento_adiantamentos.adiantamento_id", "adiantamentos.id")
		.where('cliente_id', cliente_id.cliente_id)
		return DataResponse.response("success", 200, "Dados actualizados com sucesso", cliente_adiantamento);
	}


}

module.exports = ContaCorrenteController
