'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Fornecedor = use('App/Models/Fornecedor');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');

/**
 * Resourceful controller for interacting with empresas
 */
class FornecedorController {
  /**
   * Show a list of all empresas.
   * GET empresas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async index({ request }) {
    
	
	const {start, end, search} = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select('id', 'nome', 'telefone', 'localizacao', 'nif').from('fornecedors').paginate(start, end);
    } else {
      res = await Database.select('*').from('fornecedors')
        .where('nome', 'like', '%' + search + "%")
        .orWhere('nif', 'like', '%' + search + "%")
		.orWhere('localizacao', 'like', '%' + search + "%")
		.orWhere('telefone', 'like', '%' + search + "%")
		.orWhere(Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'), 'like', '%' + search + "%").paginate(start, end);
    }

    return DataResponse.response("success", 200, "", res);
  }

 /**
   * Show a list of all artigos.
   * GET artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async selectFornecedores() { 
    let res = await Fornecedor.all();
    return DataResponse.response("success", 200, "", res);

  }
  
  /**
   * Render a form to be used for creating a new empresa.
   * GET empresas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({
    request,
    response,
    view
  }) {}

  /**
   * Create/save a new empresa.
   * POST empresas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({
    request,auth
  }) {
    const data = request.only(['nif', 'localizacao','nome', 'telefone']);	
	const fornecedor = await Fornecedor.findOrCreate({ nif: data.nif },{ user_id: auth.user.id,...data });	
	return DataResponse.response("success", 200, "Fornecedor registado com sucesso.", fornecedor);
  }

  /**
   * Display a single empresa.
   * GET empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({
    params,
    request,
    response,
    view
  }) {}

  /**
   * Render a form to update an existing empresa.
   * GET empresas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({
    params,
    request,
    response,
    view
  }) {}

  /**
   * Update Fornecedor details.
   * PUT or PATCH clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.only(['nome', 'telefone', 'localizacao', 'nif']);
    /*const validation = await validate(data, Fornecedor.rules);
    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages(), data);
    }*/
    // update with new data entered 
    const fornecedor = await Fornecedor.find(params.id);
    fornecedor.merge(data);
    await fornecedor.save();

    return DataResponse.response("success", 200, "Dados actualizados com sucesso", fornecedor);

  }

  /**
   * Delete a empresa with id.
   * DELETE empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({
    params,
    request,
    response
  }) {}
}

module.exports = FornecedorController
