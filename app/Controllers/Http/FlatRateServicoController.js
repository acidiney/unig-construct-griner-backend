"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with flatrateservicos
 */
const DataResponse = use("App/Models/DataResponse");
const FlatRateServico = use("App/Models/FlatRateServico");
const LogFlatRate = use("App/Models/LogFlatRate");
const Database = use("Database");
class FlatRateServicoController {
  /**
   * Show a list of all flatrateservicos.
   * GET flatrateservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {}

  /**
   * Render a form to be used for creating a new flatrateservico.
   * GET flatrateservicos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new flatrateservico.
   * POST flatrateservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */ 
  async store ({ request, auth, response }) {
    const data = request.only([ 'valor', 'capacidade', 'origem', 'destino', 'artigo_id','moeda_id', 'imposto_id', 'servico_id' ]);  
    // update with new data entered 
    const dataResponse = await FlatRateServico.create({ user_id: auth.user.id, ...data });
    return DataResponse.response(
      "success",
      200,
      "Dado creado com sucesso",
      dataResponse
    );    
  }

  /**
   * Display a single flatrateservico.
   * GET flatrateservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const { servicoId } = request.all();
    const flatRate = await Database.select(
      "flat_rate_servicos.id",
      "flat_rate_servicos.valor",
      "flat_rate_servicos.capacidade",
      "flat_rate_servicos.origem",
      "flat_rate_servicos.destino",
      "flat_rate_servicos.moeda_id",
      "flat_rate_servicos.imposto_id",
      "flat_rate_servicos.artigo_id", 
      "flat_rate_servicos.servico_id", 
      "moedas.nome as moeda",
      "impostos.descricao as imposto",
      "produtos.nome as artigo",
      "servicos.chaveServico as servico"
    )
      .from("flat_rate_servicos")
      .leftJoin("servicos", "servicos.id", "flat_rate_servicos.servico_id")
      .leftJoin("moedas", "moedas.id", "flat_rate_servicos.moeda_id")
      .leftJoin("impostos", "impostos.id", "flat_rate_servicos.imposto_id")
      .leftJoin("produtos", "produtos.id", "flat_rate_servicos.artigo_id")
      .where("flat_rate_servicos.servico_id", servicoId).orderBy('flat_rate_servicos.created_at','DESC');
    return DataResponse.response("success", 200, "", flatRate);
  }
  
  /**
   * Render a form to update an existing flatrateservico.
   * GET flatrateservicos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update flatrateservico details.
   * PUT or PATCH flatrateservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, auth }) { 

    const dados = request.only([ 'valor', 'capacidade', 'origem', 'destino', 'artigo_id','moeda_id', 'imposto_id' ]);
    const flatRateServico = await FlatRateServico.find(params.id);
    flatRateServico.merge(dados);
    await flatRateServico.save(); 

    // update with new data entered 
      await LogFlatRate.create({
        valor: flatRateServico.valor,  capacidade: flatRateServico.capacidade , 
        origem: flatRateServico.origem,   destino: flatRateServico.destino, 
        artigo_id: flatRateServico.artigo_id,
        moeda_id: flatRateServico.moeda_id,  imposto_id : flatRateServico.imposto_id,
        descricao:'FlatID ('+flatRateServico.id+') registo editado por '+auth.user.nome,  user_id: auth.user.id
      })
    
    return DataResponse.response("success", 200, "Dados actualizados com sucesso", request.all()  ); 
  }

  /**
   * Delete a flatrateservico with id.
   * DELETE flatrateservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   /**
   * Delete a Tarifario with id.
   * DELETE Tarifarios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, auth }) {
    const listResponse = await FlatRateServico.query().where("id", params.id).select("*").first();
    if (listResponse == null) {
      return DataResponse.response( "error", 500, "Erro: Registo não encontrado", listResponse);
    } 
    try {
          // update with new data entered 
          await LogFlatRate.create({
            valor: listResponse.valor,  capacidade: listResponse.capacidade , 
            origem: listResponse.origem,   destino: listResponse.destino, 
            artigo_id: listResponse.artigo_id,
            moeda_id: listResponse.moeda_id,  imposto_id : listResponse.imposto_id,
            descricao: 'registo excluido por '+auth.user.nome,  user_id: auth.user.id
          })
        await FlatRateServico.query().where('id', params.id).delete()
    } catch (e) {
      return DataResponse.response( "success", 201, "Aviso: Lamentamos informar que o registo «"+listResponse.id+"» não pode ser eliminado", null);
    }
    return DataResponse.response( "success", 200, "Dados eliminado com sucesso", listResponse);
  }

  async getFlatrateServico({ params }) {

    const flatRate = await Database.select(
      "flat_rate_servicos.id",
      "flat_rate_servicos.valor",
      "flat_rate_servicos.capacidade",
      "flat_rate_servicos.origem",
      "flat_rate_servicos.destino",
      "flat_rate_servicos.moeda_id",
      "flat_rate_servicos.imposto_id",
      "flat_rate_servicos.artigo_id", 
      "flat_rate_servicos.servico_id", 
      "moedas.nome as moeda",
      "impostos.descricao as imposto",
      "produtos.nome as artigo",
      "servicos.chaveServico as servico"
    )
      .from("flat_rate_servicos")
      .leftJoin("servicos", "servicos.id", "flat_rate_servicos.servico_id")
      .leftJoin("moedas", "moedas.id", "flat_rate_servicos.moeda_id")
      .leftJoin("impostos", "impostos.id", "flat_rate_servicos.imposto_id")
      .leftJoin("produtos", "produtos.id", "flat_rate_servicos.artigo_id")
      .where("flat_rate_servicos.servico_id", params.id)
      .orderBy('flat_rate_servicos.created_at','DESC');
    return DataResponse.response("success", 200, "", flatRate);
  }
}

module.exports = FlatRateServicoController;