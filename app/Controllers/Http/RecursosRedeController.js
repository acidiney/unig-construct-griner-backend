'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

/**
 * Resourceful controller for interacting with recursosredes
 */
class RecursosRedeController {
  /**
   * Show a list of all recursosredes.
   * GET recursosredes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  async centralListagem() {

    const centrais = await Database.select("*")
      .from("central_recurso_redes")
      .orderBy("descricao", "ASC")

    return DataResponse.response("success", 200, "", centrais);

  }

  async centralDataTable({ request }) {

    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select('*').from('central_recurso_redes')
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);

    } else {
      res = await Database.select('*').from('central_recurso_redes')
        .orWhere(Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'), 'like', '%' + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }


  async armarioByCentral({ params }) {
    //console.log(1)
    const armarios = await Database.select('id', 'descricao').from('armario_recurso_redes')
      .where('central_id', params.id)
      .orderBy('descricao', 'ASC')
    return DataResponse.response("success", 200, "", armarios);
  }


  async armarioListagem() {

    const armarios = await Database.select("*")
      .from("armario_recurso_redes")
      .orderBy("descricao", "ASC")

    return DataResponse.response("success", 200, "", armarios);

  }

  async caixaRecursoRedeListagem() {

    const caixaRecursos = await Database.select("*")
      .from("caixa_recurso_redes")
      .orderBy("descricao", "ASC")

    return DataResponse.response("success", 200, "", caixaRecursos);

  }

  async idCaixaByCentral({ params }) {
    //console.log(1)
    const res = await Database.select('id', 'descricao').from('caixa_recurso_redes')
      .where('central_id', params.id)
      .orderBy('descricao', 'ASC')
    return DataResponse.response("success", 200, "", res);
  }


  async idCaboByCentral({ params }) {
    //console.log(1)
    const res = await Database.select('id', 'descricao').from('cabo_recurso_redes')
      .where('central_id', params.id)
      .orderBy('descricao', 'ASC')
    return DataResponse.response("success", 200, "", res);
  }

  async caboRecursoRedeListagem() {

    const caixaRecursos = await Database.select("*")
      .from("cabo_recurso_redes")
      .orderBy("descricao", "ASC")

    return DataResponse.response("success", 200, "", caixaRecursos);

  }

  async parCaboByCentral({ params }) {
    //console.log(1)
    const res = await Database.select('id', 'descricao').from('par_cabos')
      .where('central_id', params.id)
      .orderBy('descricao', 'ASC')
    return DataResponse.response("success", 200, "", res);
  }

  async parCaboListagem() {

    const caixaRecursos = await Database.select("*")
      .from("par_cabos")
      .orderBy("descricao", "ASC")

    return DataResponse.response("success", 200, "", caixaRecursos);

  }

  async armarioPrimarioByCentral({ params }) {
    //console.log(1)
    const res = await Database.select('id', 'descricao').from('armario_primarios')
      .where('central_id', params.id)
      .orderBy('descricao', 'ASC')
    return DataResponse.response("success", 200, "", res);
  }

  async armarioPrimarioListagem() {

    const caixaRecursos = await Database.select("*")
      .from("armario_primarios")
      .orderBy("descricao", "ASC")

    return DataResponse.response("success", 200, "", caixaRecursos);

  }

  async armarioSecundarioByCentral({ params }) {
    //console.log(1)
    const res = await Database.select('id', 'descricao').from('armario_secundarios')
      .where('central_id', params.id)
      .orderBy('descricao', 'ASC')
    return DataResponse.response("success", 200, "", res);
  }

  async armarioSecundarioListagem() {

    const caixaRecursos = await Database.select("*")
      .from("armario_secundarios")
      .orderBy("descricao", "ASC")

    return DataResponse.response("success", 200, "", caixaRecursos);

  }

  async parCaixaByCentral({ params }) {
    //console.log(1)
    const res = await Database.select('id', 'descricao').from('par_caixas')
      .where('central_id', params.id)
      .orderBy('descricao', 'ASC')
    return DataResponse.response("success", 200, "", res);
  }

  async parADSLByCentral({ params }) {
    //console.log(1)
    const res = await Database.select('id', 'descricao').from('par_adsls')
      .where('central_id', params.id)
      .orderBy('descricao', 'ASC')
    return DataResponse.response("success", 200, "", res);
  }


  async parCaixaListagem() {

    const caixaRecursos = await Database.select("*")
      .from("par_caixas")
      .orderBy("descricao", "ASC")

    return DataResponse.response("success", 200, "", caixaRecursos);

  }

  /**
   * Render a form to be used for creating a new recursosrede.
   * GET recursosredes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new recursosrede.
   * POST recursosredes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single recursosrede.
   * GET recursosredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing recursosrede.
   * GET recursosredes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update recursosrede details.
   * PUT or PATCH recursosredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a recursosrede with id.
   * DELETE recursosredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = RecursosRedeController
