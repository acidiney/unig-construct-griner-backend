'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const TipoIdentidade = use("App/Models/TipoIdentidade");
const DataResponse = use("App/Models/DataResponse"); 
/**
 * Resourceful controller for interacting with tipoidentidades
 */
class TipoIdentidadeController {
  /**
   * Show a list of all tipoidentidades.
   * GET tipoidentidades
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index () {

    const tipoIdentidade = await TipoIdentidade.all();
    return DataResponse.response("success", 200, "", tipoIdentidade);
  }

  /**
   * Render a form to be used for creating a new tipoidentidade.
   * GET tipoidentidades/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new tipoidentidade.
   * POST tipoidentidades
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single tipoidentidade.
   * GET tipoidentidades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing tipoidentidade.
   * GET tipoidentidades/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update tipoidentidade details.
   * PUT or PATCH tipoidentidades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a tipoidentidade with id.
   * DELETE tipoidentidades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = TipoIdentidadeController
