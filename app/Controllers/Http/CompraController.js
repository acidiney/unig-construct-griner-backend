'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Compra = use('App/Models/Compra'); 
const LinhaProdutoCompra = use('App/Models/LinhaProdutoCompra');  
const User = use('App/Models/User');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');

/**
 * Resourceful controller for interacting with compras
 */
class CompraController {
  /**
   * Show a list of all compras.
   * GET compras
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) { 
	 const {start, end, search} = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select('*').from('compras').innerJoin('fornecedors', 'fornecedors.id', 'compras.fornecedor_id').paginate(start, end)
    } else {
      res = await Database.select('*').from('compras').innerJoin('fornecedors', 'fornecedors.id', 'compras.fornecedor_id')
		.where('fornecedors.nome', 'like', '%' + search + "%")
		.orWhere('compras.total', 'like', '%' + search + "%")
		.orWhere('compras.totalProdutos', 'like', '%' + search + "%") 
		.orWhere(Database.raw('DATE_FORMAT(compras.created_at, "%Y-%m-%d")'), 'like', '%' + search + "%").paginate(start, end);
    }

    return DataResponse.response("success", 200, "", res);
  }
  /**
   * Render a form to be used for creating a new compra.
   * GET compras/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new compra.
   * POST compras
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
	   
    //let linhas = [];
    const { compra  } = request.all(); 
	
    const compr = await Compra.create({
       fornecedor_id: compra.fornecedor_id,
	   dataCompra: compra.dataCompra,
	   totalProdutos: compra.produtos.length,
	   total: compra.total,
	   user_id: auth.user.id	   
    });
    
    for (let index = 0; index < compra.produtos.length; index++) {
        await LinhaProdutoCompra.create({
        artigo_id: compra.produtos[index].id,
		preco: compra.produtos[index].valor,
        compra_id: compr.id,
        user_id: auth.user.id,         
      }); 	  
    }
	  
    return DataResponse.response("success", 200,"reistado com sucesso", compr);
  }


  /**
   * Display a single compra.
   * GET compras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing compra.
   * GET compras/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update compra details.
   * PUT or PATCH compras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a compra with id.
   * DELETE compras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = CompraController
