"use strict";

const Role = use("App/Models/Role");
const RoleUser = use("App/Models/RoleUser");
const User = use("App/Models/User");
const Database = use("Database");
const DataResponse = use("App/Models/DataResponse");
const commons = use("App/Models/Commons");
const speakeasy = require("speakeasy");

class AuthController {
  async index({ request ,response }) {

    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "users.id",
        "users.nome",
        "users.telefone",
        "users.email",
        "users.username",
        "users.morada",
        "users.status",
        "users.created_at",
        "users.empresa_id",
        "users.loja_id",
        "role_user.role_id",
        "users.updated_at",
        "roles.name as role",
        "empresas.companyName",
        "lojas.nome as loja",
        "filials.nome as filial"
      )
        .from("users")
        .leftJoin("empresas", "empresas.id", "users.empresa_id")
        .leftJoin("role_user", "role_user.user_id", "users.id")
        .leftJoin("roles", "role_user.role_id", "roles.id")
        .leftJoin("lojas", "lojas.id", "users.loja_id")
        .leftJoin("filials", "filials.id", "lojas.filial_id")
        .orderBy("users.created_at", "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "users.id",
        "users.nome",
        "users.telefone",
        "users.email",
        "users.username",
        "users.morada",
        "users.status",
        "users.created_at",
        "users.empresa_id",
        "users.loja_id",
        "role_user.role_id",
        "users.updated_at",
        "roles.name as role",
        "empresas.companyName",
        "lojas.nome as loja",
        "filials.nome as filial"
      )
        .from("users")
        .leftJoin("empresas", "empresas.id", "users.empresa_id")
        .leftJoin("role_user", "role_user.user_id", "users.id")
        .leftJoin("roles", "role_user.role_id", "roles.id")
        .leftJoin("lojas", "lojas.id", "users.loja_id")
        .leftJoin("filials", "filials.id", "lojas.filial_id")
        .where("users.nome", "like", "%" + search + "%")
        .orWhere("users.username", "like", "%" + search + "%")
        .orWhere("users.morada", "like", "%" + search + "%")
        .orWhere("users.telefone", "like", "%" + search + "%")
        .orWhere("lojas.nome", "like", "%" + search + "%")
        .orWhere("filials.nome", "like", "%" + search + "%")
 
        .orderBy("users.created_at", "DESC")
        .paginate((pagination.page == null ? 1 : pagination.page), pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async register({ request }) {
    const data = request.only([
      "nome",
      "telefone",
      "username",
      "email",
      "password",
      "morada",
      "empresa_id",
      "role_id"
    ]);
    const user = await User.create(data);
    return DataResponse.response(
      "success",
      200,
      "Utilizador registado com sucesso",
      user
    );
  }

  async authenticate({ request, auth, response }) {
    let dado = { user: null, token: null, role: null, permissions: null };
    let permissions = [];

    const { username, password } = request.all();
 

    //try {
      
      const user = await User.findBy("username", username);
      dado.role = await user.getRoles();
      dado.user = {
        email: user.email,
        funcao_id: user.funcao_id,
        id: user.id,
        is_active: user.status,
        nome: user.nome,
        telefone: user.telefone,
        username: user.username,
        alterPassword: user.is_alterPassword
      };

      for (let v = 0; v < dado.role.length; v++) {
        let role = await Role.findBy("slug", dado.role[v]);

        let permissionSlugs = await role.getPermissions();

        for (let i = 0; i < permissionSlugs.length; i++) {
          permissions.push(permissionSlugs[i]);
        }
      }
      dado.permissions = permissions;
	  
      const data = await auth
        .withRefreshToken()
        .query(builder => {
          builder.where("status", 1);
        })
        .attempt(username, password,dado);
		
      return DataResponse.response(
        "error",
        response.response.statusCode,
        "Seja Bem vindo senhor(a) " + dado.user.nome,
        data
      );
    /*} catch (e) {
      return response.status(400).send({
        title: "Falha na Autenticação",
        message:
          "Nome de utilizador ou Password Inválido, ou consulta o administrador para verificar se a sua conta está activa"
      });
      // return response.send(Mensagem.response(401, 'Username ou password inválido', dado))
    }*/
  }
 
  /**
   * Update user details.
   * PUT or PATCH Roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async update({ params, request, response }) {
    const data = request.only([
      "nome",
      "telefone", 
      "email",
      "morada",
      "empresa_id",
      "role_id",
      "status"
    ]);

    const loja = request.only(["loja_id"]);

    //console.log(loja.loja_id)
    /*
    // update with new data entered
    const user = await User.find(params.id);
    user.merge({'loja_id': loja.loja_id, ...data});
    await user.save();

    */

    /*if (loja.loja_id == "null") {
      await Database.table("users")
        .where("id", params.id)
        .update({ loja_id: null, ...data });
    } else {
      await Database.table("users")
        .where("id", params.id)
        .update({ loja_id: loja.loja_id, ...data });
    }*/
	
	await User.query().where("id", params.id).update({ loja_id: loja.loja_id, ...data });

    const role = await Database.select("*")
      .table("role_user")
      //.where('role_id', data.role_id)
      //.where('user_id', user.id)
      .where("user_id", params.id)
      .first();

    if (role == undefined) {
      await Database.table("role_user").insert({
        role_id: data.role_id,
        user_id: params.id
      });
    } else {
      await Database.table("role_user")
        .where("id", role.id)
        .update("role_id", data.role_id);
    }

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      ""
    );
  }

  async show({ params, response }) {
    /*const user = await User.find(params.id);
    const res = {
      nome: user.nome,
      username: user.username,
      email: user.email
    };*/

    const user = await Database.select(
      "users.id",
      "users.nome",
      "users.telefone",
      "users.email",
      "users.username",
      "users.morada",
      "users.status",
      "users.created_at",
      "users.empresa_id",
      "users.role_id",
      "users.updated_at",
      "empresas.companyName",
      "empresas.addressDetail",
      "empresas.email",
      "empresas.telefone",
      "empresas.city",
      "empresas.province",
      "empresas.taxRegistrationNumber",
      "empresas.logotipo",
      "empresas.projecto_isActive",
      "roles.slug",
      "roles.name"
    )
      .from("users")
      .innerJoin("empresas", "empresas.id", "users.empresa_id")
      .innerJoin("roles", "roles.id", "users.role_id")
      .where("users.id", params.id)
      .first();

    const res = {
      id: user.id,
      nome: user.nome,
      username: user.username,
      telefone: user.telefone,
      email: user.email,
      morada: user.morada,
      role: user.role_id,
      role_slug: user.slug,
      role_name: user.name,
      projecto_isActive: user.projecto_isActive
    };

    return response.json(res);
  }

  async resetPassword({ request, response, auth }) {
    const Persona = use("Persona");
    const payload = request.only([
      "old_password",
      "password",
      "password_confirmation",
      "is_alterPassword"
    ]); 
    try {
      const user = await User.find(auth.user.id);
      console.log(user);
      await Database.table("users").where("id", user.id).update({ is_alterPassword: true });
      await Persona.updatePassword(user, payload);
      
      return DataResponse.response(
        "success",
        200,
        "Senha alterada com sucesso.",
        null
      );
    } catch (e) {
      return DataResponse.response(
        "success",
        500,
        "Falha na Aleração da Senha, Verifica as senhas digitadas",
        null
      );
    }
  }

  /**
   * Display a single roleuser.
   * POST changePassword/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async changePassword({ request }) {
    // add this to the top of the file
    const { user_id } = request.all();
    const data = request.only(["password"]);
    // update with new data entered
    const user = await User.find(user_id);
    user.merge(data);
    await Database.table("users")
      .where("id", user.id)
      .update({ is_alterPassword: false });
    const u = await user.save();
    return DataResponse.response(
      "success",
      200,
      "Senha redefinida com sucesso",
      null
    );
  }

  async isAlterPassword({ request, auth }) {
    var r = null;
    r = await Database.select("*").from("users").where("id", auth.user.id).first();  
    return DataResponse.response("success", 200, null, { alterPassword: r.is_alterPassword }); 
  }

  
  /**
   * Revoke refresh access
   * POST /auth/logout
   *
   * @param {Object} ctx - context
   * @param {Auth} ctx.auth - auth
   * @param {Response} ctx.response - response
   * @auth Caniggia Moreira <caniggia.moreira@ideiasdinamicas.com>
   */
  async logout({ auth, response, request }) { 
      const refreshToken = request.input('refreshToken') 
      await auth.authenticator('jwt').revokeTokens([refreshToken], true) 
      return DataResponse.response("success", 200, "Sessão terminada!"); 
  }
}

module.exports = AuthController;
