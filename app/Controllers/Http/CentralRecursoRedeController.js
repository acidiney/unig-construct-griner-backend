'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Central = use('App/Models/CentralRecursoRede');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database'); 
/**
 * Resourceful controller for interacting with centralrecursoredes
 */
class CentralRecursoRedeController {
  /**
   * Show a list of all centralrecursoredes.
   * GET centralrecursoredes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new centralrecursorede.
   * GET centralrecursoredes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new centralrecursorede.
   * POST centralrecursoredes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async storeCentral({ request, auth }) {
    const { descricao, provincia_id } = request.all();
    const central = await Central.query()
      .where("descricao", descricao)
      .getCount();
    if (central > 0) {
      return DataResponse.response(
        "success",
        500,
        "Já existe uma Central com a mesma descrição",
        central
      );
    } else {
      const central = await Central.create({
        descricao: descricao,
        provincia_id: provincia_id,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Central registada com sucesso",
        null
      );
    }
  }

  /**
   * Display a single centralrecursorede.
   * GET centralrecursoredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing centralrecursorede.
   * GET centralrecursoredes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update centralrecursorede details.
   * PUT or PATCH centralrecursoredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async updateCentral({ params, request }) {
    const data = request.only(["descricao","provincia_id"]);

    //console.log(data)

    const central = await Central.query()
    .where("descricao", data.descricao)
    .whereNot({ id: params.id })
    .getCount();

    //console.log(params.id)
  if (central > 0) {
    return DataResponse.response(
      "success",
      500,
      "Já existe uma Central com o mesmo nome",
      central
    );
  }else{

    // update with new data entered
    const central = await Central.find(params.id);
    central.merge(data);
    await central.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      central
    );

  }
  }

  /**
   * Delete a centralrecursorede with id.
   * DELETE centralrecursoredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = CentralRecursoRedeController
