'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
 
const EntidadeCativadora = use("App/Models/EntidadeCativadora");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

/**
 * Resourceful controller for interacting with entidadecativadoras
 */
class EntidadeCativadoraController {
  /**
   * Show a list of all entidadecativadoras.
   * GET entidadecativadoras
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select('entidade_cativadoras.estado','clientes.nome as clienteNome','entidade_cativadoras.cliente_id as cliente_id','entidade_cativadoras.nome','entidade_cativadoras.valor', 'entidade_cativadoras.created_at','tipo_entidade_cativadoras.nome as  tipo')
        .from('entidade_cativadoras')
        .leftJoin('tipo_entidade_cativadoras','tipo_entidade_cativadoras.id','entidade_cativadoras.tipo_entidade_cativadora_id')
        .leftJoin('clientes','clientes.id','entidade_cativadoras.cliente_id')
        .orderBy(orderBy == null ? 'entidade_cativadoras.created_at' : orderBy, 'DESC')
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select('entidade_cativadoras.estado','clientes.nome as clienteNome','entidade_cativadoras.cliente_id as cliente_id','entidade_cativadoras.nome','entidade_cativadoras.valor', 'entidade_cativadoras.created_at','tipo_entidade_cativadoras.nome as  tipo')
        .from('entidade_cativadoras')
        .leftJoin('tipo_entidade_cativadoras','tipo_entidade_cativadoras.id','entidade_cativadoras.tipo_entidade_cativadora_id')
        .leftJoin('clientes','clientes.id','entidade_cativadoras.cliente_id')
        .where("entidade_cativadoras.nome", "like", "%" + search + "%")
        .orWhere("tipo_entidade_cativadoras.nome", "like", "%" + search + "%") 
        .orWhere("clientes.nome", "like", "%" + search + "%") 
        .orWhere(  Database.raw('DATE_FORMAT(entidade_cativadoras.created_at, "%Y-%m-%d")'),"like",  "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new entidadecativadora.
   * GET entidadecativadoras/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new entidadecativadora.
   * POST entidadecativadoras
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const {  estado, tipo_id, cliente_id } = request.all();

    const pedido = await EntidadeCativadora.create({
      cliente_id:cliente_id,
      //valor:valor,
      estado:estado,
      tipo_entidade_cativadora_id: tipo_id,
      user_id: auth.user.id
    });


    return DataResponse.response("success", 200, "Entidade registada com sucesso", pedido);

  }

  async searchClientes({ request }) {
    const { start, end, search } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select("id", "nome", "telefone", "email", "contribuente")
        .from("clientes")
        .paginate(start, end);
    } else {
      res = await Database.select("id", "nome", "telefone", "email", "contribuente")
        .from("clientes")
        .where("nome", "like", "%" + search + "%")
        .orWhere("telefone", "like", "%" + search + "%")
        .orWhere("contribuente", "like", "%" + search + "%")
        .orWhere("email", "like", "%" + search + "%")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Display a single entidadecativadora.
   * GET entidadecativadoras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing entidadecativadora.
   * GET entidadecativadoras/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update entidadecativadora details.
   * PUT or PATCH entidadecativadoras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a entidadecativadora with id.
   * DELETE entidadecativadoras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = EntidadeCativadoraController
