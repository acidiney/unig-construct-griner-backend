'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const ParceriaCliente = use("App/Models/ParceriaCliente"); 
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Mensagem = use("App/Models/ResponseMessage");
const Conta = use("App/Models/Conta");
const Servico = use("App/Models/Servico");
const FlatRateServico = use("App/Models/FlatRateServico");
var moment = require("moment");

/**
 * Resourceful controller for interacting with parceriaclientes
 */
class ParceriaClienteController {
  /**
   * Show a list of all parceriaclientes.
   * GET parceriaclientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;
    const unificado='nao'

    if (search == null) {
      res = await Database.select("parceria_clientes.id","clientes.nome","clientes.id as cliente_id",'parceria_clientes.codigo_interconexao','parceria_clientes.estado','parceria_clientes.trunk_in','parceria_clientes.trunk_out','moedas.nome as moeda','moedas.id as moeda_id','parceria_clientes.nacao_sigla','parceria_clientes.imposto_id','impostos.descricao as imposto','servicos.chaveServico','flat_rate_servicos.origem','flat_rate_servicos.destino','tarifarios.id as tarifario_id','tarifarios.descricao as tarifario','servicos.conta_id','servicos.id as servico_id').from("parceria_clientes")
      .innerJoin('clientes','clientes.id','parceria_clientes.cliente_id')
      .leftJoin('moedas','moedas.id','parceria_clientes.moeda_id')
      .leftJoin('impostos','impostos.id','parceria_clientes.imposto_id')
      .leftJoin('servicos','servicos.id','parceria_clientes.servico_id')
      .leftJoin('flat_rate_servicos','flat_rate_servicos.servico_id','servicos.id')
      .leftJoin('tarifarios','tarifarios.id','servicos.tarifario_id')
      .orderBy(orderBy == null ? "parceria_clientes.created_at" : orderBy, "DESC").paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select("parceria_clientes.id","clientes.nome","clientes.id as cliente_id",'parceria_clientes.codigo_interconexao','parceria_clientes.estado','parceria_clientes.trunk_in','parceria_clientes.trunk_out','moedas.nome as moeda','moedas.id as moeda_id','parceria_clientes.nacao_sigla','parceria_clientes.imposto_id','impostos.descricao as imposto','servicos.chaveServico','flat_rate_servicos.origem','flat_rate_servicos.destino','tarifarios.id as tarifario_id','tarifarios.descricao as tarifario','servicos.conta_id','servicos.id as servico_id').from("parceria_clientes")
        .from("parceria_clientes").innerJoin('clientes','clientes.id','parceria_clientes.cliente_id')
        .leftJoin('moedas','moedas.id','parceria_clientes.moeda_id')
        .leftJoin('impostos','impostos.id','parceria_clientes.imposto_id')
        .leftJoin('servicos','servicos.id','parceria_clientes.servico_id')
        .leftJoin('flat_rate_servicos','flat_rate_servicos.servico_id','servicos.id')
        .leftJoin('tarifarios','tarifarios.id','servicos.tarifario_id')
        .where("clientes.nome", "like", "%" + search + "%")
        .orWhere("clientes.contribuente", "like", "%" + search + "%")
        .orWhere("clientes.id", "like", "%" + search + "%")
        .orWhere("clientes.numeroExterno", "like", "%" + search + "%")
        .orWhere("clientes.morada", "like", "%" + search + "%")
        .orWhere("clientes.telefone", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(parceria_clientes.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(
          orderBy == null ? "parceria_clientes.created_at" : orderBy,
          'DESC'
        )
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Render a form to be used for creating a new parceriacliente.
   * GET parceriaclientes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new parceriacliente.
   * POST parceriaclientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single parceriacliente.
   * GET parceriaclientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ request }) {
    const res = await Database.select('id', 'codigo_interconexao as nome')
    .from('parceria_clientes')    
    .orderBy("parceria_clientes.codigo_interconexao", "ASC");
    return res;
  }

  /**
   * Render a form to update an existing parceriacliente.
   * GET parceriaclientes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update parceriacliente details.
   * PUT or PATCH parceriaclientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response,auth}) {

    const data = request.only([
      "id",
      "cliente_id",
      "codigo_interconexao",
      "estado",
      "trunk_in",
      "trunk_out",
      "moeda_id",
      "nacao_sigla",
      "imposto_id",
      "origem",
      "destino",
      "chaveServico",
      "tarifario_id",
      "servico_id",
      "conta_id"
    ]);

   

    const flatverific = await Database.select("servico_id")
    .from("flat_rate_servicos")
    .where("servico_id", data.servico_id)
    .first();


 // if(flatverific == undefined && data.origem != null || data.destino !=null){

   // return DataResponse.response("Falha", 500, "Adiciona FlateRate Para editar origem e destino ",null);
 // } else{}

 
  
    await Conta.query("id").where("cliente_id",data.cliente_id).andWhere('contaDescricao', 'like','%'+'Parceria Interconexão'+'%').update({
    moeda_id: data.moeda_id,
    user_id: auth.user.id
  });
  

   await Servico.query().where("conta_id",data.conta_id).update(
    {
    chaveServico: data.chaveServico,
    tarifario_id: data.tarifario_id,
    user_id: auth.user.id
  });
  
   await FlatRateServico.query().where("servico_id",data.servico_id).update({
    moeda_id: data.moeda_id,
    imposto_id: data.imposto_id,
    origem:data.origem,
    destino: data.destino,
    user_id: auth.user.id
  });


    await ParceriaCliente.query().where("id",params.id).update({ 
      codigo_interconexao: data.codigo_interconexao,
      estado: data.estado,
      trunk_in: data.trunk_in,
      trunk_out: data.trunk_out,
      moeda_id: data.moeda_id,
      nacao_sigla:data.nacao_sigla,
      imposto_id:data.imposto_id,
      user_id: auth.user.id
    });
    return DataResponse.response("success", 200, "Registo Actualizado com sucesso.",null);
  
  }

  /**
   * Delete a parceriacliente with id.
   * DELETE parceriaclientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ParceriaClienteController
