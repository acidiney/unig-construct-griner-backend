'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */

const Imposto = use('App/Models/Imposto');
const Database = use('Database');
const DataResponse = use('App/Models/DataResponse');
class ImpostoController {

  /**
   * Show a list of all artigos.
   * GET artigos
   *
   * @param {object} ctx
   * @author vpangola@gmail.com
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null; 

    if (search == null) {
      res = await Database.select("*")
        .from("impostos") 
        .orderBy(orderBy == null ? "impostos.created_at" : orderBy, orderBy == "created_at" ? "DESC" : "ASC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select("*")
        .from("impostos") 
        .where("impostos.descricao", "like", "%" + search + "%")
        .orWhere("impostos.codigo", "like", "%" + search + "%") 
        .orWhere(
          Database.raw('DATE_FORMAT(impostos.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "impostos.created_at" : orderBy, "DESC")
        .paginate(
          pagination.page == null ? 1 : pagination.page,
          pagination.perPage
        );
    }

    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Show a list of all artigos.
   * GET artigos
   *
   * @param {object} ctx
   * @author vpangola@gmail.com
   */
  async getAll() {
    const impostos = await Database.select('id', 'descricao').from('impostos');
    return impostos;
  }


  /**
   * Create/save a new artigo.
   * POST artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({
    request,
    auth
  }) {
    const {
      codigo,
      descricao,
      valor,
      activo
    } = request.all();
    const imposto = await Imposto.create({
      codigo: codigo,
      descricao: descricao,
      valor: valor,
      activo: activo,
      user_id: auth.user.id
    });
    
    return DataResponse.response("success", 200, "Registo efectuado com sucesso.", imposto);
  }

  async update({ request,params,response}) {
   
   const dados = request.only([
     "codigo",
     "descricao",
     "valor",
     "activo",  
   ]);
   const imposto = await Imposto.find(params.id);
   imposto.merge(dados);
   await imposto.save();
   return DataResponse.response("success",
    200,
    "Dados actualizados com sucesso",
    dados
  );
  }

}

module.exports = ImpostoController
