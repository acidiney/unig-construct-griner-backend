'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Database = use("Database");
const DataResponse = use("App/Models/DataResponse");
const Relatorio = use("App/Models/Relatorio");
/**
 * Resourceful controller for interacting with relatorios
 */
class RelatorioController {
  /**
   * Show a list of all relatorios.
   * GET relatorios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async relatorioFacturacaoRealizadaCobrancaGlobal({ request }) {
    let c = await Relatorio.relatorioFacturacaoRealizadaCobrancaGlobal(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioFacturacaopagamentoGlobal({ request }) {
    let c = await Relatorio.relatorioFacturacaopagamentoGlobal(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioFacturacaoporGestorGlobal({request}){
    let relatorio= await Relatorio.relatorioFacturacaoporGestorGlobal(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioFacturacaoClienteGlobal({request}){
    let relatorio= await Relatorio.relatorioFacturacaoClienteGlobal(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioPagamentosClienteGlobal({request}){
    let relatorio= await Relatorio.relatorioPagamentosClienteGlobal(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioFacturacaoDiariaPorGestorGlobal({request}){
    let relatorio= await Relatorio.relatorioFacturacaoDiariaPorGestorGlobal(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioFacturacaoRealizadaCobrancaGlobalPago({request}){
    let relatorio= await Relatorio.relatorioFacturacaoRealizadaCobrancaGlobalPago(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioFacturacaoDetalhadaPosPago({request}){
    let relatorio= await Relatorio.relatorioFacturacaoDetalhadaPosPago(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioRecibos({request}){
    let relatorio= await Relatorio.relatorioRecibos(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioFacturacaoporServicoGlobal({request}){
    let relatorio= await Relatorio.relatorioFacturacaoporServicoGlobal(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioFacturaEnvidasPorEmail({request}){
    let relatorio= await Relatorio.relatorioFacturaEnvidasPorEmail(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioVendaporLojaporProdutoGlobal({request}){
    let relatorio= await Relatorio.relatorioVendaporLojaporProdutoGlobal(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioDiarioVendasProdutoServicos({request}){
    let relatorio= await Relatorio.relatorioDiarioVendasProdutoServicos(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioResumoContaCorrente({request}){
    let relatorio= await Relatorio.relatorioResumoContaCorrente(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioLoja({ request }) {
    let c = await Relatorio.relatorioLoja(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioFacturacaoDiariaPorServico({request}){
    let relatorio= await Relatorio.relatorioFacturacaoDiariaPorServico(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioIVA({ request }) {
    let c = await Relatorio.relatorioIVA(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async facturacaoDiariaGlobal({ request }) {
    let c = await Relatorio.facturacaoDiariaGlobal(request.all());

    //console.log(c)
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioMovimentosCaixa({ request }) {
    let c = await Relatorio.relatorioMovimentosCaixa(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioServicoContratados({ request }) {
    let c = await Relatorio.relatorioServicoContratados(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioClients({ request }) {
    let c = await Relatorio.relatorioClients(request);
    return DataResponse.response("success", 200, null, c);
  }

  async reportingBI() {
    let c = await Relatorio.reportingBI();
    return DataResponse.response("success", 200, null, c);
  }

  async reportingBILastMonth() {
    let c = await Relatorio.reportingBILastMonth();
    return DataResponse.response("success", 200, null, c);
  }

}

module.exports = RelatorioController
