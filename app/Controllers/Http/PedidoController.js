'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Pedido = use("App/Models/Pedido");
const LogEstadoPedido = use("App/Models/LogEstadoPedido");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with pedidos
 */
class PedidoController {
  /**
   * Show a list of all pedidos.
   * GET pedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params }) {
    //const pedidos = await Pedido.query().where("cliente_id", params.id).fetch(); 
    const pedidos = await Database.select('pe.id as id', 'pe.cliente_id as cliente_id', 'pe.tipoPedido as tipoPedido', 'pe.telefone as telefone', 'pe.dataPedido as dataPedido', 'pe.observacao as observacao', 'ta.descricao as tarifario')
      .table('pedidos as pe')
      .leftJoin('tarifarios as ta', 'ta.id', 'pe.tarifario_id')
      .where("pe.cliente_id", params.id)

    return DataResponse.response("success", 200, "", pedidos);
  }

  /**
   * Render a form to be used for creating a new pedido.
   * GET pedidos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new pedido.
   * POST pedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { cliente_id, tipoPedido, observacao, dataPedido, telefone, tarifario, capacidade, origem, destino } = request.all();

    let filial = null;
    let str = "Luanda";
    let provincia = str.toUpperCase();
    //let provincia = "Luanda";

    filial = await Database
      .select('filials.id as provincia')
      .table('users')
      .leftJoin('lojas', 'lojas.id', 'users.loja_id')
      .leftJoin('filials', 'filials.id', 'lojas.filial_id')
      .where('users.id', auth.user.id).first()

    //console.log(filial.provincia)
    //console.log(provincia)

    if (filial.provincia == null) {

      //filial = await Database.raw('select filials.id as provincia from filials where UPPER(nome) = ?', [provincia])
      filial = await Database
        .select('filials.id as provincia')
        .from('filials')
        .whereRaw('UPPER(nome) = ?', [provincia]).first()

    }

    //console.log(filial.provincia)

    const pedido = await Pedido.create({
      cliente_id: cliente_id,
      tipoPedido: tipoPedido,
      observacao: observacao,
      dataPedido: dataPedido,
      telefone: telefone,
      tarifario_id: tarifario,
      capacidade: capacidade,
      origem: origem,
      destino: destino,
      provincia_id: filial.provincia,
      user_id: auth.user.id
    });





    return DataResponse.response("success", 200, "Pedido registado com sucesso", pedido);

  }

  /**
   * Display a single pedido.
   * GET pedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing pedido.
   * GET pedidos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  async gerarPdfInstalacao({ params }) {

    let cliente = null;
    let pedido = null;

    pedido = await Database.select(Database.raw('DATE_FORMAT(pe.dataPedido, "%d-%m-%Y") as pedidoData'), 'pe.id as id', 'pe.id as codigo', 'pe.cliente_id as cliente_id', 'pe.tipoPedido as tipoPedido', 'pe.telefone as telefone', 'pe.dataPedido as dataPedido', 'pe.observacao as observacao', 'ta.descricao as tarifario')
      .table('pedidos as pe')
      .leftJoin('tarifarios as ta', 'ta.id', 'pe.tarifario_id')
      .where("pe.id", params.id).first()

    //console.log(pedido)
    cliente = await Database.select('cli.genero as genero', 'pro.nome as clienteProvincia', 'cli.observacao as obsCliente', 'tc.tipoClienteDesc as tipoCliente', 'cli.nome as clienteNome', 'cli.telefone as clienteTelefone', 'cli.morada as clienteMorada', 'cli.contribuente  as clieteContribuente', 'td.tipoIdentificacao as tipoIdentificacao')
      .table('clientes as cli')
      .innerJoin("pedidos as pe", "pe.cliente_id", "cli.id")
      .leftJoin("tipo_identidades as td", "td.id", "cli.tipo_identidade_id")
      .leftJoin("provincias as pro", "pro.id", "cli.province")
      .leftJoin("tipo_clientes as tc", "tc.id", "cli.tipo_cliente_id")
      .where("pe.cliente_id", pedido.cliente_id).first()

    let data = {
      cliente: cliente,
      pedido: pedido,

    };

    //console.log(data)


    return DataResponse.response("success", 200, "", data);
  }

  /**
   * Update pedido details.
   * PUT or PATCH pedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination, filter } = request.all();

    // console.log(request.all())
    let res = null;

    //if (search == null) {
    res = await Database.select('pe.created_at as created_at', 'pe.id as id',
      'pe.cliente_id as cliente_id', 'pe.tipoPedido as tipoPedido',
      'pe.telefone as telefone', 'pe.dataPedido as dataPedido',
      'pe.observacao as observacao', 'ta.descricao as tarifario',
      'pe.estado as estadoPedido',
      'pe.telefone as pedidoTelefone',
      'cli.nome as clienteNome',
      'cli.morada as clienteMorada',
      'cli.telefone as clienteTelefone',
      'est.sigla as siglaEstadoPedido',
      'est.designacao as estadoDesignacao',
      'filialUser.nome as provinciaPedidoUser',
      'tipo_pedidos.id as tipoPedidoId',
      'filialPedido.nome as filialPedido'
    )
      .table('pedidos as pe')
      .innerJoin('clientes as cli', 'pe.cliente_id', 'cli.id')
      .leftJoin('tarifarios as ta', 'ta.id', 'pe.tarifario_id')
      .leftJoin('estado_pedidos as est', 'est.id', 'pe.estado')
      .leftJoin('users', 'users.id', 'pe.user_id')
      .leftJoin('lojas', 'users.loja_id', 'lojas.id')
      .leftJoin('filials as filialUser', 'filialUser.id', 'lojas.filial_id')
      //REVER
      .leftJoin('tipo_pedidos', 'tipo_pedidos.slug', 'pe.tipoPedido')
      //REVER
      .leftJoin('filials as filialPedido', 'filialPedido.id', 'pe.provincia_id')
      .where(function () {
        if (filter.estado == null || filter.estado == 'T') {
          this
            .whereNotNull("pe.estado")
            .orWhere("pe.estado", null)
        } else {
          this
            .where("est.sigla", filter.estado)
        }
      })
      //.whereIn("est.sigla",filter.estado == 'T' || filter.estado == null  ? Database.select("sigla").from("estado_pedidos"): [filter.estado])
      .whereIn("pe.provincia_id", filter.provincia == 'T' || filter.provincia == null ? Database.select("id").from("filials") : [filter.provincia])
      .whereIn("pe.tipoPedido", filter.tipo == 'T' || filter.tipo == null ? Database.select("tipoPedido").from("pedidos") : [filter.tipo])
      .whereIn("cli.id", search == null ? Database.select("id").from("clientes") : Database.select("id").from("clientes").where("nome", 'like', '%' + search + '%'))
      .whereIn(Database.raw('DATE_FORMAT(pe.created_at, "%Y-%m-%d")'), filter.data == null ? Database.select(Database.raw('DATE_FORMAT(pe.created_at, "%Y-%m-%d")')).from("pedidos") : [filter.data])
      .orderBy(orderBy == null ? 'pe.created_at' : orderBy, 'DESC')
      .paginate(pagination.page, pagination.perPage);
    //}

    /*else {
      res = await Database.select('pe.created_at as created_at', 'pe.id as id',
        'pe.cliente_id as cliente_id', 'pe.tipoPedido as tipoPedido',
        'pe.telefone as telefone', 'pe.dataPedido as dataPedido',
        'pe.observacao as observacao', 'ta.descricao as tarifario',
        'pe.estado as estadoPedido',
        'pe.telefone as pedidoTelefone',
        'cli.nome as clienteNome',
        'cli.morada as clienteMorada',
        'cli.telefone as clienteTelefone',
        'est.sigla as siglaEstadoPedido',
        'est.designacao as estadoDesignacao',
        'filialUser.nome as provinciaPedidoUser',
        'tipo_pedidos.id as tipoPedidoId',
        'filialPedido.nome as filialPedido'
      )
        .table('pedidos as pe')
        .innerJoin('clientes as cli', 'pe.cliente_id', 'cli.id')
        .leftJoin('tarifarios as ta', 'ta.id', 'pe.tarifario_id')
        .leftJoin('estado_pedidos as est', 'est.id', 'pe.estado')
        .leftJoin('users', 'users.id', 'pe.user_id')
        .leftJoin('lojas', 'users.loja_id', 'lojas.id')
        .leftJoin('filials as filialUser', 'filialUser.id', 'lojas.filial_id')
        //REVER
        .leftJoin('tipo_pedidos', 'tipo_pedidos.slug', 'pe.tipoPedido')
        //REVER
        .leftJoin('filials as filialPedido', 'filialPedido.id', 'pe.provincia_id')
        .where("cli.nome", "like", "%" + search + "%")
        .orWhere("cli.telefone", "like", "%" + search + "%")
        .orWhere("pe.tipoPedido", "like", "%" + search + "%")
        .orWhere("filialPedido.nome", "like", "%" + search + "%")
        .orWhere("filialUser.nome", "like", "%" + search + "%")
        .orWhere("est.designacao", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(pe.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "pe.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }*/

    return DataResponse.response("success", 200, "", res);
  }

  async estadosPedidos() {
    const estados = await Database.select('id', 'designacao').table('estado_pedidos')
      .orderBy('designacao', 'ASC');
    return DataResponse.response("success", 200, "", estados);
  }

  async selectBox({ params }) {

    const pedidoEstado = await Database.select('pedidos.id',
      'pedidos.capacidade as capacidade',
      'pedidos.origem as origem',
      'pedidos.destino as destino',
      'tarifarios.tecnologia',
      'pedidos.tipoPedido as tipoPedido',
      'pedidos.estado',
      'pedidos.observacao as observacao',
      'tarifarios.descricao as tarifarioDescricao',
      'tarifarios.id as idTarifario',
      'pedidos.central as central',
      'pedidos.armario as armario',
      'pedidos.caixa as caixa',
      'pedidos.cabo as cabo_id',
      'pedidos.par_cabo as par_cabo',
      'pedidos.armario_primario as armario_primario',
      'pedidos.armario_secundario as armario_secundario',
      'pedidos.par_caixa as par_caixa',
      'pedidos.par_adsl as par_adsl'
    )
      .table('pedidos')
      .innerJoin('tarifarios', 'tarifarios.id', 'pedidos.tarifario_id')
      .where('pedidos.id', params.id).first()

    return DataResponse.response("success", 200, "", pedidoEstado);
  }

  async totalDashBoard({ request }) {

    let totalFinalizado = null;
    let totalRejeitado = null;
    let totalOutro = null;

    totalFinalizado = await Database.select(
      Database.raw("count(pedidos.id) as totalFinalizado")
    )
      .from("pedidos")
      .innerJoin("estado_pedidos", "estado_pedidos.id", "pedidos.estado")
      .where("estado_pedidos.sigla", "FN")
      .first();


    totalRejeitado = await Database.select(
      Database.raw("count(pedidos.id) as totalRejeitado")
    )
      .from("pedidos")
      .innerJoin("estado_pedidos", "estado_pedidos.id", "pedidos.estado")
      .where("estado_pedidos.sigla", "RJ")
      .first();

    totalOutro = await Database.select(
      Database.raw("count(pedidos.id) as totalOutro")
    )
      .from("pedidos")
      .innerJoin("estado_pedidos", "estado_pedidos.id", "pedidos.estado")
      .whereNot("estado_pedidos.sigla", "FN")
      .whereNot("estado_pedidos.sigla", "RJ")
      .first();

    let data = {
      totalFinalizado: totalFinalizado,
      totalRejeitado: totalRejeitado,
      totalOutro: totalOutro
    };


    // console.log(data)


    return DataResponse.response("success", 200, "", data);
  }

  async estadoByPedido({ params, request }) {
    //console.log(params)

    const data = request.only(['tipo_pedido']);

    //console.log(data)

    if (data.tipo_pedido == "LTE PRE-PAGO") {
      const tipoPedido = await Database
        .select('id')
        .from('tipo_pedidos')
        //.where('slug', 'LTE_PRE').first()
        .where('slug', 'LTE PRE-PAGO').first()

      params.id = tipoPedido.id;
    }

    const pedidoEstado = await Database.select(
      'estado_pedidos.id', 'estado_pedidos.designacao',
      'estado_pedidos.sigla', 'estado_pedidos.rota_form',
      'tipo_pedidos.slug'
    )
      .table('estado_pedidos')
      .innerJoin('tipo_pedidos', 'tipo_pedidos.id', 'estado_pedidos.tipo_pedido_id')
      .where('estado_pedidos.tipo_pedido_id', params.id)

    return DataResponse.response("success", 200, "", pedidoEstado);
  }

  async updatEstado({ params, request, auth }) {
    const data = request.only(['estado', 'observacao']);
    const log = request.only(['estado', 'estado_actual', 'observacao_old']);

    const pedido = await Pedido.find(params.id);
    pedido.merge(data);
    await pedido.save();

    await LogEstadoPedido.create({ 'pedido_id': params.id, 'id_estado_anterior': log.estado_actual, 'user_id': auth.user.id, 'id_estado_novo': log.estado, 'observacao_old': log.observacao_old, 'observacao_new': data.observacao })

    return DataResponse.response("success", 200, "Estado actualizado com sucesso", pedido);

  }

  async estadosPedidosFNRJ() {
    const estados = await Database.select('id', 'designacao', 'sigla').table('estado_pedidos')
      .where('sigla', 'FN')
      .orWhere('sigla', 'RJ')
      .orderBy('designacao', 'ASC')

    return DataResponse.response("success", 200, "", estados);
  }

  async updatePedido({ params, request, auth }) {
    const { par_cabo, par_caixa, par_adsl, armario_primario, armario_secundario, estado_pedido, observacao_pedido, cabo_id, caixa, central, armario } = request.all()

    await Database
      .table('pedidos')
      .where('id', params.id)
      .update({
        'caixa': caixa,
        'armario': armario,
        'cabo': cabo_id,
        'central': central,
        'par_cabo': par_cabo,
        'par_caixa': par_caixa,
        'par_adsl': par_adsl,
        'armario_primario': armario_primario,
        'armario_secundario': armario_secundario,
        'observacao': observacao_pedido

      })
    return DataResponse.response("success", 200, "Pedido actualizado com sucesso", null);

  }

  async updatePedidoCliente({ request }) {
    const { pedidoId, tipoPedido, tarifario, telefone } = request.all()


    //console.log(tarifario)

    await Database
      .table('pedidos')
      .where('id', pedidoId)
      .update({
        'tipoPedido': tipoPedido,
        'tarifario_id': tarifario,
        'telefone': telefone
      })

    return DataResponse.response("success", 200, "Pedido actualizado com sucesso", null);


  }


  async servicoByTelefone({ request }) {

    const data = request.only(['telefone']);

    const servico = await Database
      .select('sim_cards.iccid', 'sim_cards.ki', 'sim_cards.imsi', 'sim_cards.nome')
      .from('servicos')
      .innerJoin('sim_cards', 'sim_cards.id', 'servicos.sim_card_id')
      .where('servicos.chaveServico', data.telefone).first()


    return DataResponse.response("success", 200, "", servico);
  }

  //Retorna true se um cliente tem o serviço cobre...
  async isCobre({ params }) {

    const res = await Database
      .select('*')
      .from('pedidos as pe')
      .innerJoin('clientes as c', 'c.id', 'pe.cliente_id')
      .where('c.id', params.id)
      .where('pe.tipoPedido', 'Cobre').first();
    if (res != null) {
      return true;
    }
    return false;
  }



  /**
   * Delete a pedido with id.
   * DELETE pedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = PedidoController
