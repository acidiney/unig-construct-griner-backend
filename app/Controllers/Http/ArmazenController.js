"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const { validate } = use("Validator");

const Armazem = use("App/Models/Armazen");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with armazens
 */
class ArmazenController {
  /**
   * Show a list of all armazens.
   * GET armazens
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { start, end, search, order } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select("*")
        .from("armazens")
        .orderBy(order, "asc")
        .paginate(start, end);
    } else {
      res = await Database.select("*")
        .from("armazens")
        .where("nome", "like", "%" + search + "%")
        .orWhere("descricao", "like", "%" + search + "%")
        .orderBy(order, "asc")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new armazen.
   * GET armazens/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, auth }) {
    const data = request.only(["nome", "descricao"]);
    const armazem = await Armazem.create(data);
    return DataResponse.response(
      "success",
      200,
      "Armazem registado com sucesso.",
      armazem
    );
  }

  /**
   * Create/save a new armazen.
   * POST armazens
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {}

  /**
   * Display a single armazen.
   * GET armazens/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing armazen.
   * GET armazens/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update armazen details.
   * PUT or PATCH armazens/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.only(["nome", "descricao"]);
    const validation = await validate(data, Armazem.rules);
    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages(), data);
    }
    // update with new data entered
    const armazem = await Armazem.find(params.id);
    armazem.merge(data);
    await armazem.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      armazem
    );
  }

  /**
   * Delete a armazen with id.
   * DELETE armazens/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}

  /**
   * Show a list of all armazens.
   * GET armazens
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async getAll() {
    let res = await Database.select("*").from("armazens") 
    return DataResponse.response("success", 200, "", res);
  }
}

module.exports = ArmazenController;
