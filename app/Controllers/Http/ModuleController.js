'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Database = use('Database'); 
const DataResponse = use('App/Models/DataResponse');  
const Module = use('App/Models/Module');
/**
 * Resourceful controller for interacting with modules
 */
class ModuleController {
  /**
   * Show a list of all modules.
   * GET modules
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async index({ request }) { 
 
	const {start, end, search, order} = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select('*').from('modules').orderBy(order, 'asc').paginate(start, end);
    } else {
      res = await Database.select('*').from('modules') 
        .where('nome', 'like', '%' + search + "%") 
		.orWhere('descricao', 'like', '%' + search + "%") 
		.orWhere(Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'), 'like', '%' + search + "%") 
		.orderBy(order, 'asc').paginate(start, end);
    }


    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new module.
   * GET modules/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new module.
   * POST modules
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store ({ request }) {
	   
    const { nome, descricao } = request.all(); 
    const modulo = await Module.create({nome, descricao });
	return DataResponse.response("success", 200, "registado com sucesso", modulo);    
     
  }

  /**
   * Display a single module.
   * GET modules/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing module.
   * GET modules/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update module details.
   * PUT or PATCH modules/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async update({ params, request, response }) {
    const data = request.only(['nome', 'descricao']); 
    // update with new data entered 
    const modulo = await Module.find(params.id);
    modulo.merge(data);
    await modulo.save();
    return DataResponse.response("success", 200, "Dados actualizados com sucesso", modulo);

  }

  /**
   * Delete a module with id.
   * DELETE modules/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
  
  /**
   * selectOptionRole a list of all modules.
   * GET Roles
   *
   * @param {object} ctx 
   */
   async selectOptionModulo() {  
      const res = await Database.select('*').from('modules');  
    return DataResponse.response("success", 200, "", res);
  }
  async moduloPermissions(){
	  
	const modulosPermissions = [];  
	const modulos = await Database.select('*').from('modules') 	
	for (let modulo of modulos) {
	  const  permissions = await Database.select('*').from('permissions').where('module_id',modulo.id);  
	  modulosPermissions.push({modulo, permissions})
	}	
	let data = { 
		 modulosPermissions: modulosPermissions
	} 		
    return DataResponse.response("success", 200, "", modulosPermissions);
  }
}

module.exports = ModuleController
