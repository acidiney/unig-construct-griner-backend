'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const Loja = use("App/Models/Loja");
const Database = use("Database");
/**
 * Resourceful controller for interacting with lojas
 */
class LojaController {
  /**
   * Show a list of all lojas.
   * GET lojas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;
    if (search == null) {
      res = await Database.select(
        "lojas.id",
        "lojas.nome",
        "lojas.numero",
        "lojas.telefone",
        "lojas.email",
        "lojas.endereco",
        "lojas.is_active",
        "lojas.provincia_id",
        "lojas.serie_id",
        "lojas.filial_id",
        "lojas.municipio_id",
        "lojas.serie_id_recibo",
        "lojas.created_at",
        "municipios.nome as municipio",
        "provincias.nome as provincia",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.documento_id",
        "filials.nome as filial",
        "users.nome as user_chefe"
      )
        .from("lojas")
        .leftJoin("municipios", "municipios.id", "lojas.municipio_id")
        .leftJoin("provincias", "provincias.id", "lojas.provincia_id")
        .leftJoin("filials", "filials.id", "lojas.filial_id")
        .leftJoin("series", "series.id", "lojas.serie_id")
        .leftJoin("documentos", "documentos.id", "series.documento_id")
        .leftJoin("users", "users.id", "lojas.user_chefe_id")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "lojas.id",
        "lojas.nome",
        "lojas.numero",
        "lojas.telefone",
        "lojas.email",
        "lojas.endereco",
        "lojas.is_active",
        "lojas.provincia_id",
        "lojas.serie_id",
        "lojas.filial_id",
        "lojas.municipio_id",
        "lojas.serie_id_recibo",
        "lojas.created_at",
        "municipios.nome as municipio",
        "provincias.nome as provincia",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.documento_id",
        "filials.nome as filial",
        "users.nome as user_chefe"
      )
        .from("lojas")
        .leftJoin("municipios", "municipios.id", "lojas.municipio_id")
        .leftJoin("provincias", "provincias.id", "lojas.provincia_id")
        .leftJoin("filials", "filials.id", "lojas.filial_id")
        .leftJoin("series", "series.id", "lojas.serie_id")
        .leftJoin("documentos", "documentos.id", "series.documento_id")
        .leftJoin("users", "users.id", "lojas.user_chefe_id")
        .where("lojas.nome", "like", "%" + search + "%")
        .orWhere("lojas.numero", "like", "%" + search + "%")
        .orWhere("lojas.endereco", "like", "%" + search + "%")
        .orWhere("provincias.nome", "like", "%" + search + "%")
        .orWhere("municipios.nome", "like", "%" + search + "%")

        .orWhere(
          Database.raw('DATE_FORMAT(lojas.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy("lojas.created_at", "DESC")
        .paginate(
          pagination.page == null ? 1 : pagination.page,
          pagination.perPage
        );
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new loja.
   * GET lojas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new loja.
   * POST lojas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    const {
      nome,
      numero,
      telefone,
      email,
      endereco,
      is_active,
      provincia_id,
      municipio_id,
      serie_id,
      filial_id,
      serie_id_recibo
    } = request.all();
    const loja = await Loja.create({
      nome: nome,
      numero: numero,
      telefone: telefone,
      email: email,
      endereco: endereco,
      is_active: is_active,
      provincia_id: provincia_id,
      municipio_id: municipio_id,
      serie_id: serie_id,
      serie_id_recibo:serie_id_recibo,
      filial_id: filial_id,
      user_id: auth.user.id
    });

    return DataResponse.response(
      "success",
      200,
      "Registo Efectuado com sucesso",
      loja
    );
  }

  /**
   * Display a single loja.
   * GET lojas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing loja.
   * GET lojas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update loja details.
   * PUT or PATCH lojas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a loja with id.
   * DELETE lojas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}

  async selectBox() {
    const lojas = await Loja.all();
    return DataResponse.response("success", 200, "", lojas);
  }

  async addChefeLoja({ request }) {
    const { loja_id, user_chefe_id} = request.all();
    var l = await Loja.query().where("id", loja_id).update({ user_chefe_id: user_chefe_id }); 
    return DataResponse.response("success", 200, "Registado com sucesso", l);

  }
}

module.exports = LojaController
