"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Database = use("Database");
const DataResponse = use("App/Models/DataResponse");
const RelatorioMediation = use("App/Models/RelatorioMediation");

/**
 * Resourceful controller for interacting with relatoriomediations
 */
class RelatorioMediationController {
  /**
   * Show a list of all relatoriomediations.
   * GET relatoriomediations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response }) {
    const { type } = request.all();
    const data =
      type == "fixo-exterior"
        ? await RelatorioMediation.relatorioMediationFixoExterior(request.all())
        : type == "fixo-fixo"
        ? await RelatorioMediation.relatorioMediationFixoFixo(request.all())
        : type == "fixo-movel"
        ? await RelatorioMediation.relatorioMediationFixoFixo(request.all())
        : type == "exterior-fixo"
        ? await RelatorioMediation.relatorioMediationExteriorFixo(request.all())
        : "tipo de relatorios invalido";
    return DataResponse.response("success", 200, null, data);
  }

  /**
   * @description FIXO-EXTERIOR
   * Tráfego da rede de telefonia fixa para o exterior do país (minutos)
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {*} filter
   */
  async relatorioMediationFixoExterior({ request }) {
    let c = await RelatorioMediation.relatorioMediationFixoExterior(
      request.all()
    );
    return DataResponse.response("success", 200, null, c);
  }

  /**
   * @description FIXO-FIXO
   * Tráfego doméstico de uma rede de telefonia fixa para outra rede de telefonia fixa (minutos)
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {*} filter
   */
  async relatorioMediationFixoFixo({ request }) {
    let c = await RelatorioMediation.relatorioMediationFixoFixo(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  /**
   * @description FIXO-MOVEL
   * Tráfego doméstico de uma rede de telefonia fixa para uma rede móvel (minutos)
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {*} filter
   */
  async relatorioMediationFixoMovel({ request }) {
    let c = await RelatorioMediation.relatorioMediationFixoMovel(request.all());
    return DataResponse.response("success", 200, null, c);
  }
  /**
   * @description EXTERIOR-FIXO
   * Tráfego da rede de telefonia fixa para o exterior do país (minutos)
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {*} filter
   */
  async relatorioMediationExteriorFixo({ request }) {
    let c = await RelatorioMediation.relatorioMediationExteriorFixo(
      request.all()
    );
    return DataResponse.response("success", 200, null, c);
  }
}

module.exports = RelatorioMediationController;
