'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DetalheInterconexao = use("App/Models/DetalheInterconexao");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

/**
 * Resourceful controller for interacting with detalheinterconexaos
 */
class DetalheInterconexaoController {
  /**
   * Show a list of all detalheinterconexaos.
   * GET detalheinterconexaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

   
  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "detalhe_interconexaos.id", 
        "detalhe_interconexaos.periodo", 
        "detalhe_interconexaos.origin", 
        "detalhe_interconexaos.destination", 
        "detalhe_interconexaos.service", 
        "detalhe_interconexaos.call", 
        "detalhe_interconexaos.minutes", 
        "detalhe_interconexaos.rate", 
        "detalhe_interconexaos.amount", 
        "detalhe_interconexaos.billing_operator", 
        "detalhe_interconexaos.factura_id", 

        Database.raw(
          'DATE_FORMAT(detalhe_interconexaos.updated_at, "%Y-%m-%d") as dataActualizacao'
        ),
        "detalhe_interconexaos.created_at"
      )
        .from("detalhe_interconexaos")        
        .orderBy(orderBy == null ? "detalhe_interconexaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "detalhe_interconexaos.id", 
        "detalhe_interconexaos.periodo", 
        "detalhe_interconexaos.origin", 
        "detalhe_interconexaos.destination", 
        "detalhe_interconexaos.service", 
        "detalhe_interconexaos.call", 
        "detalhe_interconexaos.minutes", 
        "detalhe_interconexaos.rate", 
        "detalhe_interconexaos.amount", 
        "detalhe_interconexaos.billing_operator", 
        "detalhe_interconexaos.factura_id", 

        Database.raw(
          'DATE_FORMAT(detalhe_interconexaos.updated_at, "%Y-%m-%d") as dataActualizacao'
        ),
        "detalhe_interconexaos.created_at"
      )
        .from("detalhe_interconexaos")
        .where("detalhe_interconexaos.periodo", "like", "%" + search + "%")
        .orWhere("detalhe_interconexaos.origin", "like", "%" + search + "%")
        .orWhere("detalhe_interconexaos.destination", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(detalhe_interconexaos.updated_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "detalhe_interconexaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new detalheinterconexao.
   * GET detalheinterconexaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new detalheinterconexao.
   * POST detalheinterconexaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request }) {
    /*
    const {
      periodo,
      origin,
      destination,
      service,
      call,
      minutes,
      rate,
      amount,
      billing_operator,
      updated_at
    } = request.all();

    const detalheInterconexao = await DetalheInterconexao.create({
      periodo: periodo,
      origin: origin,
      destination: destination,
      service: service,
      call: call,
      minutes: minutes,
      rate: rate,
      amount: amount,
      billing_operator: billing_operator,
      updated_at: updated_at
    });
*/
    
    const {
      periodo,
      billing_operator,
      updated_at
    } = request.all();

    const detalheInterconexao = await DetalheInterconexao.create({
      periodo: periodo,
      billing_operator: billing_operator,
      updated_at: updated_at
    });
    //console.log(tarifario)
    return DataResponse.response(
      "success",
      200,
      "Detalhe de Interconexão registado com sucesso",
      detalheInterconexao
    );
  }

  /**
   * Display a single detalheinterconexao.
   * GET detalheinterconexaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }


  /**
   * Render a form to update an existing detalheinterconexao.
   * GET detalheinterconexaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update detalheinterconexao details.
   * PUT or PATCH detalheinterconexaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    /*
    const data = request.only([
      "origin", 
      "destination", 
      "service", 
      "amount", 
      "billing_operator", 
      "factura_id", 
    ]); 
    */

    const data = request.only([
      "periodo", 
      "billing_operator",
    ]);

    /*
    const validation = await validate(data, Serie.rules);
    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages(), data);
    }*/
    // update with new data entered
    const detalheInterconexao = await DetalheInterconexao.find(params.id);
    detalheInterconexao.merge(data);
    await detalheInterconexao.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      detalheInterconexao
    );
  }

  /**
   * Delete a detalheinterconexao with id.
   * DELETE detalheinterconexaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async interconexoesAll ({ params, view, request, auth }) {
    let res = null;

    res = await Database.select("*")
      .from("detalhe_interconexaos");

      return DataResponse.response("success", 200, "", res);
  }

  async currentYear ({ response }) {
    console.log(new Date().getFullYear());
    res = await Database.raw("SELECT year(now()) as ano");
      return res;
  }


}

module.exports = DetalheInterconexaoController
