'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const SerieProjecto = use("App/Models/SerieProjecto");

/**
 * Resourceful controller for interacting with projectoseries
 */
class ProjectoSerieController {
  /**
   * Show a list of all projectoseries.
   * GET projectoseries
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {}

  /**
   * Render a form to be used for creating a new projectoserie.
   * GET projectoseries/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new projectoserie.
   * POST projectoseries
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { projecto_id, serie_id } = request.all();
    const projecto = await SerieProjecto.query().where("projecto_id", projecto_id).where("serie_id", projecto_id).getCount();

    if (projecto > 0) {
      return DataResponse.response("success",500,"Está serie já existente num projecto",projecto);
    } else {
      const serie = await ProjectoSerie.create({ 
        projecto_id: cliente_id,
        serie_id: serie_id,
        user_id: auth.user.id
      });
      return DataResponse.response("success",200,"serie registada com sucesso",serie);
    }
  }

  /**
   * Display a single projectoserie.
   * GET projectoseries/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing projectoserie.
   * GET projectoseries/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update projectoserie details.
   * PUT or PATCH projectoseries/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a projectoserie with id.
   * DELETE projectoseries/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = ProjectoSerieController
