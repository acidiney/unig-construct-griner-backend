'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const LogEstadoPedido = use("App/Models/LogEstadoPedido");
const Database = use("Database");
/**
 * Resourceful controller for interacting with logestadopedidos
 */
class LogEstadoPedidoController {
  /**
   * Show a list of all logestadopedidos.
   * GET logestadopedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params }) {

    const historicos = await Database.select('log.id','esold.designacao as oldEstado','esnew.designacao as newEstado','log.observacao_old','log.observacao_new', Database.raw('DATE_FORMAT(log.created_at, "%d-%m-%Y %H:%i:%s") as created_at'),'user.nome as operador')
      .table('log_estado_pedidos as log')
      .leftJoin('estado_pedidos as esold','esold.id','log.id_estado_anterior')
      .leftJoin('estado_pedidos as esnew','esnew.id','log.id_estado_novo')
      .innerJoin('users as user','user.id','log.user_id')
      .where('log.pedido_id', params.id)
      .orderBy('log.id','ASC')

    return DataResponse.response("success", 200, "", historicos);
  }

  /**
   * Render a form to be used for creating a new logestadopedido.
   * GET logestadopedidos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new logestadopedido.
   * POST logestadopedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single logestadopedido.
   * GET logestadopedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing logestadopedido.
   * GET logestadopedidos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update logestadopedido details.
   * PUT or PATCH logestadopedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a logestadopedido with id.
   * DELETE logestadopedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = LogEstadoPedidoController
