'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
 
const Conta = use("App/Models/Conta");
const LogEstadoConta = use("App/Models/LogEstadoConta");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database"); 
const Mensagem = use('App/Models/ResponseMessage')
/**
 * Resourceful controller for interacting with contas
 */
class ContaController {
  /**
   * Show a list of all contas.
   * GET contas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params }) {

    /*
    const contas = await Conta.query()
      .where("cliente_id", params.id)
      .fetch();
    */
      const contas =  await Database.select('co.id as id','co.id as idConta','co.estado as estado','co.contaDescricao as contaDescricao','co.moeda_id as moeda_id', 'co.agencia_id as agencia_id', 'co.tipoFacturacao as tipoFacturacao','mo.nome as moeda','lo.nome as loja')
      .table('contas as co')
      .leftJoin("moedas as mo", "co.moeda_id", "mo.id")
      .leftJoin("lojas as lo", "co.agencia_id", "lo.id")
      .where("co.cliente_id", params.id)

    return DataResponse.response("success", 200, "", contas);
  }

  /**
   * Render a form to be used for creating a new conta.
   * GET contas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new conta.
   * POST contas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request }) {
    const {
      contaDescricao,
      cliente_id,
      moeda_id,
      agencia_id,
      estado,
      dataEstado,
      tipoFacturacao
    } = request.all();

    const conta = await Conta.create({
      contaDescricao: contaDescricao,
      cliente_id: cliente_id,
      moeda_id: moeda_id,
      agencia_id: agencia_id,
      estado: estado,
      dataEstado: dataEstado,
      tipoFacturacao: tipoFacturacao
    });
    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      conta
    );
  }

  /**
   * Display a single conta. 
   * GET contas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ request }) {  
    const { conta_id } = request.all(); 
    let contas = null;
    contas = await Database.select("co.id as contaID","co.contaDescricao as contaDescricao", "cli.nome", "cli.id","cli.contribuente").from("contas as co")
      .innerJoin("clientes as cli", "cli.id", "co.cliente_id").where("co.id",(conta_id == undefined ? "":conta_id)).first(); 
      if (contas == null) {
        return DataResponse.response("error", 500, "", null);
      }
      return DataResponse.response( "success",  200, "", contas);
  }

  /**
   * Render a form to update an existing conta.
   * GET contas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update conta details.
   * PUT or PATCH contas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a conta with id.
   * DELETE contas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}

  async selectBoxContasCliente({ params, auth }) {

    //if (auth.user.loja_id == null) {
        const contas = await Conta.query().where('estado', true).where("cliente_id", params.id).fetch();
    //}else{

    //}

    
    return DataResponse.response("success", 200, "", contas);
  }

  async getContaEstado({ params }) {
      const conta =  await Database.select('id','estado')
      .table('contas')
      .where('id', params.id).first()

      //console.log(conta)

    return DataResponse.response("success", 200, "", conta);
  }

  async updateEstado({ params, request, auth }) {
    const data = request.only(['estado']);
    const log = request.only(['estado', 'estado_actual']);

    const conta = await Conta.find(params.id);
    conta.merge(data);
    await conta.save();

    await LogEstadoConta.create({ 'conta_id': params.id, 'estado_anterior': log.estado_actual, 'user_id': auth.user.id, 'estado_novo': log.estado })

    return DataResponse.response("success", 200, "Estado actualizado com sucesso", conta);

  }

  async ContaPosPagoCliente({ request }) {
    //const { serie_id } = request.all();

    const contas = await Conta.query()
      .where("tipoFacturacao", "POS_PAGO").where('estado',true)
      .paginate(1, 1000000);

    return DataResponse.response("success", 200, "", contas);
  }

  async getClienteContas({ params, request }) {

    const dado = request.only(['conta_antiga']);

    let cliente = null;
    let contas = null;

    cliente = await Database.select(
      "cli.id as clienteID",
      "cli.nome as clienteNome"
    )
      .from("servicos as se")
      .innerJoin("contas as co", "co.id", "se.conta_id")
      .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
      .where("se.id", params.id).first()

   contas = await Database.select(
      "co.id as contaID",
      "co.contaDescricao as contaDescricao"
    )
      .from("contas as co")
      .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
      .where("co.cliente_id", cliente.clienteID)
      .whereNot("co.id", dado.conta_antiga)
      let data = {
        cliente: cliente,
        contas: contas,
  
      };

      //console.log(data)

    return DataResponse.response("success", 200, "", data);
  }


  async getClienteContasPedidos({ params }) {

    let cliente = null;
    let contas = null;

    cliente = await Database.select(
      "cli.id as clienteID",
      "cli.nome as clienteNome"
    )
      .from("pedidos as pe")
      //.innerJoin("contas as co", "co.id", "pe.conta_id")
      .innerJoin("clientes as cli", "cli.id", "pe.cliente_id")
      .where("pe.id", params.id).first()

   contas = await Database.select(
      "co.id as contaID",
      "co.contaDescricao as contaDescricao"
    )
      .from("contas as co")
      .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
      .where("co.cliente_id", cliente.clienteID)

      let data = {
        cliente: cliente,
        contas: contas,
  
      };

      //console.log(data)

    return DataResponse.response("success", 200, "", data);
  }


  async getClienteContasSearch({ params, response }) {
    let contas = null;
    contas = await Database.select(
      "co.id as contaID",
      "co.contaDescricao as contaDescricao"
    )
      .from("contas as co")
      .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
      .where("co.cliente_id", params.id);

      return response
        .status(200)
        .send(Mensagem.response(response.response.statusCode, "sucesso", contas));
   
  }

  
}

module.exports = ContaController
