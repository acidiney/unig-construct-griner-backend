"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const Charge = use("App/Models/Charge");
const LogCharge = use("App/Models/LogCharge");
const Database = use("Database");
const LogEstadoServico = use("App/Models/LogEstadoServico");

/**
 * Resourceful controller for interacting with charges
 */
class ChargeController {
  /**
   * Show a list of all charges.
   * GET charges
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, auth }) {
    const { search, orderBy, pagination,filter} = request.all(); 
  
   var periodo = "" + filter.ano + "" +  filter.mes;
   let logs = null;
    //console.log("" + filter.ano + "" +  filter.mes);
    const charges = await Database.select(Database.raw("IF(charges.is_facturado,'Facturado','Não Facturado')as facturado"),
        "charges.id", "charges.conta_id", "charges.periodo", "charges.is_facturado",
        "charges.valor", "charges.invoiceText", "charges.created_at", "clientes.nome as cliente","clientes.direccao",
        "clientes.gestor_conta",  "servicos.chaveServico",  "contas.contaDescricao as conta")
        .from("charges")
        .innerJoin("contas", "contas.id", "charges.conta_id")
        .innerJoin("clientes", "clientes.id", "contas.cliente_id")
        .innerJoin("servicos", "servicos.id", "charges.servico_id") 
        .whereIn("charges.is_facturado", filter.is_facturado == 'T' || filter.is_facturado == null ? Database.select("is_facturado").from("charges") : [filter.is_facturado])
        .whereIn("servicos.id", filter.chaveServico == null || filter.chaveServico == "" ? Database.select("id").from("servicos"): Database.select("id").from("servicos").where('chaveServico',filter.chaveServico))
       // .whereIn("clientes.gestor_conta", filter.gestor == 'T' || filter.gestor == null ? Database.select("gestor_conta").from("clientes"): [filter.gestor])
        .whereIn("clientes.direccao", filter.direccao == 'T' || filter.direccao == null ? Database.select("designacao").from("direccaos"): [filter.direccao])
        .whereIn("charges.periodo", filter.ano == null && filter.mes == null || filter.ano == "null" && filter.mes == "null" ? Database.select("periodo").from("charges"): Database.select("periodo").from("charges").where("periodo",periodo))
        .whereIn("clientes.id", search == null && (filter.gestor == 'T' || filter.gestor == null) ? Database.select("id").from("clientes") : search != null && filter.gestor != null ? Database.select("id").from("clientes").where("nome",'like','%'+search+'%').where('gestor_conta', filter.gestor) : filter.gestor != null ? Database.select("id").from("clientes").where('gestor_conta', filter.gestor) : Database.select("id").from("clientes").where("nome",'like','%'+search+'%') )
        .orderBy("charges.is_facturado","charges.periodo", "ASC")
        .paginate(pagination.page==null? 1:pagination.page, filter.is_excel==true?pagination.total:pagination.perPage);
     
       if (filter.is_excel){
         logs = await Database.select( Database.raw('DATE_FORMAT(log_charges.created_at, "%d-%m-%Y") as created_at'),
         "charges.id", "charges.conta_id", "charges.periodo", "charges.is_facturado",
         "charges.valor", "charges.invoiceText", "clientes.nome as cliente","clientes.direccao",
         "clientes.gestor_conta",  "servicos.chaveServico","log_charges.valor_old","log_charges.valor_new",
         "log_charges.observacao",
         "users.nome").from("log_charges")
         .innerJoin("charges","charges.id","log_charges.charge_id")
         .innerJoin("contas", "contas.id", "charges.conta_id")
         .innerJoin("clientes", "clientes.id", "contas.cliente_id")
         .innerJoin("servicos", "servicos.id", "charges.servico_id")
         .innerJoin("users","users.id","log_charges.user_id"); 
       }

    return DataResponse.response("success", 200, "", {charges, logs});
  }

  async log_charge({request, auth}){
        const { charge_id, periodo} = request.all();
        let resposta = null;
        resposta = await Database.select(Database.raw('DATE_FORMAT(log_charges.created_at, "%d-%m-%Y %H:%i") as created_at'),
        "log_charges.valor_old","log_charges.valor_new",
        "log_charges.observacao","users.nome as user_id")
        .from("log_charges")
        .innerJoin("users","users.id","log_charges.user_id")
        .rightJoin("charges", "charges.id", "log_charges.charge_id")
        .where("charge_id",charge_id);

  return DataResponse.response("success", 200, "", resposta);
  }


  /**
   * Render a form to be used for creating a new charge.
   * GET charges/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new charge.
   * POST charges
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */ 
  async store({ request, auth }){
    const { mes, ano, flatrate_id, servico_id, conta_id, cliente_id, invoiceText} = request.all();
      
    var periodo = "" + ano + "" +  mes; 
    var moment = require("moment");


    const charges = await Database.select('*').from("charges").where('servico_id', servico_id).where('conta_id', conta_id).where('periodo', periodo).where('invoiceText', invoiceText).where('is_facturado',0)

    if(charges.length > 0){
      return DataResponse.response("error",201,"Já existe essa flatrate", charges);
    } 
   let servico = await Database.select(
    "flat_rate_servicos.id", "servicos.conta_id",
    "flat_rate_servicos.servico_id",
    "flat_rate_servicos.valor",  
    Database.raw('DATE_FORMAT(servicos.created_at, "%Y-%m-%d") as data'),
    Database.raw('DATE_FORMAT(servicos.created_at, "%Y-%m") as mesAno'),
    Database.raw('DATE_FORMAT(servicos.created_at, "%d") as dia'),
    "produtos.nome as invoiceText",
    "contas.tipoFacturacao",
    "servicos.estado"
    ).from("flat_rate_servicos")
    .innerJoin("servicos","servicos.id", "flat_rate_servicos.servico_id")
    .leftJoin("contas", "contas.id", "servicos.conta_id") 
    .innerJoin("produtos","produtos.id", "flat_rate_servicos.artigo_id") 
    .where('flat_rate_servicos.servico_id', servico_id)
    .where('servicos.conta_id', conta_id)
    .where('contas.cliente_id', cliente_id)
    .where('flat_rate_servicos.id', flatrate_id).first();

    const perido_prefact = "" + ano + "-" +(mes < 10 ? "0" + mes : mes);

    let log_estado = await LogEstadoServico.query().select(Database.raw('DATE_FORMAT(log_estado_servicos.created_at, "%Y-%m") as mesAno'),
    Database.raw('DATE_FORMAT(log_estado_servicos.created_at, "%d") as dia') ).where("servico_id",servico.servico_id)
    .where(Database.raw('DATE_FORMAT(log_estado_servicos.created_at, "%Y-%m")'),perido_prefact).orderBy("id","desc").fetch();
      log_estado = log_estado.toJSON();
      
    var dailyestadoServico = 0;
    if(log_estado.length != 0){
      dailyestadoServico = log_estado[0].dia ; 
    }

    if(servico.tipoFacturacao=='PRE-PAGO'){
      return DataResponse.response("success",500,"Escolhe uma conta POS-PAGO", null);
    }
    if(servico.estado !=1){
      return DataResponse.response("success",500,"Serviço não está activo", null);
    } 
   
    var Current = new Date(ano+"-"+mes + "-01");     
    var ultDia = new Date(Current.getFullYear(), Current.getMonth() + 1, 0); 
    var ultimoDia = moment(ultDia).format("DD");  
   
    
      var daily = (perido_prefact == servico.mesAno ? ultimoDia - servico.dia + 1 : ultimoDia) 
      var valor = (servico.valor/ ultimoDia)* daily  

      
      var dataservico = servico.created_at == null? servico.created_at:moment(servico.created_at).format("YYYY-MM");
      
      if(dailyestadoServico > 0){
       await Charge.create({  
        invoiceText: servico.invoiceText,
        valor: valor,
        servico_id: servico.servico_id,
        conta_id: servico.conta_id,
        periodo: periodo,
        user_id: auth.user.id,
        daily: daily,
        valorOriginal:  servico.valor
      })}
       else if (perido_prefact >= dataservico || dataservico == null){
       await Charge.create({  
        invoiceText: servico.invoiceText,
        valor: valor,
        servico_id: servico.servico_id,
        conta_id: servico.conta_id,
        periodo: periodo,
        user_id: auth.user.id,
        daily: daily,
        valorOriginal:  servico.valor
      })  
    }
 
    return DataResponse.response("success",200,"Registo efectuado com sucesso", servico); 

  }

  /**
   * Display a single charge.
   * GET charges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing charge.
   * GET charges/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update charge details.
   * PUT or PATCH charges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, auth }) {
    const { charge_id, valor_new, valor_old, observacao } = request.all();

    await Charge.query().where("id", charge_id).update({ valor: valor_new });

    await LogCharge.create({
      valor_new: valor_new,
      valor_old: valor_old,
      charge_id: charge_id,
      observacao: observacao,
      user_id: auth.user.id
    });

    return DataResponse.response("success", 200, "Registo Actualizado com sucesso.",null);

  }

  async anular({ params, request, auth }) {
    const { charge_id, valor_old,observacao } = request.all();

    await Charge.query().where("id", charge_id).update({ is_facturado: 2, observacao: observacao });

    await LogCharge.create({
      valor_new: valor_old,
      valor_old: valor_old,
      charge_id: charge_id,
      observacao: observacao,
      user_id: auth.user.id
    });

    return DataResponse.response("success", 200, "Registo Actualizado com sucesso.",null);

  }

  /**
   * Delete a charge with id.
   * DELETE charges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = ChargeController;
