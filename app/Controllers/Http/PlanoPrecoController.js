'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const PlanoPreco = use("App/Models/PlanoPreco");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

/**
 * Resourceful controller for interacting with planoprecos
 */
class PlanoPrecoController {
  /**
   * Show a list of all planoprecos.
   * GET planoprecos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const planoPrecos = await PlanoPreco.all();
    return DataResponse.response("success", 200, "", planoPrecos);
  }
  

  /**
   * Render a form to be used for creating a new planopreco.
   * GET planoprecos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new planopreco.
   * POST planoprecos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store({ request, auth }) {
    const { precoDescricao, imposto_id, rateTableCod, estado } = request.all(); 
 
      const planoPreco = await PlanoPreco.create({
        precoDescricao: precoDescricao,
        imposto_id: imposto_id,
        estado: estado
      });
      return DataResponse.response("success", 200, "Registo efectuado com sucesso", planoPreco);
    
  }

  /**
   * Display a single planopreco.
   * GET planoprecos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ response}) {
    const res = await Database.select('id', 'precoDescricao').from('plano_precos');
    return res;
  }

  /**
   * Render a form to update an existing planopreco.
   * GET planoprecos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update planopreco details.
   * PUT or PATCH planoprecos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a planopreco with id.
   * DELETE planoprecos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }


  async planoPreco(){
    const planoPrecos = await PlanoPreco.all();
    return DataResponse.response("success", 200, "", planoPrecos);
  }
   
}

module.exports = PlanoPrecoController
