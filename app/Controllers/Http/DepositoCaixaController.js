'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DepositoCaixa = use("App/Models/DepositoCaixa");
const Caixa = use("App/Models/Caixa");
const Loja = use("App/Models/Loja");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database"); 

/**
 * Resourceful controller for interacting with depositocaixas
 */
class DepositoCaixaController {
  /**
   * Show a list of all depositocaixas.
   * GET depositocaixas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  async listagemDepositos({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "de.valor_deposito as valor_deposito",
        "de.data_deposito as data_deposito",
        "de.created_at",
        "de.referencia_banco as referencia_banco",
        "us.nome as userNome",
        "ba.nome as bancoNome"
      )
        .from("deposito_caixas as de")
        .innerJoin("bancos as ba", "ba.id", "de.banco_id")
        .innerJoin("users as us", "us.id", "de.user_id")
        .orderBy(orderBy == null ? "de.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "de.valor_deposito as valor_deposito",
        "de.data_deposito as data_deposito",
        "de.created_at",
        "de.referencia_banco as referencia_banco",
        "us.nome as userNome",
        "ba.nome as bancoNome"
      )
        .from("deposito_caixas as de")
        .innerJoin("bancos as ba", "ba.id", "de.banco_id")
        .innerJoin("users as us", "us.id", "de.user_id")
        .where("de.valor_deposito", "like", "%" + search + "%")
        .orWhere("us.nome", "like", "%" + search + "%")
        .orWhere("de.data_deposito", "like", "%" + search + "%")
        .orWhere("ba.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(de.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "de.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new depositocaixa.
   * GET depositocaixas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new depositocaixa.
   * POST depositocaixas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const {
      valor_deposito,
      referencia_banco,
      observacao,
      data_deposito,
      banco_id,
      caixas
    } = request.all();

    if (auth.user.loja_id == null) {
        return DataResponse.response( "info", 201,"Caro operador, lamentamos informar que essa operação é exclusivamente para operador Loja.", null );
    }

    const getCountLoja = await Loja.query().where('user_chefe_id', auth.user.loja_id).whereNotNull('user_chefe_id').getCount();

    if (getCountLoja) {
      return DataResponse.response("info", 201, "Caro operador, lamentamos informe, mas não tens permissão para efectuar essa operação", null);
    }

    var moment = require("moment"); 


    var hoje = new Date();
    var dataOntem = new Date(hoje.getTime());
    dataOntem.setDate(hoje.getDate() - 1);   
    dataOntem = moment(dataOntem).format("YYYY-MM-DD"); 
 
 
    const cc = await Caixa.query().whereBetween('data_abertura', ['2019-12-10', dataOntem]).where('is_active', 1).where("loja_id", auth.user.loja_id).getCount();

    if (cc > 0) {
      return DataResponse.response( "info", 201, "Caro operador chefe de loja, lamentamos informe que não será possivel efectuar o desposito enquanto existir caixa aberto na tua loja. Existe operador que não fecho o caixa dos dias anteriores", null );
    }


    const getCount = await DepositoCaixa.query().where('referencia_banco', referencia_banco).getCount();
    if (getCount) {
      return DataResponse.response("info", 201, "Caro operador, lamentamos informe que essa referência bancária já foi utilizada ", null);
    } else {
      //const caixa = await Database.select("*").from("caixas").where("id", caixa_id).where("is_active", false).where("user_id", auth.user.id).first();

      const d = await DepositoCaixa.create({
        valor_deposito: valor_deposito,
        data_deposito: data_deposito,
        referencia_banco: referencia_banco,
        observacao: observacao,
        banco_id: banco_id,
        user_id: auth.user.id
      });

      for (let index = 0; index < caixas.length; index++) {
        const caixa_id = caixas[index];
        var c = await Caixa.query().where("id", caixa_id).update({ deposito_id: d.id });

      }

      return DataResponse.response("success", 200, "Registado com sucesso", d);

    }
  }

  /**
   * Display a single depositocaixa.
   * GET depositocaixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing depositocaixa.
   * GET depositocaixas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update depositocaixa details.
   * PUT or PATCH depositocaixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a depositocaixa with id.
   * DELETE depositocaixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = DepositoCaixaController
