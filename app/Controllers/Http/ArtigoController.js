"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const { validate } = use("Validator");
const Produto = use("App/Models/Produto");
const DataResponse = use("App/Models/DataResponse");
const ProdutoImposto = use("App/Models/Filial");

const Filial = use("App/Models/ProdutoImposto");

const Database = use("Database");
/**
 * Resourceful controller for interacting with artigos
 */
class ArtigoController {
  /**
   * Show a list of all artigos.
   * GET artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "produtos.id",
        "produtos.nome",
        "produtos.valor",
        "produtos.quantidade",
        "produtos.barcode",
        "produtos.tipo",
        "produtos.imposto_id",
        "produtos.moeda_id",
        "produtos.is_trigger",
        "produtos.is_active",
        "produtos.created_at",
        "produtos.observacao",
        "moedas.codigo_iso",
        "moedas.nome as moeda",
        "impostos.valor as imposto_valor"
      )
        .from("produtos")
        .leftJoin("moedas", "moedas.id", "produtos.moeda_id")
        .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
        .orderBy("produtos.created_at", "ASC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "produtos.id",
        "produtos.nome",
        "produtos.valor",
        "produtos.quantidade",
        "produtos.barcode",
        "produtos.tipo",
        "produtos.imposto_id",
        "produtos.moeda_id",
        "produtos.created_at",
        "moedas.codigo_iso",
        "produtos.is_trigger",
        "produtos.is_active",
        "moedas.nome as moeda",
        "produtos.observacao",
        "impostos.valor as imposto_valor"
      )
        .from("produtos")
        .leftJoin("moedas", "moedas.id", "produtos.moeda_id")
        .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
        .where("produtos.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(produtos.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy("produtos.created_at", "ASC")
        .paginate(pagination.page, pagination.perPage);
    }

     

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Show a list of all artigos.
   * GET artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async searchArtigo({ request }) {  
    const { servico, search } = request.all(); 
    let res = null;
     var trigger = servico == null ? 0 : 1; 
     res = await Database.raw("select * from produtos where nome like '%"+search+"%' AND is_active = 1 AND is_trigger = "+trigger+" AND id not in (select produto_id from produto_tarifarios)  union select * from produtos where id in (select pt.produto_id from produto_tarifarios pt, servicos s WHERE s.tarifario_id = pt.tarifario_id AND s.id = "+servico+") limit 10");
     return DataResponse.response("success", 200, "", res[0]);
    }

  /**
   * Show a list of all artigos.
   * GET artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async selectProdutos() {
    let res = await Produto.all();
    return DataResponse.response("success", 200, "", res);
  }

  async selectServicos({ request, view, response, auth}) {
  let servicos = null;
  const { filtros} = request.all();
  
   if(filtros == null){
    servicos = await Database.select("*").from("produtos").where("tipo","S").distinct('nome').orderBy('nome', 'asc')
    }else{
      servicos = await Database.select("produtos.nome","produtos.id")
      .from("produtos")
      .innerJoin("linha_facturas", "linha_facturas.artigo_id", "produtos.id")
      .innerJoin("facturas", "facturas.id", "linha_facturas.factura_id")
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
      .innerJoin("moedas", "facturas.moeda_id", "moedas.id")
      .innerJoin("bill_runs", "facturas.id", "bill_runs.factura_utilitie_id")
      .innerJoin("bill_run_headers", "bill_runs.bill_run_header_id", "bill_run_headers.id")
      .where("bill_run_headers.ano", filtros.ano)
      .whereIn("bill_run_headers.mes", filtros.mes == 'T' || filtros.mes == null ? Database.select("mes").from("bill_run_headers") : [filtros.mes])
      .whereIn("moedas.id", filtros.moeda_id == null || filtros.moeda_id=="T" ? Database.select("id").from("moedas") : [filtros.moeda_id])
      .whereIn("clientes.direccao", filtros.direccao == 'T' || filtros.direccao == null ? Database.select("designacao").from("direccaos") : [filtros.direccao])
      .whereIn("clientes.gestor_conta", filtros.gestor == 'T' || filtros.gestor == null ? Database.select("gestor_conta").from("clientes") : [filtros.gestor])
      .where("produtos.tipo","S")
      .groupBy('produtos.nome','produtos.id')
      .orderBy('produtos.nome', 'asc')
    }
   

    return DataResponse.response("success", 200, null, servicos);
  }

  /**
   * Create/save a new artigo.
   * POST artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const data = request.all();
    const {
      nome,
      valor,
      barcode,
      tipo,
      quantidade,
      is_trigger,
      imposto_id,
      moeda_id,
      is_active,
      observacao
    } = request.all();

    const validation = await validate(data, Produto.rules);

    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages(), null);
    }

    const produto = await Produto.create({
      nome: nome,
      valor: valor,
      quantidade: quantidade,
      barcode: barcode,
      tipo: tipo,
      user_id: auth.user.id,
      imposto_id: imposto_id,
      moeda_id:moeda_id,
      is_trigger:is_trigger,
      is_active:is_active,
      observacao: observacao
    });
  

    return DataResponse.response(
      "success",
      200,
      "Produto criado com sucesso",
      produto
    );
  }

  /**
   * Display a single artigo.
   * GET artigos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ request, auth }) {
    const {
      produto_id,
      quantidade,
      desconto,
      cliente_id,
      observacao,
      preco
    } = request.all();

    

    let retorno = {
      produto_id: 0,
      nome: null,
      valor: 0.0,
      quantidade: quantidade,
      desconto: 0.0,
      total: 0.0,
      valorImposto: 0.0,
      imposto_id: null,
      linhaTotalSemImposto: 0.0,
      cliente_id: cliente_id,
      observacao: observacao,
      is_trigger:0,
      valor_original:null
    };

    const produto = await Database.select(
      "produtos.id",
      "produtos.tipo",
      "produtos.nome",
      "produtos.valor",
      "produtos.imposto_id",
      "produtos.moeda_id",
      "produtos.is_trigger",
      "impostos.valor as valor_imposto"
    ).from("produtos")
    .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
    .where("produtos.id", produto_id);
    
    retorno.valor_original = produto[0].valor;

     
    if(auth.user.loja_id !=null){

      const filial = await Database.select("impostos.valor as valor_imposto","filials.imposto_id")
        .from("lojas")
        .innerJoin("filials", "filials.id", "lojas.filial_id")
        .innerJoin("impostos", "impostos.id", "filials.imposto_id")
        .whereNotNull('filials.imposto_id')
        .where("lojas.id", auth.user.loja_id);

        if (filial.length != 0 && produto[0].tipo == "A") {
          produto[0].valor_imposto = filial[0].valor_imposto;
          produto[0].imposto_id = filial[0].imposto_id;
        }

        
    }
      

    
      if(produto[0].valor == 0){
         //await Database.table("produtos").where("id",produto_id ).update("valor", preco);
         produto[0].valor = preco
      }

     var cambio = null; 
     cambio = await Database.select('moedas.codigo_iso','cambios.valor_cambio','cambios.id').from("cambios")
     .innerJoin("moedas", "moedas.id", "cambios.moeda_id").where("moedas.id", produto[0].moeda_id).where("cambios.is_active", true).first();

     if (cambio != null) {  
       if (cambio.codigo_iso != "AOA") {   
         produto[0].valor = produto[0].valor * cambio.valor_cambio;         
       }
     }
     
    
    retorno.desconto = parseFloat(parseFloat(produto[0].valor * (desconto / 100)).toFixed(2));

    var desc = produto[0].valor - retorno.desconto;

    retorno.valorImposto = parseFloat(parseFloat( quantidade * (desc * (produto[0].valor_imposto / 100))).toFixed(2));


     retorno.linhaTotalSemImposto = parseFloat(parseFloat(desc * quantidade).toFixed(2));

    retorno.total = retorno.linhaTotalSemImposto + retorno.valorImposto;
    retorno.is_trigger = produto[0].is_trigger;
    
    //parseFloat(parseFloat(quantidade * (desc + retorno.valorImposto)).toFixed(2));

    

    retorno.produto_id = produto[0].id;
    retorno.nome = produto[0].nome;
    retorno.valor = produto[0].valor;
    retorno.imposto_id = produto[0].imposto_id;
    return DataResponse.response("success", 200, "", retorno);
  }

  /**
   * Update artigo details.
   * PUT or PATCH artigos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.only([
      "nome",
      "valor",
      "barcode",
      "tipo",
      "quantidade",
      "is_trigger",
      "imposto_id",
      "moeda_id",
      "is_active",
      "observacao"
    ]);

    const count = await Database.from("linha_facturas").where("artigo_id", params.id).getCount() // returns array
    //const total = count[0]['count("cliente_id")'];   
    if (count != 0 || count == undefined) {
      //return DataResponse.response("error", 500,"Os dados não podem ser editados porque o produto já possui facturas associada", data);
    }

    const validation = await validate(data, Produto.rules_update);
    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages(), data);
    }
    // update with new data entered
    const produto = await Produto.find(params.id);
    produto.merge(data);
    await produto.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      produto
    );
  }

  /**
   * Delete a artigo with id.
   * DELETE artigos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = ArtigoController;
