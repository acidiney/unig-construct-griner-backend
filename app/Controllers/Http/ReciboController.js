"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Factura = use("App/Models/Factura");
const Recibo = use("App/Models/Recibo");
const LinhaFactura = use("App/Models/LinhaFactura");
const LinhaRecibo = use("App/Models/LinhaRecibo");
const Cliente = use("App/Models/Cliente");
const User = use("App/Models/User");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var numeral = require("numeral");
const ReferenciaBancaria = use("App/Models/ReferenciaBancaria");
const LinhaPagamento = use("App/Models/LinhaPagamento");

const MovimentoAdiantamento = use("App/Models/MovimentoAdiantamento");

const Adiantamento = use("App/Models/Adiantamento");
var moment = require("moment");
const Pagamento = use("App/Models/Pagamento");
/**
 * Resourceful controller for interacting with recibos
 */
class ReciboController {
  /**
   * Show a list of all recibos.
   * GET recibos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, auth }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    //const docs = (auth.user.loja_id == null? ['FT'])
    if (search == null) {
      res = await Database.select(
        "recibos.id as recibo_id",
        "recibos.recibo_sigla",
        "recibos.observacao",
        "recibos.hash",
        "recibos.hash_control",
        "recibos.status",
        "recibos.status_date",
        "recibos.status_reason",
        "recibos.total",
        "recibos.user_id",
        "recibos.id",
        "recibos.numero",
        "recibos.cliente_id",
        "recibos.created_at",
        "recibos.pago",
        "recibos.serie_id",
        "recibos.user_id",
        "recibos.user_id",
        "recibos.user_id",
        "recibos.status",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla",
        "clientes.nome as clienteNome"
      )
        .from("recibos")
        .innerJoin("series", "series.id", "recibos.serie_id")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .innerJoin("clientes", "clientes.id", "recibos.cliente_id")
        .orderBy(orderBy == null ? "recibos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "recibos.id as recibo_id",
        "recibos.recibo_sigla",
        "recibos.observacao",
        "recibos.hash",
        "recibos.hash_control",
        "recibos.status",
        "recibos.status_date",
        "recibos.status_reason",
        "recibos.total",
        "recibos.user_id",
        "recibos.id",
        "recibos.numero",
        "recibos.cliente_id",
        "recibos.created_at",
        "recibos.pago",
        "recibos.serie_id",
        "recibos.user_id",
        "recibos.user_id",
        "recibos.user_id",
        "recibos.status",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla",
        "clientes.nome as clienteNome"
      )
        .from("recibos")
        .innerJoin("series", "series.id", "recibos.serie_id")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .innerJoin("clientes", "clientes.id", "recibos.cliente_id")
        .where("clientes.nome", "like", "%" + search + "%")
        .orWhere("recibos.numero", "like", "%" + search + "%")
        .orWhere("recibos.pago", "like", "%" + search + "%")
        .orWhere("recibos.status", "like", "%" + search + "%")

        .orWhere("documentos.sigla", "like", "%" + search + "%")
        .orWhere("recibos.recibo_sigla", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(recibos.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "recibos.created_at" : orderBy, "DESC")
        .paginate(pagination.page==null? 1: pagination.page, pagination.perPage);
    }

    //console.log(res)

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new recibo.
   * GET recibos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new factura.
   * POST facturas
   * @author victorino pedro vpangola@gmail.com
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const {
      facturas,
      cliente_id,
      serie_id,
      troco,  
      total_saldado,
      total_valor_recebido,
      forma_pagamento_id, 
      is_adiantamento,
      adiantamentos,
      linha_pagamentos
    } = request.all();

    var estatistica = {
      recibo: null,
      facts: [],
      contTotalFacturaSaldadas: 0,
      contTotalFacturaAbertas: 0,
      contTotalFacturaNaoAvaliadas: 0
    };
 
 
    var countRef = 0 
    for (let index = 0; index < linha_pagamentos.length; index++) {
      const r = await Database.from('referencia_bancarias').where('referencia', linha_pagamentos[index].referencia_banco).where('banco_id', linha_pagamentos[index].banco_id)
  
      if (r.length > 0) {
        countRef++;
        estatistica.facts.push(linha_pagamentos[index]); 
      }
    }

    if(countRef > 0){
      return DataResponse.response( "success", 500, "Referencias invalidas", estatistica );
    }

    const serie = await Database.select(
      "series.id as id",
      "series.nome",
      "series.proximo_numero",
      "series.activo",
      "documentos.nome as documento",
      "documentos.sigla"
    )
      .from("series")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("series.id", serie_id);

    var moment = require("moment");

    var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    var doc_date = moment(new Date()).format("YYYY-MM-DD");
    var doc_red =
      moment(new Date()).format("YYYY-MM-DD") +
      "T" +
      moment(new Date()).format("HH:mm:ss");

    var l_hash =
      doc_date +
      ";" +
      doc_red +
      ";" +
      serie[0].sigla +
      " " +
      serie[0].nome +
      "/" +
      serie[0].proximo_numero +
      ";" +
      numeral(total_saldado).format("0.00") +
      ";";

    const max_n = await Database.from("recibos")
      .where("serie_id", serie_id)
      .max("numero as total");
    var cc = 0;

    if (max_n[0].total == null) {
      cc = 0;
    } else {
      cc = max_n[0].total;
    }

    if (cc > 0) {
      const doc_ant = await Database.select("hash as hash")
        .from("recibos")
        .where("serie_id", serie_id)
        .where("numero", cc);
      l_hash = l_hash + doc_ant[0].hash;
    }
     
     const pagament = await Pagamento.create({
       valor_recebido: total_valor_recebido,
       troco: troco,
       total_pago: total_saldado,
       isTrocoAdiantamento: (troco > 0 && is_adiantamento ==1 ? true : false),
       documento_sigla: serie[0].sigla,
       user_id: auth.user.id,
     });

    const recibo = await Recibo.create({
      numero: serie[0].proximo_numero,
      recibo_sigla:  serie[0].sigla + " " + serie[0].nome + "/" + serie[0].proximo_numero,
      serie_id: serie[0].id,
      total: numeral(total_saldado).format("0.00"),
      pago: false,
      status: "N",
      status_date: data,
      hash: this.gearAss(l_hash),
      hash_control: "1", //l_hash,
      cliente_id: cliente_id,
      user_id: auth.user.id,
      pagamento_id: pagament.id
    });

    for (let index = 0; index < linha_pagamentos.length; index++) {
      

      await LinhaPagamento.create({ 
        valor_recebido: linha_pagamentos[index].valor_entrada,
        referencia: linha_pagamentos[index].referencia_banco,
        banco_id: linha_pagamentos[index].banco_id,
        data_pagamento: linha_pagamentos[index].data_pagament,
        forma_pagamento_id: linha_pagamentos[index].id,
        pagamento_id: pagament.id,
        user_id: auth.user.id
      });

      if(linha_pagamentos[index].referencia_banco!=null){
        await ReferenciaBancaria.create({ 
          referencia: linha_pagamentos[index].referencia_banco,
          data_pagamento: moment(linha_pagamentos[index].data_pagament).format("YYYY-MM-DD"),
          banco_id: linha_pagamentos[index].banco_id,
          recibo_id: recibo.id,
          user_id: auth.user.id
        });
     }    

    }
 
    estatistica.recibo = recibo 

  var ve = Math.ceil(total_valor_recebido); // valor de entrada
   
  //console.log("total_valor_recebido : " + ve);
  for (let index = 0; index < facturas.length; index++) {
      var factura = facturas[index];  
      var va = factura.valor_aberto == null && factura.pago==0 ? factura.total : factura.valor_aberto; // valor actual em aberto da factura  
      var saldado = 0  // recebe o valor saldado na factura
     if(ve > 0){ 

        ve = parseFloat(parseFloat(ve - va).toFixed(2));//Math.ceil(ve - va);
        saldado = parseFloat(parseFloat(ve < 0 ? ve - va * -1 : va).toFixed(2)); //Math.ceil((ve < 0 ? ve - va*-1 : va)) // calcula o valor saldado
        var van = parseFloat(parseFloat(va - saldado).toFixed(2));//Math.ceil(va - saldado) // calcula o novo valor em aberto para a factura 
        await LinhaRecibo.create({
          factura_id: factura.id,
          recibo_id: recibo.id,
          valor_saldado: saldado,
          user_id: auth.user.id,
          novo_valor_aberto: van
        });  
        await Database.table("facturas").where("id", factura.id).update({ "pago": (van <= 0 ? true : false),"valor_aberto": van }); 

      /* console.log("---------------------------");
       console.log("Factura : " + factura.factura_sigla);
       console.log("Factura Valor Aberto: " + factura.valor_aberto);
       console.log("Valor Entrada: " + ve);
       console.log("Valor Saldado: " + saldado);
       console.log("Valor Aberto: " + va);
       console.log("Valor Aberto Novo: " + van);
       console.log("---------------------------");*/

       estatistica.facts.push({
         factura_sigla: factura.factura_sigla,// número da factura
         saldado: saldado, // valor saldado(resto)
         va_ant: va, // valor aberto anterior
         va_new: van // valor aberto novo
       });
       
       if (van == 0) {
         estatistica.contTotalFacturaSaldadas++
       }else{
         estatistica.contTotalFacturaAbertas++
       }
      
     }else{
        estatistica.contTotalFacturaNaoAvaliadas++;
     }

   }

    /*console.log(estatistica.facts);
    console.log("facturas saldadas: " + estatistica.contTotalFacturaSaldadas);
    console.log("facturas Em aberto: " + estatistica.contTotalFacturaAbertas);
    console.log("facturas Não Avaliadas: " + estatistica.contTotalFacturaNaoAvaliadas);
    console.log("Valor Entrada: " + ve); */
     
    for (let index = 0; index < adiantamentos.length; index++) { 
        await Database.table("movimento_adiantamentos").where("id", adiantamentos[index].id).update({ saldado: true, recibo_id: recibo.id });
      }

      if (adiantamentos.length > 0 || is_adiantamento == 1) {
        let a = null
        a = await Database.table("adiantamentos").where("cliente_id", cliente_id).first();
        if (a == null) {
          a = await Adiantamento.create({
            valor: 0,
            cliente_id: cliente_id,
            descricao: "adiantamento gerado automatico pelo recibo "+recibo.recibo_sigla,
            user_id: auth.user.id
          });
        }

        if (is_adiantamento == 1 && troco > 0) { await MovimentoAdiantamento.create({ valor: troco, 
          descritivo: "adiantamento gerado automatico pelo recibo "+recibo.recibo_sigla,
          saldado: false, saldado_recibo: recibo.id, adiantamento_id: a.id, user_id: auth.user.id }); }
        await Database.raw("UPDATE adiantamentos SET valor = (SELECT SUM(valor) FROM movimento_adiantamentos WHERE saldado = 0 AND adiantamento_id = " + a.id + ") WHERE cliente_id = " + a.cliente_id);

      } 


    await Database.table("series")
      .where("id", serie[0].id)
      .update("proximo_numero", Number(serie[0].proximo_numero) + 1);

     return DataResponse.response(
      "success",
      200,
      "recibo gerado com sucesso",
      estatistica
    );  
  


  }

  /**
   * Display a single recibo.
   * GET recibos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */ 
  async show({ params }) {
    var data = await Recibo.findById(params.id);
    if (data == null) {
      return DataResponse.response("success", 500, "Nenhum resultado encontrado",null);
    }
    return DataResponse.response("success",200, "", data);
  }
  /**
   * Render a form to update an existing recibo.
   * GET recibos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update recibo details.
   * PUT or PATCH recibos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a recibo with id.
   * DELETE recibos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}

  /**
   * Update anular factura details.
   * PUT or PATCH clientes/:id
   * @author caniggiamoreira@hotmail.com ou caniggiamoreira@itgest.pt
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async printerRecibo({ params, request, response }) {
    let cliente = null;
    let user = null;
    let factura = null;
    let recibo = null;
    let conta = null; 
    let linha_pagamentos = null;
    recibo = await Database.select(
      "recibos.id as recibo_id",
      "recibos.recibo_sigla", 
      "recibos.observacao",
      "recibos.hash",
      "recibos.hash_control",
      "recibos.status",
      "recibos.status_date",
      "recibos.status_reason",
      "recibos.total",
      "recibos.user_id",
      "recibos.id",
      "recibos.numero",
      "recibos.cliente_id",
      "recibos.created_at",
      "recibos.pago",
      "recibos.serie_id",
      "recibos.user_id",
      "recibos.user_id",
      "recibos.user_id",
      "recibos.pagamento_id",

      "movimento_adiantamentos.valor as valorCriadoAdiantamento",
      "adiantamentos.valor as saldoAdiantamento",

      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla",

      "pagamentos.valor_recebido",
      "pagamentos.troco",
      "forma_pagamentos.designacao",
      "pagamentos.forma_pagamento_id",

      "movimento_adiantamentos.saldado_recibo"
    )
      .from("recibos")
      .innerJoin("series", "series.id", "recibos.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .innerJoin("pagamentos", "pagamentos.id", "recibos.pagamento_id")
      .leftJoin(
        "movimento_adiantamentos",
        "movimento_adiantamentos.saldado_recibo",
        "recibos.id"
      )
      .leftJoin(
        "adiantamentos",
        "adiantamentos.cliente_id",
        "recibos.cliente_id"
      )
      .leftJoin(
        "forma_pagamentos",
        "forma_pagamentos.id",
        "pagamentos.forma_pagamento_id"
      )
      .where("pagamentos.documento_sigla", "RC")
      .where("recibos.id", params.id)
      .first();
 
    cliente = await Cliente.find(recibo.cliente_id);

    user = await Database.select(
      "users.id",
      "users.nome",
      "users.telefone",
      "users.email",
      "users.username",
      "users.morada",
      "users.status",
      "users.created_at",
      "users.empresa_id",
      "users.role_id",
      "users.updated_at",
      "empresas.companyName",
      "empresas.addressDetail",
      "empresas.email",
      "empresas.telefone",  
      "empresas.city",
      "empresas.province",
      "empresas.taxRegistrationNumber",
      "empresas.logotipo",
      "empresas.projecto_isActive",
      "lojas.nome as loja"      
    )
      .from("users")
      .innerJoin("empresas", "empresas.id", "users.empresa_id")
      .leftJoin("lojas", "lojas.id", "users.loja_id") 
      .where("users.id", recibo.user_id)
      .first();

    factura = await Database.select(
      "facturas.id as factura_id",
      "facturas.factura_sigla",
      "facturas.observacao",
      "facturas.hash",
      "facturas.hash_control",
      "facturas.status",
      "facturas.status_date",
      "facturas.status_reason",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.user_id",
      "facturas.id",
      "facturas.numero",
      "facturas.cliente_id",
      "facturas.created_at",
      "facturas.pago",
      "facturas.serie_id",
      "facturas.user_id",
      "facturas.numero_origem_factura",
      "facturas.data_origem_factura",
      "facturas.is_nota_credito",
      "facturas.moeda_iso",
      "facturas.total_contra_valor",
      "facturas.valor_aberto",
      "facturas.valor_cambio",
      "facturas.totalKwanza",
      "facturas.user_id",
      "facturas.user_id",
      "facturas.conta_id",
      "facturas.servico_id",
      "facturas.data_vencimento",
      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla",
      "linha_recibos.valor_saldado",
      "linha_recibos.novo_valor_aberto"
    )
      .from("linha_recibos")
      .innerJoin("facturas", "linha_recibos.factura_id", "facturas.id")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .leftJoin("contas", "contas.id", "facturas.conta_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("linha_recibos.recibo_id", recibo.recibo_id);
       

      linha_pagamentos = await Database.select('*').from("linha_pagamentos")
      .innerJoin("forma_pagamentos", "forma_pagamentos.id", "linha_pagamentos.forma_pagamento_id")
      .where("linha_pagamentos.pagamento_id", recibo.pagamento_id);

    if (recibo.forma_pagamento_id != null) {
      linha_pagamentos = [{ designacao: recibo.designacao, valor_recebido: recibo.valor_recebido }]
    }
     

    let data = {
      recibo: recibo,
      cliente: cliente,
      factura: factura,
      user: user,
      linha_pagamentos: linha_pagamentos,
      conta: conta
    };

    return DataResponse.response( "success", 200, "Gerando o recibo... ",  data );
  }

  async consultarRecibo({ params }) {
    let recibos = null;

    recibos = await Database.select(
      "recibos.id as recibo_id",
      "recibos.recibo_sigla",
      "recibos.observacao",
      "recibos.hash",
      "recibos.hash_control",
      "recibos.status",
      "recibos.status_date",
      "recibos.status_reason",
      "recibos.total",
      "recibos.user_id",
      "recibos.id",
      "recibos.numero",
      "recibos.cliente_id",
      "recibos.created_at",
      "recibos.pago",
      "recibos.serie_id",
      "recibos.user_id",
      "recibos.user_id",
      "recibos.user_id",
      "recibos.status",
      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla"
    )
      .from("recibos")
      .innerJoin("series", "series.id", "recibos.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("recibos.cliente_id", params.id);
      
    if (recibos.length == 0) { 
      return DataResponse.response( "success", 500,"Nenhum resultado encontrado.", null);
    }

    return DataResponse.response("success", 200, null, recibos);
  }

  /**
   * Update anular recibo details.
   * PUT or PATCH clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async reciboAnular({ request, auth }) {
    var moment = require("moment");
    var statusData = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    const dados = {
      status: "A",
      status_reason: request.input("status_reason"),
      status_date: statusData,
      status_user_id: auth.user.id
    };
 
    // update with new data entered 
    let recibo = await Recibo.findById(request.input("recibo_id")); 

    if (recibo.status == "A") {
      return DataResponse.response( "error", 500, "O recibo já se encontra anulado.", null );
    }
 
      if (recibo.adiantamento != null) {
        if (recibo.adiantamento.saldado == 1) {
          return DataResponse.response("error", 500, recibo.serie.documento.nome + " nº " + recibo.recibo_sigla + " não pode ser anulada porque o seu adiantamento já foi utilizado", null);
        }
      } 

      const maNew = await Database.select('*').from("movimento_adiantamentos").where('saldado',0).where("saldado_recibo", recibo.id);

      if (maNew.length != 0) { 
         await Database.raw("UPDATE adiantamentos SET valor = valor - (SELECT SUM(valor) FROM movimento_adiantamentos WHERE saldado = 0 AND saldado_recibo ="+recibo.id+") WHERE cliente_id = " + recibo.cliente_id);
         const ad = { saldado: 1, status: "A", annulment_reason: request.input("status_reason"), annulment_date: statusData };
         await MovimentoAdiantamento.query().where("saldado_recibo", recibo.id).update(ad);
      }
      const maOld = await Database.select('*').from("movimento_adiantamentos").where('saldado',1).where("recibo_id", recibo.id);

      if (maOld.length != 0) { 
        await Database.raw("UPDATE adiantamentos SET valor = (SELECT SUM(valor) FROM movimento_adiantamentos WHERE saldado = 1 AND recibo_id ="+recibo.id+") WHERE cliente_id = " + recibo.cliente_id);
        await Database.raw("UPDATE movimento_adiantamentos SET saldado = 0 WHERE recibo_id = " + recibo.id);
      }
    
    const annulment = { status: "A", annulment_reason: request.input("status_reason"), annulment_date: statusData }; 
    await Pagamento.query().where("id", recibo.pagamento_id).update(annulment);

    const linhaRecibos = await Database.select('*').from("linha_recibos") .where("recibo_id", recibo.id); 
    for (let index = 0; index < linhaRecibos.length; index++) {
        const element = linhaRecibos[index]; 
        var select = (element.valor_saldado==null? "UPDATE facturas SET valor_aberto = total, pago = 0  WHERE id = "+element.factura_id : "UPDATE facturas SET valor_aberto = valor_aberto + "+element.valor_saldado+", pago = 0  WHERE id = "+element.factura_id)
        await Database.raw(select);         
    }
 
    await Recibo.query().where("id", recibo.id).update(dados);
    await Database.table("referencia_bancarias").where("recibo_id", recibo.id).update({ "estado": false});
    return DataResponse.response( "success", 200,"recibo anulado com sucesso.",recibo);
  }


  gearAss(string) {
    const crypto = require("crypto");
    const fs = require("fs");
    var s = crypto.createSign("RSA-SHA1");
    var key =
      "-----BEGIN RSA PRIVATE KEY-----\n" +
      "MIICXQIBAAKBgQC4faWshk9wvZUouz4A3K4Zzb2NOtbp262HcB1mJYF1QDs3wAnd\n" +
      "kGiqPcBx7TGeIEjuBtg6DFtSy29w1dRCANdqIDqaCqX+/PNE8dz8foCauiy5OEU2\n" +
      "segqAeN3X8PXBevqGThd/x9OPJ4pV2Kgx/oAs7Bwg3/C2AM3qraj0UulhwIDAQAB\n" +
      "AoGAW0RlQk0LXaWb9ZNzn++L/V3niMdz7Crt1JOlJ5QkUAHfibvp5X78GEQGQRXr\n" +
      "NuOX0JD4RPc58mKLldFieOh7p8B/dx8UZyWd11TUOnVwOSJaFd3rwnHzobEUJgH2\n" +
      "24b1bGOWsk+0XEisS1B7xl4d8T74+Dpnpugg4nU/1rAKgjECQQDfpe5Nihi9Fgfz\n" +
      "rcr8s9oGGdKV7nyVXUmBN5Dm5PMfAev49Wo6ZvhO9EW1mb15Kuqfc56Sq5ErDdRg\n" +
      "MPnODP8JAkEA0y2nOcjWn3ZsX0lPvGpKotnFUgO4WlpJfd6fzxTfQrLqHf6ixFPt\n" +
      "wTApqhU0fx9xMWl6m1Kh0WiegMYk8LwUDwJATafsCvh8hotzz2T1KrG4bo3g1Tau\n" +
      "A58Uus10fvfYg1fDe/qbHBRM+/1NhzUO2VfRh/Q5h2wTSAPRTmUzGBzjIQJBAIQw\n" +
      "z70cOz0WpEABZChNYOsP5rSwH3ZvjhF8igzWw+q8lFCyVLEQ2INV4r7VB0eMJw8H\n" +
      "N/iCgUjUdGOnpPgMw4ECQQCboFsTKkzrLOkZJiipgid08xPiBJCfd5Pjl7ggnwoj\n" +
      "CL9JXAsSBvOTvTuo2XnBHOpaWO8oOEUt5xBbUPGLhdaA\n" +
      "-----END RSA PRIVATE KEY-----";
    s.update(string);
    var signature = s.sign(key, "base64");
    return signature;
  }

}

module.exports = ReciboController;
