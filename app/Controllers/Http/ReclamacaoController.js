'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const Reclamacao = use("App/Models/Reclamacao");
const LogEstadoReclamacao = use("App/Models/LogEstadoReclamacao");
const TipoReclamacao = use("App/Models/TipoReclamacao");
const Database = use("Database");
/**
 * Resourceful controller for interacting with reclamacaos
 */
class ReclamacaoController {
  /**
   * Show a list of all reclamacaos.
   * GET reclamacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ params}) {
    //const pedidos = await Pedido.query().where("cliente_id", params.id).fetch(); 
    const reclamacoes = await  Database.select(Database.raw('DATE_FORMAT(re.created_at, "%d-%m-%Y") as dataReclamacao'),'re.id','re.observacao as observacao','pri.designacao as prioridadeDesignacao','tr.designacao as tipoReclDesignacao')
          .table('reclamacaos as re')
          .leftJoin('tipo_reclamacaos as tr', 'tr.id', 're.tipo_reclamacao_id')
          .leftJoin('prioridades as pri', 'pri.id', 're.prioridade_id')
          .where("re.cliente_id", params.id)
 
    return DataResponse.response("success", 200, "", reclamacoes);
  }

  async tiposelectBox() {
    const tipos = await Database.select('*')
    .table('tipo_reclamacaos')
    .orderBy('designacao', 'ASC');
    return DataResponse.response("success", 200, "", tipos);
  }
  
  async listagemReclamacoes({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "reclamacaos.id as idReclamacao",
        "reclamacaos.observacao as observacao",
        "reclamacaos.created_at as created_at",
        "prioridades.designacao as prioridade",
        "tipo_reclamacaos.designacao as tipoReclamacao",
        "est.designacao as estadoDesignacao",
        "est.sigla as siglaEstado",
        "clientes.nome as clienteNome",
        "clientes.telefone as clienteTelefone"
      )
        .from("reclamacaos")
        .innerJoin("clientes", "clientes.id", "reclamacaos.cliente_id")
        .innerJoin("tipo_reclamacaos", "tipo_reclamacaos.id", "reclamacaos.tipo_reclamacao_id")
        .innerJoin("prioridades", "prioridades.id", "reclamacaos.prioridade_id")
        .leftJoin("estado_reclamacaos as est", "est.id", "reclamacaos.estado_reclamacao_id")
        .orderBy(orderBy == null ? "reclamacaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "reclamacaos.id as idReclamacao",
        "reclamacaos.observacao as observacao",
        "reclamacaos.created_at as created_at",
        "prioridades.designacao as prioridade",
        "tipo_reclamacaos.designacao as tipoReclamacao",
        "est.designacao as estadoDesignacao",
        "est.sigla as siglaEstado",
        "clientes.nome as clienteNome",
        "clientes.telefone as clienteTelefone"
      )
        .from("reclamacaos")
        .innerJoin("clientes", "clientes.id", "reclamacaos.cliente_id")
        .innerJoin("tipo_reclamacaos", "tipo_reclamacaos.id", "reclamacaos.tipo_reclamacao_id")
        .innerJoin("prioridades", "prioridades.id", "reclamacaos.prioridade_id")
        .leftJoin("estado_reclamacaos as est", "est.id", "reclamacaos.estado_reclamacao_id")
        .where("clientes.nome", "like", "%" + search + "%")
        .orWhere("clientes.telefone", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(reclamacaos.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "reclamacaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async listagemTipoReclamacao({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "tipo_reclamacaos.id",
        "tipo_reclamacaos.designacao",
        "tipo_reclamacaos.created_at"
      )
        .from("tipo_reclamacaos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "tipo_reclamacaos.id",
        "tipo_reclamacaos.designacao",
        "tipo_reclamacaos.created_at"
      )
        .from("tipo_reclamacaos")
        .where("tipo_reclamacaos.designacao", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(tipo_reclamacaos.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new reclamacao.
   * GET reclamacaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new reclamacao.
   * POST reclamacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { cliente_id, observacao, tipo_reclamacao_id, prioridade_id } = request.all(); 
 
      const reclamacao = await Reclamacao.create({
      cliente_id: cliente_id,  
		  observacao: observacao, 
      tipo_reclamacao_id: tipo_reclamacao_id,
      prioridade_id: prioridade_id,
      //user_id: auth.user.id
     });
     
   
      return DataResponse.response("success", 200, "Reclamação registado com sucesso", reclamacao);    
  
  }
  async store_portal({ request, auth }) {
    const { cliente_id, observacao, tipo_reclamacao_id, prioridade_id, estado_reclamacao_id } = request.all(); 
 
      const reclamacao = await Reclamacao.create({
      cliente_id: cliente_id,  
		  observacao: observacao, 
      tipo_reclamacao_id: tipo_reclamacao_id,
      prioridade_id: prioridade_id,
      estado_reclamacao_id:estado_reclamacao_id
      //user_id: auth.user.id
     });
     
   
      return DataResponse.response("success", 200, "Reclamação registado com sucesso", reclamacao);    
  
  }
  async storeTipoReclamacao({ request, auth }) {
    const {
      designacao
    } = request.all();
    const tipoReclamacao = await TipoReclamacao.query()
      .where("designacao", designacao)
      .getCount();
    if (tipoReclamacao > 0) {
      return DataResponse.response(
        "success",
        500,
        "Já existe um tipo de reclamação com as mesma designação",
        tipoReclamacao
      );
    } else {
      const tipoReclamacao = await TipoReclamacao.create({
        designacao: designacao,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Tipo de reclamação registado com sucesso",
        tipoReclamacao
      );
    }
  }

  async estadosReclamacao() {
    const estados = await Database.select('id','designacao').table('estado_reclamacaos')
    .orderBy('designacao','ASC');
    return DataResponse.response("success", 200, "", estados);
  }

  async selectBox({ params }) {

    const pedidoEstado = await Database.select('estado_reclamacao_id as estado','id', 'observacao')
      .table('reclamacaos')
      .where('id', params.id).first()

    return DataResponse.response("success", 200, "", pedidoEstado);
  }

  async updatEstado({ params, request, auth }) {
    const data = request.only(['estado_reclamacao_id','observacao']);
    const log = request.only(['estado_reclamacao_id', 'estado_actual', 'observacao_old']);

    const reclamacao = await Reclamacao.find(params.id);
    reclamacao.merge(data);
    await reclamacao.save();

    await LogEstadoReclamacao.create({ 'reclamacao_id': params.id, 'id_estado_anterior': log.estado_actual, 'user_id': auth.user.id, 'id_estado_novo': log.estado_reclamacao_id, 'observacao_old' :log.observacao_old, 'observacao_new': data.observacao })

    return DataResponse.response("success", 200, "Estado actualizado com sucesso", reclamacao);

  }

  /**
   * Display a single reclamacao.
   * GET reclamacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing reclamacao.
   * GET reclamacaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update reclamacao details.
   * PUT or PATCH reclamacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async updateTipoReclamacao({ params, request }) {
    const data = request.only([
      "designacao"
    ]);

    //console.log(data)

    const tipoReclamacao = await TipoReclamacao.query()
    .where("designacao", data.designacao)
    .whereNot({ id: params.id })
    .getCount();
  if (tipoReclamacao > 0) {
    return DataResponse.response(
      "success",
      500,
      "Já existe um tipo de reclamação com as mesma designação",
      tipoReclamacao
    );
  }else{

    // update with new data entered
    const tipoReclamacao = await TipoReclamacao.find(params.id);
    tipoReclamacao.merge(data);
    await tipoReclamacao.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tipoReclamacao
    );

  }
  }

  /**
   * Delete a reclamacao with id.
   * DELETE reclamacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ReclamacaoController
