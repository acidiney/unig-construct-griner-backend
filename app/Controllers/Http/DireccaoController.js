'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Direccao = use("App/Models/Direccao");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with direccaos
 */
class DireccaoController {
  /**
   * Show a list of all direccaos.
   * GET direccaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "id",
        "designacao",
        "slug",
        "created_at"
      )
        .from("direccaos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "id",
        "designacao",
        "slug",
        "created_at"
      )
        .from("direccaos")
        .where("designacao", "like", "%" + search + "%")
        .orWhere("slug", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new direccao.
   * GET direccaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new direccao.
   * POST direccaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { designacao, slug } = request.all();
    const direccao = await Direccao.query()
      .where("designacao", designacao)
      .getCount();
    if (direccao > 0) {
      return DataResponse.response(
        "success",
        500,
        "Já existe uma direcção com a mesma designação",
        direccao
      );
    } else {
      const direccao = await Direccao.create({
        designacao: designacao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Direcção registada com sucesso",
        null
      );
    }
  }

  async selectBox () {

    const res = await Database.select('id','designacao')
    .from('direccaos')
    .orderBy('designacao','ASC')

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Display a single direccao.
   * GET direccaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing direccao.
   * GET direccaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update direccao details.
   * PUT or PATCH direccaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["designacao","slug"]);

    //console.log(data)

    const direccao = await Direccao.query()
    .where("designacao", data.designacao)
    .whereNot({ id: params.id })
    .getCount();

    //console.log(params.id)
  if (direccao > 0) {
    return DataResponse.response(
      "success",
      500,
      "Já existe uma direcção com a mesma designação",
      direccao
    );
  }else{

    // update with new data entered
    const direccao = await Direccao.find(params.id);
    direccao.merge(data);
    await direccao.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );

  }
  }

  /**
   * Delete a direccao with id.
   * DELETE direccaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = DireccaoController
