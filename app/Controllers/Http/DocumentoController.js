'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Documento = use('App/Models/Documento');
const Database = use('Database');
const DataResponse = use("App/Models/DataResponse");

/**
 * Resourceful controller for interacting with documentos
 */
class DocumentoController {
  /**
   * Show a list of all documentos.
   * GET documentos
   *
   * @author vitorino pedro vpangola@gmail.com
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index () { 
    const documentos = await Documento.all();;
    return DataResponse.response("success", 2000, null, documentos); 
  }

  /**
   * Create/save a new documento.
   * POST documentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, auth }) {
    const {nome, sigla} = request.all();
    const documento =  await Database.select('id').from('documentos').where('nome', nome).first();

    if(documento == null || documento == undefined ){
      const documento = await Documento.create({nome: nome, sigla: sigla, user_id: auth.user.id});
    }else{
      return {
        type: "error",
        message: "Já existe um documento "+nome
      };
    }

    return documento;
  }

  /**
   * Display a single documento.
   * GET documentos/:id
   *@author victorino pedro vpangola@gmail.com
   */
  async show () {

    /*const docs = await Database.from('series').select('documento_id as id').distinct();
    let result = [];
    for(var i in docs)
      result[i] = docs[i].id;*/
    //return docs;
    const documentos = await Database.select('id', 'nome', 'sigla').from('documentos');
    return documentos;
  }

  /**
   * Render a form to update an existing documento.
   * GET documentos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update documento details.
   * PUT or PATCH documentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const { nome, sigla } = request.all();  


      var d = await Documento.query()
        .where("id", params.id)
        .update({ nome: nome, sigla: sigla });

      return DataResponse.response("success", 200, "Registado actualizado com sucesso", d);
    
     
  }

  /**
   * Delete a documento with id.
   * DELETE documentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = DocumentoController
