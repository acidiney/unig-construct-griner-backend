'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Serie = use('App/Models/Serie');
const Database = use('Database');
const DataResponse = use('App/Models/DataResponse');

/**
 * Resourceful controller for interacting with series
 */
class SerieController {
  /**
   * Show a list of all series.
   * GET series
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "series.id",
        "series.nome",
        "series.proximo_numero",
        "series.activo",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.documento_id",
        "series.created_at"
      )
        .from("series")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "series.id",
        "series.nome",
        "series.proximo_numero",
        "series.activo",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.documento_id",
        "series.created_at"
      )
        .from("series")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .where("series.nome", "like", "%" + search + "%")
        .orWhere("documentos.nome", "like", "%" + search + "%")
        .orWhere("series.proximo_numero", "like", "%" + search + "%")
        .orWhere("series.movimento", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(series.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Show a list of all series.
   * GET series
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const series = await Database.select(
      "series.id",
      "series.nome",
      "series.proximo_numero",
      "series.activo",
      "documentos.nome as documento",
      "documentos.sigla",
      "series.movimento",
      "series.tipo_movimento",
      "series.created_at"
    )
      .from("series")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("activo", true);
    return series;
  }

  /**
   * Create/save a new serie.
   * POST series
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const {
      nome,
      proximo_numero,
      movimento,
      tipo_movimento,
      activo,
      documento_id
    } = request.all();
    const serie = await Serie.query()
      .where("nome", nome)
      .where("documento_id", documento_id)
      .getCount();
    if (serie > 0) {
      return DataResponse.response(
        "success",
        500,
        "Já existe uma série com as mesmas configurações",
        serie
      );
    } else {
      const serie = await Serie.create({
        nome: nome,
        proximo_numero: proximo_numero,
        activo: activo,
        movimento: movimento,
        tipo_movimento: tipo_movimento,
        documento_id: documento_id,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Serie registado com sucesso",
        serie
      );
    }
  }

  /**
   * Display a single serie.
   * GET series/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing serie.
   * GET series/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update Serie details.
   * PUT or PATCH clientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.only([
      "nome",
      "tipo_movimento",
      "activo",
      "movimento",
      ""
    ]);

    /*
    const validation = await validate(data, Serie.rules);
    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages(), data);
    }*/
    // update with new data entered
    const serie = await Serie.find(params.id);
    serie.merge(data);
    await serie.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      serie
    );
  }

  /**
   * Delete a serie with id.
   * DELETE series/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}

  async selectSerieLojaBox() {
    const series_lojas = await Database.select("serie_id")
      .from("lojas")
      .whereNotNull("serie_id");
    let sl = [];
    series_lojas.forEach(element => {
      sl.push(element.serie_id);
    });

    const series = await Database.select(
      "series.id",
      "series.nome",
      "series.proximo_numero",
      "series.activo",
      "documentos.nome as documento",
      "documentos.sigla",
      "series.movimento",
      "series.tipo_movimento",
      "series.created_at"
    )
      .from("series")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("activo", true)
      .where("documentos.sigla", "FR")
      .whereNotIn("series.id", sl);

    return DataResponse.response("success", 200, "", series);
  }

  async selectSeriesRecibosNotInLojasBox() {
    const series = await Database.select(
      "series.id",
      "series.nome",
      "series.proximo_numero",
      "series.activo",
      "documentos.nome as documento",
      "documentos.sigla",
      "series.movimento",
      "series.tipo_movimento",
      "series.created_at"
    )
      .from("series")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("activo", true)
      .where("documentos.sigla", "RC")
      .whereNotIn(
        "series.id",
        Database.select("serie_id_recibo")
          .from("lojas")
          .whereNotNull("serie_id_recibo")
      );

    return DataResponse.response("success", 200, "", series);
  }

  async selectSerieLojaUserBox({ auth }) {
    if (auth.user.loja_id == null) {
      const s = await Database.select(
        "series.id",
        "series.nome",
        "series.proximo_numero",
        "series.activo",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.created_at"
      )
        .from("series")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .where("activo", true);
      let series = [];
      s.forEach(element => {
        if (element.sigla == "FT" || element.sigla == "FR") {
          series.push(element);
        }
      });
      return DataResponse.response("success", 200, "", series);
    } else {
      let series = null;
      series = await Database.select(
        "series.id",
        "series.nome",
        "series.proximo_numero",
        "series.activo",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.created_at"
      )
        .from("series")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .innerJoin("lojas", "lojas.serie_id", "series.id")
        .where("activo", true)
        .where("lojas.id", auth.user.loja_id)
        .where("documentos.sigla", "FR");
      return DataResponse.response("success", 200, "", series);
    }
  }

  async selectBoxSerieOrcamento({ auth }) {
    if (auth.user.loja_id == null) {
      const series = await Database.select(
        "series.id",
        "series.nome",
        "series.proximo_numero",
        "series.activo",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.created_at"
      )
        .from("series")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .whereIn("documentos.sigla", ["OR"])
        .where("activo", true);

      return DataResponse.response("success", 200, "", series);
    } else {
      let series = null;
      series = await Database.select(
        "series.id",
        "series.nome",
        "series.proximo_numero",
        "series.activo",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.created_at"
      )
        .from("series")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .innerJoin("lojas", "lojas.serie_id", "series.id")
        .where("activo", true)
        .whereIn("documentos.sigla", ["OR"])
        .where("lojas.id", auth.user.loja_id);
      return DataResponse.response("success", 200, "", series);
    }
  }

  async selectSerieRecibosBox({ auth }) {
    if (auth.user.loja_id == null) {
      const series = await Database.select(
        "series.id",
        "series.nome",
        "series.proximo_numero",
        "series.activo",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.created_at"
      )
        .from("series")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .whereIn("documentos.sigla", ["RC"])
        .where("activo", true);

      return DataResponse.response("success", 200, "", series);
    } else {
      let series = null;
      series = await Database.select(
        "series.id",
        "series.nome",
        "series.proximo_numero",
        "series.activo",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.created_at"
      )
        .from("series")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .innerJoin("lojas", "lojas.serie_id_recibo", "series.id")
        .whereIn("documentos.sigla", ["RC"])
        .where("activo", true)
        .where("lojas.id", auth.user.loja_id);
      return DataResponse.response("success", 200, "", series);
    }
  }

  async selectBoxSeries({ request }) {
    const { documentos } = request.all() 

      const series = await Database.select(
        "series.id",
        "series.nome",
        "series.proximo_numero",
        "series.activo",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.created_at"
      ).from("series").innerJoin("documentos", "documentos.id", "series.documento_id")
      .whereIn("documentos.sigla", documentos)
      .where("activo", true);

      return DataResponse.response("success", 200, null, series); 
  }
}

module.exports = SerieController
