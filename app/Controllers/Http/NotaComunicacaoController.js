'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use('App/Models/DataResponse');
const NotaComunicacao = use('App/Models/NotaComunicacao');
const Database = use("Database");
/**
 * Resourceful controller for interacting with notacomunicacaos
 */
class NotaComunicacaoController {
   
  /**
   * Show a list of all notacomunicacaos.
   * GET notacomunicacaos
   * 
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const filters = request.except(["page"]);
    const page = request.input("page"); 
    const r = await NotaComunicacao.getAllCommunicationNotes(page, filters);
    return DataResponse.response("success", 200, "", r);
  }

  /**
   * Create/save a new notacomunicacao.
   * POST notacomunicacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const data = request.all();
    // create with new data entered 
    const r = await NotaComunicacao.createNotaComunicacao(auth.user.id, data);
    return DataResponse.response("success",200,"Dados actualizados com sucesso",r);
  }

  /**
   * Display a single notacomunicacao.
   * GET notacomunicacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params }) {
    const r = await NotaComunicacao.findById(params.id)
    return DataResponse.response("success", 2000, null, r);
  }
 
  /**
   * Update notacomunicacao details.
   * PUT or PATCH notacomunicacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.all();     
    const r = await NotaComunicacao.updateNotaComunicacao(params.id, data);
    return DataResponse.response("success", 200, "Dados actualizados com sucesso",  r );
  }
  /**
   * Delete a notacomunicacao with id.
   * DELETE notacomunicacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params }) { 
    const r = await NotaComunicacao.deleteNotaComunicacao(params.id);
    return DataResponse.response("success",200,r,null);
  }

  async status({ params, request }) {
    const data = request.only(["status"]); 
    const r = await NotaComunicacao.statusNotaComunicacao(params.id, data);
    return DataResponse.response("success", 200, r, null);
  }
}

module.exports = NotaComunicacaoController
