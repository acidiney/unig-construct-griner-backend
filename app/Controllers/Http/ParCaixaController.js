'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const ParCaixa = use('App/Models/ParCaixa');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database'); 
/**
 * Resourceful controller for interacting with parcaixas
 */
class ParCaixaController {
  /**
   * Show a list of all parcaixas.
   * GET parcaixas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new parcaixa.
   * GET parcaixas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new parcaixa.
   * POST parcaixas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { descricao, central_id } = request.all(); 
 
      const res = await ParCaixa.create({
        descricao: descricao,
        central_id: central_id,
        user_id: auth.user.id
      });
      return DataResponse.response("success", 200, "Registo efectuado com sucesso", res);
    
  }

  /**
   * Display a single parcaixa.
   * GET parcaixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing parcaixa.
   * GET parcaixas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update parcaixa details.
   * PUT or PATCH parcaixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a parcaixa with id.
   * DELETE parcaixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ParCaixaController
