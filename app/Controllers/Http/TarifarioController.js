'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Tarifario = use("App/Models/Tarifario");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

/**
 * Resourceful controller for interacting with tarifas
 */
class TarifarioController {
  /**
   * Show a list of all tarifas.
   * GET tarifas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "tarifarios.id",
        "tarifarios.descricao",
        "tarifarios.condicoes",
        "tarifarios.tecnologia_id",
        "tarifarios.plano_preco_id",
        "tarifarios.credMensalInDefault",
        "plano_precos.precoDescricao as planoPrecoDescricao",
        "tecnologias.nome as tecnologiaDescricao",
        "tarifarios.estado",
        Database.raw(
          'DATE_FORMAT(tarifarios.dataEstado, "%Y-%m-%d") as dataEstado'
        ),
        "tarifarios.created_at"
      )
        .from("tarifarios")
        .innerJoin(
          "plano_precos",
          "plano_precos.id",
          "tarifarios.plano_preco_id"
        ).innerJoin(
          "tecnologias",
          "tecnologias.id",
          "tarifarios.tecnologia_id"
        )
        .orderBy(orderBy == null ? "tarifarios.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "tarifarios.id",
        "tarifarios.descricao",
        "tarifarios.condicoes",
        "tarifarios.plano_preco_id",
        "tarifarios.tecnologia_id",
        "tarifarios.credMensalInDefault",
        "plano_precos.precoDescricao as planoPrecoDescricao",
        "tecnologias.nome as tecnologiaDescricao",
        "tarifarios.estado",
        Database.raw(
          'DATE_FORMAT(tarifarios.dataEstado, "%Y-%m-%d") as dataEstado'
        ),
        "tarifarios.created_at"
      )
        .from("tarifarios")
        .innerJoin(
          "plano_precos",
          "plano_precos.id",
          "tarifarios.plano_preco_id"
        ).innerJoin(
          "tecnologias",
          "tecnologias.id",
          "tarifarios.tecnologia_id"
        )
        .where("tarifarios.descricao", "like", "%" + search + "%")
        .orWhere("plano_precos.precoDescricao", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(tarifarios.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "tarifarios.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async index({ request, response, view }) { }

  /**
   * Render a form to be used for creating a new tarifa.
   * GET tarifas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) { }

  /**
   * Create/save a new tarifa.
   * POST tarifas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request }) {
    const {
      descricao,
      condicoes,
      credMensalInDefault,
      plano_preco_id,
      tecnologia_id,
      estado,
      dataEstado
    } = request.all();
    //const tarifario = await Tarifario.query().where('descricao',descricao).where('plano_preco_id',plano_preco_id).getCount()
    //if(tarifario > 0){
    //return DataResponse.response("success", 500, "Já existe uma série com as mesmas configurações", tarifario);
    //}else{
    const tarifario = await Tarifario.create({
      descricao: descricao,
      condicoes: condicoes,
      credMensalInDefault: credMensalInDefault,
      plano_preco_id: plano_preco_id,
      tecnologia_id: tecnologia_id,
      estado: estado,
      dataEstado: dataEstado
    });

    //console.log(tarifario)
    return DataResponse.response(
      "success",
      200,
      "Tarifário registado com sucesso",
      tarifario
    );
    //}
  }

  /**
   * Display a single tarifa.
   * GET tarifas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) { }

  /**
   * Render a form to update an existing tarifa.
   * GET tarifas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) { }

  /**
   * Update tarifa details.
   * PUT or PATCH tarifas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only([
      "descricao",
      "condicoes",
      "credMensalInDefault",
      "plano_preco_id",
      "estado",
      "tecnologia_id",
      "dataEstado"
    ]);

    /*
    const validation = await validate(data, Serie.rules);
    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages(), data);
    }*/
    // update with new data entered
    const tarifario = await Tarifario.find(params.id);
    tarifario.merge(data);
    await tarifario.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tarifario
    );
  }

  /**
   * Delete a tarifa with id.
   * DELETE tarifas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) { }

  async tarifarios({ params }) {
    //const tarifas = await Tarifario.all();

    const tarifas = await Database.from("tarifarios").whereNotExists(
      function () {
        this.from("servicos")

          .where("servicos.conta_id", params.id)
          .where("tarifarios.id", "servicos.tarifario_id");
      }
    );

    return DataResponse.response("success", 200, "", tarifas);
  }

  async tecnologias({ params }) {

    var tecnologia = params.id.replace("%20", " ");

    /* console.log(tecnologia) */

    if (tecnologia == 'ADSL') {
          tecnologia = 'Cobre+ADSL';
    }

    const tarifas = await Database.select("ta.id", "ta.descricao")
      .table("tarifarios as ta")
      .leftJoin("plano_precos as pp", "ta.plano_preco_id", "pp.id")
      .where("ta.tecnologia", tecnologia);

    return DataResponse.response("success", 200, "", tarifas);
  }

  async planoPrecoTecnologiaSelectBox({ params }) {
    const tarifas = await Database.select("*").table("tarifarios").where("tecnologia_id", params.t);
    return DataResponse.response("success", 200, "", tarifas);
  }

  async selectBoxTarifarioNovo({ request }) {
    const { tecnologia, tarifario } = request.all()
    const tarifas = await Database.select("*").table("tarifarios").where('tecnologia', tecnologia).whereNot("id", tarifario);
    return DataResponse.response("success", 200, "", tarifas);
  }
  async selectBoxTarifarioTecnologiaInterconexao() {
    const tarifas = await Database.select("ta.id", "ta.descricao", "t.nome as tecnologia")
      .table("tarifarios as ta")
      .innerJoin("tecnologias as t", "t.id", "ta.tecnologia_id")
      .where("t.nome", 'INTERCONEXAO');
    return DataResponse.response("success", 200, "", tarifas);

  }

}

module.exports = TarifarioController;
