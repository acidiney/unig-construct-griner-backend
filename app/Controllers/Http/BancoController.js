'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const Banco = use("App/Models/Banco");
const DataResponse = use("App/Models/DataResponse"); 
const Database = use("Database");


/**
 * Resourceful controller for interacting with bancos
 */
class BancoController {
  /**
   * Show a list of all bancos.
   * GET bancos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index() {
    const bancos = await Database.select("*").from("bancos");
    return DataResponse.response("success",2000,null,bancos);
  }

  /**
   * Render a form to be used for creating a new Banco.
   * GET bancos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new Banco.
   * POST bancos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { nome, abreviatura, iban, numero_conta, activo } = request.all();
    const banco = await Banco.query().where("nome", nome).where("iban", iban).where("numero_conta", iban).getCount();

    if (banco > 0) {
      return DataResponse.response("success",  500, "Já existe uma Banco com as mesmas configurações",null);
    } else {
      const banco = await Banco.create({
        nome: nome,
        abreviatura: abreviatura,
        iban: iban,
        activo: activo,
        numero_conta:numero_conta,
        user_id: auth.user.id
      });
      return DataResponse.response("success", 200, "Banco registado com sucesso", banco);
    }
  }

  /**
   * Display a single banco.
   * GET bancos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing banco.
   * GET bancos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update banco details.
   * PUT or PATCH bancos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a banco with id.
   * DELETE bancos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = BancoController
