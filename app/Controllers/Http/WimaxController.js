'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Servico = use("App/Models/Servico");
const Wimax = use("App/Models/Wimax");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')
var moment = require("moment");

/**
 * Resourceful controller for interacting with wimaxes
 */
class WimaxController {
  /**
   * Show a list of all wimaxes.
   * GET wimaxes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  async validate({ request, response, auth }) {

    const chaveServico = request.all().chaveServico

    const filia = await Database
      .select('fi.id as filialID', 'fi.nome as filialNome')
      .table('lojas as lo')
      .innerJoin('filials as fi', 'fi.id', 'lo.filial_id')
      .where('lo.id', auth.user.loja_id).first()
    //console.log(filia)
    let res = null;
    let res2 = null;
    let servico = null;
    res = await Wimax.query().where('Mainkey', chaveServico).where('AgenciaFilialID', filia.filialID).select('*').first()
    res2 = await Wimax.query().where('EqNbrSerie', chaveServico).where('AgenciaFilialID', filia.filialID).select('*').first()

    if (res) {
      servico = await Servico.query().where('chaveServico ', res.Mainkey).select('*').first()
      if ((res.ServicoID == null) && (!servico)) {
        //encontrado e não usado
        return response.status(200).send(Mensagem.response(response.response.statusCode, 'Sucesso', res))
      } else {
        //encotrado e usado
        return response.status(302).send(Mensagem.response(response.response.statusCode, 'Ocupado!', res))
      }
    } else if (res2) {

      servico = await Servico.query().where('chaveServico ', res2.Mainkey).select('*').first()
      if ((res2.ServicoID == null) && (!servico)) {
        //encontrado e não usado
        return response.status(200).send(Mensagem.response(response.response.statusCode, 'Sucesso', res2))
      } else {
        //encotrado e usado
        return response.status(302).send(Mensagem.response(response.response.statusCode, 'Ocupado!', res2))
      }
    } else if (!res && !res2) {
      return response.status(404).send(Mensagem.response(response.response.statusCode, 'Não encontrado!', null))
    }

    /*
        //const servico = await Servico.query().where('chaveServico ', res.Mainkey).select('*').first()
        console.log(res)
    
        if(res){
            if((res.ServicoID == null) && (!servico) ){
              //encontrado e não usado
              return response.status(200).send(Mensagem.response(response.response.statusCode, 'Sucesso', res))
            }else{
              //encotrado e usado
              return response.status(302).send(Mensagem.response(response.response.statusCode, 'Ocupado!', res))
            }
        }else{
          //Não encontrado
          return response.status(404).send(Mensagem.response(response.response.statusCode, 'Não encontrado!', null))
        }
       // return DataResponse.response.status(429).send("success", 200, "", res);
       */


  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "wimaxes.id",
        "wimaxes.EqNbrSerie",
        "wimaxes.Mainkey",
        "wimaxes.MAC",
        "wimaxes.PasswordDados",
        "wimaxes.PasswordVoz",
        "wimaxes.ModeloTelefone",
        "wimaxes.AgenciaFilialID",
        "filials.id as filialID",
        "filials.nome as filialNome",
        //"lojas.nome as lojaNome",
        "wimaxes.created_at"
      )
        .from("wimaxes")
        .leftJoin("filials", "filials.id", "wimaxes.AgenciaFilialID")
        //.leftJoin("lojas", "lojas.filial_id", "filials.id")
        .orderBy(orderBy == null ? "wimaxes.id" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "wimaxes.id",
        "wimaxes.EqNbrSerie",
        "wimaxes.Mainkey",
        "wimaxes.MAC",
        "wimaxes.PasswordDados",
        "wimaxes.PasswordVoz",
        "wimaxes.ModeloTelefone",
        "wimaxes.AgenciaFilialID",
        "filials.id as filialID",
        "filials.nome as filialNome",
        //"lojas.nome as lojaNome",
        "wimaxes.created_at"
      )
        .from("wimaxes")
        .leftJoin("filials", "filials.id", "wimaxes.AgenciaFilialID")
        //.leftJoin("lojas", "lojas.filial_id", "filials.id")
        .where("wimaxes.EqNbrSerie", "like", "%" + search + "%")
        .orWhere("wimaxes.Mainkey", "like", "%" + search + "%")
        .orWhere("wimaxes.MAC", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(wimaxes.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "wimaxes.id" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    //console.log(res)

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new wimax.
   * GET wimaxes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new wimax.
   * POST wimaxes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { EqNbrSerie, Mainkey, MAC, PasswordDados, PasswordVoz, ModeloTelefone, AgenciaFilialID } = request.all();

    var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

    const numeroSerie = await Wimax.query()
      .where("EqNbrSerie", EqNbrSerie)
      .getCount();

    const main_key = await Wimax.query()
      .where("Mainkey", Mainkey)
      .getCount();

    const macInsert = await Wimax.query()
      .where("MAC", MAC)
      .getCount();

    if (numeroSerie > 0) {
      return DataResponse.response(null, 500 ,"Esse número de série já existe", numeroSerie);
      //return response.status(500).send(Mensagem.response(response.response.statusCode, 'Esse número de série já existe', numeroSerie))
    } else if (main_key > 0) {
      return DataResponse.response(
        null,
        500,
        "Esse Mainkey já existe",
        numeroSerie
      );
    } else if (macInsert > 0) {
      return DataResponse.response(
        null,
        500,
        "Esse Mac já existe",
        numeroSerie
      );
    } else {
      await Wimax.create({
        EqNbrSerie: EqNbrSerie,
        Mainkey: Mainkey,
        MAC: MAC,
        PasswordDados: PasswordDados,
        PasswordVoz: PasswordVoz,
        ModeloTelefone: ModeloTelefone,
        AgenciaFilialID: AgenciaFilialID,
        NbrSequencia: '1',
        Fabricante: '25',
        Status: '0',
        StatusData: data,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Dispositivo registado com sucesso",
        null
      );
    }
  }

  /**
   * Display a single wimax.
   * GET wimaxes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing wimax.
   * GET wimaxes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update wimax details.
   * PUT or PATCH wimaxes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["EqNbrSerie", "Mainkey", "MAC", "PasswordDados", "PasswordVoz", "ModeloTelefone", "AgenciaFilialID"]);

    //console.log(data)

    const numeroSerie = await Wimax.query()
      .where("EqNbrSerie", data.EqNbrSerie)
      .whereNot({ id: params.id })
      .getCount();

    const main_key = await Wimax.query()
      .where("Mainkey", data.Mainkey)
      .whereNot({ id: params.id })
      .getCount();

    const macInsert = await Wimax.query()
      .where("MAC", data.MAC)
      .whereNot({ id: params.id })
      .getCount();

      if (numeroSerie > 0) {
        return DataResponse.response(
          null,
          500,
          "Esse número de série já existe",
          numeroSerie
        );
      } else if (main_key > 0) {
        return DataResponse.response(
          null,
          500,
          "Esse Mainkey já existe",
          numeroSerie
        );
      } else if (macInsert > 0) {
        return DataResponse.response(
          null,
          500,
          "Esse Mac já existe",
          numeroSerie
        );
      } else {

      // update with new data entered
      const wimax = await Wimax.find(params.id);
      wimax.merge(data);
      await wimax.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        null
      );

    }
  }

  /**
   * Delete a wimax with id.
   * DELETE wimaxes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = WimaxController
