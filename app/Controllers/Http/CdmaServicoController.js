'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const CdmaServico = use("App/Models/CdmaServico");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')
const LogCdmaServico = use("App/Models/LogCdmaServico");
/**
 * Resourceful controller for interacting with cdmaservicos
 */
class CdmaServicoController {
  /**
   * Show a list of all cdmaservicos.
   * GET cdmaservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async getCDMAServico({ params }) {

    const res =  await Database.select('cs.id as cdmaServicoID', 'cs.servico_id as servicoID', 'cs.ChaveServico as chaveServico', 'cs.cdma_equipamento_id', 'ce.id as idCdmaEquip', 'ce.Num_Serie', 'cf.FabricanteDesc as fabricante', 'cm.DescricaoModelo as modelo')
      .table('cdma_servicos as cs')
      .innerJoin('cdma_equipamentos as ce', 'ce.id', 'cs.cdma_equipamento_id')
      .innerJoin('servicos as se', 'se.id', 'cs.servico_id')
      .innerJoin('cdma_fabricantes as cf', 'ce.Fabricante', 'cf.IDFabricante')
      .innerJoin('cdma_modelos as cm', 'ce.IDModelo', 'cm.IDModelo')
      .where('se.id', params.id)
      .first();
     
    console.log(res);
    if (res == undefined) {
      return DataResponse.response("error", 500, "Nenhum resultado encontrado", null);
    }
      

    return DataResponse.response("success", 200, "sucesso", res);
  }

  
  /**
   * Render a form to be used for creating a new cdmaservico.
   * GET cdmaservicos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new cdmaservico.
   * POST cdmaservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single cdmaservico.
   * GET cdmaservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing cdmaservico.
   * GET cdmaservicos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update cdmaservico details.
   * PUT or PATCH cdmaservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async updateEquipamento({ params, request, auth }) {

    const { old_cdma_equipamento_id, new_cdma_equipamento_id } = request.all();

    await Database
        .table('cdma_servicos')
        .where('id', params.id)
        .update({ 'cdma_equipamento_id': new_cdma_equipamento_id })

    await LogCdmaServico.create({ 'cdma_servico_id': params.id, 'operacao': 'UPDATE EQUIPAMENTO', 'old_cdma_equipamento_id': old_cdma_equipamento_id, 'new_cdma_equipamento_id': new_cdma_equipamento_id, 'user_id': auth.user.id})

    return DataResponse.response("success", 200, "Equipamento actualizado com sucesso", null);

  }

  /**
   * Delete a cdmaservico with id.
   * DELETE cdmaservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = CdmaServicoController
