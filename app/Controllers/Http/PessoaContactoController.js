'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const PessoaContacto = use("App/Models/PessoaContacto");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database"); 
/**
 * Resourceful controller for interacting with pessoacontactos
 */
class PessoaContactoController {
  /**
   * Show a list of all pessoacontactos.
   * GET pessoacontactos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params }) {

      const contactos =  await Database.select('pc.nome as nomeCliente','pc.telefone as telefoneCliente','pc.email as emailCliente','pc.tipo_contacto as tipo_contacto')
      .table('pessoa_contactos as pc')
      .leftJoin("clientes as cl", "pc.cliente_id", "cl.id")
      .where("pc.cliente_id", params.id)

    return DataResponse.response("success", 200, "", contactos);
  }

  /**
   * Render a form to be used for creating a new pessoacontacto.
   * GET pessoacontactos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new pessoacontacto.
   * POST pessoacontactos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const {
      nome,
      cliente_id,
      telefone,
      email,
      tipo_contacto
    } = request.all();

    const contacto = await PessoaContacto.create({
      nome: nome,
      cliente_id: cliente_id,
      telefone: telefone,
      email: email,
      tipo_contacto: tipo_contacto,
      user_id: auth.user_id
    });
    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      contacto
    );
  }

  /**
   * Display a single pessoacontacto.
   * GET pessoacontactos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing pessoacontacto.
   * GET pessoacontactos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update pessoacontacto details.
   * PUT or PATCH pessoacontactos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a pessoacontacto with id.
   * DELETE pessoacontactos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = PessoaContactoController
