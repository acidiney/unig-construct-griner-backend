'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Role = use('App/Models/Role')
const User = use('App/Models/User')

/**
 * Resourceful controller for interacting with roleusers
 */
class RoleUserController {
  /**
   * Show a list of all roleusers.
   * GET roleusers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({request, response, view}) {
  }

  /**
   * Render a form to be used for creating a new roleuser.
   * GET roleusers/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({request, response, view}) {
  }

  /**
   * Create/save a new roleuser.
   * POST roleusers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({request, response}) {
  }

  /**
   * Display a single roleuser.
   * GET roleusers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({params, request, response, view}) {
  }

  /**
   * Render a form to update an existing roleuser.
   * GET roleusers/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({params, request, response, view}) {
  }

  /**
   * Update roleuser details.
   * PUT or PATCH roleusers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({params, request, response}) {

    const {roles, userId} = request.all()
    // return roles
    const user = await User.find(userId)
    let oldRoles = await user.getRoles();

    for (let v = 0; v < oldRoles.length; v++) {
      const ro = await Role.findByOrFail('slug', oldRoles[v])
      if (roles.includes(ro.id + '') === false) {
        await user.roles().detach([ro.id])
      }
    }
    oldRoles = await user.getRoles()
    for (let x = 0; x < roles.length; x++) {
      const rol = await Role.find(roles[x])
      if (oldRoles.includes(rol.slug) === false) {
        await user.roles().attach(rol.id)
      }
    }


    response.json({
      message: "Perfis actualizados com sucesso",
      data: null
    });
  }

  /**
   * Delete a roleuser with id.
   * DELETE roleusers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({params, request, response}) {
  }
}

module.exports = RoleUserController
