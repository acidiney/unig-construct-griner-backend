'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use('App/Models/DataResponse');
const SipServico = use('App/Models/SipServico');
const SipServicoNumero = use('App/Models/SipServicoNumero');
const Numeracao = use('App/Models/Numeracao');
const Database = use("Database");

/**
 * Resourceful controller for interacting with sipservicos
 */
class SipServicoController {
 /**
   * Show a list of all sipservicos.
   * GET sipservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const { servico_id } = request.all(); 
    const sipservicos = await SipServico.query().where("servico_id", servico_id).with("servico").with("user").fetch();  
    return DataResponse.response("success", 200, "", sipservicos);
  }

  /**
   * Render a form to be used for creating a new SipServico.
   * GET sipservicos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new SipServico.
   * POST sipservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, auth, response }) {
    const data = request.only(['descricao', 'trunk_in', 'status', 'numeracao_inicio','numeracao_fim','servico_id','tecnologia_id']); 

     const getinicio = await Database.select('*').from('numeracoes').where("numero",data.numeracao_inicio).where("status",1).getCount();
     const getfim = await Database.select('*').from('numeracoes').where("numero",data.numeracao_fim).where("status",1).getCount();
      if (getinicio != 0) {

      return DataResponse.response( "success", 201,"Número "+data.numeracao_inicio+" Indisponível!",getinicio);
     }else
      if (getfim != 0) {
      return DataResponse.response( "success", 201,"Número "+data.numeracao_fim+ " Indisponível!",getfim);
     }
     
      const  count = await Database.select('*').from('numeracoes').where("tecnologia_id",data.tecnologia_id)
      .whereNotNull("tecnologia_id").whereBetween('numero', [data.numeracao_inicio,data.numeracao_fim]).where("status",1)
      .orWhereRaw('IFNULL( TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1) BETWEEN 0 and 6').whereBetween('numero', [data.numeracao_inicio,data.numeracao_fim]).getCount();
      if( count == 0){

    const trx = await Database.beginTransaction();
    try{

    
      const numers = await Numeracao.query()
      .where("tecnologia_id",data.tecnologia_id).whereNotNull("tecnologia_id").whereBetween('numero', [data.numeracao_inicio,data.numeracao_fim])
      .where("status",0).orWhereRaw('IFNULL( TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1) BETWEEN 0 and 6')
      .whereBetween('numero', [data.numeracao_inicio,data.numeracao_fim],trx).fetch();
      const numeros = numers.toJSON()

      const sip = await SipServico.create({ user_id: auth.user.id,
      numeracao_inicio: data.numeracao_inicio,
      numeracao_fim: data.numeracao_fim,
      servico_id: data.servico_id,
      descricao: data.descricao,
      trunk_in: data.trunk_in,
      status: data.status
      },trx)

      for (let index = 0; index < numeros.length; index++) {
        const element = numeros[index]; 
        const sip_numero = await SipServicoNumero.create({
        user_id: sip.user_id,
        sip_servico_id: sip.id,
        numeracao_id: element.id, 
        },trx)
         
        const update = await Numeracao.query().transacting(trx).where("id",sip_numero.numeracao_id).update({status: 1});
      }
      await trx.commit();
      return  response.status(200).send(DataResponse.response( "success", 200,"Sucesso, Sip com "+numeros.length+" Números Disponível")) ;
    }catch(error){
      await trx.rollback();
      return  response.status(500).send(DataResponse.response( "success", 500,"Ocorreu um erro Inesperado ao inserir o registo")); 
    }
    }else{
      return  response.status(201).send(DataResponse.response("success", 201,"Neste Intervalo existe "+ count +" número Indisponível!"));
    }
    
  }

  /**
   * Display a single SipServico.
   * GET sipservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params }) {  
    const sipservico = await SipServico.sipSercoByID(params.id); 
    return DataResponse.response("success", 200, "", sipservico);
  }

  /**
   * Render a form to update an existing SipServico.
   * GET sipservicos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, auth }) {
    const data = request.only(['descricao', 'trunk_in', 'status', 'numeracao_inicio', 'numeracao_fim', 'servico_id', 'tecnologia_id']);

    const sips = await SipServico.find(params.id); 
    const trx = await Database.beginTransaction();
     try{
      
      
      if (data.numeracao_inicio !== sips.numeracao_inicio || data.numeracao_fim !== sips.numeracao_fim) {

      await SipServico.updateStatusSipNumber(sips.id);
      await SipServico.deleteSipNumber(sips.id);
      await SipServico.deletesipservico(sips.id);

      const numers = await Numeracao.query()
      .where("tecnologia_id",data.tecnologia_id).whereNotNull("tecnologia_id").whereBetween('numero', [data.numeracao_inicio,data.numeracao_fim])
      .where("status",0).orWhereRaw('IFNULL( TIMESTAMPDIFF(MONTH, data_desactivacao, NOW()),-1) BETWEEN 0 and 6')
      .whereBetween('numero', [data.numeracao_inicio,data.numeracao_fim],trx).fetch();
      const numeros = numers.toJSON()

      const sip = await SipServico.create({ user_id: auth.user.id,
      numeracao_inicio: data.numeracao_inicio,
      numeracao_fim: data.numeracao_fim,
      servico_id: data.servico_id,
      descricao: data.descricao,
      trunk_in: data.trunk_in,
      status: data.status
      },trx)

      for (let index = 0; index < numeros.length; index++) {
        const element = numeros[index]; 
        const sip_numero = await SipServicoNumero.create({
        user_id: sip.user_id,
        sip_servico_id: sip.id,
        numeracao_id: element.id, 
        },trx)
         
        const update = await Numeracao.query().transacting(trx).where("id",sip_numero.numeracao_id).update({status: 1});
          
      } 
      await trx.commit();
      return  response.status(200).send(DataResponse.response( "success", 200,"Sucesso, Sip com "+numeros.length+" Números Disponível")) ;
   
    }
        sips.merge({
          descricao: data.descricao,
           trunk_in: data.trunk_in,
             status: data.status},trx);
        await sips.save(); 
        await trx.commit();

        return  response.status(200).send(DataResponse.response( "success", 200,"Sucesso, Sip Actualizada com Sucesso!")) ;
      
      } catch(error){

        await trx.rollback();
        return  response.status(500).send(DataResponse.response( "success", 500,"Ocorreu um erro Inesperado ao inserir o registo")); 
      }
 


  }

  /**
   * Update SipServico details.
   * PUT or PATCH sipservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) { 

 }
  /**
   * Delete a SipServico with id.
   * DELETE sipservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params }) {

    const sips = await SipServico.find(params.id); 

    await SipServico.updateStatusSipNumber(sips.id);
    await SipServico.deleteSipNumber(sips.id);
    await SipServico.deletesipservico(sips.id);
    return DataResponse.response( "success", 200, "Dados eliminado com sucesso", null);
  }

  async updateStatus({ params, request }) { 
    const data = request.only(['status','servico_id']); 

    const existe = await SipServico.query().where("id", params.id).select("*").first();
    if (existe == null) {
      return DataResponse.response( "success", 500, "Erro: Registo não encontrado", null);
    }  
    // update with new data entered
    const sipservicosdesable = await SipServico.query().where("servico_id",data.servico_id).update({ status: false }); 
    const sipservicos = await SipServico.query().where("id", params.id).update({ status: (data.status==true ? false: true) });
   
    return DataResponse.response( "success", 200, "Dados status actualizados com sucesso", sipservicosdesable,sipservicos);
  }
  
  async findByRangeSip({ params }) {
    //range de numeração 
    const sip = await SipServico.query().with('servico').where('numeracao_inicio', '<=', params.numero).where('numeracao_fim', '>=', params.numero).first();
    return DataResponse.response("success", 200, sip==null? "Nenhum registo encontrado":"", sip);
  }
}

module.exports = SipServicoController
