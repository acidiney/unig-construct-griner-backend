/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const LojaFormaPagamento = use("App/Models/LojaFormaPagamento");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with lojaformapagamentos
 */
class LojaFormaPagamentoController {
  /**
   * Show a list of all lojaformapagamentos.
   * GET lojaformapagamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {iew} ctx.view
   */
  async index({ request }) {
    const { id } = request.all();
    let res = null;
    res = await Database.select(
      "lfp.id",
      "fp.designacao",
      "fp.descricao",
      "lfp.created_at",
      "lfp.updated_at",
      "lfp.is_active"
    )
      .from("loja_forma_pagamentos as lfp")
      .innerJoin("forma_pagamentos as fp", "fp.id", "lfp.forma_pagamento_id")
      .where("lfp.loja_id", id);
    return DataResponse.response("success", null, null, res);
  }

  /**
   * Render a form to be used for creating a new lojaformapagamento.
   * GET lojafrmapagamentos/create
   *
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new lojaformapagamento.
   * POST lojformapagamentos
   *
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { loja_id, is_active, forma_pagamento_id } = request.all();

    const loja = await LojaFormaPagamento.create({
      loja_id: loja_id,
      forma_pagamento_id: forma_pagamento_id,
      is_active: is_active,
      user_id: auth.user.id
    });

    return DataResponse.response( "success",200,"Registo Efectuado com sucesso",loja);
  }

  /**
   * Display a single lojaformapagamento.
   * GET lojaformapagamentos/:id
   *
   * @param object} ctx
   * @param {Request} ctx. * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing lojaformapagamento.
   * GET lojaformapagamentos/:id/edit
   *
   * @param {object} ctx
   * @param Request} ctx.request
   * @param {Resp * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update lojaformapagamento details.
   * PUT or PATCH lojaformapagamentos/:id
   *
   * @param {object} ctx
   * @param {Rquest} ctx.request
   * @param { * @param {Response} ctx.response
   */
  async update({ params, request }) { 
    const data = request.only(['is_active', 'id']);      
    // update with new data entered 
    const lojaFormaPagamento = await LojaFormaPagamento.find(data.id);
    lojaFormaPagamento.merge(data);
    await lojaFormaPagamento.save();

    return DataResponse.response("success", 201, "Dados actualizados com sucesso", lojaFormaPagamento);

  }

  /**
   * Delete a lojaformapagamento with id.
   * DELETE lojaformapagamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Reponse} ctx.response
   */

  async destroy({ params, request, response }) {}

  /**
   * Show a list of all facturas.
   * GET facturas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async selectBox({ request }) {
    const { loja_id } = request.all();
    let res = null;
    let subquery = Database.select("forma_pagamento_id")
      .from("loja_forma_pagamentos")
      .where("loja_id", loja_id);
    res = await Database.select("*")
      .from("forma_pagamentos")
      .whereNotIn("id", subquery);
    return DataResponse.response("success", 200, "", res);
  }
}

module.exports = LojaFormaPagamentoController;
