'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Armario = use('App/Models/ArmarioRecursoRede');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database'); 
/**
 * Resourceful controller for interacting with armariorecursoredes
 */
class ArmarioRecursoRedeController {
  /**
   * Show a list of all armariorecursoredes.
   * GET armariorecursoredes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new armariorecursorede.
   * GET armariorecursoredes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new armariorecursorede.
   * POST armariorecursoredes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { descricao, central_id } = request.all(); 
 
      const armario = await Armario.create({
        descricao: descricao,
        central_id: central_id,
        user_id: auth.user.id
      });
      return DataResponse.response("success", 200, "Registo efectuado com sucesso", armario);
    
  }

  /**
   * Display a single armariorecursorede.
   * GET armariorecursoredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing armariorecursorede.
   * GET armariorecursoredes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update armariorecursorede details.
   * PUT or PATCH armariorecursoredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a armariorecursorede with id.
   * DELETE armariorecursoredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ArmarioRecursoRedeController
