'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const SimCard = use("App/Models/SimCard");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')
/**
 * Resourceful controller for interacting with simcards
 */
class SimCardController {
  /**
   * Show a list of all simcards.
   * GET simcards
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }
  async searchSerieSim({ request }) {
    const { start, end, search } = request.all();

    let res = null;

    if (search == null) {
     res = await Database
      .select('id', 'nome', 'imsi', 'iccid', 'estadoInventario')
      .from('sim_cards')
      .whereNot('estadoInventario','EM_SERVICO')
      //.where('estadoInventario','LIVRE')
      .orderBy('id','ASC')
      .paginate(start, end);
    } else {

      res = await Database
      .select('id', 'nome', 'imsi', 'iccid', 'estadoInventario')
      .from('sim_cards')
      .whereNot('estadoInventario','EM_SERVICO')
      //.where('estadoInventario','LIVRE')
      .where("iccid", "like", "%" + search + "%")
      .orderBy('id','ASC')
      .paginate(start, end);
    }
    

    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Render a form to be used for creating a new simcard.
   * GET simcards/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new simcard.
   * POST simcards
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single simcard.
   * GET simcards/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing simcard.
   * GET simcards/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update simcard details.
   * PUT or PATCH simcards/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a simcard with id.
   * DELETE simcards/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = SimCardController
