'use strict'
const gestaoTicket = use("App/Models/servico_suporte/criarTicket");
const fs = require("fs");
var moment = require('moment');
class SupporteController {

    //listar Categoria suporte geral
    async listarSuporteCategoria() {
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.listarPrioridadeEstadoCategoria("categoria_suportes");
    }
    // listar Categoria Apenas Activa
    async listarCatergoriaActiva() {
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.listarPrioridadeEstadoCategoria("categoria_suportes", "activo");
    }

    //Activar Categoria 
    async AtivarCategoria({ request }) {
        const { valor, id } = request.all()
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.activar_desativarfuncao(valor, id, "categoria_suportes", "Categoria")
    }

    // adicinoar  Categoriar 
    async adionarCategoria({ request }) {
        const { tipoCategoria } = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.criar_categoria_prioridade_estado(tipoCategoria, "categoria_suportes", "tipoCategoria")
    }
    // Actualizar Categoriar 

    async actualizarCategoria({ request }) {
        const { id, categoria } = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.Actualizar_categoria_prioridade_estado(id, categoria, "categoria_suportes")


    }

    // listar  Suporte Estados
    async listarSupporteEstado() {
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.listarPrioridadeEstadoCategoria("estado_supportes");
    }

    // listar Suporte Esto Apenas aqueles que estão ativa 
    async listarSupporteEstadoActiva() {
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.listarPrioridadeEstadoCategoria("estado_supportes", "activo");
    }

    // adicionar Estados
    async adicionarEstados({ request }) {
        const { estado } = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.criar_categoria_prioridade_estado(estado, "estado_supportes", "designacao")

    }

    // actualizar  Estados 
    async actualizarEstado({ request }) {
        const { id, estado_designacao } = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.Actualizar_categoria_prioridade_estado(id, estado_designacao, "estado_supportes")

    }


    // Activar Estado por default o estado é Activa
    async AtivarEstado({ request }) {
        const { valor, id } = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.activar_desativarfuncao(valor, id, "estado_supportes", "Estado")

    }
    // listar Suporte Prioridades
    async listarSuportePrioridade() {
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.listarPrioridadeEstadoCategoria("suporte_prioridades");
    }

    async listarSuportePrioridadeActiva() {
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.listarPrioridadeEstadoCategoria("suporte_prioridades", "activo");
    }

    // adicionar Prioridades
    async adicionarPrioridades({ request }) {
        const { prioridade } = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.criar_categoria_prioridade_estado(prioridade, "suporte_prioridades", "designacao")


    }
    // atualizar  Prioridade 
    async atualizarPrioridade({ request }) {
        const { id, prioridade_designacao } = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.Actualizar_categoria_prioridade_estado(id, prioridade_designacao, "suporte_prioridades")


    }
    // activar ou desativar  Prioridades
    async ativarPrioridade({ request }) {
        const { valor, id } = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.activar_desativarfuncao(valor, id, "suporte_prioridades", "Prioridade")
    }


    async CriarSupporte({ request }) {
        const { categoria, descricao, img, user_id, estado, prioridade, ip, location } = request.all();
        const gestao_ticket = new gestaoTicket();
        return   gestao_ticket.CriarSuporte(categoria, descricao, img, user_id, estado, prioridade, ip, location, "supportes");


    }


    //listar suporet 
    async ListarSuporte({ request }) {
        const { pagination, search,orderBy} = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.ListarSuporteTicket(pagination,search,orderBy);



    }
    // Actulizar Suporte 
    async atualizarSuporte({ request }) {
        const { categoria_suporte_id, suporte_prioridade_id, estado_suporte_id, descricao, id } = request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.ActualizarSuporte(categoria_suporte_id, suporte_prioridade_id, estado_suporte_id, descricao, id, "supportes");
    }
    // pega o id do ticket ou do suporte  e Manda para gestão_ticke class
    async pegarSupportId({request}){
        const {id_supporte}= request.all();
        const gestao_ticket = new gestaoTicket();
        return gestao_ticket.MostrarSuporteImg(id_supporte);

    }

    // envio ticket por email quando o user o envio apenas uma email 
    async envioTicketPorEmailController({request}){
        const{id}= request.all();
        const gestao_ticket= new gestaoTicket();
        return gestao_ticket.envioTicketPorEmail(id);

    }

    // Quando o user envia mais de um email 
    async envioTicketPorEmailMassa(){
        const gestao_ticket=  await new gestaoTicket();

        return gestao_ticket.envioMassaEmial();

    }


  

}

module.exports = SupporteController
