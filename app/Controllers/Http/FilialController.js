'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const Filial = use("App/Models/Filial");
const Loja = use("App/Models/Loja");
const Database = use("Database");


/**
 * Resourceful controller for interacting with filials
 */
class FilialController {
  /**
   * Show a list of all filials.
   * GET filials
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {}

  /**
   * Render a form to be used for creating a new filial.
   * GET filials/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new filial.
   * POST filials
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {

   
  }

  /**
   * Display a single filial.
   * GET filials/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing filial.
   * GET filials/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update filial details.
   * PUT or PATCH filials/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a filial with id.
   * DELETE filials/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}

  async selectBox() {
    const filial = await Database
    .select('*')
    .from('filials')
    .orderBy('nome','ASC')

    return DataResponse.response("success", 200, "", filial);
  }
  async selectBoxFilialPorLojas({ params }) { 
     const lojas = await Loja.query().where("filial_id", params.id).fetch();  
     return DataResponse.response("success", 200, "", lojas);
  }

  async selectPhoneIndicativo({ params }) {
    const filial = await Database
    .select('indicativo_telefone')
    .from('filials')
    .where('id', params.id).first()

    return DataResponse.response("success", 200, "", filial);
  }
}

module.exports = FilialController
