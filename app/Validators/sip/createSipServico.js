'use strict'

class createSipServico {
  get rules() {
    return {
      numeracao_inicio: "required|number|min:1|exists:numeracoes,numero",
      numeracao_fim: "required|number|min:1|exists:numeracoes,numero",
      trunk_in: "required|min:1",
      servico_id: "required|integer|unique:sip_servicos,servico_id,status,0|min:1|exists:servicos,id",
      status: "required",
    };
  }

  get messages() {
    return {
      "numeracao_inicio.required": "É obrigatório informar o",
      "numeracao_fim.required": "É obrigatório informar a ",
      "trunk_in.required": "É obrigatório informar a ",
      "servico_id.required": "É obrigatório informar o ",
      "status.required": "É obrigatório informar o ",

      "unique":"serviço já activo único",
      "exists": "É obrigatório, informe um registo valido no campo ",
      "integer": "É obrigatório, informe um número no campo ",
    };
  }

  async fails(errorMessages) {
   return this.ctx.response.status(400).send(errorMessages[0].message+" "+errorMessages[0].field);
  }
  
}

module.exports = createSipServico
