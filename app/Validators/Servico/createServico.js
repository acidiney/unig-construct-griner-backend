'use strict'

class createServico {
  get rules() {
    const tecnologia = this.ctx.request.input("tecnologia");
    const table = tecnologia == "WIMAX" ? "wimaxes, Mainkey" : "numeracoes, numero"; 
    const serie_table = tecnologia == 'CDMA' ? 'cdma_equipamentos,Num_Serie' : tecnologia == 'LTE PRE-PAGO'?'lte_cpes, numero_serie':'';
      
    const imsi_sim = tecnologia == "LTE PRE-PAGO" ? `required` : `string`;

    return {
      tecnologia_id: `required|integer|min:1|exists:tecnologias,id`,
      tarifario_id: `required|integer|min:1|exists:tarifarios,id`,
      chaveServico: `required|integer|exists:${table}`,
      dataEstado: `required|min:3|max:80`,
      numero_serie: `exists:${serie_table}`,
      imsi_sim: `${imsi_sim}`,
      conta_id: `required|integer|min:1|exists:contas,id`,
    };
  }

    //dataEstado, numero_serie, sim_card_id, imsi_sim, serie_sim, chaveServico, wimax_id, tecnologia, tecnologia_id, cdma_equipamento_id
  get messages() { 
    return {  
        'imsi_sim.required': "É obrigatório informar o ICCID do SIM",
        'required': "É obrigatório informar o",
        'chaveServico.required': "É obrigatório infome o Nº Telefone /", 
        'chaveServico.exists': "É obrigatório infome um registo valido no campo Nº Telefone / ",
        'exists': "É obrigatório infome um registo valido no campo ",
        'unique': "É obrigatório infome um registo de caracter único no campo ",
        'integer': "É obrigatório informe um número no campo ",
    };
  }

  async fails(errorMessages) {
    return this.ctx.response
      .status(400)
      .send(errorMessages[0].message + " " + errorMessages[0].field.replace("_id","").replace("_"," "));
  }
}

module.exports = createServico
