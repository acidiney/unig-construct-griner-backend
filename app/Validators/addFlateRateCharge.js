'use strict'

class addFlateRateCharge {
  get rules () {
    return {
      conta_id: "required|number",
      servico_id: "required|number",
      flatrate_id:"required|number",
      invoiceText: "required|min:1",
      //valor: "required|number|min:1",
      mes: "required",
      ano:"required",
      // validation rules
    }
  }

  get messages() {
    return {
      "invoiceText.required": "É obrigatório informar o Artigo",
      "valor.required": "É obrigatório informar a ",
      "servico_id.required": "É obrigatório informar o Serviço ",
      "conta_id.required": "É obrigatório informar a Conta",
      "mes.required": "É obrigatório informar o Mes ",
      "ano.required": "É obrigatório informar o Ano",
      "flatrate_id.required": "É obrigatório informar a Flat Rate",
     
    };
  }
  async fails(errorMessages) {
    return this.ctx.response.status(400).send(errorMessages[0].message);
   }
}

module.exports = addFlateRateCharge
