"use strict";

class rangeStartEndNumberExist {
  get rules() {
    return {
      startNumber: "required|number|min:1|exists:numeracoes,numero",
      endNumber: "required|number|min:1|exists:numeracoes,numero",
    };
  }

  get messages() {
    return {
      "startNumber.required": "É obrigatório informar o",
      "endNumber.required": "É obrigatório informar a ",
      exists: "É obrigatório informe um registo valido no campo ",
      integer: "É obrigatório informe um número no campo ",
    };
  }

  async fails(errorMessages) {
    return this.ctx.response.status(400).send(errorMessages[0].message + " " + errorMessages[0].field);
  }
}

module.exports = rangeStartEndNumberExist;
