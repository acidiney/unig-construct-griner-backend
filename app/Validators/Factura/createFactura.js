'use strict'

class facturaCreateFactura {
  get rules() {
    return {
      produtos: "required",
      serie_id: "required|integer|min:1|exists:series,id",
    };
  }
  get messages() {
    return { 

      "serie_id.required": "É obrigatório informar a ",

      "exists": "É obrigatório infome um registo valido no campo ",
      "integer": "É obrigatório informe um número no campo ",
    };
  }

  async fails(errorMessages) {
   return this.ctx.response.status(400).send(errorMessages[0].message+" "+errorMessages[0].field);
  }
}

module.exports = facturaCreateFactura
