'use strict'

class anularFatura {

  get validateAll() {
    return true
  }

  get rules() {
    return {
      status_reason: "required|max:50"
    };
  }
  get messages() {
    return {
      "status_reason.min": "O motivo deve conter mais caracters",
      "status_reason.max": "O campo motivo não deve conter mais de 50 caracters",
      "status_reason.required": "É obrigatório mencionar o motivo da anulação da Fatura"
    };
  }

  async fails(errorMessages) {
    return this.ctx.response.status(400).send(errorMessages[0].message);
  }
}

module.exports = anularFatura
