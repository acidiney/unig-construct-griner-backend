'use strict'
const DataResponse = use("App/Models/DataResponse");
class createFlateRate {
  get rules() {
    return {
      valor: "required |number|min:1",
      capacidade: "required|min:1",
      origem: "required",
      destino: "required",
      artigo_id: "required|integer|min:1|exists:produtos,id",
      moeda_id: "required|integer|min:1|exists:moedas,id",
      imposto_id: "required|integer|min:1|exists:impostos,id",
      servico_id: "required|integer|min:1|exists:servicos,id" 
    };
  }
  get messages() {
    return {
      "valor.required": "É obrigatório informar o",
      "capacidade.required": "É obrigatório informar a ",
      "origem.required": "É obrigatório informar a ",
      "destino.required": "É obrigatório informar o ",
      "artigo_id.required": "É obrigatório informar o ",
      "moeda_id.required": "É obrigatório informar a ",
      "imposto_id.required": "É obrigatório informar o ",
      "servico_id.required": "É obrigatório informar o ",

      "exists": "É obrigatório infome um registo valido no campo ",
      "integer": "É obrigatório informe um número no campo ",
    };
  }

  async fails(errorMessages) {
   return this.ctx.response.status(400).send(errorMessages[0].message+" "+errorMessages[0].field);
  }
  
}

module.exports = createFlateRate
