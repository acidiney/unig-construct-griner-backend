'use strict'

class ClienteCreateEntidadeCativadora {
  get rules() {
    return {
      estado: `required|min:1|max:1`,
      cliente_id: `required|integer|min:1|exists:clientes,id|unique:entidade_cativadoras,cliente_id`,
      tipo_id: `required|integer|min:1|exists:tipo_entidade_cativadoras,id`,
    };
  }
  get messages() {
    return {
      required: "É obrigatório informar o", 
      exists: "É obrigatório infome um registo valido no campo ",
      unique: "O cliente informado já existe",
      integer: "É obrigatório informe um número no campo ",
    };
  }

  async fails(errorMessages) {
    return this.ctx.response
      .status(400)
      .send(errorMessages[0].message + " " + errorMessages[0].field.replace("_id","").replace("_"," "));
  }
}

module.exports = ClienteCreateEntidadeCativadora
