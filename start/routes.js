'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
require("./api/v2")(Route);

Route.get('/', () => {

  return {
    greeting: 'UNIG'
  }
})

Route.post("/factura/ict", "FacturaUtilitieController.storeFacturacaoInteConexao");

Route.post('/user/authenticate', 'AuthController.authenticate');
Route.post("/auth/logout", "AuthController.logout").middleware(["auth"]);

Route.get('/user/getuser/:id', 'AuthController.show')
Route.get('/user/perfil/:id', 'UserController.perfil')

Route.post('/email/send', 'SendMailController.store');

Route.get("/user/selectBox/:id", "UserController.selectBox");
Route.get("/user/operador", "UserController.selectBoxOperadores");
Route.get("/factura/arquivo", "FacturaController.criarArquivoSaft");


Route.post("/privatekey", "FacturaController.privateKey");



Route.post("/stream", "BillRunHeaderController.stream");

Route.post("/saftAo/getstream/:id", "SaftController.getStream");

Route.group(() => {

Route.post("/charge/listar", "ChargeController.index");
Route.post("/charge/excel", "ChargeController.index");
Route.post("/charge/update", "ChargeController.update");
Route.post("/charge/alteracoes", "ChargeController.log_charge");
Route.post("/charge/anular", "ChargeController.anular");
Route.post("/charge/registar", "ChargeController.store").validator("addFlateRateCharge")

Route.post("/saftAo", "SaftController.criarArquivoSaft");
Route.post("/safts", "SaftController.index");


  Route.post('/user/update/:id', 'AuthController.update');

  Route.post("/password", "AuthController.resetPassword")//.middleware(['can:reset_password']);
  Route.post("/redefinir-password/", "AuthController.changePassword")//.middleware(['can:redefinir_password']);
  Route.post("/isAlterPassword/", "AuthController.isAlterPassword");//.middleware(['can:redefinir_password']);

  Route.post("/user/register", "UserController.store");


  Route.post('/artigo/teste/pdf', 'ArtigoController.pdfTeste');

  Route.post('/fornecedor/register', 'FornecedorController.store');
  Route.post('/fornecedor/listar', 'FornecedorController.index');
  Route.post('/fornecedor/update/:id', 'FornecedorController.update');
  Route.get('/fornecedor/selectFornecedores', 'FornecedorController.selectFornecedores');

  //rotas para artigos
  Route.post('/artigo/create', 'ArtigoController.store');
  Route.post('/artigo/listar', 'ArtigoController.index')
  Route.post('/artigo/pesquisar', 'ArtigoController.show');
  Route.post('/artigo/update/:id', 'ArtigoController.update');
  Route.get('/artigo/selectProdutos', 'ArtigoController.selectProdutos');
  Route.post('/artigo/selectServicos', 'ArtigoController.selectServicos');
  Route.get('/artigo/selectServicos', 'ArtigoController.selectServicos');
  Route.post('/artigo/search', 'ArtigoController.searchArtigo');


  //rotas para clientes
  Route.post('/cliente/register', 'ClienteController.store');
  Route.post('/cliente/listar', 'ClienteController.index');
  Route.post('/cliente/pesquisar', 'ClienteController.show');

  Route.post('/cliente/update/:id', 'ClienteController.update');
  //Route.get('/cliente/visualizar/:id', 'ClienteController.visualizar');

  Route.post("/cliente/search-cliente", "ClienteController.searchCliente");
  Route.post("/cliente/search-gestor", "ClienteController.searchGestor");
  Route.post("/cliente/searchClienteInFacturas", "ClienteController.searchClienteInFacturas");
  Route.post("/cliente/searchClienteTelefone", "ClienteController.searchClienteTelefone");
  Route.post("/cliente/searchClienteFacturaEmail", "ClienteController.searchClienteFacturaEmail");
  Route.get('/cliente/search-cliente-agt/:id', 'ClienteController.serviceAGT');

  Route.get("/cliente/clienteAll", "ClienteController.clienteAll");
  Route.post("/cliente/incumprimento", "ClienteController.incumprimentCliente");

  Route.get("/cliente/tipoCliente/listar", "TipoClienteController.index");
  Route.get("/cliente/tipoIdentidade/listar", "TipoIdentidadeController.index");
  //Busca Cliente para Mudança de Titularidade ou Conta
  Route.post("/search/cliente/:id", "ClienteController.getCliente");
  Route.post("/search/clienteContas/:id", "ContaController.getClienteContasSearch");



  //Unificar Clientes
  Route.post("/unificar/clientes", "UnificarClienteController.unificarCliente");

  //rotas para empresa
  Route.post('/empresa/listar', 'EmpresaController.index');
  Route.post('/empresa/update/:id', 'EmpresaController.update');
  Route.post('/empresa/upload/:id', 'EmpresaController.upload');
  Route.post('/empresa/register', 'EmpresaController.store');
  Route.get('/empresa/select-option', 'EmpresaController.selectOptionEmpresas');
  Route.get('/empresa/empresa-user', 'EmpresaController.userEmpresa');

  //rotas para armazém
  Route.post('/armazem/listar', 'ArmazenController.index');
  Route.post('/armazem/register', 'ArmazenController.create');
  Route.post('/armazem/update/:id', 'ArmazenController.update');

  Route.get("/armazem/all", "ArmazenController.getAll");

  //rotas para tarefas
  Route.get("/tarefa/listar", "TarefaController.index");
  Route.post("/tarefa/registar", "TarefaController.store");


  //rotas para compra
  Route.post('/compra/listar', 'CompraController.index');
  Route.post('/compra/update/:id', 'CompraController.update');
  Route.post('/compra/register', 'CompraController.store');
  // SendMail



  //rotas para users
  Route.post('/user/listar', 'AuthController.index');
  Route.post('/user/bloquear/:id', 'UserController.bloquear');

  //rotas para impostos
  Route.post('/imposto/listar', 'ImpostoController.index');
  Route.post('/imposto/create', 'ImpostoController.store');
  Route.post('/imposto/update/:id', 'ImpostoController.update');
  Route.post('/imposto/getall', 'ImpostoController.getAll');

  //rotas para regra impostos
  Route.post('/imposto/regras/listar', 'RegraImpostoController.index');
  Route.post('/imposto/create', 'RegraImpostoController.store');


  //rotas para regra pagamentos
  Route.post('/tipopagamento/listar', 'TipoPagamentoController.index');
  Route.post('/tipopagamento/create', 'TipoPagamentoController.store');

  //rotas para formas Pagamentos
  Route.post('/formaPagamento/register', 'FormaPagamentoController.store');
  Route.post('/formaPagamento/listar', 'FormaPagamentoController.index');
  Route.post('/formaPagamento/update/:id', 'FormaPagamentoController.update');
  Route.get('/formaPagamento/formas', 'FormaPagamentoController.formas');
  Route.get('/formaPagamento/selectbox', 'FormaPagamentoController.selectBoxformas');

  //rotas para recibo
  Route.post('/recibo/gerarRecibo', 'ReciboController.store');
  Route.post('/recibo/printerRecibo/:id', 'ReciboController.printerRecibo');

  //rotas para facturas
  Route.post('/factura/create', 'FacturaController.store');
  Route.post("/factura/list", "FacturaController.index"); 
  Route.post('/orcamento/list', 'FacturaController.listaOrcamento');
  Route.post('/factura/notpay', 'FacturaController.getFacturasNaoPagas');
  Route.post('/factura/arquivo', 'FacturaController.criarArquivo');
  Route.post('/factura/notacredito/artigos', 'FacturaController.getFacturaNotaCrediro');
  Route.post('/factura/notacredito/linhas', 'FacturaController.getLinhasFacturaNotaCrediro');

  Route.post('/factura/anular/:id', 'FacturaController.anular').validator("/factura/anularFatura");
  Route.post('/factura/gerarFactura/:id', 'FacturaController.gerarFactura');
  Route.post("/factura/findFactFromNc/:id", "FacturaController.findFactFromNc");
  
  
  Route.post("/factura/gerar-saft-xml", "FacturaController.saftXMLDocument");
  Route.get("/factura/dashboard", "FacturaController.dashboardFacturacao");

  //Rotas para pedidos
  Route.post("/pedido/register", "PedidoController.store");
  Route.get("/pedido/listar/:id", "PedidoController.index");
  Route.post('/pedido/pdf/instalacao/:id', 'PedidoController.gerarPdfInstalacao');
  Route.post('/pedido/listagem', 'PedidoController.listagem');
  Route.post('/pedido/update_test', 'PedidoController.updatePedidoCliente');
  Route.get("/pedido/search-cobre/:id", "PedidoController.isCobre");

  //Bill Run header
  Route.post('/billRunHeader/listagem', 'BillRunHeaderController.listagem');
  Route.get("/enviar/factura/email/:id", "BillRunHeaderController.findFacturasToSend");
  Route.get("/enviar/factura/email/one/:id", "BillRunHeaderController.findOneFacturasToSend");
  Route.post('/billRunHeader/meses', 'BillRunHeaderController.meses');
  Route.post("/save/factura/base64", "BillRunHeaderController.downloadPDF");

  //rotas para documentos
  Route.post('/documento/listar', 'DocumentoController.index');
  Route.post('/documento/create', 'DocumentoController.store');
  Route.post('/documento/show', 'DocumentoController.show');
  Route.post("/documento/update/:id", "DocumentoController.update");

  //rota pra tarifários
  Route.post('/planoPreco/show', 'PlanoPrecoController.show');
  Route.post('/tarifario/listagem', 'TarifarioController.listagem');
  Route.post('/tarifario/create', 'TarifarioController.store');
  Route.post('/tarifario/update/:id', 'TarifarioController.update');
  Route.get("/planoPreco/selectBox", "PlanoPrecoController.planoPreco");

  Route.get("/tarifario/planoPrecoTecnologiaSelectBox/:t", "TarifarioController.planoPrecoTecnologiaSelectBox");

  Route.get("/tecnologia/nome/:id", "TecnologiaController.selectTecnologiaNome");

  Route.post("/tarifario/selectBoxTarifarioNovo", "TarifarioController.selectBoxTarifarioNovo");



  //rotas para series
  Route.post('/serie/listar', 'SerieController.index');
  Route.post('/serie/create', 'SerieController.store');
  Route.post('/serie/listagem', 'SerieController.listagem');
  Route.post("/serie/update/:id", "SerieController.update");
  Route.get("/serie/selectBoxSerieOrcamento", "SerieController.selectBoxSerieOrcamento");
  Route.post("/serie/selectBoxSeries", "SerieController.selectBoxSeries");


  //rotas Interconexao
  Route.post('parceriaCliente/show', 'ParceriaClienteController.show');
  Route.post('interconexao/currentYear', 'DetalheInterconexaoController.currentYear');

  Route.post('interconexao/listagem', 'DetalheInterconexaoController.listagem');
  Route.post('/interconexao/create', 'DetalheInterconexaoController.store');
  Route.post("/interconexao/update/:id", "DetalheInterconexaoController.update");


  //rota stock moviemnto
  Route.post('/stockMovimento/listar', 'StockMovimentoController.index');


  //conta corrente
  //Route.post("/contaCorrente/contas/:id", "ContaCorrenteController.contas");
  Route.post("/contaCorrente/contas", "ContaCorrenteController.contas");
  Route.post("/contaCorrente/report", "ContaCorrenteController.contaConrrenteReport");




  //rotas para modulos
  Route.post('/modulo/listar', 'ModuleController.index');
  Route.post('/modulo/registar', 'ModuleController.store');
  Route.post('/modulo/update/:id', 'ModuleController.update');
  Route.get('/modulo/select-option', 'ModuleController.selectOptionModulo');

  Route.get('/modulo/moduloPermissions', 'ModuleController.moduloPermissions');


  //rotas para roles
  Route.post('/role/listar', 'RoleController.index');
  Route.post('/role/registar', 'RoleController.store');
  Route.post('/role/update/:id', 'RoleController.update');
  Route.get('/role/select-option', 'RoleController.selectOptionRole');
  Route.post('/role/adicionarPermissionRole', 'RoleController.adicionarPermissionRole');

  Route.get('/role/getAllPermissionsOfRole/:id', 'PermissionController.getAllPermissionsOfRole');

  Route.post('/roles/reportDiario', 'RoleController.roleReportDiario');

  Route.post('/roles/reportDiarioAuto', 'RoleController.roleReportDiarioAuto');

  //Route.post("/enviar/reportDiario/email/:id", "UserController.usersByRole");
  Route.post("/get/RoleSlug/:id", "RoleController.getRoleSlug");

  Route.post("/save/reportDiario/base64", "BillRunHeaderController.saveReportDiarioPDF");



  //rotas para permissionss
  Route.post('/permission/listar', 'PermissionController.index');
  Route.post('/permission/registar', 'PermissionController.store');
  Route.post('/permission/update/:id', 'PermissionController.update');
  Route.get('/permission/show/:id', 'PermissionController.show');


  //rotas para inventario
  Route.post('/inventario/register', 'InventarioController.store');
  Route.post('/inventario/listar', 'InventarioController.index');


  //rotas para moeda
  Route.post("/moeda/register", "MoedaController.store");
  Route.get("/moeda/listar", "MoedaController.index");
  Route.post("/moeda/moedas", "MoedaController.moedas");
  Route.post("/moeda/update/:id", "MoedaController.update");
  // cambio
  Route.post("/moeda/cambio/register", "CambioController.store");
  Route.get("/moeda/cambio/listar/:id", "CambioController.index");


  //rotas para projectos
  Route.post("/projecto/register", "ProjectoController.store");
  Route.post("/projecto/listar", "ProjectoController.index");
  Route.post("/projecto/update/:id", "ProjectoController.update");
  Route.post("/projecto/eliminar/:id", "ProjectoController.destroy");

  Route.post("/projecto/serie/adicionar", "ProjectoController.adicionarSerieProjecto");
  Route.post("/projecto/serie/listar", "ProjectoController.serieProjecto");


  //rotas para banco
  Route.post('/banco/register', 'BancoController.store');
  Route.get("/banco/listar", "BancoController.index");

  Route.get('/dashboard/listar', 'DashboardController.index');


  Route.get("/planoPreco/listar", "PlanoPrecoController.index");
  Route.get("/planoPreco/planos", "PlanoPrecoController.planoPreco");
  Route.post("/planoPreco/register", "PlanoPrecoController.store");
  Route.post("/planoPreco/update/:id", "PlanoPrecoController.update");
//Tarifários
  Route.get("/tarifario/tarifas/:id", "TarifarioController.tarifarios");
  Route.get("/tarifario/tecnologia/:id", "TarifarioController.tecnologias");
  //Serviços 
  Route.post("/pedido/servico/conta", "ServicoController.servicoPedidoCreate");
  Route.get("/tarifario/servico/listar", "ServicoController.index");
  Route.post("/servicos/conta/cliente/:id", "ServicoController.servicosClientes");
  Route.post('/servico/contrato/:id', 'ServicoController.gerarContracto');
  Route.post('/servico/storeServicoPospago', 'ServicoController.storeServicoPospago');

  Route.post('/servico/detalhe/:id', 'ServicoController.DetalheServico');
  Route.post('/servico/update/estado/:id', 'ServicoController.updatEstado');
  Route.post('/mudanca/conta/servico', 'ServicoController.mudancaContaServico');

  Route.get("/getServico/:id", "ServicoController.getServico");
  Route.get("servico/getServicoToChaveServico/:id", "ServicoController.getServicoToChaveServico");

  Route.get("/get/cdma/servico/:id", "CdmaServicoController.getCDMAServico");
  
  //Logs Carregamentos
  Route.get("/servicos/ver/carregamentos/:id", "LogsCarregamentoController.getCarregamentos");

  Route.post("/servico/mudarTarifarioServico", "ServicoController.mudarTarifarioServico");

  //Route.get("/servico/chaveServicos", "ChaveServicoController.selectBox");
  Route.post("/servico/chaveServicos", "WimaxController.validate");

  //CDMA Equipamento
  Route.post("/cdmaEquip/validate/serie", "CdmaEquipamentoController.validateSerie");

  Route.post("/cdmaEquip/validate/serie/edit", "CdmaEquipamentoController.validateSerieEditSerie");

  Route.post('/cdmaServico/update/equipamento/:id', 'CdmaServicoController.updateEquipamento');
  
  Route.get('/cdmaequipamento/search/:num_serie', 'CdmaEquipamentoController.searchCDMAEquipamentoByNumSerie');


  //
  Route.get("/cliente/conta/listar/:id", "ContaController.index");
  Route.post("/cliente/conta/register", "ContaController.store");
  Route.get("/getContaEstado/:id", "ContaController.getContaEstado");
  Route.post('/conta/update/estado/:id', 'ContaController.updateEstado');
  Route.post("/cliente/conta/show", "ContaController.show");

   // Contactos clientes
   Route.get("/cliente/contacto/listar/:id", "PessoaContactoController.index");
   Route.post("/cliente/contacto/register", "PessoaContactoController.store");

   //lojas
   Route.post("/loja/listar", "LojaController.index");
   Route.post("/loja/create", "LojaController.store");
   Route.post("/loja/update/:id", "LojaController.update");
   Route.get("/loja/selectBox", "LojaController.selectBox");

   Route.get("/tipoPedidos/selectBox", "TipoPedidoController.selectBox");
   Route.post('/tipos/pedidos/listagem', 'TipoPedidoController.listagem');
   Route.post('/tipos/pedido/create', 'TipoPedidoController.store');
   Route.post("/tipos/pedido/update/:id", "TipoPedidoController.update");

   Route.post("/loja/listarFormaPagamentoPorLoja", "LojaFormaPagamentoController.index");
   Route.post("/loja/forma/selectBox", "LojaFormaPagamentoController.selectBox");
   Route.post("/loja/storeFormaPagamentoLoja", "LojaFormaPagamentoController.store");
   Route.post("/loja/formaPagamentoLoja/active", "LojaFormaPagamentoController.update");


  // rotas para supporte
  Route.get('/supporte/categoria',"SupporteController.listarSuporteCategoria");
  Route.get("/supporte/categoria/activa","SupporteController.listarCatergoriaActiva")
  Route.get("/supporte/estado", 'SupporteController.listarSupporteEstado');
  Route.post("/supporte/estado/activar","SupporteController.AtivarEstado")
  Route.get("/supporte/estados/ativdo", "SupporteController.listarSupporteEstadoActiva");
  Route.get("/suporte/prioridades", "SupporteController.listarSuportePrioridade");
  Route.post("/supporte/criar",'SupporteController.CriarSupporte');
  Route.post('/supporte/listagem', 'SupporteController.ListarSuporte');
  Route.post('/supporte/actuliazar', "SupporteController.atualizarSuporte");
  Route.post('/supporte/categoriar/criar',"SupporteController.adionarCategoria");
  Route.post('/supporte/categoria/actualizar',"SupporteController.actualizarCategoria");
  Route.post('/supporte/estado/criar', 'SupporteController.adicionarEstados');
  Route.post('/supporte/estado/eliminar', "SupporteController.eliminarEstado");
  Route.post('/supporte/estado/atualizar', "SupporteController.actualizarEstado");
  Route.post("/supporte/prioridade/crirar", "SupporteController.adicionarPrioridades");
  Route.post('/supporte/prioridade/actualizar', "SupporteController.atualizarPrioridade")
  Route.get("/supporte/prioridade/activo", "SupporteController.listarSuportePrioridadeActiva")
  Route.post("/supporte/prioridade/activar","SupporteController.ativarPrioridade")
  Route.post('/supporte/categoria/estado',"SupporteController.AtivarCategoria");
  Route.post("/supporte/pegar/img/suporte", "SupporteController.pegarSupportId");
  Route.post("/supporte/envio_ticket/email", "SupporteController.envioTicketPorEmailController");
  Route.get("/supporte/envio_ticket/email_massa", "SupporteController.envioTicketPorEmailMassa");
   // Reclamação
   Route.get("/tipoReclamacao/selectBox", "ReclamacaoController.tiposelectBox");
 
   Route.get("/reclamacao/listar/:id", "ReclamacaoController.index");
   Route.post("/reclamacao/register", "ReclamacaoController.store");
   Route.post('/reclamacao/listagem', 'ReclamacaoController.listagemReclamacoes');
   // Reclamação
   Route.post('/tipo_reclamacao/listagem', 'ReclamacaoController.listagemTipoReclamacao');
   Route.post('/tipo_reclamacao/create', 'ReclamacaoController.storeTipoReclamacao');
   Route.post('/tipo_reclamacao/update/:id', 'ReclamacaoController.updateTipoReclamacao');
   Route.get("/reclamacao/estados", "ReclamacaoController.estadosReclamacao");
   Route.get("/reclamacao/estado/selectBox/:id", "ReclamacaoController.selectBox");
   Route.post('/reclamacao/update/estado/:id', 'ReclamacaoController.updatEstado');

   Route.get("/historico/reclamacao/:id", "LogEstadoReclamacaoController.index");

   // Prioridade
   Route.get("/prioridade/selectBox", "PrioridadeController.selectBox");
   Route.post('/prioridade/listagem', 'PrioridadeController.listagem');
   Route.post('/prioridade/create', 'PrioridadeController.store');
   Route.post('/prioridade/update/:id', 'PrioridadeController.update');

   Route.post("/loja/addChefe", "LojaController.addChefeLoja");
   Route.get("/filial/selectBox", "FilialController.selectBox");
   Route.get("/filial/selectBoxFilialPorLojas/:id", "FilialController.selectBoxFilialPorLojas");
   Route.get("/filial/indicativo/phone/:id", "FilialController.selectPhoneIndicativo");

  //lojas
  Route.post("/loja/listar", "LojaController.index");
  Route.post("/loja/create", "LojaController.store");
  Route.post("/loja/update/:id", "LojaController.update");
  Route.get("/loja/selectBox", "LojaController.selectBox");

  //CMDA NÚMERO

Route.get("/serie/loja/selectSerieLojaUserBox", "SerieController.selectSerieLojaUserBox");
Route.get("/serie/recibos", "SerieController.selectSerieRecibosBox");
Route.post("/recibo/consultarRecibo/:id", "ReciboController.consultarRecibo");
Route.post("/recibo/listarRecibo", "ReciboController.index");
Route.post("/factura/consultarFacturaPosPagoVoz/:id", "BillRunHeaderController.consultarFacturasPospagoVoz");
Route.post("/recibo/anular", "ReciboController.reciboAnular");

  Route.get("/cdma/selectBox", "CdmaNumeroController.selectBox");

  Route.post("/cdma/search-cdma-numero", "CdmaNumeroController.searchCdmaNumero");

  // SIM CARD

  Route.post("/search-serie-sim", "SimCardController.searchSerieSim");

  //LTE

  Route.post("/lte-search/numero-serie", "LteCpeController.searchNumeroSerie");
  Route.post("/lte-search/telefone", "LteCpeController.searchTelefone");
  Route.post("/lte-search/fabricante", "LteCpeController.searchFabricante");
  Route.post("/lte-search/modelo", "LteCpeController.searchModelo");
  Route.post("/lte-search/tipo", "LteCpeController.searchTipo");

  Route.post('/LteCpe/create', 'LteCpeController.store');

  Route.post('/LteCpe/listagem', 'LteCpeController.listagem');

  Route.post("/lteTelefone/validate/telefone", "LteCpeController.validateTelefoneLTE");

  //WIMAX DISPOSITIVOS
  Route.post('/wimax/listagem', 'WimaxController.listagem');
  Route.post('/wimax/create', 'WimaxController.store');
  Route.post('/wimax/update/:id', 'WimaxController.update');

  //CDMA DISPOSITIVOS
  Route.post('/cdma/equipamento/listagem', 'CdmaEquipamentoController.listagem');
  Route.post('/cdma/equipamento/create', 'CdmaEquipamentoController.store');
  Route.post('/cdma/equipamento/update/:id', 'CdmaEquipamentoController.update');


  Route.get("/provincia/selectBox", "ProvinciaController.selectBox");
  Route.get("/municipio/selectBox/:id", "MunicipioController.selectBox");

  Route.post("/caixa/abertura", "CaixaController.store").middleware(['can:abrir_caixa'])
  Route.post("/caixa/fecho", "CaixaController.update").middleware(['can:fechar_caixa'])
  Route.post("/caixa/deposito", "DepositoCaixaController.store").middleware(['can:fechar_caixa'])
  Route.post('/deposito/listagem', 'DepositoCaixaController.listagemDepositos');

  Route.post("/caixa/movimentoCaixa", "CaixaController.index").middleware(['can:movimento_caixa']);
  Route.get("/caixa/selectBox", "CaixaController.selectBox");
  Route.get("/caixa/selectBoxCaixasFechados/:data", "CaixaController.selectBoxCaixasFechados");
  Route.post("/caixa/totalVendas", "CaixaController.totalVendas");
  Route.get("/caixa/open", "CaixaController.getCaixaAberto");

  //relatorios de venda caixas
  Route.post("/diario/vendas", "CaixaController.getDiarioVendas");

  Route.get("/serie/loja/selectSerieLojaBox", "SerieController.selectSerieLojaBox");

  Route.get("/serie/loja/selectSeriesRecibosNotInLojasBox", "SerieController.selectSeriesRecibosNotInLojasBox");

  Route.post("/cliente/conta/ContaPosPagoCliente", "ContaController.ContaPosPagoCliente");

  Route.post("/cliente/parceria/create", "ClienteController.createParceriaCliente");

  Route.post("/cliente/parceria/listar", "ParceriaClienteController.index");
  Route.post("/cliente/parceria/editar/:id", "ParceriaClienteController.update");
  Route.post("/cliente/parceria/contrato/", "ContratoController.index");
  Route.post("/cliente/parceria/contrato/registar", "ContratoController.store");
  Route.post("/cliente/parceria/contrato/editar/:id", "ContratoController.update");

  Route.post("/factura/processarContas", "FacturaUtilitieController.processarContas");
  Route.post("/factura/posPago", "FacturaUtilitieController.store");
  Route.post("/factura/preFacturacao", "FacturaUtilitieController.preFacturacao");



  Route.post("/get/cliente/contas/:id", "ContaController.getClienteContas");
  Route.post("/get/cliente/contas/pedido/:id", "ContaController.getClienteContasPedidos");

  Route.get("/cliente/conta/selectBox/:id", "ContaController.selectBoxContasCliente");
  Route.get("/tarifario/servico/selectBoxServicosConta/:id", "ServicoController.selectBoxServicosConta");
  Route.get("/servico/estado/selectBox", "EstadoServicoController.selectBox");


 Route.post("/adiantamento/create", "AdiantamentoController.store");

  Route.get("/selectBoxTarifarioTecnologiaInterconexao", "TarifarioController.selectBoxTarifarioTecnologiaInterconexao");

  Route.get("/pedido/estados", "PedidoController.estadosPedidos");

  Route.get("/pedido/totalDashBoard", "PedidoController.totalDashBoard");

  Route.get("/pedido/estadosFNRJ", "PedidoController.estadosPedidosFNRJ");
  Route.get("/pedido/estado/selectBox/:id", "PedidoController.selectBox");

  Route.post("/estadoByPedido/:id", "PedidoController.estadoByPedido");

  Route.post("/pedido/servico", "PedidoController.servicoByTelefone");

  Route.get("/estadoPedidoValue/:id", "EstadoPedidoController.estadoPedidoValue");

  Route.get("/rotaTipoPedido/:id", "TipoPedidoController.rotaTipoPedido");

  Route.post('/pedido/update/estado/:id', 'PedidoController.updatEstado');

  Route.post('/pedido/update/:id', 'PedidoController.updatePedido');

  Route.get("/historico/estado/pedido/:id", "LogEstadoPedidoController.index");

  Route.get("/factura/dashboard", "FacturaController.dashboardFacturacao");
 

Route.post("/vendas/resumoRecebimentoPorCaixa", "CaixaController.reportResumoRecebimentoPorCaixa");
Route.post("/vendas/fechoCaixaResumoRecebimentoVendas", "CaixaController.fechoCaixaResumoRecebimentoVendas");



//Numeração
Route.post('/numeracao/listagem', 'NumeracaoController.index');
Route.post('/numeracao/create', 'NumeracaoController.store');
Route.get("/tecnologia/numeracao/selectBox", "TecnologiaController.selectPrePago");
Route.post('/numeracao/update/:id', 'NumeracaoController.update');
Route.post('/numeracao/disponibilizar/:id', 'NumeracaoController.disponibilizar');

//Estado Reclamação
Route.post('/estado/reclamacao/listagem', 'EstadoReclamacaoController.listagem');
Route.post('/estado/reclamacao/create', 'EstadoReclamacaoController.store');
Route.post('/estado/reclamacao/update/:id', 'EstadoReclamacaoController.update');

//Direcção
Route.post('/direccao/listagem', 'DireccaoController.listagem');
Route.post('/direccao/create', 'DireccaoController.store');
Route.post('/direccao/update/:id', 'DireccaoController.update');
Route.get('/direccao/selectBox', 'DireccaoController.selectBox');

//Estado Reclamação
Route.post('/estado/pedidos/listagem', 'EstadoPedidoController.listagem');
Route.post('/estado/pedidos/create', 'EstadoPedidoController.store');
Route.post('/estado/pedidos/update/:id', 'EstadoPedidoController.update');

// Unificação clientes
Route.post("/unificar/getContas", "UnificarClienteController.client_a_Unificar");
Route.post("/unificar/clienteUnificados", "UnificarClienteController.getClente_a_Unificar");
Route.post("/unificar/getConta_Cliente_manter", "UnificarClienteController.conta_cliente_manter");
Route.post("/unificar/getConta_Cliente_unificar", "UnificarClienteController.conta_cliente_unificar");
Route.post("/unificar/delete_unificado","UnificarClienteController.delete_unificar_cliente");
Route.post("/unificar/update_unificado","UnificarClienteController.upDateClienteUnificar");
Route.post("/unificar/desfazer_unificado","UnificarClienteController.desfazerUnificacao");
Route.get("/unificar/masssa","UnificarClienteController.unificar_em_massa");

Route.post("/unificar/clientes", "UnificarClienteController.unificarCliente");




Route.post("/entidade-cativadora/listar", "EntidadeCativadoraController.index");
Route.post("/entidade-cativadora/registar", "EntidadeCativadoraController.store").validator("cliente/createEntidadeCativadora") 

Route.post("/entidade-cativadora/search-cliente", "EntidadeCativadoraController.searchClientes");

Route.get("/entidade-cativadora/tipoEntidadeCativadoraSelectBox", "TipoEntidadeCativadoraController.tipoEntidadeCativadoraSelectBox");
Route.post("/tipo-entidade-cativadora/registar", "TipoEntidadeCativadoraController.store");

//RECURSOS DE REDE

Route.get("/central/selectBox", "RecursosRedeController.centralListagem");
Route.get("/armarioRecursoRede/selectBox", "RecursosRedeController.armarioListagem");

Route.get("/caixaRecursoRede/selectBox", "RecursosRedeController.caixaRecursoRedeListagem");
Route.get("/caboRecursoRede/selectBox", "RecursosRedeController.caboRecursoRedeListagem");
Route.get("/parCabo/selectBox", "RecursosRedeController.parCaboListagem");
Route.get("/armarioPrimario/selectBox", "RecursosRedeController.armarioPrimarioListagem");
Route.get("/armarioSecundario/selectBox", "RecursosRedeController.armarioSecundarioListagem");
Route.get("/parCaixa/selectBox", "RecursosRedeController.parCaixaListagem");

Route.post('/central/listaDataTable', 'RecursosRedeController.centralDataTable');

Route.post('/central/register', 'CentralRecursoRedeController.storeCentral');
Route.post('/central/update/:id', 'CentralRecursoRedeController.updateCentral');
Route.get("/select-armario/armarioByCentral/:id", "RecursosRedeController.armarioByCentral");
Route.get("/select/idCaixaByCentral/:id", "RecursosRedeController.idCaixaByCentral");
Route.get("/select/idCaboByCentral/:id", "RecursosRedeController.idCaboByCentral");
Route.get("/select/parCaboByCentral/:id", "RecursosRedeController.parCaboByCentral");
Route.get("/select/armarioPrimarioByCentral/:id", "RecursosRedeController.armarioPrimarioByCentral");
Route.get("/select/armarioSecundarioByCentral/:id", "RecursosRedeController.armarioSecundarioByCentral");
Route.get("/select/parCaixaByCentral/:id", "RecursosRedeController.parCaixaByCentral");
Route.get("/select/parADSLByCentral/:id", "RecursosRedeController.parADSLByCentral");
Route.post("/idArmario/central/register", "ArmarioRecursoRedeController.store");
Route.post("/idCaixa/central/register", "CaixaRecursoRedeController.store");
Route.post("/idCabo/central/register", "CaboRecursoRedeController.store");
Route.post("/parCabo/central/register", "ParCaboController.store");
Route.post("/armarioPrimario/central/register", "ArmarioPrimarioController.store");
Route.post("/armarioSecundario/central/register", "ArmarioSecundarioController.store");
Route.post("/parCaixa/central/register", "ParCaixaController.store");
Route.post("/parADSL/central/register", "ParAdslController.store");


// relatorios


//rotas para Flat_Rate 

Route.post('servico/flate_rate/visualizar',"FlatRateServicoController.show");
Route.post('servico/flate_rate/update/:id', "FlatRateServicoController.update").validator("createFlateRate")
Route.post('servico/flate_rate/register', "FlatRateServicoController.store").validator("createFlateRate")
Route.post('servico/flate_rate/eliminar/:id',"FlatRateServicoController.destroy");
Route.get('servico/flate_rate/getAllflateRate/:id',"FlatRateServicoController.getFlatrateServico");
 
Route.post('/integracao/contabilidade/listar',"IntContabilidadeController.index"); 
Route.post('/integracao/contabilidade/create', "IntContabilidadeController.store");

Route.post('/integracao/contabilidade/selectBox', "IntContabilidadeController.selectBox");

Route.post('/integracao/contabilidade/linha/listar',"LinhaIntContabilidadeController.index"); 
Route.post('/integracao/contabilidade/linha/create', "LinhaIntContabilidadeController.store");
Route.post('/integracao/contabilidade/linha/update/:id', "LinhaIntContabilidadeController.store");

Route.post('/cliente/adiantamento/movimento', "AdiantamentoController.index");
  



}).middleware(['auth']);


Route.get("/reclamacao/register_async", "ReclamacaoController.store_portal");



Route.post("/adiantamento/factura/adiantamentoFactura", "AdiantamentoController.adiantamentoFactura");
Route.post("/adiantamento/factura/adiantamentoContaCliente", "ContaCorrenteController.adiantamentos");

Route.get("/direccao/selectBox", "ClienteController.direccaosSelectBox");
Route.post("/direccaoReport/selectBox", "ClienteController.direccaosSelectBoxreport");


Route.post("/gestor/selectBox", "ClienteController.gestoresSelectBox");
Route.get("/gestor/selectBox", "ClienteController.gestoresSelectBox");
Route.get("/tipocliente/selectBox", "ClienteController.TipoclienteSelectBox");
Route.get("/tipoidentidade/selectBox", "ClienteController.TipoIdentidadeSelectBox");

 

Route.post("/transactions-facturacao", "FacturaController.transactionFacturacaoStore");
 

Route.post("/referencia/validation", "ReferenciaBancariaController.validarReferenciaBancaria");

Route.get("/factura/getFtFromToNC/:id", "FacturaController.findFactFromNc");
  


