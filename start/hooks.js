const { hooks } = require('@adonisjs/ignitor')
 
hooks.after.providersRegistered (() => {
  const Validator = use('Validator')
  const existsFn = use('App/Validators/Custom/exists')
  Validator.extend('exists', existsFn)
})
