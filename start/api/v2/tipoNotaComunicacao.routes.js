module.exports = (ApiRoute, Route) =>
  // Protected routes
    ApiRoute(() => {
      Route.get("/", "TipoComunicacaoController.index");
      Route.get("/all", "TipoComunicacaoController.getAll");
      Route.post("/", "TipoComunicacaoController.store");
      Route.get("/:id", "TipoComunicacaoController.show");
      Route.put("/:id", "TipoComunicacaoController.update");
      Route.delete("/:id", "TipoComunicacaoController.destroy");
      Route.put("/status/:id", "TipoComunicacaoController.updateStatus");
    },'tipo-comunicacao').middleware(["auth"]); 
