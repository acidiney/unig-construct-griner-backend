

 module.exports = (ApiRoute, Route) =>
  
    // Protected routes
 ApiRoute(() => {
       
        Route.get("/", "TecnologiaController.index");
        Route.post("/", "TecnologiaController.store");
        Route.get("/:id", "TecnologiaController.show");
        Route.put("/:id", "TecnologiaController.update");
        Route.delete("/:id", "TecnologiaController.destroy"); 
        Route.post("/selectBox", "TecnologiaController.selectBox");
        Route.get('/listagem', 'TecnologiaController.listagem');
        Route.get('/prepago', 'TecnologiaController.TecnologiasPrepago');
        Route.post('/create', 'TecnologiaController.store');
        Route.put('/update/:id', 'TecnologiaController.update');
        
}, "tecnologia").middleware(["auth"]);
   