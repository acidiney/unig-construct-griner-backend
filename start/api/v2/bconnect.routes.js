module.exports = (ApiRoute, Route) =>
// Protected routes
ApiRoute(() => {
    Route.get("/contact/:id", "ClienteController.getClientByContact"); 
    Route.get("/contract/detail/:id", "ClienteController.getClientContractDetailByClientId");       
}, "clients")//.middleware(["auth"]);