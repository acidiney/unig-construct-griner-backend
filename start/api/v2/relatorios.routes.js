module.exports = (ApiRoute, Route) =>
  // Protected routes
ApiRoute(() => {
    
    Route.post("/financeira/cobranca-global", "RelatorioController.relatorioFacturacaoRealizadaCobrancaGlobal");
    Route.post("/financeira/vendalojaproduto", "RelatorioController.relatorioVendaporLojaporProdutoGlobal");
    Route.post("/financeira/servico-global", "RelatorioController.relatorioFacturacaoporServicoGlobal");
    Route.post("/financeira/pagamento", "RelatorioController.relatorioFacturacaopagamentoGlobal");
    Route.post("/financeira/porgestor", "RelatorioController.relatorioFacturacaoporGestorGlobal");
    Route.post("/financeira/vendadiaria", "RelatorioController.relatorioDiarioVendasProdutoServicos");
    //Route.post("/financeira/cobranca-global", "RelatorioController.relatorioFacturacaoRealizadaCobrancaGlobal");
    Route.post("/financeira/facturacaodetalhadapospago", "RelatorioController.relatorioFacturacaoDetalhadaPosPago"); 
    Route.post("/financeira/facturacaodiariaservico", "RelatorioController.relatorioFacturacaoDiariaPorServico");
    Route.post("/financeira/diariaporgestor", "RelatorioController.relatorioFacturacaoDiariaPorGestorGlobal");
    Route.post("/financeira/recibos", "RelatorioController.relatorioRecibos");
    Route.post("/financeira/facturacaoClintes", "RelatorioController.relatorioFacturacaoClienteGlobal");
    Route.post("/financeira/pagamentosClinte", "RelatorioController.relatorioPagamentosClienteGlobal");
    Route.post("/financeira/resumocontacorrente", "RelatorioController.relatorioResumoContaCorrente");
    Route.post("/financeira/facturaenviadaemail", "RelatorioController.relatorioFacturaEnvidasPorEmail");

    //Route.post("/financeira/cobranca-global", "RelatorioController.relatorioFacturacaoRealizadaCobrancaGlobal");
    Route.post("/financeira/cobranca-global-pago", "RelatorioController.relatorioFacturacaoRealizadaCobrancaGlobalPago")
    Route.post("/financeira/loja", "RelatorioController.relatorioLoja");
    Route.post("/financeira/iva", "RelatorioController.relatorioIVA");
    Route.post("/facturacao/diaria", "RelatorioController.facturacaoDiariaGlobal");
    Route.post("/financeira/servicoContratados", "RelatorioController.relatorioServicoContratados");
    Route.post("/clientes", "RelatorioController.relatorioClients");

    Route.post("/financeira/movimentocaixa", "RelatorioController.relatorioMovimentosCaixa"); 

    Route.get("/reporting_e_bi", "RelatorioController.reportingBI");
    Route.get("/reporting_e_bi_last_month", "RelatorioController.reportingBILastMonth");

}, "relatorio")//.middleware(["auth"]);
