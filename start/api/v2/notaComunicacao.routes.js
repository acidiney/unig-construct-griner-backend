module.exports = (ApiRoute, Route) =>
  // Protected routes
 ApiRoute(() => {
      
    Route.get("/", "NotaComunicacaoController.index");
    Route.post("/", "NotaComunicacaoController.store");
    Route.get("/:id", "NotaComunicacaoController.show");
    Route.put("/:id", "NotaComunicacaoController.update");
    Route.delete("/:id", "NotaComunicacaoController.destroy");
    Route.put("/status/:id", "NotaComunicacaoController.updateStatus");
    
  },'nota-comunicacao').middleware(["auth"]);
