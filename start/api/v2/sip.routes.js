// Sip Routes
 
module.exports = (ApiRoute, Route) =>
  
    // Protected routes
    ApiRoute(() => {
       
      Route.post("/sip/dataList/", "SipServicoController.index");

      Route.post("/sip/dataRegister", "SipServicoController.store").validator("sip/createSipServico")

      Route.put("/sip/:id", "SipServicoController.edit").validator("sip/createSipServico")

      Route.delete("/sip/:id", "SipServicoController.destroy");

      Route.put("/sip/status/:id", "SipServicoController.updateStatus");

      Route.get("/sip/range/:numero", "SipServicoController.findByRangeSip");

      Route.get("/sip/:id", "SipServicoController.show");
              
  },"servico").middleware(["auth"]);