
// Facturas Routes
 
module.exports = (ApiRoute, Route) =>
  // Protected routes
    ApiRoute(() => {
      
    Route.get("/", "FacturaController.index").validator("factura/listFactura")
      
    Route.post("/", "FacturaController.store");
    
    Route.get("/:id", "FacturaController.show");
    
    Route.put("/anular/:id", "FacturaController.anular").validator("factura/anularFatura");  
    
    Route.get("/preview/:id", "FacturaController.gerarFactura");
        
  }, "facturas").middleware(["auth"]);