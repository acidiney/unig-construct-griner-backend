
// Recibos Routes
 
module.exports = (ApiRoute, Route) =>
  
    // Protected routes
    ApiRoute(() => {
      
    Route.get("/", "RecibosController.index");
      
    Route.post("/", "RecibosController.store");
    
    Route.get("/:id", "RecibosController.show");
    
    Route.put("/anular/:id", "RecibosController.anular");  
        
  }, "recibos").middleware(["auth"]);