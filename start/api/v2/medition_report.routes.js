module.exports = (ApiRoute, Route) =>
  // Protected routes
    ApiRoute(() => {

      Route.get("/", "RelatorioMediationController.index"); 
        
      Route.get("/fixoExterior", "RelatorioMediationController.relatorioMediationFixoExterior");
      Route.get("/fixoFixo", "RelatorioMediationController.relatorioMediationFixoFixo");
      Route.get("/fixoMovel", "RelatorioMediationController.relatorioMediationFixoMovel");
      Route.get("/exteriorFixo", "RelatorioMediationController.relatorioMediationExteriorFixo"); 

  }, "relatorios/mediation")//.middleware(["auth"]);
