 module.exports = (ApiRoute, Route) =>
  
    // Protected routes
    ApiRoute(() => {
       
        Route.get("/", "NumeracaoController.index"); 

        Route.post("/", "NumeracaoController.store");

        Route.get("/:id", "NumeracaoController.show");

        Route.put("/:id", "NumeracaoController.update");

        Route.delete("/:id", "NumeracaoController.destroy");  

        Route.get("/range/:numero", "NumeracaoController.findByRangeSip"); 
        
        Route.post("/range", "NumeracaoController.range").validator('Numeracao/rangeStartEndNumberExist')

        Route.get("/number/verify/:number", "NumeracaoController.verifyNumber");
        
  },"numeracoes").middleware(["auth"]);