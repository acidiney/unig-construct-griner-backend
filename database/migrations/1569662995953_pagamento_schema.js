'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentoSchema extends Schema {
  up () {
    this.table('pagamentos', (table) => {
      // alter table
      table.double("valor_recebido", 50, 2).nullable().alter();
      table.double("troco", 50, 2).nullable().alter();
      table.double("total_pago", 50, 2).nullable().alter();
    })
  }

  down () {
    this.table('pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PagamentoSchema
