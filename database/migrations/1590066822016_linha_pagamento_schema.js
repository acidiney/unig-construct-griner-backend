'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaPagamentoSchema extends Schema {
  up () {
    this.table('linha_pagamentos', (table) => {
      // alter table
      table.integer('banco_id').unsigned().nullable().references('id').inTable('bancos').onUpdate('CASCADE').index('bancos_id').after("data_pagamento");
    })
  }

  down () {
    this.table('linha_pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaPagamentoSchema
