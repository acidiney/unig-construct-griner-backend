'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SipServicoNumeroSchema extends Schema {
  up () {
    this.create('sip_servico_numeros', (table) => {
      table.increments()
      table.integer('numero');
      table.boolean('status').nullable().defaultTo(true);
      table.integer('numeracoes_id').unsigned().nullable().references('id').inTable('numeracoes'); 
      table.integer('sip_servico_id').unsigned().nullable().references('id').inTable('sip_servicos');
      table.integer('user_id').unsigned().nullable().references('id').inTable('users');
      table.timestamps()
    })
  }

  down () {
    this.drop('sip_servico_numeros')
  }
}

module.exports = SipServicoNumeroSchema
