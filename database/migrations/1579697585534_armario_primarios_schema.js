'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArmarioPrimariosSchema extends Schema {
  up () {
    this.table('armario_primarios', (table) => {
      // alter table
	table.integer('central_id').unsigned().references('id').inTable('central_recurso_redes').after('descricao')
    })
  }

  down () {
    this.table('armario_primarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ArmarioPrimariosSchema
