'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoEntidadeCativadoraSchema extends Schema {
  up () {
    this.create('tipo_entidade_cativadoras', (table) => {
      table.increments()
      table.string("nome", 200).nullable();
       table.integer('user_id').unsigned().notNullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_entidade_cativadoras')
  }
}

module.exports = TipoEntidadeCativadoraSchema
