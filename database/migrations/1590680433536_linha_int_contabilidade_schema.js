'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaIntContabilidadeSchema extends Schema {
  up () {
    this.create('linha_int_contabilidades', (table) => {
      table.increments() 

      table.integer('contaRazaoDebito').unsigned().nullable();
      table.integer('contaRazaoMercadoria').unsigned().nullable();
      table.integer('contaRazaoImposto').unsigned().nullable(); 

      table.integer('moeda_id').unsigned().nullable().references('id').inTable('moedas').onUpdate('CASCADE')
      table.integer('direccao_id').unsigned().nullable().references('id').inTable('direccaos').onUpdate('CASCADE')

      table.integer('int_contabilidade_id').unsigned().nullable().references('id').inTable('int_contabilidades').onUpdate('CASCADE')
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('linha_int_contabilidades')
  }
}

module.exports = LinhaIntContabilidadeSchema
