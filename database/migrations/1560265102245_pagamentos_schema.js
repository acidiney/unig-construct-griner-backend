'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentosSchema extends Schema {
  up () {
    this.table('pagamentos', (table) => {
      // alter table
	  table.string('documento_sigla',30).nullable().after('forma_pagamento_id'); 
	  
    })
  }

  down () {
    this.table('pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PagamentosSchema
