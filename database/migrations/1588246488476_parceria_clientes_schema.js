'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ParceriaClientesSchema extends Schema {
  up () {
    this.table('parceria_clientes', (table) => {
      // alter table
	table.string('nacao_sigla',3).after('imposto_id')
    })
  }

  down () {
    this.table('parceria_clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ParceriaClientesSchema
