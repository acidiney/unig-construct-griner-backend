'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CdmaServicoSchema extends Schema {
  up () {
    this.create('cdma_servicos', (table) => {
      table.increments()
	  table.integer('cdma_equipamento_id').unsigned().notNullable().references('id').inTable('cdma_equipamentos').index('cdma_equipamento_index');
	  table.integer('ChaveServico', 12).notNullable()
	  table.integer('servico_id').unsigned().notNullable().references('id').inTable('servicos').index('servico_id_index');
      table.timestamps()
    })
  }

  down () {
    this.drop('cdma_servicos')
  }
}

module.exports = CdmaServicoSchema
