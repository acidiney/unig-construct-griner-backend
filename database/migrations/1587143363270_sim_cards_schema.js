'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SimCardsSchema extends Schema {
  up () {
    this.table('sim_cards', (table) => {
      // alter table
	table.string('iccid',130).alter()
    })
  }

  down () {
    this.table('sim_cards', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SimCardsSchema
