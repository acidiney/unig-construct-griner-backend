'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EntidadeCativadoraSchema extends Schema {
  up () {
    this.table('entidade_cativadoras', (table) => {
      // alter table 
      table.integer('tipo_entidade_cativadora_id').unsigned().nullable().references('id').inTable('tipo_entidade_cativadoras').onUpdate('CASCADE').index();
    })
  }

  down () {
    this.table('entidade_cativadoras', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EntidadeCativadoraSchema
