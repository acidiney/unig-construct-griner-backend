'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
      table.integer('caixa_id').nullable().unsigned().index();
      table.foreign('caixa_id').references('id').on('caixas').onDelete('cascade')
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturaSchema
