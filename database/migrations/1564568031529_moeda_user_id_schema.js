'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MoedaUserIdSchema extends Schema {
  up () {
    this.table('moedas', (table) => {
      // alter table
      table.integer('user_id').unsigned().index('user_id_index').references('id').inTable('users').nullable();
    })
  }

  down () {
    this.table('moedas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = MoedaUserIdSchema
