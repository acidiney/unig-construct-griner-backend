'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoServicoSchema extends Schema {
  up () {
    this.create('estado_servicos', (table) => {
      table.increments() 
      table.string('nome',255).notNullable(); 
      table.timestamps()
    })
  }

  down () {
    this.drop('estado_servicos')
  }
}

module.exports = EstadoServicoSchema
