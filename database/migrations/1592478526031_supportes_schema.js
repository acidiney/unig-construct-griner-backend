'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SupportesSchema extends Schema {
  up () {
    this.table('supportes', (table) => {
      table.integer("enviado_por_email").defaultTo(0).after('img_base64');

      // alter table
    })
  }

  down () {
    this.table('supportes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SupportesSchema
