'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EntidadeCativadorasSchema extends Schema {
  up () {
    this.table('entidade_cativadoras', (table) => {
      // alter table
	table.string("nome", 255).nullable().alter();
    })
  }

  down () {
    this.table('entidade_cativadoras', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EntidadeCativadorasSchema
