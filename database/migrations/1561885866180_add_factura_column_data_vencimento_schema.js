'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddFacturaColumnDataVencimentoSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
      table.date('data_venciemento').nullable();  
    })
  }

  down () {
    this.table('factuas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddFacturaColumnDataVencimentoSchema
