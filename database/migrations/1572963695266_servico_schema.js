'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServicoSchema extends Schema {
  up () {
    this.table('servicos', (table) => {
      // alter table
      table.integer('tecnologia_id').unsigned().nullable().references('id').inTable('tecnologias').onUpdate('CASCADE').index();
    })
  }

  down () {
    this.table('servicos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ServicoSchema
