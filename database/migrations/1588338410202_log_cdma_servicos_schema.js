'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogCdmaServicosSchema extends Schema {
  up () {
    this.create('log_cdma_servicos', (table) => {
      table.increments()
	  table.string('operacao',50)
	  table.integer('cdma_servico_id').unsigned().references('id').inTable('cdma_servicos')
	  table.integer('old_cdma_equipamento_id')
	  table.integer('new_cdma_equipamento_id')
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('log_cdma_servicos')
  }
}

module.exports = LogCdmaServicosSchema
