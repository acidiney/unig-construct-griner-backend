'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoComunicacaoSchema extends Schema {
  up () {
    this.create('tipo_comunicacaos', (table) => {
      
      table.increments()

      table.string("nome", 255).notNullable();
      table.string("descricao", 255).nullable();
      table.boolean("status").notNullable().defaultTo(true);
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE')
      table.timestamp('created_at').defaultTo(this.fn.now())
      table.timestamp('updated_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('tipo_comunicacaos')
  }
}

module.exports = TipoComunicacaoSchema
