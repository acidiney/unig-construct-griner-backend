'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogEstadoReclamacaosSchema extends Schema {
  up () {
    this.table('log_estado_reclamacaos', (table) => {
      // alter table
	table.text('observacao_old').nullable().after('id_estado_novo')
	table.text('observacao_new').nullable().after('observacao_old')
    })
  }

  down () {
    this.table('log_estado_reclamacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LogEstadoReclamacaosSchema
