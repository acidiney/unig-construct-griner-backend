'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SuportePrioridadesSchema extends Schema {
  up () {
    this.table('suporte_prioridades', (table) => {
      // alter table
      table.integer('estado').defaultTo(1).after('designacao')
    })
  }

  down () {
    this.table('suporte_prioridades', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SuportePrioridadesSchema
