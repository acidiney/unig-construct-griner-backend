'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogMudancaContaServicoSchema extends Schema {
  up () {
    this.create('log_mudanca_conta_servicos', (table) => {
      table.increments()
	  table.integer('conta_antiga_id').unsigned().references('id').inTable('contas')
	  table.integer('conta_nova_id').unsigned().references('id').inTable('contas')
	  table.integer('servico_id').unsigned().references('id').inTable('servicos')
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('log_mudanca_conta_servicos')
  }
}

module.exports = LogMudancaContaServicoSchema
