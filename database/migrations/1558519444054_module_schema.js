'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ModuleSchema extends Schema {
  up () {
    this.create('modules', (table) => {
      table.increments()
	  table.string('nome', 30).notNullable().unique();
	  table.string('descricao', 100).nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('modules')
  }
}

module.exports = ModuleSchema
