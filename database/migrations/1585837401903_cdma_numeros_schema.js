'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CdmaNumerosSchema extends Schema {
  up () {
    this.table('cdma_numeros', (table) => {
      // alter table
	table.integer('user_id').unsigned().references('id').inTable('users').after('filialID')
    })
  }

  down () {
    this.table('cdma_numeros', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CdmaNumerosSchema
