'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RecibosSchema extends Schema {
  up () {
    this.table('recibos', (table) => {
      // alter table
	  table.string('observacao', 255).nullable().after('status_reason');
    })
  }

  down () {
    this.table('recibos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RecibosSchema
