'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClienteSchema extends Schema {
  up () {
    this.table('clientes', (table) => {
      // alter table
      table.integer('gestor_id').unsigned().index('gestor_id').references('id').inTable('users').nullable().after('gestor_conta')
    })
  }

  down () {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClienteSchema
