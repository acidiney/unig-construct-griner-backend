'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BillRunSchema extends Schema {
  up () {
    this.create('bill_runs', (table) => {
      table.increments()
      table.integer('facturaMesAuto_id').nullable().unsigned().index();
      table.foreign('facturaMesAuto_id').references('id').on('factura_mes_automaticas').onDelete('cascade')

      table.integer('conta_id').nullable().unsigned().index();
      table.foreign('conta_id').references('id').on('contas').onDelete('cascade')

      table.integer('factura_utilitie_id').nullable().unsigned().index();
      table.timestamps()
    })
  }

  down () {
    this.drop('bill_runs')
  }
}

module.exports = BillRunSchema
