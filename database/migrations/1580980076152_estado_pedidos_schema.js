'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoPedidosSchema extends Schema {
  up () {
    this.table('estado_pedidos', (table) => {
      // alter table
	table.string('rota_form',200).nullable().after('sigla')
	table.integer('tipo_pedido_id').unsigned().references('id').inTable('tipo_pedidos').after('rota_form')
    })
  }

  down () {
    this.table('estado_pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EstadoPedidosSchema
