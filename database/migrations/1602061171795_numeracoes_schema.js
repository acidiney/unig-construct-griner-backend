'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NumeracoesSchema extends Schema {
  up () {
    this.table('numeracoes', (table) => {
      // alter table
      table.integer('numeracoes_origens_id').unsigned().nullable().references('id').inTable('numeracoes_origens').after("central_recurso_rede_id"); 
    })
  }

  down () {
    this.table('numeracoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = NumeracoesSchema
