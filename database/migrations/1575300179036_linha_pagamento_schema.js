'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaPagamentoSchema extends Schema {
  up () {
    this.table('linha_pagamentos', (table) => {
      // alter table
      table.string("referencia").nullable().alter();
    })
  }

  down () {
    this.table('linha_pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaPagamentoSchema
