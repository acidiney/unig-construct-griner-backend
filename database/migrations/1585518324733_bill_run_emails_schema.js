'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BillRunEmailsSchema extends Schema {
  up () {
    this.table('bill_run_emails', (table) => {
      // alter table
	table.text('server_response').after('cliente_email')
    })
  }

  down () {
    this.table('bill_run_emails', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BillRunEmailsSchema
