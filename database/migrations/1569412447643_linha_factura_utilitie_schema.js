'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturaUtilitieSchema extends Schema {
  up () {
    this.create('linha_factura_utilities', (table) => {
      table.increments()
      table.double("total").notNullable(); 
      table.integer('quantidade').notNullable().defaultTo(1);
      table.double('valor').notNullable();
      table.double("valor_imposto").notNullable();
      table.double("linhaTotalSemImposto").notNullable();
      table.double("valor_desconto").nullable().defaultTo(0.0);


      table.integer('factura_utilitie_id').unsigned().notNullable().references('id').inTable('factura_utilities').onUpdate('CASCADE').index();
      table.integer('servico_id').unsigned().notNullable().references('id').inTable('servicos').onUpdate('CASCADE').index();
      table.integer('tarifario_id').unsigned().notNullable().references('id').inTable('tarifarios').onUpdate('CASCADE').index();
      table.integer('plano_preco_id').unsigned().notNullable().references('id').inTable('plano_precos').onUpdate('CASCADE').index();
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index();
      table.integer('imposto_id').unsigned().notNullable().references('id').inTable('impostos').onUpdate('CASCADE').index();

      table.timestamps()
    })
  }

  down () {
    this.drop('linha_factura_utilities')
  }
}

module.exports = LinhaFacturaUtilitieSchema
