'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TarifarioSchema extends Schema {
  up () {
    this.create('tarifarios', (table) => {
      table.increments()

      table.string("descricao", 255).notNullable();
      table.string("condicoes",255).nullable();
      table.integer("credMensalInDefault")
      table.integer("plano_preco_id");
      table.boolean('estado').nullable().defaultTo(true); 
      table.date("dataEstado").nullable();
      

      table.timestamps()
    })
  }

  down () {
    this.drop('tarifarios')
  }
}

module.exports = TarifarioSchema
