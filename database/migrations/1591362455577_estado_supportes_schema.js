'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoSupportesSchema extends Schema {
  up () {
    this.table('estado_supportes', (table) => {
      // alter table
      table.integer("estado").defaultTo(1).after('designacao')
    })
  }

  down () {
    this.table('estado_supportes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EstadoSupportesSchema
