'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentoSchema extends Schema {
  up () {
    this.table('pagamentos', (table) => {
      // alter table      
      table.dropForeign('forma_pagamento_id', "pagamentos_forma_pagamento_id_foreign");       
    })
  }

  down () {
    this.table('pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PagamentoSchema
