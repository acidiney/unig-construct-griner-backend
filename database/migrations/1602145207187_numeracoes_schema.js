'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NumeracoesSchema extends Schema {
  up () {
    this.table('numeracoes', (table) => {
      // alter table
      table.datetime("data_desactivacao").nullable().after("status"); 
    })
  }

  down () {
    this.table('numeracoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = NumeracoesSchema
