'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentoSchema extends Schema {
  up () {
    this.table('pagamentos', (table) => {
      // alter table
      
      table.string("status").notNullable().defaultTo('N').after("documento_sigla"); 
      table.boolean("isTrocoAdiantamento").notNullable().defaultTo(false).after("status"); 
      table.text("annulment_reason").nullable().after("isTrocoAdiantamento");
      table.datetime("annulment_date").nullable().after("annulment_reason"); 
      
    })
  }

  down () {
    this.table('pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PagamentoSchema
