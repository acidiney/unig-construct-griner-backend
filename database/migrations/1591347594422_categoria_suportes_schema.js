'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategoriaSuportesSchema extends Schema {
  up () {
    this.table('categoria_suportes', (table) => {
      // alter table
      table.integer('estado').defaultTo(1).after('tipoCategoria')

    })
  }

  down () {
    this.table('categoria_suportes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CategoriaSuportesSchema
