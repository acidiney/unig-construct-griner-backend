'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CdmaEquipamentosSchema extends Schema {
  up () {
    this.table('cdma_equipamentos', (table) => {
      // alter table
	table.integer('IDEquipamentoAKEY',11).nullable().alter()
	table.integer('IDAgente',11).nullable().alter()
	table.integer('Fabricante',11).nullable().alter()
	table.integer('IDModelo',11).nullable().alter()
    })
  }

  down () {
    this.table('cdma_equipamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CdmaEquipamentosSchema
