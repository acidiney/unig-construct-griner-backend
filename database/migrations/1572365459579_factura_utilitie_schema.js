'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaUtilitieSchema extends Schema {
  up () {
    this.table('factura_utilities', (table) => {
      // alter table
      table.integer('cambio_id').unsigned().nullable().alter();
      table.integer('moeda_id').unsigned().nullable().alter(); 
      table.double('valor_cambio',100,2).nullable().alter(); 
      table.string("status",5).nullable().alter(); 
      ;
    })
  }

  down () {
    this.table('factura_utilities', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturaUtilitieSchema
