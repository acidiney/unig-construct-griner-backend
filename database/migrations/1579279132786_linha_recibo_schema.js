'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaReciboSchema extends Schema {
  up () {
    this.table('linha_recibos', (table) => {
      // alter table
      table.double("valor_cativado", 250, 2).nullable();
    })
  }

  down () {
    this.table('linha_recibos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaReciboSchema
