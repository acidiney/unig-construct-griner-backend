'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidoSchema extends Schema {
  up () {
    this.table('pedidos', (table) => {
      // alter table 
      table.string("capacidade", 200).nullable();
      table.string("origem", 200).nullable();
      table.string("destino", 200).nullable();
    })
  }

  down () {
    this.table('pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PedidoSchema
