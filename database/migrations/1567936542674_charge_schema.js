'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargeSchema extends Schema {
  up () {
    this.create('charges', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('charges')
  }
}

module.exports = ChargeSchema
