'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DepositoCaixaSchema extends Schema {
  up () {
    this.create('deposito_caixas', (table) => {

      table.increments()
      table.double("valor_deposito", 30, 2).nullable();
      table.date("data_deposito").nullable(); 
      table.integer("referencia_banco").nullable(); 

      table.text('observacao').nullable();

      table.integer('banco_id').nullable().unsigned().index();
      table.foreign('banco_id').references('id').on('bancos').onDelete('cascade')

      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('deposito_caixas')
  }
}

module.exports = DepositoCaixaSchema
