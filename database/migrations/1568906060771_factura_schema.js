'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
       table.double("total_contra_valor", 25, 2).nullable();
       table.double("valor_cambio", 25, 2).nullable(); 
       table.integer('cambio_id').nullable();
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturaSchema
