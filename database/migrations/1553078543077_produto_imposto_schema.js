'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoImpostoSchema extends Schema {
  up () {
    this.create('produto_impostos', (table) => {
      table.increments()
      table.integer('regra_imposto_id').notNullable()
      table.integer('produto_id').notNullable()
      table.integer('imposto_id').notNullable()
      table.integer('user_id').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('produto_impostos')
  }
}

module.exports = ProdutoImpostoSchema
