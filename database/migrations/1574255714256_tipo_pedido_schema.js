'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoPedidoSchema extends Schema {
  up () {
    this.create('tipo_pedidos', (table) => {
      table.increments()
	  table.string('slug',250).notNullable().unique()
	  table.string('descricao',250).notNullable().unique()
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_pedidos')
  }
}

module.exports = TipoPedidoSchema
