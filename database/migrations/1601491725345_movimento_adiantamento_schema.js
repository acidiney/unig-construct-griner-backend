'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MovimentoAdiantamentoSchema extends Schema {
  up () {
    this.table('movimento_adiantamentos', (table) => {
      // alter table
      table.string("status").nullable().defaultTo('N').after("saldado_recibo");  
      table.text("annulment_reason").nullable().after("status");
      table.datetime("annulment_date").nullable().after("annulment_reason"); 
    })
  }

  down () {
    this.table('movimento_adiantamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = MovimentoAdiantamentoSchema
