'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresaSchema extends Schema {
  up () {
    this.table('empresas', (table) => {
      // alter table
       table.boolean('isConvertLineFactura').nullable().defaultTo(false);       
    })
  }

  down () {
    this.table('empresas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EmpresaSchema
