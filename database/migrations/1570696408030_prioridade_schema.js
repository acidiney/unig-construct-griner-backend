'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PrioridadeSchema extends Schema {
  up () {
    this.create('prioridades', (table) => {
      table.increments()
	  table.string('designacao',50).notNullable()
	  table.integer('user_id').unsigned().index('user_id').references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('prioridades')
  }
}

module.exports = PrioridadeSchema
