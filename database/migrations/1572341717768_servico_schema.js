'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServicoSchema extends Schema {
  up () {
    this.table('servicos', (table) => {
      // alter table
      table.string("ligacao_cacti", 150).nullable();
      table.string("nome", 150).nullable();
      table.text("descricao_operacao").nullable();
      table.date('dataContrato').nullable(); 
    })
  }

  down () {
    this.table('servicos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ServicoSchema
