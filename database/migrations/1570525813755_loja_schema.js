'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojaSchema extends Schema {
  up () {
    this.table('lojas', (table) => {
      // alter table
      table.integer('user_chefe_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();
    })
  }

  down () {
    this.table('lojas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LojaSchema
