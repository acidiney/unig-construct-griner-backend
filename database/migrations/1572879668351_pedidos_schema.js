'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidosSchema extends Schema {
  up () {
    this.table('pedidos', (table) => {
      // alter table
	table.integer('estado').unsigned().references('id').inTable('estado_pedidos').after('observacao')
    })
  }

  down () {
    this.table('pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PedidosSchema
