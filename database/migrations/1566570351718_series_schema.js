'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeriesSchema extends Schema {
  up () {
    this.table('series', (table) => {
      // alter table
      table.string("nome", 55).notNullable().alter();
    })
  }

  down () {
    this.table('series', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SeriesSchema
