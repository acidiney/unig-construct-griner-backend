'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReclamacaosSchema extends Schema {
  up () {
    this.table('reclamacaos', (table) => {
      // alter table
	  table.integer('estado_reclamacao_id').unsigned().index('estado_reclamacao_id').references('id').inTable('estado_reclamacaos').nullable().after('prioridade_id')
    })
  }

  down () {
    this.table('reclamacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ReclamacaosSchema
