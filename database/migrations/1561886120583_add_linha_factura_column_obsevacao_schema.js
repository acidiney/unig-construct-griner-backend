'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddLinhaFacturaColumnObsevacaoSchema extends Schema {
  up () {
    this.table('linha_facturas', (table) => {
      // alter table
      table.text('observacao').nullable();  
    })
  }

  down () {
    this.table('linha_facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddLinhaFacturaColumnObsevacaoSchema
