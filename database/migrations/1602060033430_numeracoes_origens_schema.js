'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NumeracoesOrigensSchema extends Schema {
  up () {
    this.create('numeracoes_origens', (table) => {
      table.increments()
      table.string('designacao',200).nullable()
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('numeracoes_origens')
  }
}

module.exports = NumeracoesOrigensSchema
