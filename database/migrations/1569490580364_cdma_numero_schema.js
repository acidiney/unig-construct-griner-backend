'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CdmaNumeroSchema extends Schema {
  up () {
    this.create('cdma_numeros', (table) => {
      table.increments()
	  table.integer('ChaveServico', 11).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('cdma_numeros')
  }
}

module.exports = CdmaNumeroSchema
