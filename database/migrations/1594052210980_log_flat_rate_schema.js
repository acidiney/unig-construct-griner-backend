'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogFlatRateSchema extends Schema {
  up () {
    this.create('log_flat_rates', (table) => {
      table.increments()

      table.double("valor", 250, 2).nullable();
      table.string('capacidade',55).nullable();
      table.string('origem',255).nullable();
      table.string('destino',255).nullable();
      table.string('descricao',255).nullable();

      table.integer('artigo_id').unsigned().nullable();
      table.integer('moeda_id').unsigned().nullable();
      table.integer('imposto_id').unsigned().nullable();

      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE')

 
      table.timestamps()
    })
  }

  down () {
    this.drop('log_flat_rates')
  }
}

module.exports = LogFlatRateSchema
