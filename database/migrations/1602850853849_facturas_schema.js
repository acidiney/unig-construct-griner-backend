'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturasSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      table.integer('anulado_user_id').nullable()
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturasSchema
