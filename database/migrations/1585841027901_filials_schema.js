'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FilialsSchema extends Schema {
  up () {
    this.table('filials', (table) => {
      // alter table
	table.integer('indicativo_telefone',3).nullable().after('user_id')
    })
  }

  down () {
    this.table('filials', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FilialsSchema
