'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WimaxSchema extends Schema {
  up () {
    this.create('wimaxes', (table) => {
      table.increments()
	  table.string('ServicoID', 200).nullable()
	  table.string('NbrSequencia', 200).nullable()
	  table.string('EqNbrSerie', 200).nullable()
	  table.string('Mainkey', 200).nullable()
	  table.string('MAC', 200).nullable()
	  table.string('PasswordDados', 200).nullable()
	  table.string('PasswordVoz', 200).nullable()
	  table.string('Fabricante', 200).nullable()
	  table.string('ModeloTelefone', 200).nullable()
	  table.boolean('Status').notNullable().defaultTo('false')
	  table.string('StatusData', 200).nullable()
	  table.string('AgenciaFilialID', 200).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('wimaxes')
  }
}

module.exports = WimaxSchema
