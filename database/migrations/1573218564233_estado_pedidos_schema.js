'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoPedidosSchema extends Schema {
  up () {
    this.table('estado_pedidos', (table) => {
      // alter table
	 table.integer('user_id').unsigned().index('user_id').references('id').inTable('users').nullable().after('sigla')
    })
  }

  down () {
    this.table('estado_pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EstadoPedidosSchema
