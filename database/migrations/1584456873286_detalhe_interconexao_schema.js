'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetalheInterconexaoSchema extends Schema {
  up () {
    this.create('detalhe_interconexaos', (table) => {
      table.increments()
      table.string("v", 200).nullable(),
      table.string("origin", 200).nullable(),
      table.string("destination",200).nullable(),
      table.string('service',255).nullable(),
      table.double('call',255, 5).nullable(),
      table.double('minutes',255,5).nullable(),
      table.double('rate', 255,5).nullable(),
      table.double('amount', 255,5).nullable(),
      table.string('billing_operator', 255)
      table.timestamps()
    })
  }

  down () {
    this.drop('detalhe_interconexaos')
  }
}

module.exports = DetalheInterconexaoSchema
