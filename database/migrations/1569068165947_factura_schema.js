'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
      table.double('totalKwanza', 25,2).nullable();
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturaSchema
