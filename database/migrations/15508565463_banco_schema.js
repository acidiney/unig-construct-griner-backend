'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BancoSchema extends Schema {
  up () {
    this.create('bancos', (table) => {
      table.increments()

      table.string("nome", 50).notNullable();
      table.string("abreviatura", 50).notNullable();
      table.string("numero_conta").notNullable().unique();
      table.string("iban").notNullable().unique(); 
      table.boolean("activo").notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('bancos')
  }
}

module.exports = BancoSchema
