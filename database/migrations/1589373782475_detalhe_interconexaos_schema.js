'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetalheInterconexaosSchema extends Schema {
  up () {
    this.table('detalhe_interconexaos', (table) => {
      // alter table
	table.string('nacao_sigla',3).after('billing_operator')
    })
  }

  down () {
    this.table('detalhe_interconexaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = DetalheInterconexaosSchema
