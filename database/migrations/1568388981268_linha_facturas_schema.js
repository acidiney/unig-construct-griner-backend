'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturasSchema extends Schema {
  up () {
    this.table('linha_facturas', (table) => {
      table.double('total', 25,2).notNullable().alter();
      table.double('valor', 25,2).notNullable().alter();
      table.double('valor_imposto', 25,2).notNullable().alter();
      table.double('valor_desconto', 25,2).notNullable().alter();
    })
  }

  down () {
    this.table('linha_facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaFacturasSchema
