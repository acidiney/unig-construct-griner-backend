'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlanoPrecoSchema extends Schema {
  up () {
    this.table('plano_precos', (table) => {
      // alter table
      table.integer('moeda_id').nullable().unsigned().index();
      table.foreign('moeda_id').references('id').on('moedas').onDelete('cascade')
    })
  }

  down () {
    this.table('plano_precos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PlanoPrecoSchema
