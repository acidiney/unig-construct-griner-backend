'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SipServicoNumeroSchema extends Schema {
  up () {
    this.table('sip_servico_numeros', (table) => {
      // alter table
      table.dropColumn('numero');
    })
  }

  down () {
    this.table('sip_servico_numeros', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SipServicoNumeroSchema
