'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoClienteSchema extends Schema {
  up () {
    this.create('tipo_clientes', (table) => {
      table.increments()
      table.string("tipoClienteDesc", 120).notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_clientes')
  }
}

module.exports = TipoClienteSchema
