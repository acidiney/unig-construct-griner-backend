'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BancoUserIdSchema extends Schema {
  up () {
    this.table('bancos', (table) => {
      // alter table
      table.integer('user_id').unsigned().index('user_id_index').references('id').inTable('users').notNullable();
    })
  }

  down () {
    this.table('bancos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BancoUserIdSchema
