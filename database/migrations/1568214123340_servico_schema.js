'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServicoSchema extends Schema {
  up () {
    this.create('servicos', (table) => {
      table.increments() 
      table.integer('chaveServico').nullable();
      table.integer('conta_id').nullable();
      table.integer('tipoServico').nullable();
      table.integer('contrato_id').nullable();
      table.boolean('estado').nullable().defaultTo(true); 
      table.date("dataEstado").nullable();      
      table.date("dataCriacao").nullable();



      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade');
      table.timestamps()
    })
  }

  down () {
    this.drop('servicos')
  }
}

module.exports = ServicoSchema
