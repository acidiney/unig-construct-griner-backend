'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargeSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      // alter table
      table.integer('daily').unsigned().nullable().after('valor');
      table.double("valorOriginal", 250, 2).nullable().after('daily');
    })
  }

  down () {
    this.table('charges', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargeSchema
