'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BillRunHeaderSchema extends Schema {
  up () {
    this.create('bill_run_headers', (table) => {
      table.increments()

      table.integer('mes').nullable()
      table.integer("ano").nullable().unsigned();
      table.integer('estado').nullable().defaultTo(0);

      table.integer('serie_id').unsigned().nullable().references('id').inTable('series').onUpdate('CASCADE').index();
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();
      table.timestamps()
    })
  }

  down () {
    this.drop('bill_run_headers')
  }
}

module.exports = BillRunHeaderSchema
