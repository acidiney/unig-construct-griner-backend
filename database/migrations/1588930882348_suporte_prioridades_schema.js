'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SuportePrioridadesSchema extends Schema {
  up () {
    this.create('suporte_prioridades', (table) => {
      table.increments()
      table.string('designacao',50).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('suporte_prioridades')
  }
}

module.exports = SuportePrioridadesSchema
