'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContaSchema extends Schema {
  up () {
    this.create('contas', (table) => {
      table.increments()

      table.string('contaDescricao').nullable();
      table.integer('cliente_id').nullable();
      table.integer('tipoServico').nullable();
      table.integer("gestorConta").nullable();
      table.integer("moeda_id").nullable();
      table.integer("agencia_id").nullable();
      table.boolean('estado').nullable().defaultTo(true); 
      table.date("dataEstado").nullable();      
      table.date("dataCriacao").nullable();

      table.timestamps()
    })
  }

  down () {
    this.drop('contas')
  }
}

module.exports = ContaSchema
