'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoSchema extends Schema {
  up () {
    this.table('contratoes', (table) => {
      // alter table
      table.date('data_inicio').nullable().after('id')
      table.date('data_fim').nullable().after('data_inicio')
      table.string('observacao',200).nullable().after('data_fim')
      table.boolean('status').nullable().defaultTo(0).after('observacao')
      table.integer('parceria_id').unsigned().references('id').inTable('parceria_clientes').after('status')
    })
  }

  down () {
    this.table('contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContratoSchema
