'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaMesAutomaticaSchema extends Schema {
  up () {
    this.create('factura_mes_automaticas', (table) => {

      table.increments() 

      table.integer('mes').nullable()
      table.integer("ano").nullable().unsigned();
      

      table.integer('serie_id').nullable()

      table.integer('user_id').nullable().unsigned()

      table.timestamps()
    })
  }

  down () {
    this.drop('factura_mes_automaticas')
  }
}

module.exports = FacturaMesAutomaticaSchema
