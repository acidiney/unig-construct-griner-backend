'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReferenciaBancariaSchema extends Schema {
  up () {
    this.table('referencia_bancarias', (table) => {
      // alter table
      table.boolean("estado").notNullable().defaultTo(true).after("banco_id");
      table.integer('recibo_id').unsigned().nullable().references('id').inTable('recibos').onUpdate('CASCADE').index('recibo_id_index').after("banco_id");
      table.integer('factura_id').unsigned().nullable().references('id').inTable('facturas').onUpdate('CASCADE').index('factura_id_index').after("banco_id");
    })
  }

  down () {
    this.table('referencia_bancarias', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ReferenciaBancariaSchema
