'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentosSchema extends Schema {
  up () {
    this.table('pagamentos', (table) => {
      // alter table
	   table.dropColumn('descricao')
	   table.dropColumn('imagem')
    })
  }

  down () {
    this.table('pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PagamentosSchema
