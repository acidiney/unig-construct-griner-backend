'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NumeracaoSchema extends Schema {
  up () {
    this.table('numeracaos', (table) => {
      // alter table
      table.integer('central_recurso_rede_id').unsigned().references('id').inTable('central_recurso_redes').after('filial_id');
    })
  }

  down () {
    this.table('numeracaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = NumeracaoSchema
