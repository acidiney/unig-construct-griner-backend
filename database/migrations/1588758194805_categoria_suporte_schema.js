'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategoriaSuporteSchema extends Schema {
  up () {
    this.create('categoria_suportes', (table) => {
      table.increments()
   
      table.timestamps()
    })
  }

  down () {
    this.drop('categoria_suportes')
  }
}

module.exports = CategoriaSuporteSchema
