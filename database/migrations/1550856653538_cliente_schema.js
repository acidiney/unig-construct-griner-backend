'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClienteSchema extends Schema {
  up () {
      this.create('clientes', (table) => {
      table.increments()
      table.string('nome', 120).notNullable();
      table.string('telefone', 254).nullable()
      table.string("morada", 254).nullable();
      table.string('contribuente', 20)
      table.string('contactPerson', 254).nullable() 
      table.string('buildingNumber', 254).nullable()
      table.string('streetName', 254).nullable()
      table.string('addressDetail', 254).nullable() 
      table.string('city', 254).nullable()
      table.string('province', 254).nullable()
      table.string('email', 254).nullable()
      table.string('unificado', 254).notNullable()
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index'); 
      table.timestamps()
    })
  }

  down () {
    this.drop('clientes')
  }
}

module.exports = ClienteSchema
