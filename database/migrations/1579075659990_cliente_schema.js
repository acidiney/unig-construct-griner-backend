'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClienteSchema extends Schema {
  up () {
    this.table('clientes', (table) => {
      // alter table
       table.integer('entidade_cativadora_id').unsigned().nullable().references('id').inTable('entidade_cativadoras').onUpdate('CASCADE').index();
      
    })
  }

  down () {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClienteSchema
