'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SipServicoSchema extends Schema {
  up () {
    this.create('sip_servicos', (table) => {
      table.increments()
      table.integer('trunk_in').unsigned().notNullable();
      table.string('descricao',255).nullable();
      table.boolean('status').nullable().defaultTo(true);
      table.integer('servico_id').unsigned().nullable().references('id').inTable('servicos').onUpdate('CASCADE')
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE')
      
      table.timestamp('created_at').defaultTo(this.fn.now())
      table.timestamp('updated_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('sip_servicos')
  }
}

module.exports = SipServicoSchema
