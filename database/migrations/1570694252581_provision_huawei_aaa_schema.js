'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProvisionHuaweiAaaSchema extends Schema {
  up () {
    this.create('provision_huawei_aaas', (table) => {
      table.increments()

      table.integer('acessoFeature').nullable();
      table.integer('huaweiTarifario').nullable()
      table.string("huaweiTarifarioCode", 255).notNullable();
      table.string("huaweiTarifarioDesc", 255).notNullable();
      table.integer("huaweiPayType").notNullable();
      table.string("huaweiSubsType", 50).notNullable();
      table.string("huaweiSubsTypeFormated", 50).notNullable();
      table.string("huaweiDomain", 90).notNullable();
      table.string("huaweiDateFormat", 50).notNullable();

      table.double("valor", 100,2).notNullable();
      table.string("limMaxTempo").nullable();
      table.string("unidLimMaxTempo").nullable();

      table.integer('produto_id').nullable().unsigned().index(); 

      table.integer('tarifario_id').nullable().unsigned().index();
      table.foreign('tarifario_id').references('id').on('tarifarios').onDelete('cascade')
      
      table.timestamps()
    })
  }

  down () {
    this.drop('provision_huawei_aaas')
  }
}

module.exports = ProvisionHuaweiAaaSchema
