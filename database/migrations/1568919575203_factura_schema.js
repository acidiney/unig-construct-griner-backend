'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
      table.string("moeda_iso", 25).nullable();
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturaSchema
