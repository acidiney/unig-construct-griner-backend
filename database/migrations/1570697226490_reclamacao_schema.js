'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReclamacaoSchema extends Schema {
  up () {
    this.create('reclamacaos', (table) => {
      table.increments()
	  table.integer('tipo_reclamacao_id').unsigned().index('tipo_reclamacao_id').references('id').inTable('tipo_reclamacaos')
	  table.integer('prioridade_id').unsigned().index('prioridade_id').references('id').inTable('prioridades')
	  table.text('observacao').nullable()
	  table.integer('user_id').unsigned().index('user_id').references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('reclamacaos')
  }
}

module.exports = ReclamacaoSchema
