'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SerieSchema extends Schema {
  up () {
    this.create('series', (table) => {
      table.increments();
      table.string('nome', 50).notNullable();
      table.string('proximo_numero', 250).notNullable();
      table.boolean('activo').notNullable();
      table.integer('documento_id').unsigned().notNullable().references('id').inTable('documentos').onUpdate('CASCADE').index('documento_id_index');
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index');
      table.timestamps();
    })
  }

  down () {
    this.drop('series')
  }
}

module.exports = SerieSchema
