'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidosSchema extends Schema {
  up () {
    this.table('pedidos', (table) => {
      // alter table
	table.string('cabo',200).nullable().after('telefone')
	table.string('armario',200).nullable().after('cabo')
	table.string('central',200).nullable().after('armario')
	table.string('caixa',200).nullable().after('central')
    })
  }

  down () {
    this.table('pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PedidosSchema
