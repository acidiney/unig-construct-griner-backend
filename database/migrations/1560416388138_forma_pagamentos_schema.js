'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormaPagamentosSchema extends Schema {
  up () {
    this.table('forma_pagamentos', (table) => {
      // alter table
	  table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index').after('id');
    })
  }

  down () {
    this.table('forma_pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FormaPagamentosSchema
