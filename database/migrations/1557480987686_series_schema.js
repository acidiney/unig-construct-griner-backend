'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeriesSchema extends Schema {
  up () {
    this.table('series', (table) => {
      // alter table
	  table.string('movimento', 1).nullable().after('user_id');
	  table.string('tipo_movimento', 30).nullable();
    })
  }

  down () {
    this.table('series', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SeriesSchema
