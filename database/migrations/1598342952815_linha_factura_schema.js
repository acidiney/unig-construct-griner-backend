'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturaSchema extends Schema {
  up () {
    this.table('linha_facturas', (table) => {
      // alter table  
      table.integer('artigo_id').unsigned().references('id').inTable('produtos').alter()
    })
  }

  down () {
    this.table('linha_facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaFacturaSchema
 