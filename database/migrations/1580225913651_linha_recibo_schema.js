'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaReciboSchema extends Schema {
  up () {
    this.table('linha_recibos', (table) => {
      // alter table
      table.double("novo_valor_aberto", 250, 2).nullable().after('valor_saldado');
    })
  }

  down () {
    this.table('linha_recibos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaReciboSchema
