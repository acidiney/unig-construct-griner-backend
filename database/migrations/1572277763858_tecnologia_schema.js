'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TecnologiaSchema extends Schema {
  up () {
    this.create('tecnologias', (table) => {
      table.increments()
      table.string("nome", 50).nullable();
      table.string("opcao", 30).nullable(); // Mediação ou Flat Rate 
      table.string("tipoFacturacao", 30).nullable(); // POS-PAGO, PRE-PAGO OU MRC
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();

      table.timestamps()
    })
  }

  down () {
    this.drop('tecnologias')
  }
}

module.exports = TecnologiaSchema
