'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LteCpesSchema extends Schema {
  up () {
    this.create('lte_cpes', (table) => {
      table.increments()
	  table.string('fabricante',130)
	  table.string('modelo',130)
	  table.string('numero_serie',130)
	  table.string('tipo',70)
      table.timestamps()
    })
  }

  down () {
    this.drop('lte_cpes')
  }
}

module.exports = LteCpesSchema
