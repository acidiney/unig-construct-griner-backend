'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NotaComunicacaoSchema extends Schema {
  up () {
    this.create('nota_comunicacaos', (table) => {
      table.increments()
      
      table.text("feedback").notNullable();
      table.date("data_contacto").notNullable();
      table.time("hora_contacto").notNullable(); 
      table.boolean("status").notNullable().defaultTo(true);

      table.integer('tipo_comunicacao_id').unsigned().nullable().references('id').inTable('tipo_comunicacaos').onUpdate('CASCADE')   
      table.integer('cliente_id').unsigned().nullable().references('id').inTable('clientes').onUpdate('CASCADE')   
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE')      
      table.timestamp('created_at').defaultTo(this.fn.now())
      table.timestamp('updated_at').defaultTo(this.fn.now())
    })
  }

  down () {
    this.drop('nota_comunicacaos')
  }
}

module.exports = NotaComunicacaoSchema
