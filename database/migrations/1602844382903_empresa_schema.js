'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresaSchema extends Schema {
  up () {
    this.table('empresas', (table) => {
      table.integer('width').nullable().after('logotipo').defaultTo(28);
      table.integer('height').nullable().after('width').defaultTo(28);
    })
  }

  down () {
    this.table('empresas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EmpresaSchema
