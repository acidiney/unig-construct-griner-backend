'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IntContabilidadeSchema extends Schema {
  up () {
    this.create('int_contabilidades', (table) => {
      table.increments()
      table.string('tipoLancamento', 255).notNullable();
      table.integer('ano').unsigned().nullable();
      table.integer('diario').unsigned().nullable();
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('int_contabilidades')
  }
}

module.exports = IntContabilidadeSchema
