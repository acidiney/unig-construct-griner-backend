'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReciboSchema extends Schema {
  up () {
    this.table('recibos', (table) => {
      // alter table 
      table.integer('status_user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index').after('cliente_id')

    })
  }

  down () {
    this.table('recibos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ReciboSchema
