'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TecnologiaSchema extends Schema {
  up () {
    this.table('tecnologias', (table) => {
      table.boolean('is_sip').nullable().defaultTo(false).after('opcao')
      // alter table
    })
  }

  down () {
    this.table('tecnologias', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TecnologiaSchema
