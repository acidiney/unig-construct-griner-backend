'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaProdutoCompraSchema extends Schema {
  up () {
    this.create('linha_produto_compras', (table) => {
      table.increments()   
	  table.float('preco').notNullable();  
	  table.integer('artigo_id').unsigned().notNullable().references('id').inTable('produtos').onUpdate('CASCADE').index('artigo_id_index');
      table.integer('compra_id').unsigned().notNullable().references('id').inTable('compras').onUpdate('CASCADE').index('compra_id_index');
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index');
      table.timestamps();
    })
  }

  down () {
    this.drop('linha_produto_compras')
  }
}

module.exports = LinhaProdutoCompraSchema
