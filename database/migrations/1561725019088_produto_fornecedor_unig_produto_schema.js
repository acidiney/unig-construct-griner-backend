'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoFornecedorUnigProdutoSchema extends Schema {
  up () {
    this.create('produto_fornecedor_unig_produtos', (table) => {
      table.increments()
      table.integer('artigo_id').unsigned().notNullable().references('id').inTable('produtos').onUpdate('CASCADE').index('artigo_id_index');
      table.integer('produto_fornecedor_id').unsigned().notNullable().references('id').inTable('produto_fornecedors').onUpdate('CASCADE').index('produto_fornecedors_id_index');
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index');
      table.timestamps()
    })
  }

  down () {
    this.drop('produto_fornecedor_unig_produtos')
  }
}

module.exports = ProdutoFornecedorUnigProdutoSchema
