'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategoriaSuportesSchema extends Schema {
  up () {
    this.table('categoria_suportes', (table) => {
      // alter table
      table.string('tipoCategoria', 222)
    })
  }

  down () {
    this.table('categoria_suportes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CategoriaSuportesSchema
