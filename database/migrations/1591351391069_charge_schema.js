'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargeSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      // alter table
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE')
    })
  }

  down () {
    this.table('charges', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargeSchema
