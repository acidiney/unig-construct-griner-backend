'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChangenamenumeracaoSchema extends Schema {
  up () {
    this.rename('numeracaos', 'numeracoes')
  }

  down () {
    this.rename('numeracaos', 'numeracoes')
  }
}

module.exports = ChangenamenumeracaoSchema
