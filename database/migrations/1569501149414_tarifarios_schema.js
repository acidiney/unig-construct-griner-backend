'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TarifariosSchema extends Schema {
  up () {
    this.table('tarifarios', (table) => {
      // alter table
	table.string('tecnologia', 20).nullable().after('credMensalInDefault')
    })
  }

  down () {
    this.table('tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TarifariosSchema
