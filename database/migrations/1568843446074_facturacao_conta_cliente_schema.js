'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturacaoContaClienteSchema extends Schema {
  up () {
    this.create('facturacao_conta_clientes', (table) => {
      table.increments() 
         
      table.string('nome_cliente',100).nullable()
      table.string('contaDescricao',100).nullable()
      table.string('tarifarioDescricao',100).nullable()
      table.string('planoPrecoDescricao',100).nullable()
      table.integer('chaveServico',100).nullable()



      table.integer('factura_id').nullable().unsigned().index();
      table.foreign('factura_id').references('id').on('facturas').onDelete('cascade') 

      table.integer('recibo_id').nullable().unsigned().index();
      table.foreign('recibo_id').references('id').on('recibos').onDelete('cascade') 

      table.integer('cliente_id').nullable().unsigned().index();
      table.foreign('cliente_id').references('id').on('clientes').onDelete('cascade')

      table.integer('conta_id').nullable().unsigned().index();
      table.foreign('conta_id').references('id').on('contas').onDelete('cascade')

      table.integer('servico_id').nullable().unsigned().index();
      table.foreign('servico_id').references('id').on('servicos').onDelete('cascade')

      table.integer('tarifario_id').nullable().unsigned().index();
      table.foreign('tarifario_id').references('id').on('tarifarios').onDelete('cascade')

      table.integer('plano_preco_id').nullable().unsigned().index();
      table.foreign('plano_preco_id').references('id').on('plano_precos').onDelete('cascade')

      

      table.timestamps()
    })
  }

  down () {
    this.drop('facturacao_conta_clientes')
  }
}

module.exports = FacturacaoContaClienteSchema
 

