'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoTarifarioSchema extends Schema {
  up () {
    this.create('produto_tarifarios', (table) => {
      table.increments()
      table.integer('produto_id').unsigned().notNullable().references('id').inTable('produtos').onUpdate('CASCADE').index();
      table.integer('tarifario_id').unsigned().notNullable().references('id').inTable('tarifarios').onUpdate('CASCADE').index();
      table.timestamps()
    })
  }

  down () {
    this.drop('produto_tarifarios')
  }
}

module.exports = ProdutoTarifarioSchema
