'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SupoporteSchema extends Schema {
  up () {
    this.table('supportes', (table) => {
      // alter table
      table.longText("img_base64").after('descricao')
    })
  }

  down () {
    this.table('supportes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SupoporteSchema
