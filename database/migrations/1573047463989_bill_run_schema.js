'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BillRunSchema extends Schema {
  up () {
    this.table('bill_runs', (table) => {
      // alter table
      table.integer('bill_run_header_id').unsigned().nullable().references('id').inTable('bill_run_headers').onUpdate('CASCADE').index().after('factura_utilitie_id');
    })
  }

  down () {
    this.table('bill_runs', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BillRunSchema
