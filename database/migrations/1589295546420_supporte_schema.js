'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SupporteSchema extends Schema {
  up () {
    this.create('supportes', (table) => {
      table.increments()
      
      table.integer('categoria_suporte_id').unsigned().nullable().references('id').inTable('categoria_suportes')
      table.string('descricao', 200)
      table.integer('user_id').unsigned().nullable().references('id').inTable('users')
      table.integer('estado_suporte_id').unsigned().nullable().references('id').inTable('estado_supportes')
      table.integer('suporte_prioridade_id').unsigned().nullable().references('id').inTable('suporte_prioridades')
      table.string("ip", 50);
      table.string("url",200);
      table.timestamps()
    })
  }

  down () {
    this.drop('supportes')
  }
}

module.exports = SupporteSchema
