'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoReclamacaosSchema extends Schema {
  up () {
    this.table('estado_reclamacaos', (table) => {
      // alter table
	 table.integer('user_id').unsigned().index('user_id').references('id').inTable('users').nullable().after('sigla')
    })
  }

  down () {
    this.table('estado_reclamacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EstadoReclamacaosSchema
