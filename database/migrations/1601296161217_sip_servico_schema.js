'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SipServicoSchema extends Schema {
  up () {
    this.table('sip_servicos', (table) => {
      // alter table
      table.integer('numeracao_inicio').nullable().after('servico_id')
      table.integer('numeracao_fim').nullable().after('numeracao_inicio')
    })
  }

  down () {
    this.table('sip_servicos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SipServicoSchema
