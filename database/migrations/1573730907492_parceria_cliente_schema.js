'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ParceriaClienteSchema extends Schema {
  up () {
    this.create('parceria_clientes', (table) => {
      
      table.increments()

      table.text('codigo_interconexao').nullable(); 
      table.boolean('estado').nullable().defaultTo(true);    
       
      table.integer('cliente_id').unsigned().nullable().references('id').inTable('clientes').onUpdate('CASCADE').index();
      table.integer('servico_id').unsigned().nullable().references('id').inTable('servicos').onUpdate('CASCADE').index();
      table.integer('moeda_id').unsigned().nullable().references('id').inTable('moedas').onUpdate('CASCADE').index();
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();

      table.timestamps()
    })
  }

  down () {
    this.drop('parceria_clientes')
  }
}

module.exports = ParceriaClienteSchema
