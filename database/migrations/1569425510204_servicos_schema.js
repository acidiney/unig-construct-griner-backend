'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServicosSchema extends Schema {
  up () {
    this.table('servicos', (table) => {
      // alter table
	 table.string('tecnologia', 20).nullable().after('conta_id')
    })
  }

  down () {
    this.table('servicos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ServicosSchema
