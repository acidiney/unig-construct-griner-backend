'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidoSchema extends Schema {
  up () {
    this.create('pedidos', (table) => {
      table.increments()
	  table.integer('cliente_id').unsigned().notNullable().references('id').inTable('clientes').index('cliente_id_index');
      table.string('tipoPedido', 50).notNullable()
	  table.datetime('dataPedido').notNullable()
	  table.text('observacao').nullable()
	  table.timestamps()
    })
  }

  down () {
    this.drop('pedidos')
  }
}

module.exports = PedidoSchema
