'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WimaxesSchema extends Schema {
  up () {
    this.table('wimaxes', (table) => {
      // alter table
	table.integer('user_id').unsigned().references('id').inTable('users').after('AgenciaFilialID')
    })
  }

  down () {
    this.table('wimaxes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = WimaxesSchema
