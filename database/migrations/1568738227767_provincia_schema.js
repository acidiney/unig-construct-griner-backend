'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProvinciaSchema extends Schema {
  up () {
    this.create('provincias', (table) => {
      table.increments()
      table.string("nome", 50).notNullable();
      table.string("abreviatura", 50).notNullable(); 

      table.boolean('is_active').nullable().defaultTo(true); 

      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('provincias')
  }
}

module.exports = ProvinciaSchema
