'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturaSchema extends Schema {
  up () {
    this.create('linha_facturas', (table) => {
      table.increments()
      table.float('total').notNullable();
      table.integer('quantidade').notNullable();
      table.float('valor').notNullable();
      table.float('valor_imposto').notNullable();
      table.float('valor_desconto').notNullable();
      table.integer('factura_id').unsigned().notNullable().references('id').inTable('facturas').onUpdate('CASCADE').index('factura_id_index');
      table.integer('artigo_id').unsigned().notNullable().references('id').inTable('produtos').onUpdate('CASCADE').index('artigo_id_index');
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index');
      table.integer('imposto_id').unsigned().notNullable().references('id').inTable('impostos').onUpdate('CASCADE').index('imposto_id_index');
      table.timestamps()
    })
  }

  down () {
    this.drop('linha_facturas')
  }
}

module.exports = LinhaFacturaSchema
