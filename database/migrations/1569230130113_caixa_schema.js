'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CaixaSchema extends Schema {
  up () {
    this.create('caixas', (table) => {
      table.increments()

      table.double("valor_abertura", 30, 2).nullable();  
      table.date("data_abertura").nullable();
      table.time("hora_abertura").nullable();

      table.double("valor_fecho", 30, 2).nullable();
      table.double("valor_vendas", 30, 2).nullable(); 
      table.date("data_fecho").nullable();
      table.time("hora_fecho").nullable();
 
      table.double("valor_deposito", 30, 2).nullable();
      table.date("data_deposito").nullable();
      table.integer("referencia_banco").nullable(); 
      
      table.text('observacao').nullable();
        

      table.integer('banco_id').nullable().unsigned().index();
      table.foreign('banco_id').references('id').on('bancos').onDelete('cascade')

      table.boolean('is_active').nullable().defaultTo(true);

      table.integer('loja_id').nullable().unsigned().index();
      table.foreign('loja_id').references('id').on('lojas').onDelete('cascade')

      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('caixas')
  }
}

module.exports = CaixaSchema
