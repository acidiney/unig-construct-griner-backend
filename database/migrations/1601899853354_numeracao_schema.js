'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NumeracaoSchema extends Schema {
  up () {
    this.table('numeracaos', (table) => {
      // alter table
      table.renameColumn('filial_id','central_recurso_rede_id')
    })
  }

  down () {
    this.table('numeracaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = NumeracaoSchema
