'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoPedidosSchema extends Schema {
  up () {
    this.table('tipo_pedidos', (table) => {
      // alter table
	table.string('rota_crm_inicial',200).nullable().after('descricao')
    })
  }

  down () {
    this.table('tipo_pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TipoPedidosSchema
