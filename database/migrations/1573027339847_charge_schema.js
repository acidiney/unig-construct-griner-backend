'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargeSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      // alter table
      table.text('invoiceText').nullable().after('id');
      table.double('valor',250,2).nullable().after('id');
      table.boolean('is_facturado').nullable().defaultTo(false).after('id');
      table.integer('servico_id').nullable().after('id');
      table.integer('conta_id').nullable().after('id');
    })
  }

  down () {
    this.table('charges', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargeSchema
