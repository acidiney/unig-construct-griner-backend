'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddPagamentoColumnBancoIdSchema extends Schema {
  up () {
    this.table('pagamentos', (table) => {
      // alter table 
      table.integer('banco_id').unsigned().index('banco_id').references('id').inTable('bancos').after('documento_sigla');
       table.integer('moeda_id').unsigned().index('moeda_id').references('id').inTable('moedas').after('banco_id');
      
    })
  }

  down () {
    this.table('pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddPagamentoColumnBancoIdSchema
