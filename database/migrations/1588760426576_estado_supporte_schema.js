'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoSupporteSchema extends Schema {
  up () {
    this.create('estado_supportes', (table) => {
      table.increments()
      
      table.string("designacao", 200)
      table.timestamps()
    })
  }

  down () {
    this.drop('estado_supportes')
  }
}

module.exports = EstadoSupporteSchema
