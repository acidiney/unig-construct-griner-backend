'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArmarioPrimarioSchema extends Schema {
  up () {
    this.create('armario_primarios', (table) => {
      table.increments()
	  table.string('descricao', 150).notNullable()
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('armario_primarios')
  }
}

module.exports = ArmarioPrimarioSchema
