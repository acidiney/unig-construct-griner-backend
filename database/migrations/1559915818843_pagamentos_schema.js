'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentosSchema extends Schema {
  up () {
    this.table('pagamentos', (table) => {
      // alter table
	   table.float('valor_recebido').nullable().after('id');
	   table.float('troco').nullable().after('valor_recebido');
	   table.float('total_pago').nullable().after('troco');
	   table.integer('referencia').nullable().after('total_pago');
	   table.date('data_pagamento').after('referencia'); 
	   table.integer('forma_pagamento_id').unsigned().notNullable().references('id').inTable('forma_pagamentos').onUpdate('CASCADE').after('data_pagamento');
	   
    })
  }

  down () {
    this.table('pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PagamentosSchema
