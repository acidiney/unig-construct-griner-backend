'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReciboSchema extends Schema {
  up () {
    this.table('recibos', (table) => {
      // alter table
      table.text("status_reason").alter()
    })
  }

  down () {
    this.table('recibos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ReciboSchema
