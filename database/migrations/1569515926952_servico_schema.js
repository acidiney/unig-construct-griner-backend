'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServicoSchema extends Schema {
  up () {
    this.table('servicos', (table) => {
      // alter table
      table.text('adsl_username').nullable();
      table.text('adsl_password').nullable();
    })
  }

  down () {
    this.table('servicos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ServicoSchema
