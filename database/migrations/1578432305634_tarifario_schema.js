'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TarifarioSchema extends Schema {
  up () {
    this.table('tarifarios', (table) => {
      // alter table
      table.string("tecnologia", 200).nullable().alter();
      
    })
  }

  down () {
    this.table('tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TarifarioSchema
