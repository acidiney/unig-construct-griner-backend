'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EntidadeCativadoraSchema extends Schema {
  up () {
    this.create('entidade_cativadoras', (table) => {
      table.increments()
      table.string("nome", 255).notNullable();
      table.double("valor", 255, 2).notNullable();
      table.boolean('estado').nullable().defaultTo(true);      
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();
      
      table.timestamps()
    })
  }

  down () {
    this.drop('entidade_cativadoras')
  }
}

module.exports = EntidadeCativadoraSchema
