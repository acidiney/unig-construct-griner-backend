'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MovimentoAdiantamentoSchema extends Schema {
  up () {
    this.create('movimento_adiantamentos', (table) => {
      table.increments()
 

      table.double("valor", 130, 2).nullable(); 

      table.text("descritivo").nullable();

      table.boolean('saldado').nullable().defaultTo(true); 


      table.integer('factura_id').nullable().unsigned().index();
      table.foreign('factura_id').references('id').on('facturas').onDelete('cascade')

      table.integer('recibo_id').nullable().unsigned().index();
      table.foreign('recibo_id').references('id').on('recibos').onDelete('cascade')


      table.integer('saldado_factura').nullable().unsigned().index();
      table.foreign('saldado_factura').references('id').on('facturas').onDelete('cascade')

      table.integer('saldado_recibo').nullable().unsigned().index();
      table.foreign('saldado_recibo').references('id').on('recibos').onDelete('cascade')


      table.integer('adiantamento_id').nullable().unsigned().index();
      table.foreign('adiantamento_id').references('id').on('adiantamentos').onDelete('cascade')

      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade')

      table.timestamps()
    })
  }

  down () {
    this.drop('movimento_adiantamentos')
  }
}

module.exports = MovimentoAdiantamentoSchema
