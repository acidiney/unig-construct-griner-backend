'use strict'

const { table } = require('../../app/Models/Loja')

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SupportesSchema extends Schema {
  up () {
      this.alter("supportes", (table)=>{
        table.longText('descricao').alter();
      })
  }

  down () {
    this.table('supportes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SupportesSchema
