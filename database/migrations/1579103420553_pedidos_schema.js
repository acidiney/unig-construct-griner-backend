'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidosSchema extends Schema {
  up () {
    this.table('pedidos', (table) => {
      // alter table
	 table.string('par_caixa', 200).nullable().after('caixa');
	 table.string('armario_secudario', 200).nullable().after('par_caixa');
	 table.string('armario_primario', 200).nullable().after('armario_secudario');
	 table.string('cabo_id', 200).nullable().after('armario_primario');
    })
  }

  down () {
    this.table('pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PedidosSchema
