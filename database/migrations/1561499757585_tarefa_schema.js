"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class TarefaSchema extends Schema {
  up() {
    this.create("tarefas", table => {
      table.increments()
      table.text("tarefa").nullable()
      table.integer("user_id").unsigned().notNullable().unique().references("id").inTable("users").onUpdate("CASCADE").index("user_id_index")
      table.timestamps();
    });
  }

  down() {
    this.drop("tarefas");
  }
}

module.exports = TarefaSchema;
