'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoReclamacaosSchema extends Schema {
  up () {
    this.table('estado_reclamacaos', (table) => {
      // alter table
	table.string('sigla',2).nullable().after('designacao');
    })
  }

  down () {
    this.table('estado_reclamacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EstadoReclamacaosSchema
