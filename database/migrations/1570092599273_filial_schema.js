'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FilialSchema extends Schema {
  up () {
    this.table('filials', (table) => {
      // alter table
      table.integer('imposto_id').unsigned().nullable().references('id').inTable('impostos').onUpdate('CASCADE').index();
    })
  }

  down () {
    this.table('filials', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FilialSchema
