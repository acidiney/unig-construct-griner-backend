'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ParCaboSchema extends Schema {
  up () {
    this.create('par_cabos', (table) => {
      table.increments()
	  table.string('descricao', 150).notNullable()
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('par_cabos')
  }
}

module.exports = ParCaboSchema
