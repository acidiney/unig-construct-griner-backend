'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DirectoreSchema extends Schema {
  up () {
    this.create('directores', (table) => {
      table.increments()
	  table.integer('filial_id').nullable()
	  table.string('cargo',150).nullable()
	  table.string('nome',150).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('directores')
  }
}

module.exports = DirectoreSchema
