'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BillRunEmailSchema extends Schema {
  up () {
    this.table('bill_run_emails', (table) => {
      // alter table
	table.string('cliente_email',250).notNullable().after('factura_id')
    })
  }

  down () {
    this.table('bill_run_emails', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BillRunEmailSchema
