'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaDataVencimentoSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      table.datetime('data_vencimento').after('data_origem_factura').nullable();
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturaDataVencimentoSchema
