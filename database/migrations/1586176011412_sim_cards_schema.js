'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SimCardsSchema extends Schema {
  up () {
    this.create('sim_cards', (table) => {
      table.increments()
	  table.integer('puk2')
	  table.string('adm1',70)
	  table.integer('puk1')
	  table.integer('batch')
	  table.bigInteger('iccid')
	  table.string('ki',70)
	  table.integer('pin1')
	  table.integer('pin2')
	  table.string('typeSim',20)
	  table.string('estadoInventario',30)
	  table.string('profile',10)
	  table.string('nome',70)
	  table.bigInteger('imsi')
      table.timestamps()
    })
  }

  down () {
    this.drop('sim_cards')
  }
}

module.exports = SimCardsSchema
