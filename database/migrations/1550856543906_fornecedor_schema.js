'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FornecedorSchema extends Schema {
  up () {
    this.create('fornecedores', (table) => {
      table.increments()
      table.string('nome', 50).notNullable()
      table.string('nif', 15).notNullable().unique()
      table.string('localizacao', 50).nullable()
      table.string('email', 50).notNullable().unique()
      table.string('telefone', 250).notNullable()  
      table.timestamps()
    })
  }

  down () {
    this.drop('fornecedors')
  }
}

module.exports = FornecedorSchema
