'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogEstadoPedidoSchema extends Schema {
  up () {
    this.create('log_estado_pedidos', (table) => {
      table.increments()
	  table.integer('pedido_id').unsigned().references('id').inTable('pedidos')
	  table.integer('id_estado_anterior').unsigned().references('id').inTable('estado_pedidos')
	  table.integer('id_estado_novo').unsigned().references('id').inTable('estado_pedidos')
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('log_estado_pedidos')
  }
}

module.exports = LogEstadoPedidoSchema
