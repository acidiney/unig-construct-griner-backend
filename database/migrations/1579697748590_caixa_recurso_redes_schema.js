'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CaixaRecursoRedesSchema extends Schema {
  up () {
    this.table('caixa_recurso_redes', (table) => {
      // alter table
	table.integer('central_id').unsigned().references('id').inTable('central_recurso_redes').after('descricao')
    })
  }

  down () {
    this.table('caixa_recurso_redes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CaixaRecursoRedesSchema
