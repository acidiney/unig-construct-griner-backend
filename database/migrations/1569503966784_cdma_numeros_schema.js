'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CdmaNumerosSchema extends Schema {
  up () {
    this.table('cdma_numeros', (table) => {
      // alter table
	table.integer('FilialID', 11).nullable().after('ChaveServico')
    })
  }

  down () {
    this.table('cdma_numeros', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CdmaNumerosSchema
