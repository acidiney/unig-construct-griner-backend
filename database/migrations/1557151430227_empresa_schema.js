'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresaSchema extends Schema {
  up () {
        this.create('empresas', (table) => {
        table.increments()
        table.string('companyName', 120).nullable();
        table.string('telefone', 254).nullable()
        table.string('addressDetail', 254).nullable()
        table.string('taxRegistrationNumber', 254).nullable()
        table.string('buildingNumber', 254).nullable()
        table.string('streetName', 254).nullable()
        table.string('city', 254).nullable() 
        table.string('province', 254).nullable()
        table.string('email', 254).nullable()
        table.timestamps()
    })
  }

  down () {
    this.drop('empresas')
  }
}

module.exports = EmpresaSchema
