'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlanoPrecoSchema extends Schema {
  up () {
    this.table('plano_precos', (table) => {
      // alter table
      table.double("preco", 25, 2).nullable();
    })
  }

  down () {
    this.table('plano_precos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PlanoPrecoSchema
