'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectoSerieSchema extends Schema {
  up () {
    this.create('projecto_series', (table) => {
      table.increments()
      table.integer('projecto_id').unsigned().index('projecto_id_index').references('id').inTable('projectos');
      table.integer('serie_id').unsigned().index('serie_id_index').references('id').inTable('series');
      table.integer('user_id').unsigned().index('user_id_index').references('id').inTable('users');
      table.timestamps()
    })
  }

  down () {
    this.drop('projecto_series')
  }
}

module.exports = ProjectoSerieSchema
