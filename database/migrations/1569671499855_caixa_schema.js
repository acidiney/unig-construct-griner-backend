'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CaixaSchema extends Schema {
  up () {
    this.table('caixas', (table) => {
      // alter table 
      table.double("valor_fecho", 100, 2).nullable().alter();
      table.double("valor_vendas", 150, 2).nullable().defaultTo(0).alter(); 
      table.double("valor_deposito", 100, 2).nullable().alter();
    })
  }

  down () {
    this.table('caixas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CaixaSchema
