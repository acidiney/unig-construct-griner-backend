"use strict";

/*
|--------------------------------------------------------------------------
| PermissionSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class PermissionSeeder {
  async run() {
    /**
     * Permissões das operacoes sobre Permissions
     * */

    await Factory.model("App/Models/Permission").create({
      slug: "permission_update_to_role",
      name: "EDITAR PERMISSÕES DO PERFIL",
      description: "Permissão para editar permissões de um perfil"
    });
    // Fim das permissoes

    /**
     * Permissões das operacoes sobre utilizador
     *
     * */
    await Factory.model("App/Models/Permission").create({
      slug: "listar_utilizadores",
      name: "Listar Utilizadores",
      description: "Permissão para listar utilizadores"
    });
    await Factory.model("App/Models/Permission").create({
      slug: "registar_utilizador",
      name: "Registar Utilizador",
      description: "Permissão para Registar utilizador"
    });
    await Factory.model("App/Models/Permission").create({
      slug: "editar_utilizador",
      name: "Editar Utilizador",
      description: "Permissão para Editar utilizador"
    });

    await Factory.model("App/Models/Permission").create({
      slug: "eliminar_utilizador",
      name: "Eliminar Utilizador",
      description: "Permissão para Eliminar utilizador"
    });
    await Factory.model("App/Models/Permission").create({
      slug: "redefinir_senha_utilizador",
      name: "Redefinir Senha Utilizador",
      description: "Permissão para Redefinir senha determiado utilizador"
    }); 
    // Fim das permissoes de utlizadores


    /**
     * Permissões das operacoes sobre clientes
     *
     * */
    await Factory.model("App/Models/Permission").create({
      slug: "listar_clientes",
      name: "Listar Clientes",
      description: "Permissão para listar clientes"
    });
    await Factory.model("App/Models/Permission").create({
      slug: "registar_cliente",
      name: "Registar Clientes",
      description: "Permissão para registar clientes"
    });
    await Factory.model("App/Models/Permission").create({
      slug: "editar_cliente",
      name: "Editar Clientes",
      description: "Permissão para Editar cliente"
    });
    await Factory.model("App/Models/Permission").create({
      slug: "eliminar_cliente",
      name: "Eliminar Clientes",
      description: "Permissão para Eliminar Cliente"
    });
    await Factory.model("App/Models/Permission").create({
      slug: "conta_corrente",
      name: "conta corrente Clientes",
      description: "Permissão para conta corrente de clientes"
    });
    // Fim das permissoes de clientes

    /**
     * Permissões das operacoes sobre orçamento
     *
     * */

     await Factory.model("App/Models/Permission").create({
       slug: "listar_orcamentos",
       name: "Listar Orçamentos",
       description: "Permissão para Listar Orçamentos"
     });
     await Factory.model("App/Models/Permission").create({
       slug: "registar_orcamentos",
       name: "Registar Orçamentos",
       description: "Permissão para Registra Orçamentos"
     });
    // Fim das permissoes de orçamento

    /**
     * Permissões das operacoes sobre tarefas
     *
     * */ 
     await Factory.model("App/Models/Permission").create({
       slug: "tarefas",
       name: "tarefas",
       description: "Permissão para tarefas"
     });
    // Fim das permissoes de tarefas

     /**
     * Permissões das operacoes sobre inventarios
     *
     * */

     await Factory.model("App/Models/Permission").create({
       slug: "listar_inventarios",
       name: "Listar inventarios",
       description: "Permissão para Listar inventarios"
     }); 
    // Fim das permissoes de inventarios

     /**
     * Permissões das operacoes sobre venda -  facturação
     *
     * */

     await Factory.model("App/Models/Permission").create({
       slug: "listar_facturacao",
       name: "Listar Facturação",
       description: "Permissão para Listar Facturação"
     }); 

     await Factory.model("App/Models/Permission").create({
       slug: "registar_facturacao",
       name: "Registar Facturação",
       description: "Permissão para Registar Facturação"
     }); 
      await Factory.model("App/Models/Permission").create({
        slug: "gerar_factura",
        name: "Gerar factura",
        description: "Permissão para gerar factura"
      }); 
      await Factory.model("App/Models/Permission").create({
        slug: "anular_factura",
        name: "Anular factura",
        description: "Permissão para Anular factura"
      }); 
      await Factory.model("App/Models/Permission").create({
        slug: "nota_credito_factura",
        name: "Gerar Nota de Credito factura",
        description: "Permissão para Gerar Nota de Credito factura"
      });

      await Factory.model("App/Models/Permission").create({
        slug: "imprimir_factura",
        name: "Imprimir factura",
        description: "Permissão para Imprimir Factura"
      }); 
// Fim das permissoes de venda -  facturação
       await Factory.model("App/Models/Permission").create({
         slug: "listar_armazens",
         name: "listar armazens",
         description: "Permissão para listar armazens"
       });
    
       await Factory.model("App/Models/Permission").create({
         slug: "business_intelligence",
         name: "Business Intelligence",
         description: "Permissão para Business Intelligence"
       });

       await Factory.model("App/Models/Permission").create({
         slug: "listar_compras",
         name: "listar compras",
         description: "Permissão para listar compras"
       });

       await Factory.model("App/Models/Permission").create({
         slug: "listar_produtos",
         name: "listar produtos",
         description: "Permissão para listar produtos"
       });
       await Factory.model("App/Models/Permission").create({
         slug: "listar_projectos",
         name: "listar projectos",
         description: "Permissão para listar projectos"
       });

       await Factory.model("App/Models/Permission").create({
         slug: "gerar_saft",
         name: "gerar saft",
         description: "Permissão para gerar saft"
       });

       await Factory.model("App/Models/Permission").create({
         slug: "listar_permissions",
         name: "listar permissões",
         description: "Permissão para listar permissões"
       });

       await Factory.model("App/Models/Permission").create({
         slug: "listar_roles",
         name: "listar roles",
         description: "Permissão para listar roles"
       });

       await Factory.model("App/Models/Permission").create({
         slug: "listar_impostos",
         name: "listar impostos",
         description: "Permissão para listar impostos"
       });

       await Factory.model("App/Models/Permission").create({
         slug: "configurar_empresa",
         name: "configurar empresa",
         description: "Permissão para configurar empresa"
       });

        await Factory.model("App/Models/Permission").create({
          slug: "listar_documentos",
          name: "listar documentos",
          description: "Permissão para listar documentos"
        });

         await Factory.model("App/Models/Permission").create({
           slug: "listar_series",
           name: "listar series",
           description: "Permissão para listar series"
         });

         await Factory.model("App/Models/Permission").create({
           slug: "register_serie",
           name: "register serie",
           description: "Permissão para register serie"
         });

         ;

          await Factory.model("App/Models/Permission").create({
            slug: "listar_forma_pagamentos",
            name: "Listar formas de pagamentos",
            description: "Permissão para Listar formas de pagamentos"
          });

           await Factory.model("App/Models/Permission").create({
             slug: "listar_moedas",
             name: "listar moedas",
             description: "Permissão para listar moedas"
           });

            await Factory.model("App/Models/Permission").create({
              slug: "listar_bancos",
              name: "listar bancos",
              description: "Permissão para listar bancos"
            });



            await Factory.model("App/Models/Permission").create({
              slug: "crm",
              name: "crm",
              description: "Permissão para crm"
            });

            await Factory.model("App/Models/Permission").create({
              slug: "logistica",
              name: "Logística",
              description: "Permissão para logistica"
            });

     await Factory.model("App/Models/Permission").create({
       slug: "vendas",
       name: "vendas",
       description: "Permissão para vendas"
     });

     await Factory.model("App/Models/Permission").create({
       slug: "operacoes",
       name: "Operações",
       description: "Permissão para Operações"
     });

     await Factory.model("App/Models/Permission").create({
       slug: "configuracoes",
       name: "Configurações",
       description: "Permissão para Configurações"
     });

     await Factory.model("App/Models/Permission").create({
       slug: "stock_movimento",
       name: "stock movimento",
       description: "Permissão para stock movimento"
     });
     await Factory.model("App/Models/Permission").create({
       slug: "tarifarios",
       name: "tarifarios",
       description: "Permissão para tarifarios"
     });
     await Factory.model("App/Models/Permission").create({
       slug: "facturacao_charge",
       name: "facturacao charge",
       description: "Permissão para facturacao charge"
     });
     await Factory.model("App/Models/Permission").create({
       slug: "contratos",
       name: "contratos de clientes",
       description: "Permissão para contratos de clientes"
     });

     await Factory.model("App/Models/Permission").create({
       slug: "conta_cliente",
       name: "conta cliente",
       description: "Permissão para contratos de conta clientes"
     });
 
     await Factory.model("App/Models/Permission").create({
       slug: "facturacao_automatica",
       name: "Facturação Automatica",
       description: "Permissão Facturação Automaticas"
     });

     await Factory.model("App/Models/Permission").create({
       slug: "gerar_recibos",
       name: "Gerar Recibos",
       description: "Permissão para Gerar Recibos das facturas não pagas"
     });

     await Factory.model("App/Models/Permission").create({
       slug: "movimento_caixa",
       name: "Movimento Caixa",
       description: "Permissão para Movimento de caixa"
     });

  }
}

module.exports = PermissionSeeder;
