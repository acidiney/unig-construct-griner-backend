"use strict";

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");
const Hash = use("Hash");
Factory.blueprint("App/Models/Empresa", (faker, i, data) => {
  return {
    companyName: data.companyName,
    telefone: data.telefone,
    addressDetail: data.addressDetail,
    active_tfa: data.active_tfa,
    taxRegistrationNumber: data.taxRegistrationNumber,
    logotipo: data.logotipo
  };
});
// Factory para pré-criação Roles
Factory.blueprint("App/Models/Role", async (faker, i, data) => {
  return {
    slug: data.slug,
    name: data.name,
    description: faker.paragraph()
  };
});

// Factory para pré-criação Permissions
Factory.blueprint("App/Models/Permission", async (faker, i, data) => {
  return {
    slug: data.slug,
    name: data.name,
    description: data.description
  };
});

 
 
 

// Factory para pré-criação User
Factory.blueprint("App/Models/User", async (faker, i, data) => {
  return {
    username: data.username,
    email: faker.email({ domain: "itgest.co.ao" }),
    password: "unig",
    nome: "Super Admin", 
    'morada':'Luanda, Angola',
    telefone: "90909090", 
    empresa_id: data.empresa_id,
    status:true
  };
});
